# Decomposition
Welcome to the "Decomposition" project documentation. Our project refines parallel LU Decomposition methods within computational mathematics, focusing on effective implementation using TNL data structures and algorithms. While LU Decomposition is a long-established concept, this project explores recent variants and approaches targeting modern supercomputers.

Our aim is to provide a platform for developing new algorithms related to LU Decomposition, offering improvements in efficiency and adaptability. This documentation combines theoretical insights with practical guides, catering to a range of users from students to research scientists.

## Installation
In order to install the Decomposition project, some dependencies need to be installed first.

### Dependencies

* **Git** - to clone the TNL/Decomposition repository.
```bash
sudo apt update
sudo apt install git
```
* **CMake 3.24.2** or later - to compile the project.
```bash
wget https://github.com/Kitware/CMake/releases/download/v3.24.2/cmake-3.24.2.tar.gz
tar -zxvf cmake-3.24.2.tar.gz
cd cmake-3.24.2
./bootstrap
make
sudo make install
```
* **GCC 9.4.0** or later (or any compiler that supports the C++17 standard) - to compile the contents of the project.
```bash
sudo apt update
sudo apt install gcc-9
```
* **CUDA 11.0** or later - to compile the CUDA part of the project. The installation guide can be found in the [CUDA 11.0 documentation][CUDA-11.0-documentation].
* Python 3.8 or later (__OPTIONAL__, but recommended) - this is only if you want to use the [script for parsing benchmark results][benchmark-result-parsing-script].
```bash
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update
sudo apt install python 3.8
```
* **TNL library** - used in the project. The installation procedure can be found in [TNL's GitLab repository][TNL-gitlab-repository]. To download and install the latest stable version, run the following commands:
```bash
git clone https://gitlab.com/tnl-project/tnl.git
cd tnl
# If you have Python installed, then use:
./install all
# If you do not have Python installed, then you must let the install script know like so:
./install --with-python=no all
```
If the installation of TNL fails when using the commands above, please see the [TNL's installation instructions][tnl-installation-instructions].

### Project
If you would like to download the latest stable version of the project, then you can clone the repository via HTTPS:
```bash
git clone https://gitlab.com/tnl-project/decomposition.git
```
Then, inside this repository's root directory run the following command:
```bash
./install all
```
If you would like more information about the different options this command provides, use:
```bash
./install --help
```

## How to run
* Unit tests:
    * Go into `build/Release/bin` and run the individual scripts to run unit tests for individual formats.
* Benchmarks:
    * This project contains two benchmarks:
        1. `decomposers` - Benchmark the available LU decomposition methods (decomposers).
        2. `solvers` - Benchmark the available LU solvers ( (LU)x = b ).
    * To run benchmarks for a specific matrix:
        * Run the benchmarking executable with the mtx file as the input parameter:
            ```bash
            ${PATH_TO_DECOMPOSITION}/build/Release/bin/<benchmark_type>-benchmark --input-file ${mtx_file_name}.mtx
            ```
            where `<benchmark_type>` can be either `decomposers`, or `solvers`.
        * The benchmark will produce a log that can be parsed to HTML using a [python script][benchmark-result-parsing-script]. To use it, run the script in the directory with the log file and pass the log file as an input parameter:
            ```bash
            Python3 ~/${PATH_TO_DECOMPOSITION}/decomposition/src/Benchmarks/utils/benchmark-make-tables-json.py --input_file ${log_file_name}.log
            ```
    * To run benchmarks across multiple matrices run the script located in either `Decomposers/scripts` or `Solvers/scripts`. Example using `Decomposers/scripts`:

        Run:
        ```bash
        ~/${PATH_TO_DECOMPOSITION}/decomposition/src/Benchmarks/Decomposers/scripts/run-decomposers-benchmark
        ```
        in `src/Benchmarks/Decomposers/scripts` directory which contains the `mtx-matrices` directory.

        The `mtx-matrices` directory contains unpacked `*.mtx` files of the matrices that the benchmark is to be run on. For example:
        ```
        mtx-matrices/HB/bcsstk03.mtx
        mtx-matrices/MKS/fp.mtx
        mtx-matrices/Cejka10793.mtx
        ```

## Development

### Code Formatting
This project is developed as an extension to the TNL library. As such, the source code should follow the formatting conventions established in TNL.
Consistency can be maintained using the following tools (taken from TNL's [CONTRIBUTING.md][TNL-contributing]):

1. [.editorconfig](.editorconfig) specifies basic formatting elements, such as
   character set and indentation width. Many text editors and IDEs provide
   built-in support or a plugin for _editorconfig_, for example, [EditorConfig for VS Code][editorconfig-vscode-extension] (see https://editorconfig.org/ for details).
2. The [.clang-format](src/.clang-format) file contains detailed specification
   for C++ source code formatting. Automatic formatting may be applied with
   [clang-format] version 13 or later. To quickly run formatting run one of the following scripts in `scripts`:
   - [run-clang-format-all](scripts/run-clang-format-all) - format contents of `src`.
   - [run-clang-format-benchmarks](scripts/run-clang-format-benchmarks) - format contents of `src/Benchmarks`.
   - [run-clang-format-decomposition](scripts/run-clang-format-decomposition) - format contents of `src/Decomposition`.
   - [run-clang-format-unittests](scripts/run-clang-format-unittests) - format contents of `src/UnitTests`.

   These scripts call [run-clang-format.py](scripts/run-clang-format.py) script which assures parallel processing and correct formatting of CUDA kernel launches.
   <details><summary><code>clang-format</code> installation on Ubuntu 20.04</summary>

    Ubuntu installations may not come with the latest version of clang-format. To install it on Ubuntu 20.04 follow the steps below ([source](https://stackoverflow.com/a/66654073)):
    1. Add the repository key
        ```bash
        wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key|sudo apt-key add -
        ```
    2. Obtain the latest version by appending
        ```
        deb http://apt.llvm.org/focal/ llvm-toolchain-focal main
        deb-src http://apt.llvm.org/focal/ llvm-toolchain-focal main
        ```
        to `/etc/apt/sources.list`
    3. Run:
        ```bash
        sudo apt update
        ```
    4. Run:
        ```bash
        sudo apt install clang-format
        ```
   </details>
   <details><summary><code>clang-format</code> pre-commit hook</summary>

   It is strongly suggested to set up a pre-commit hook which will not perform the commit if `clang-format` finds an issue - to set this up follow the steps below:
   1. Go to `decomposition/.git/hooks` and create a copy of `pre-commit.sample` called `pre-commit`.
   2. Replace the contents of the file with:
        ```bash
        #!/bin/sh
        #
        # An example hook script to verify what is about to be committed.
        # Called by "git commit" with no arguments.  The hook should
        # exit with non-zero status after issuing an appropriate message if
        # it wants to stop the commit.
        #
        # To enable this hook, rename this file to "pre-commit".

        if git rev-parse --verify HEAD >/dev/null 2>&1
        then
            against=HEAD
        else
            # Initial commit: diff against an empty tree object
            against=$(git hash-object -t tree /dev/null)
        fi

        # Test clang-format
        clangformatout=$(git clang-format --diff --staged -q)

        # Redirect output to stderr.
        exec 1>&2

        if [ "$clangformatout" != "" ]
        then
                git clang-format --diff --staged
            echo -e "\n\nFormat error!"
            echo "Use 'git clang-format --staged' or override the commit with 'git commit --no-verify'"
            exit 1
        fi
        ```
   </details>
3. [.clang-tidy](src/.clang-tidy) provides a configuration to help developers fix common programming errors. Various editors and IDEs provide
[integrations](https://clang.llvm.org/extra/clang-tidy/Integrations.html). How to use:
    1. To view suggestions, run the [run-clang-tidy-wrapper](scripts/run-clang-tidy-wrapper) scripts inside `scripts`. Note that errors related to `googletest` can appear as googletest developers decided to ignore clang-tidy compatibility (see [this comment][googletest-dev-decline-clang-tidy-compatibility] on googletest's GitHub) and I have not figured out how to exclude googletest files from the scanning/analysis permanently.
    2. To apply the fixes suggested by a check, for example for `readability-simplify-boolean-expr` run the following command in the repository root:
        ```bash
        run-clang-tidy -p "./build/clang-tidy" -checks='-*,readability-simplify-boolean-expr' -fix -config-file=src/.clang-tidy
        ```

    <details><summary><code>clang-tidy</code> installation on Ubuntu 20.04</summary>

    Ubuntu installations may not come with `clang-tidy` by default - to install it run the following:
    ```
    sudo apt install clang-tidy
    ```
   </details>

### Code Coverage
To provide some assurance of quality, code coverage of unit tests is provided via a Bash script that uses the `lcov` package.

The following prerequisites are required:
1. The project was compiled with the `--coverage` flag.
   - This can be done using: `./install --build-coverage=yes --build-type=Debug all`.
2. `lcov` package with version >= 1.14 is installed.
   - The installation can be verified using `lcov --version`.

To generate an HTML report for code coverage, use the [generate-coverage] script. The report will be found in `coverage/index.html`.

**Note**: The script generates code coverage only for `Exceptions`, `LU/Decomposers` and `LU/Solvers`; other directories generated by the `--coverage` flag are are filtered out, e.g., system files, Google Test implementation, etc.

**Warning**: The [generate-coverage] script assumes that it has been launched from the `scripts` directory.

<details><summary><code>lcov</code> installation on Ubuntu 20.04</summary>

   Ubuntu installations may not come with `lcov` by default - to install it run the following:
   ```
   sudo apt install lcov
   ```
</details>

### Visual Studio Code Configuration
VS Code offers many shortcuts to speed up development.

- Build the project with `Ctrl+Shift+B`
   - If default project compilation is configured, then pressing `Ctrl+Shift+B` compiles the project.
   - To configure it, place the following into the `tasks` array in `.vscode/tasks.json`:
      ```json
      {
         "label": "installAll",
         "type": "shell",
         "command": "./install",
         "args": [
            "all"
         ],
         "group": {
            "kind": "build",
            "isDefault": true
         }
      }
      ```
      **Note**: For the `Ctrl+Shift+B` command to use this task, `"group"` must contain `"isDefault": true`.
   - An example [tasks.json](vscode_files/tasks.json) with two tasks: compile the unit tests, compile the entire project.
- Custom include directories
   - For example, VS Code may not see the TNL header files in `~/.local/include` by default.
   - You can provide the path to it and any other libraries by adding the paths as strings into the `"includePath"` array in `.vscode/c_cpp_properties.json`.


[benchmark-result-parsing-script]: src/Benchmarks/utils/benchmark-make-tables-json.py
[generate-coverage]: script/generate-coverage

[tnl-documentation]: https://tnl-project.gitlab.io/tnl
[tnl-installation-instructions]: https://tnl-project.gitlab.io/tnl/#installation
[CUDA-11.0-documentation]: https://docs.nvidia.com/cuda/archive/11.0/index.html
[TNL-gitlab-repository]: https://gitlab.com/tnl-project/tnl
[TNL-contributing]: https://gitlab.com/tnl-project/tnl/-/blob/main/CONTRIBUTING.md
[editorconfig-vscode-extension]: https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig
[clang-format]: https://clang.llvm.org/docs/ClangFormat.html
[googletest-dev-decline-clang-tidy-compatibility]: https://github.com/google/googletest/pull/3676#issuecomment-1030228060

## Additional Information
This project was implemented as part of the master's thesis "_Parallel Computation of LU Decomposition on GPUs for the Numerical Solution of Partial Differential Equations_" which is available [here](https://dspace.cvut.cz/handle/10467/111604).

Among other things, the thesis describes the comparison of performance of the decomposition algorithms provided by this project with well-known formats from existing libraries across different problems. The following is an excerpt summarizing the results of the performance comparison:
> Overall, the best-performing decomposer was CuSolverDnXgetrfPP, as it exhibited superior performance in both execution speed and accuracy in every benchmark it was a part of. When considering
the decomposers implemented by the author of this thesis, PCM_xPP demonstrated consistent
performance with an acceptable level of accuracy. However, for problems that require a preconditioner that does not need to produce highly accurate results, ICM_xPP is a suitable alternative.
It is important to note that increasing the processing tolerance of ICM_xPP can lead to higher
performance, however, there is no guarantee that it will produce usable results.

> In terms of solvers, both CuSolverDnXgetrsPP and CuBLAStrsmPP were the best-performing,
with the former being marginally faster than the latter while being slightly less accurate. It is worth
noting that CuBLAStrsmPP allows for the unit diagonal to be stored either in L or U, whereas
CuSolverDnXgetrsPP only supports a unit diagonal in L. While IS_xPP demonstrated that its
iterative approach can avoid unnecessary calculations in certain problems, it produced invalid
results for others, even with its processing tolerance set to zero. Therefore, it is not recommended
for use.
