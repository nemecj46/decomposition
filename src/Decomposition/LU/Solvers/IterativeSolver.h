#pragma once

#include <TNL/String.h>

#include <Decomposition/LU/Solvers/BaseSolver.h>

#include <Decomposition/utils/utils.h>

namespace Decomposition::LU::Solvers {

template< const int THREADS_PER_BLOCK = DEFAULT_BLOCK_SIZE, MatrixFormat MtxFormat = MatrixFormat::UnitDiagIn_U >
class IterativeSolver : public BaseSolver< MtxFormat >
{
public:
   static constexpr int threads_perBlock = THREADS_PER_BLOCK;

   template< typename Device >
   static TNL::String
   getSolverName( const bool& partialPivoting = false );

   template< typename Matrix >
   static void
   solve( const Matrix& LU, Matrix& X );

   template< typename Matrix, typename Vector >
   static void
   solve( const Matrix& LU, Matrix& X, const Vector& piv );
};

}  // namespace Decomposition::LU::Solvers

#include <Decomposition/LU/Solvers/IterativeSolver.hpp>
