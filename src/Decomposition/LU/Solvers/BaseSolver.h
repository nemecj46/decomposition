#pragma once

#include <Decomposition/Containers/MatrixFormat.h>

using MatrixFormat = Decomposition::Containers::MatrixFormat;

namespace Decomposition::LU::Solvers {

template< MatrixFormat MtxFormat >
class BaseSolver
{
public:
   static constexpr MatrixFormat
   getFormat()
   {
      return MtxFormat;
   }
};

}  // namespace Decomposition::LU::Solvers
