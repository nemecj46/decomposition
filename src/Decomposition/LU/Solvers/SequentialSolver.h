#pragma once

#include <TNL/String.h>

#include <Decomposition/LU/Solvers/BaseSolver.h>

namespace Decomposition::LU::Solvers {

template< MatrixFormat MtxFormat = MatrixFormat::UnitDiagIn_U >
class SequentialSolver : public BaseSolver< MtxFormat >
{
public:
   template< typename Device = TNL::Devices::Host >
   static TNL::String
   getSolverName( const bool& partialPivoting = false );

   template< typename Matrix >
   static void
   solve( const Matrix& LU, Matrix& X );

   template< typename Matrix, typename Vector >
   static void
   solve( const Matrix& LU, Matrix& X, const Vector& piv );
};

}  // namespace Decomposition::LU::Solvers

#include <Decomposition/LU/Solvers/SequentialSolver.hpp>
