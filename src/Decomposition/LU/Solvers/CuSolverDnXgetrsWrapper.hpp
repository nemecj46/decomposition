#pragma once

#include <Decomposition/LU/Solvers/CuSolverDnXgetrsWrapper.h>
#include <Decomposition/Exceptions/MatrixNotSquare.h>
#include <Decomposition/Exceptions/NotImplementedError.h>

#ifdef __CUDACC__
   #include <cusolverDn.h>
   #include <Decomposition/utils/CuLibsUtils.h>
#endif

#include <Decomposition/utils/utils.h>

namespace Decomposition::LU::Solvers {

template< MatrixFormat MtxFormat >
template< typename Device >
TNL::String
CuSolverDnXgetrsWrapper< MtxFormat >::getSolverName( const bool& partialPivoting )
{
   return partialPivoting ? "CuSolverDnXgetrsWrapper PP" : "CuSolverDnXgetrsWrapper";
}

template< MatrixFormat MtxFormat >
template< typename Matrix >
void
CuSolverDnXgetrsWrapper< MtxFormat >::solve( const Matrix& LU, Matrix& X )
{
   if constexpr( ! tnlObjectOnCuda< Matrix >() ) {
      throw Exceptions::NotImplementedError( getSolverName< typename Matrix::DeviceType >( false )
                                             + " is implemented only for the GPU." );
   }
   else {
#ifdef __CUDACC__
      // Required by cusolverDnXgetrs as the value-type for the pivoting vector

      using Index = typename Matrix::IndexType;
      using Real = typename Matrix::RealType;

      if constexpr( Matrix::getOrganization() != TNL::Algorithms::Segments::ColumnMajorOrder ) {
         throw Exceptions::NotImplementedError( "Input matrix uses row-major ordering - " + getSolverName<>( false )
                                                + " only supports column-major ordering of matrices in GPU memory." );
      }

      if constexpr( MtxFormat != MatrixFormat::UnitDiagIn_L ) {
         throw Exceptions::NotImplementedError( getSolverName< typename Matrix::DeviceType >()
                                                + " only supports a unit diagonal in L." );
      }

      const Index num_rows = LU.getRows();
      const Index num_cols = LU.getColumns();
      const Index numRightHandSides = X.getColumns();

      if( num_rows != num_cols ) {
         throw Exceptions::MatrixNotSquare( num_rows, num_cols, "LU" );
      }

      if( num_rows != X.getRows() ) {
         throw std::runtime_error( "-!> Matrix dimensions do not match: LU has " + std::to_string( LU.getRows() )
                                   + " rows while X (which holds B's values) has " + std::to_string( X.getRows() ) + " rows." );
      }

      int info = 0;
      int* d_info = nullptr; /* error info */

      cusolverDnHandle_t cusolverH = NULL;
      cudaStream_t stream = NULL;

      CUSOLVER_CHECK( cusolverDnCreate( &cusolverH ) );

      CUDA_CHECK( cudaStreamCreateWithFlags( &stream, cudaStreamNonBlocking ) );
      CUSOLVER_CHECK( cusolverDnSetStream( cusolverH, stream ) );

      cusolverDnParams_t params;
      CUSOLVER_CHECK( cusolverDnCreateParams( &params ) );

      CUDA_CHECK( cudaMalloc( reinterpret_cast< void** >( &d_info ), sizeof( int ) ) );

      CUSOLVER_CHECK( cusolverDnXgetrs( cusolverH,
                                        params,
                                        CUBLAS_OP_N,
                                        num_rows,
                                        numRightHandSides,
                                        traits< Real >::cuda_data_type,
                                        LU.getValues().getData(),
                                        num_rows,
                                        nullptr,  // Row Order vector not used
                                        traits< Real >::cuda_data_type,
                                        X.getValues().getData(),
                                        num_rows,
                                        d_info ) );

      CUDA_CHECK( cudaMemcpyAsync( &info, d_info, sizeof( int ), cudaMemcpyDeviceToHost, stream ) );

      CUDA_CHECK( cudaStreamSynchronize( stream ) );

      if( info < 0 ) {
         std::cout << -info << "-th parameter is wrong! Exiting." << std::endl;
         exit( 1 );
      }

      // Free resources
      CUDA_CHECK( cudaFree( d_info ) );

      CUSOLVER_CHECK( cusolverDnDestroyParams( params ) );
      CUSOLVER_CHECK( cusolverDnDestroy( cusolverH ) );
      CUDA_CHECK( cudaStreamDestroy( stream ) );
#endif
   }
}

template< MatrixFormat MtxFormat >
template< typename Matrix, typename Vector >
void
CuSolverDnXgetrsWrapper< MtxFormat >::solve( const Matrix& LU, Matrix& X, const Vector& piv )
{
   if constexpr( ! tnlObjectsOnCuda< Matrix, Vector >() ) {
      throw Exceptions::NotImplementedError( getSolverName< typename Matrix::DeviceType >( ! piv.empty() )
                                             + " is implemented only for the GPU." );
   }
   else {
#ifdef __CUDACC__
      // Required by cusolverDnXgetrs as the value-type for the pivoting vector

      using Index = typename Matrix::IndexType;
      using Real = typename Matrix::RealType;

      if constexpr( Matrix::getOrganization() != TNL::Algorithms::Segments::ColumnMajorOrder ) {
         throw Exceptions::NotImplementedError( "Input matrix uses row-major ordering - " + getSolverName<>( ! piv.empty() )
                                                + " only supports column-major ordering of matrices in GPU memory." );
      }

      if constexpr( MtxFormat != MatrixFormat::UnitDiagIn_L ) {
         throw Exceptions::NotImplementedError( getSolverName< typename Matrix::DeviceType >()
                                                + " only supports a unit diagonal in L." );
      }

      const Index num_rows = LU.getRows();
      const Index num_cols = LU.getColumns();
      const Index numRightHandSides = X.getColumns();

      if( num_rows != num_cols ) {
         throw Exceptions::MatrixNotSquare( num_rows, num_cols, "LU" );
      }

      if( num_rows != X.getRows() ) {
         throw std::runtime_error( "-!> Matrix dimensions do not match: LU has " + std::to_string( LU.getRows() )
                                   + " rows while X (which holds B's values) has " + std::to_string( X.getRows() ) + " rows." );
      }

      int info = 0;
      int* d_info = nullptr; /* error info */

      cusolverDnHandle_t cusolverH = NULL;
      cudaStream_t stream = NULL;

      CUSOLVER_CHECK( cusolverDnCreate( &cusolverH ) );

      CUDA_CHECK( cudaStreamCreateWithFlags( &stream, cudaStreamNonBlocking ) );
      CUSOLVER_CHECK( cusolverDnSetStream( cusolverH, stream ) );

      cusolverDnParams_t params;
      CUSOLVER_CHECK( cusolverDnCreateParams( &params ) );

      CUDA_CHECK( cudaMalloc( reinterpret_cast< void** >( &d_info ), sizeof( int ) ) );

      // If size of piv is 0 => piv.getData() retuns nullptr
      CUSOLVER_CHECK( cusolverDnXgetrs( cusolverH,
                                        params,
                                        CUBLAS_OP_N,
                                        num_rows,
                                        numRightHandSides,
                                        traits< Real >::cuda_data_type,
                                        LU.getValues().getData(),
                                        num_rows,
                                        piv.getData(),
                                        traits< Real >::cuda_data_type,
                                        X.getValues().getData(),
                                        num_rows,
                                        d_info ) );

      CUDA_CHECK( cudaMemcpyAsync( &info, d_info, sizeof( int ), cudaMemcpyDeviceToHost, stream ) );

      CUDA_CHECK( cudaStreamSynchronize( stream ) );

      if( info < 0 ) {
         std::cout << -info << "-th parameter is wrong! Exiting." << std::endl;
         exit( 1 );
      }

      // Free resources
      CUDA_CHECK( cudaFree( d_info ) );

      CUSOLVER_CHECK( cusolverDnDestroyParams( params ) );
      CUSOLVER_CHECK( cusolverDnDestroy( cusolverH ) );
      CUDA_CHECK( cudaStreamDestroy( stream ) );
#endif
   }
}

}  // namespace Decomposition::LU::Solvers
