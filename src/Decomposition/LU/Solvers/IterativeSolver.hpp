#pragma once

#include <TNL/Containers/Array.h>
#include <TNL/Containers/Vector.h>

#include <Decomposition/Exceptions/NotImplementedError.h>
#include <Decomposition/Exceptions/MatrixNotSquare.h>
#include <Decomposition/LU/Decomposers/BaseDecomposer.h>
#include <Decomposition/LU/Solvers/IterativeSolver.h>

#include <Decomposition/utils/utils.h>

namespace Decomposition::LU::Solvers {

template< const int THREADS_PER_BLOCK, MatrixFormat MtxFormat >
template< typename Device >
TNL::String
IterativeSolver< THREADS_PER_BLOCK, MtxFormat >::getSolverName( const bool& partialPivoting )
{
   TNL::String solverName = "IS";

   if( isCuda< Device >() ) {
      solverName.append( "_" + TNL::convertToString( THREADS_PER_BLOCK ) );
   }

   solverName.append( partialPivoting ? " PP" : "" );

   return solverName;
}

#ifdef __CUDACC__
template< typename ConstMatrixView, typename MatrixView, typename Index, typename BoolArrayView, typename Real >
__global__
void
ForwardSubstitutionKernel( const ConstMatrixView LU,
                           MatrixView Y,
                           MatrixView Ynext,
                           const MatrixView B,
                           const Index num_rows,
                           const Index nrhs,
                           BoolArrayView processed,
                           const Real tolerance )
{
   const Index row = blockIdx.x * blockDim.x + threadIdx.x;
   const Index rhs = blockIdx.y * blockDim.y + threadIdx.y;

   if( row >= num_rows || rhs >= nrhs ) {
      return;
   }

   Real sum = 0;

   // TODO: Use shared memory
   for( Index j = 0; j < row; ++j ) {
      sum = sum + LU( row, j ) * Y( j, rhs );
   }

   sum = ( B( row, rhs ) - sum ) / LU( row, row );

   if( abs( Y( row, rhs ) - sum ) > tolerance ) {
      processed( 0 ) = false;
   }

   Ynext( row, rhs ) = sum;
}
#endif

#ifdef __CUDACC__
template< typename ConstMatrixView, typename MatrixView, typename BoolArrayView, typename Real, typename Index >
__global__
void
BackwardSubstitutionKernel( const ConstMatrixView LU,
                            MatrixView X,
                            MatrixView Xnext,
                            const MatrixView Y,
                            const Index num_rows,
                            const Index num_cols,
                            const Index nrhs,
                            BoolArrayView processed,
                            const Real tolerance )
{
   const Index row = blockIdx.x * blockDim.x + threadIdx.x;
   const Index rhs = blockIdx.y * blockDim.y + threadIdx.y;

   if( row >= num_rows || rhs >= nrhs ) {
      return;
   }

   Real sum = 0;

   // TODO: Use shared memory
   for( Index j = row + 1; j < num_cols; ++j ) {
      sum = sum + LU( row, j ) * X( j, rhs );
   }

   sum = Y( row, rhs ) - sum;

   if( abs( X( row, rhs ) - sum ) > tolerance ) {
      processed( 0 ) = false;
   }

   Xnext( row, rhs ) = sum;
}
#endif

#ifdef __CUDACC__
template< typename MatrixView, typename Index >
__global__
void
MatrixAssignmentKernel( MatrixView M, MatrixView Mnext, const Index num_rows, const Index num_cols )
{
   const Index row = blockIdx.x * blockDim.x + threadIdx.x;
   const Index rhs = blockIdx.y * blockDim.y + threadIdx.y;

   if( row >= num_rows || rhs >= num_cols ) {
      return;
   }

   M( row, rhs ) = Mnext( row, rhs );
}
#endif

template< const int THREADS_PER_BLOCK, MatrixFormat MtxFormat >
template< typename Matrix >
void
IterativeSolver< THREADS_PER_BLOCK, MtxFormat >::solve( const Matrix& LU, Matrix& X )
{
   using Real = typename Matrix::RealType;
   using Device = typename Matrix::DeviceType;
   using Index = typename Matrix::IndexType;

   if constexpr( MtxFormat != MatrixFormat::UnitDiagIn_U ) {
      throw Exceptions::NotImplementedError( getSolverName< Device >() + " only supports a unit diagonal in U." );
   }
   else {
      const Index num_rows = LU.getRows();
      const Index num_cols = LU.getColumns();

      if( num_rows != num_cols ) {
         throw Exceptions::MatrixNotSquare( num_rows, num_cols, "LU" );
      }

      if( num_rows != X.getRows() ) {
         throw std::runtime_error( "-!> Matrix dimensions do not match: LU has " + std::to_string( LU.getRows() )
                                   + " rows while X (which holds B's values) has " + std::to_string( X.getRows() ) + " rows." );
      }

      const Index nrhs = X.getColumns();

      // Solve (LU)X = B

      Matrix Xnext;
      Xnext.setLike( X );

      Matrix Y;
      Y.setLike( X );
      Matrix Ynext;
      Ynext.setLike( Y );

      // X holds the values of B on input
      auto X_view = X.getView();
      auto Y_view = Y.getView();
      auto B_view = X.getView();
      auto Xnext_view = Xnext.getView();
      auto Ynext_view = Ynext.getView();

      auto LU_view = LU.getConstView();

      // Flag to indicate that a section has been processed
      TNL::Containers::Array< bool, Device > processed{ 1, true };
      auto processed_view = processed.getView();
      const Real tolerance = 0.0;

      if constexpr( tnlObjectOnHost< Matrix >() ) {
         TNL::Containers::Vector< Real, Device, Index > sum( nrhs );
         auto sum_view = sum.getView();
         Index i;

         // Forward substitution: LY = B
         do {
            for( i = 0; i < num_rows; ++i ) {
               // [ START ] Thread
               sum_view.setValue( 0 );
               for( Index j = 0; j < i; ++j ) {
                  // sum = sum + LU( i, j ) * Y( j );
                  auto y_row = Y_view.getRow( j );
                  auto rhsSum = [ = ] __cuda_callable__( Index rhs ) mutable
                  {
                     sum_view( rhs ) = sum_view( rhs ) + LU_view( i, j ) * y_row.getValue( rhs );
                  };
                  TNL::Algorithms::parallelFor< Device >( Index{ 0 }, nrhs, rhsSum );
               }
               // Ynext( i ) = ( B( i ) - sum ) / LU( i, i );
               auto ynext_row = Ynext_view.getRow( i );
               auto b_row = B_view.getRow( i );
               auto ySet = [ = ] __cuda_callable__( Index rhs ) mutable
               {
                  ynext_row.setValue( rhs, ( b_row.getValue( rhs ) - sum_view( rhs ) ) / LU_view( i, i ) );
               };
               TNL::Algorithms::parallelFor< Device >( Index{ 0 }, nrhs, ySet );
               // [ END ] Thread
            }

            processed_view( 0 ) =
               static_cast< bool >( TNL::max( TNL::abs( Y_view.getValues() - Ynext_view.getValues() ) ) <= tolerance );

            Y = Ynext;
         } while( ! processed_view( 0 ) );

         // Backward substitution: UX = Y
         processed_view( 0 ) = false;
         do {
            for( i = 0; i < num_rows; ++i ) {
               // [ START ] Thread
               sum_view.setValue( 0 );

               for( Index j = i + 1; j < num_cols; ++j ) {
                  // sum = sum + LU( i, j ) * X( j );
                  auto x_row = X_view.getRow( j );
                  auto rhsSum = [ = ] __cuda_callable__( Index rhs ) mutable
                  {
                     sum_view( rhs ) = sum_view( rhs ) + LU_view( i, j ) * x_row.getValue( rhs );
                  };
                  TNL::Algorithms::parallelFor< Device >( Index{ 0 }, nrhs, rhsSum );
               }
               // Xnext( i ) = Y( i ) - sum;
               auto xnext_row = Xnext_view.getRow( i );
               auto y_row = Y_view.getRow( i );
               auto xSet = [ = ] __cuda_callable__( Index rhs ) mutable
               {
                  xnext_row.setValue( rhs, y_row.getValue( rhs ) - sum_view( rhs ) );
               };
               TNL::Algorithms::parallelFor< Device >( Index{ 0 }, nrhs, xSet );
               // [ END ] Thread
            }

            processed_view( 0 ) =
               static_cast< bool >( TNL::max( TNL::abs( X_view.getValues() - Xnext_view.getValues() ) ) <= tolerance );

            X = Xnext;
         } while( ! processed_view( 0 ) );
      }

      if constexpr( tnlObjectOnCuda< Matrix >() ) {
#ifdef __CUDACC__
         // Round to nearest higher multiple of THREADS_PER_BLOCK - only if THREADS_PER_BLOCK is multiple of 2
         Index num_rows_rounded;
         if constexpr( THREADS_PER_BLOCK % 2 == 0 ) {
            num_rows_rounded = ( num_rows + THREADS_PER_BLOCK - 1 ) & -THREADS_PER_BLOCK;
         }
         else {
            num_rows_rounded = ( num_rows + THREADS_PER_BLOCK - 1 ) / THREADS_PER_BLOCK * THREADS_PER_BLOCK;
         }

         // Number of right hand sides is usually small
         const Index BLOCK_SIZE_y = 8;
         const Index nrhs_rounded = ( nrhs + BLOCK_SIZE_y - 1 ) / BLOCK_SIZE_y * BLOCK_SIZE_y;

         const Index block_perGrid_x = num_rows_rounded / THREADS_PER_BLOCK;
         const Index block_perGrid_y = nrhs_rounded / BLOCK_SIZE_y;

         dim3 threads_perBlock( THREADS_PER_BLOCK, BLOCK_SIZE_y );
         dim3 block_perGrid( block_perGrid_x, block_perGrid_y );

         // TODO: AssignmentKernel only needs to assign newly computed values (from the iterator var)

         // Forward substitution: LY = B
         do {
            processed_view.setElement( 0, true );
            // clang-format off
            ForwardSubstitutionKernel<<< block_perGrid, threads_perBlock >>>(
               LU_view, Y_view, Ynext_view, B_view, num_rows, nrhs, processed_view, tolerance );

            MatrixAssignmentKernel<<< block_perGrid, threads_perBlock >>>( Y_view, Ynext_view, num_rows, nrhs );
            // clang-format on
         } while( ! processed_view.getElement( 0 ) );

         // Backward substitution: UX = Y
         do {
            processed_view.setElement( 0, true );
            // clang-format off
            BackwardSubstitutionKernel<<< block_perGrid, threads_perBlock >>>(
               LU_view, X_view, Xnext_view, Y_view, num_rows, num_cols, nrhs, processed_view, tolerance );

            MatrixAssignmentKernel<<< block_perGrid, threads_perBlock >>>( X_view, Xnext_view, num_rows, nrhs );
            // clang-format on
         } while( ! processed_view.getElement( 0 ) );
#endif
      }
   }
}

template< const int THREADS_PER_BLOCK, MatrixFormat MtxFormat >
template< typename Matrix, typename Vector >
void
IterativeSolver< THREADS_PER_BLOCK, MtxFormat >::solve( const Matrix& LU, Matrix& X, const Vector& piv )
{
   using Device = typename Matrix::DeviceType;

   if constexpr( ! tnlObjectOnHost< Vector >() ) {
      throw Exceptions::NotImplementedError(
         getSolverName< Device >( ! piv.empty() )
         + " is implemented only for the CPU/GPU. However, the pivoting vector must be stored on the Host in both cases." );
   }
   else {
      if constexpr( MtxFormat != MatrixFormat::UnitDiagIn_U ) {
         throw Exceptions::NotImplementedError( getSolverName< Device >( ! piv.empty() )
                                                + " only supports a unit diagonal in U." );
      }
      else {
         // Matrix X holds values of B during input
         // -> Will be orderd and used in computation
         if( ! piv.empty() ) {
            Decomposers::BaseDecomposer::orderMatrixAccordingTo( X, piv );
         }
         solve( LU, X );
      }
   }
}

}  // namespace Decomposition::LU::Solvers
