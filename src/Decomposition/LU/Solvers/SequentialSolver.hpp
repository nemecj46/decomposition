#pragma once

#include <TNL/Algorithms/parallelFor.h>
#include <TNL/Containers/Vector.h>

#include <Decomposition/Exceptions/NotImplementedError.h>
#include <Decomposition/Exceptions/MatrixNotSquare.h>
#include <Decomposition/LU/Decomposers/BaseDecomposer.h>
#include <Decomposition/LU/Solvers/SequentialSolver.h>

namespace Decomposition::LU::Solvers {

template< MatrixFormat MtxFormat >
template< typename Device >
TNL::String
SequentialSolver< MtxFormat >::getSolverName( const bool& partialPivoting )
{
   return partialPivoting ? "SS PP" : "SS";
}

template< MatrixFormat MtxFormat >
template< typename Matrix >
void
SequentialSolver< MtxFormat >::solve( const Matrix& LU, Matrix& X )
{
   if constexpr( ! tnlObjectOnHost< Matrix >() ) {
      throw Exceptions::NotImplementedError( getSolverName< Matrix >() + " is implemented only for the CPU." );
   }
   else {
      using Real = typename Matrix::RealType;
      using Device = typename Matrix::DeviceType;
      using Index = typename Matrix::IndexType;

      if constexpr( MtxFormat != MatrixFormat::UnitDiagIn_U ) {
         throw Exceptions::NotImplementedError( getSolverName< Device >() + " only supports a unit diagonal in U." );
      }
      else {
         const Index num_rows = LU.getRows();
         const Index num_cols = LU.getColumns();

         if( num_rows != num_cols ) {
            throw Exceptions::MatrixNotSquare( num_rows, num_cols, "LU" );
         }

         if( num_rows != X.getRows() ) {
            throw std::runtime_error( "-!> Matrix dimensions do not match: LU has " + std::to_string( LU.getRows() )
                                      + " rows while X (which holds B's values) has " + std::to_string( X.getRows() )
                                      + " rows." );
         }

         const Index numRightHandSides = X.getColumns();
         TNL::Containers::Vector< Real, Device, Index > sum( numRightHandSides );

         // Solve (LU)X = B, where X holds the values of B on input

         // Just to keep the original labels in the loops for clarity
         auto X_view = X.getView();
         auto Y_view = X.getView();
         auto B_view = X.getView();

         auto LU_view = LU.getConstView();
         auto sum_view = sum.getView();

         // Forward substitution: LY = B
         for( Index i = 0; i < num_rows; ++i ) {
            sum_view.setValue( 0 );
            for( Index j = 0; j < i; ++j ) {
               // sum = sum + LU( i, j ) * Y( j );

               auto y_row = Y_view.getRow( j );
               auto rhsSum = [ = ] __cuda_callable__( Index rhs ) mutable
               {
                  sum_view( rhs ) = sum_view( rhs ) + LU_view( i, j ) * y_row.getValue( rhs );
               };
               TNL::Algorithms::parallelFor< Device >( Index{ 0 }, numRightHandSides, rhsSum );
            }

            // Y( i ) = ( B( i ) - sum ) / LU( i, i );
            auto y_row = Y_view.getRow( i );
            auto b_row = B_view.getRow( i );
            auto ySet = [ = ] __cuda_callable__( Index rhs ) mutable
            {
               y_row.setValue( rhs, ( b_row.getValue( rhs ) - sum_view( rhs ) ) / LU_view( i, i ) );
            };
            TNL::Algorithms::parallelFor< Device >( Index{ 0 }, numRightHandSides, ySet );
         }

         // Backward substitution: UX = Y
         for( Index i = num_rows - 1; i >= 0; --i ) {
            sum_view.setValue( 0 );
            for( Index j = i + 1; j < num_cols; ++j ) {
               // sum = sum + LU( i, j ) * X( j );
               auto x_row = X_view.getRow( j );
               auto rhsSum = [ = ] __cuda_callable__( Index rhs ) mutable
               {
                  sum_view( rhs ) = sum_view( rhs ) + LU_view( i, j ) * x_row.getValue( rhs );
               };
               TNL::Algorithms::parallelFor< Device >( Index{ 0 }, numRightHandSides, rhsSum );
            }

            // X_view( i ) = Y_view( i ) - sum;
            auto x_row = X_view.getRow( i );
            auto y_row = Y_view.getRow( i );
            auto xSet = [ = ] __cuda_callable__( Index rhs ) mutable
            {
               x_row.setValue( rhs, y_row.getValue( rhs ) - sum_view( rhs ) );
            };
            TNL::Algorithms::parallelFor< Device >( Index{ 0 }, numRightHandSides, xSet );
         }
      }
   }
}

template< MatrixFormat MtxFormat >
template< typename Matrix, typename Vector >
void
SequentialSolver< MtxFormat >::solve( const Matrix& LU, Matrix& X, const Vector& piv )
{
   if constexpr( ! tnlObjectsOnHost< Matrix, Vector >() ) {
      throw Exceptions::NotImplementedError( getSolverName<>( ! piv.empty() ) + " is implemented only for the CPU." );
   }
   else {
      if constexpr( MtxFormat != MatrixFormat::UnitDiagIn_U ) {
         throw Exceptions::NotImplementedError( getSolverName<>( ! piv.empty() ) + " only supports a unit diagonal in U." );
      }
      else {
         // Matrix X holds values of B during input
         // -> Will be orderd and used in computation
         if( ! piv.empty() ) {
            Decomposers::BaseDecomposer::orderMatrixAccordingTo( X, piv );
         }
         solve( LU, X );
      }
   }
}

}  // namespace Decomposition::LU::Solvers
