#pragma once

#include <TNL/Algorithms/Segments/ElementsOrganization.h>

#include <Decomposition/LU/Solvers/CuBLAStrsmWrapper.h>

#include <Decomposition/Exceptions/NotImplementedError.h>
#include <Decomposition/Exceptions/MatrixNotSquare.h>
#include <Decomposition/LU/Decomposers/BaseDecomposer.h>

#ifdef __CUDACC__
   #include <cublas_v2.h>
   #include <Decomposition/utils/CuLibsUtils.h>
#endif

#include <Decomposition/utils/utils.h>

namespace Decomposition::LU::Solvers {

template< MatrixFormat MtxFormat >
template< typename Device >
TNL::String
CuBLAStrsmWrapper< MtxFormat >::getSolverName( const bool& partialPivoting )
{
   TNL::String solverName = "CuBLAStrsmWrapper";
   solverName.append( MtxFormat == MatrixFormat::UnitDiagIn_U ? "_U" : "_L" );
   solverName.append( partialPivoting ? " PP" : "" );
   return solverName;
}

template< MatrixFormat MtxFormat >
template< typename Matrix >
void
CuBLAStrsmWrapper< MtxFormat >::solve( const Matrix& LU, Matrix& X )
{
   if constexpr( ! tnlObjectOnCuda< Matrix >() ) {
      throw Exceptions::NotImplementedError( getSolverName<>() + " is implemented only for the GPU." );
   }
   else {
#ifdef __CUDACC__
      if constexpr( Matrix::getOrganization() != TNL::Algorithms::Segments::ColumnMajorOrder ) {
         throw Exceptions::NotImplementedError( "Input matrix uses row-major ordering - " + getSolverName<>()
                                                + " only supports column-major ordering of matrices in GPU memory." );
      }

      using Real = typename Matrix::RealType;
      using Device = typename Matrix::DeviceType;
      using Index = typename Matrix::IndexType;

      const Index num_rows = LU.getRows();
      const Index num_cols = LU.getColumns();

      if( num_rows != num_cols ) {
         throw Exceptions::MatrixNotSquare( num_rows, num_cols, "LU" );
      }

      if( num_rows != X.getRows() ) {
         throw std::runtime_error( "-!> Matrix dimensions do not match: LU has " + std::to_string( LU.getRows() )
                                   + " rows while X (which holds B's values) has " + std::to_string( X.getRows() ) + " rows." );
      }

      // cublas<T>trsm stores results of:
      //    - LY = N into N
      //    - UX = Y into Y
      // => Vector X will be:
      // -> used as B
      // -> overwritten to hold values of Y
      // -> used as Y
      // -> overwritten to hold values of X

      const Index numRightHandSides = X.getColumns();

      cublasHandle_t cublasH;
      CUBLAS_CHECK( cublasCreate( &cublasH ) );

      // Some CuBLAS filler variable
      const Real one = 1.0;

      constexpr cublasDiagType_t diagTypeL = MtxFormat == MatrixFormat::UnitDiagIn_L ? CUBLAS_DIAG_UNIT : CUBLAS_DIAG_NON_UNIT;
      constexpr cublasDiagType_t diagTypeU = MtxFormat == MatrixFormat::UnitDiagIn_U ? CUBLAS_DIAG_UNIT : CUBLAS_DIAG_NON_UNIT;

      if constexpr( std::is_same_v< Real, double > ) {
         CUBLAS_CHECK( cublasDtrsm( cublasH,
                                    CUBLAS_SIDE_LEFT,
                                    CUBLAS_FILL_MODE_LOWER,
                                    CUBLAS_OP_N,
                                    diagTypeL,
                                    num_rows,
                                    numRightHandSides,
                                    &one,
                                    LU.getValues().getData(),
                                    num_rows,
                                    X.getValues().getData(),
                                    num_rows ) );

         CUBLAS_CHECK( cublasDtrsm( cublasH,
                                    CUBLAS_SIDE_LEFT,
                                    CUBLAS_FILL_MODE_UPPER,
                                    CUBLAS_OP_N,
                                    diagTypeU,
                                    num_rows,
                                    numRightHandSides,
                                    &one,
                                    LU.getValues().getData(),
                                    num_rows,
                                    X.getValues().getData(),
                                    num_rows ) );
      }
      else if constexpr( std::is_same_v< Real, float > ) {
         CUBLAS_CHECK( cublasStrsm( cublasH,
                                    CUBLAS_SIDE_LEFT,
                                    CUBLAS_FILL_MODE_LOWER,
                                    CUBLAS_OP_N,
                                    diagTypeL,
                                    num_rows,
                                    numRightHandSides,
                                    &one,
                                    LU.getValues().getData(),
                                    num_rows,
                                    X.getValues().getData(),
                                    num_rows ) );

         CUBLAS_CHECK( cublasStrsm( cublasH,
                                    CUBLAS_SIDE_LEFT,
                                    CUBLAS_FILL_MODE_UPPER,
                                    CUBLAS_OP_N,
                                    diagTypeU,
                                    num_rows,
                                    numRightHandSides,
                                    &one,
                                    LU.getValues().getData(),
                                    num_rows,
                                    X.getValues().getData(),
                                    num_rows ) );
      }

      CUBLAS_CHECK( cublasDestroy( cublasH ) );
#endif
   }
}

template< MatrixFormat MtxFormat >
template< typename Matrix, typename Vector >
void
CuBLAStrsmWrapper< MtxFormat >::solve( const Matrix& LU, Matrix& X, const Vector& piv )
{
   if constexpr( ! ( tnlObjectOnCuda< Matrix >() && tnlObjectOnHost< Vector >() ) ) {
      throw Exceptions::NotImplementedError(
         getSolverName< TNL::Devices::Cuda >( ! piv.empty() )
         + " is implemented only for the GPU. The matrix must be stored on the Device and the pivoting vector on the Host." );
   }
   else {
      if constexpr( Matrix::getOrganization() != TNL::Algorithms::Segments::ColumnMajorOrder ) {
         throw Exceptions::NotImplementedError( "Input matrix uses row-major ordering - " + getSolverName<>( ! piv.empty() )
                                                + " only supports column-major ordering of matrices in GPU memory." );
      }
      else {
         // Matrix X holds values of B during input
         // -> Will be orderd and used in computation
         if( ! piv.empty() ) {
            Decomposers::BaseDecomposer::orderMatrixAccordingTo( X, piv );
         }
         solve( LU, X );
      }
   }
}

}  // namespace Decomposition::LU::Solvers
