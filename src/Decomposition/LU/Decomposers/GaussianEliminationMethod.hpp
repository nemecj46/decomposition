#pragma once

#include <iostream>

#include <Decomposition/LU/Decomposers/GaussianEliminationMethod.h>

#include <Decomposition/Exceptions/MatrixNotSquare.h>
#include <Decomposition/Exceptions/MatrixSingular.h>
#include <Decomposition/Exceptions/NotImplementedError.h>

namespace Decomposition::LU::Decomposers {

template< typename Device >
TNL::String
GaussianEliminationMethod::getDecomposerName( const bool& partialPivoting )
{
   return partialPivoting ? "GEM PP" : "GEM";
}

template< typename Matrix, typename Vector >
void
GaussianEliminationMethod::decompose( Matrix& LU, Vector& piv )
{
   if constexpr( ! tnlObjectsOnHost< Matrix, Vector >() ) {
      throw Exceptions::NotImplementedError( getDecomposerName< typename Matrix::DeviceType >()
                                             + " is implemented only for the CPU." );
   }
   else {
      if( piv.empty() ) {
         throw Exceptions::NotImplementedError( getDecomposerName< typename Matrix::DeviceType >()
                                                + " is implemented only with partial pivoting. Pivoting vector is empty!" );
      }

      using Index = typename Matrix::IndexType;
      using Real = typename Matrix::RealType;

      const Index num_rows = LU.getRows();
      const Index num_cols = LU.getColumns();

      if( num_rows != num_cols ) {
         throw Exceptions::MatrixNotSquare( num_rows, num_cols );
      }

      Index i;
      Index j;
      Index k;

      auto LU_view = LU.getView();
      auto piv_view = piv.getView();

      BaseDecomposer::setDefaultPivotingValues( piv );

      for( j = 0; j < num_cols; ++j ) {
         std::pair< Real, Index > max_elem = pivotRowOfMatrix_host( j, LU_view, num_rows, num_cols, piv_view );

         if( max_elem.first == 0 ) {
            throw Exceptions::MatrixSingular( "LU", j, j );
         }

         for( i = j + 1; i < num_cols; ++i ) {
            LU_view( i, j ) /= LU_view( j, j );
            for( k = j + 1; k < num_cols; ++k ) {
               LU_view( i, k ) = LU_view( i, k ) - LU_view( i, j ) * LU_view( j, k );
            }
         }
      }
   }
}

}  // namespace Decomposition::LU::Decomposers
