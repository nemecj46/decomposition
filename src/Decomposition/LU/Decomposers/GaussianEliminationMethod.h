#pragma once

#include <TNL/Matrices/DenseMatrix.h>
#include <TNL/String.h>

#include <Decomposition/LU/Decomposers/BaseDecomposer.h>

namespace Decomposition::LU::Decomposers {

class GaussianEliminationMethod : public BaseDecomposer
{
public:
   static constexpr MatrixFormat
   getMatrixDiagonalFormat()
   {
      return MatrixFormat::UnitDiagIn_L;
   }

   template< typename Device >
   static TNL::String
   getDecomposerName( const bool& partialPivoting = true );

   template< typename Matrix, typename Vector >
   static void
   decompose( Matrix& LU, Vector& piv );
};

}  // namespace Decomposition::LU::Decomposers

#include <Decomposition/LU/Decomposers/GaussianEliminationMethod.hpp>
