#pragma once

#include <TNL/String.h>

#include <Decomposition/LU/Decomposers/BaseDecomposer.h>

namespace Decomposition::LU::Decomposers {

template< const int BLOCK_SIZE = DEFAULT_BLOCK_SIZE >
class IterativeCroutMethod : public BaseDecomposer
{
public:
   static constexpr MatrixFormat
   getMatrixDiagonalFormat()
   {
      return MatrixFormat::UnitDiagIn_U;
   }

   template< typename Device >
   static TNL::String
   getDecomposerName( const bool& partialPivoting = false );

   /**
    * \brief Decompose matrix \p LU into the product of L and U.
    *
    * The initial estimate of L and U is A.
    * On output, the values of L and U are stored in \p LU.
    * Note that the main diagonal of L is stored in the main diagonal of \p LU.
    * The main diagonal of U - comprising 1s - is not stored.
    *
    * \param LU Input matrix - it will contain the decomposed matrix.
    */
   template< typename Matrix >
   static void
   decompose( Matrix& LU );

   /**
    * \brief Decompose matrix \p LU with partial pivoting (if piv is non-empty) into the product of L and U.
    *
    * The initial estimate of L and U is A.
    * On output, the values of L and U are stored in \p LU.
    * Note that the main diagonal of L is stored in the main diagonal of \p LU.
    * The main diagonal of U - comprising 1s - is not stored.
    * The pivoting history of rows is stored in \p piv , e.g., piv[ 2 ] = 4 indicates that row 2 was swapped with row 4.
    *
    * \param LU Input matrix - it will contain the decomposed matrix.
    * \param piv Pivoting vector - if it is empty, then \p A is decomposed without partial pivoting.
    */
   template< typename Matrix, typename Vector >
   static void
   decompose( Matrix& LU, Vector& piv );

   /**
    * \brief Decompose matrix \p A into the product of L and U using \p LU as an initial estimate.
    *
    * The initial estimate of L and U is \p LU.
    * On output, the values of L and U are stored in \p LU.
    * Note that the main diagonal of L is stored in the main diagonal of \p LU.
    * The main diagonal of U - comprising 1s - is not stored.
    *
    * \param A Input matrix.
    * \param LU Initial estimate of the decomposed matrix - it will contain the decomposed matrix.
    */
   template< typename Matrix >
   static void
   decompose( Matrix& A, Matrix& LU );

   /**
    * \brief Decompose matrix \p A with partial pivoting (if piv is non-empty) into the product of L and U using \p LU as an
    * initial estimate.
    *
    * The initial estimate of L and U is \p LU.
    * On output, the values of L and U are stored in \p LU.
    * Note that the main diagonal of L is stored in the main diagonal of \p LU.
    * The main diagonal of U - comprising 1s - is not stored.
    * The pivoting history of rows is stored in \p piv , e.g., piv[ 2 ] = 4 indicates that row 2 was swapped with row 4.
    *
    * \param A Input matrix - its rows will be reordered to mirror that of LU.
    * \param LU Initial estimate of the decomposed matrix - it will contain the decomposed matrix.
    * \param piv Pivoting vector - if it is empty, then \p A is decomposed without partial pivoting.
    */
   template< typename Matrix, typename Vector >
   static void
   decompose( Matrix& A, Matrix& LU, Vector& piv );
};

}  // namespace Decomposition::LU::Decomposers

#include <Decomposition/LU/Decomposers/IterativeCroutMethod.hpp>
