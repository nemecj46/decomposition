#pragma once

#include <iostream>

#include <TNL/Algorithms/parallelFor.h>
#include <TNL/Matrices/DenseMatrix.h>

#include <Decomposition/Exceptions/NotImplementedError.h>
#include <Decomposition/LU/Decomposers/BaseDecomposer.h>

#include <Decomposition/utils/utils.h>

namespace Decomposition::LU::Decomposers {

TNL::String
BaseDecomposer::getDecomposerName()
{
   return "Base Decomposer";
}

template< typename Matrix >
Matrix
getEmptyMatrix()
{
   return Matrix( 0, 0 );
}

template< typename MatrixView, typename Index >
void
swapRows_host( MatrixView& M, const Index& row1, const Index& row2, const Index& num_cols )
{
   using Device = typename MatrixView::DeviceType;

   auto swap = [ = ]( Index i ) mutable
   {
      TNL::swap( M( row1, i ), M( row2, i ) );
   };

   TNL::Algorithms::parallelFor< Device >( Index{ 0 }, num_cols, swap );
}

#ifdef __CUDACC__
template< typename MatrixView, typename Index >
__global__
void
SwapRows_kernel( MatrixView M, const Index row1, const Index row2, const Index numCols )
{
   const Index col = blockIdx.x * blockDim.x + threadIdx.x;

   if( col >= numCols )
      return;

   TNL::swap( M( row1, col ), M( row2, col ) );
}
#endif

#ifdef __CUDACC__
template< typename MatrixView, typename Index >
__global__
void
SwapRows_kernel( MatrixView M1, MatrixView M2, const Index row1, const Index row2, const Index numCols )
{
   const Index col = blockIdx.x * blockDim.x + threadIdx.x;

   if( col >= numCols )
      return;

   TNL::swap( M1( row1, col ), M1( row2, col ) );
   TNL::swap( M2( row1, col ), M2( row2, col ) );
}
#endif

template< const int THREADS_PER_BLOCK = DEFAULT_THREADS_PER_BLOCK, typename MatrixView, typename Index >
void
swapRows_device( MatrixView& M, const Index& row1, const Index& row2, const Index& num_cols )
{
#ifdef __CUDACC__
   const Index block_perGrid = ( num_cols + THREADS_PER_BLOCK - 1 ) / THREADS_PER_BLOCK;
   // clang-format off
   SwapRows_kernel<<< block_perGrid, THREADS_PER_BLOCK >>>( M, row1, row2, num_cols );
   // clang-format on
#endif
}

template< const int THREADS_PER_BLOCK = DEFAULT_THREADS_PER_BLOCK, typename MatrixView, typename Index >
void
swapRows_device( MatrixView& M1, MatrixView& M2, const Index& row1, const Index& row2, const Index& num_cols )
{
#ifdef __CUDACC__
   const Index block_perGrid = ( num_cols + THREADS_PER_BLOCK - 1 ) / THREADS_PER_BLOCK;
   // clang-format off
   SwapRows_kernel<<< block_perGrid, THREADS_PER_BLOCK >>>( M1, M2, row1, row2, num_cols );
   // clang-format on
#endif
}

template< typename MatrixView, typename Index >
void
BaseDecomposer::swapRows( MatrixView& M, const Index& row1, const Index& row2, const Index& num_cols )
{
   if constexpr( tnlObjectOnHost< MatrixView >() ) {
      swapRows_host( M, row1, row2, num_cols );
   }

   if constexpr( tnlObjectOnCuda< MatrixView >() ) {
      swapRows_device( M, row1, row2, num_cols );
   }
}

template< const int order, typename Matrix, typename Vector >
void
reorderMatrixAccordingTo_host( Matrix& M, const Vector& piv )
{
   using Index = typename Vector::IndexType;

   const Index num_rows = M.getRows();
   const Index num_cols = M.getColumns();

   // Order original matrix according to piv (original matrix -> permutated matrix)
   if constexpr( order == 0 ) {
      for( Index i = 0; i < num_rows; ++i ) {
         if( piv( i ) != i + 1 ) {
            const Index temp = piv( i ) - 1;
            swapRows_host( M, i, temp, num_cols );
         }
      }
   }  // Undo ordering done by piv (permutated matrix -> original matrix)
   else {
      for( Index i = num_rows - 1; i >= 0; --i ) {
         if( piv( i ) != i + 1 ) {
            const Index temp = piv( i ) - 1;
            swapRows_host( M, i, temp, num_cols );
         }
      }
   }
}

template< const int orderMatrix, typename Matrix, typename Vector >
void
reorderMatrixAccordingTo_device( Matrix& M, const Vector& piv )
{
#ifdef __CUDACC__
   if constexpr( ! Decomposition::tnlObjectOnHost< Vector >() ) {
      throw Decomposition::Exceptions::NotImplementedError(
         "reorderMatrixAccordingTo_device only supports the pivoting vector allocated on the Host." );
   }
   else {
      using Index = typename Matrix::IndexType;

      auto M_view = M.getView();
      auto piv_view = piv.getConstView();

      const Index num_rows = M_view.getRows();
      const Index num_cols = M_view.getColumns();

      // Order original matrix according to piv (original matrix -> permutated matrix)
      if constexpr( orderMatrix == 0 ) {
         for( Index i = 0; i < num_rows; ++i ) {
            if( piv_view( i ) != i + 1 ) {
               const Index temp = piv_view( i ) - 1;
               swapRows_device( M_view, i, temp, num_cols );
            }
         }
      }  // Undo ordering done by piv (permutated matrix -> original matrix)
      else {
         for( Index i = num_rows - 1; i >= 0; --i ) {
            if( piv_view( i ) != i + 1 ) {
               const Index temp = piv_view( i ) - 1;
               swapRows_device( M_view, i, temp, num_cols );
            }
         }
      }
   }
#endif
}

template< typename Matrix, typename Vector >
void
BaseDecomposer::orderMatrixAccordingTo( Matrix& M, const Vector& piv )
{
   if( M.getRows() != piv.getSize() ) {
      throw std::runtime_error( "-!> Failed to order Matrix according to Vector. The matrix has "
                                + std::to_string( M.getRows() ) + " rows while the vector has "
                                + std::to_string( piv.getSize() ) + "." );
   }

   if constexpr( tnlObjectsOnHost< Matrix, Vector >() ) {
      reorderMatrixAccordingTo_host< 0 >( M, piv );
   }
   else if constexpr( tnlObjectOnCuda< Matrix >() ) {
      if constexpr( tnlObjectOnCuda< Vector >() ) {
         using Index = typename Vector::IndexType;
         TNL::Containers::Vector< Index, TNL::Devices::Host, Index > piv_host;
         piv_host = piv;
         reorderMatrixAccordingTo_device< 0 >( M, piv_host );
      }
      else {
         reorderMatrixAccordingTo_device< 0 >( M, piv );
      }
   }
   else {
      throw Exceptions::NotImplementedError(
         "Ordering matrix according to vector failed! The matrix is allocated on the Host and the vector and on the Device." );
   }
}

template< typename Matrix, typename Vector >
void
BaseDecomposer::undoOrderInMatrixAccordingTo( Matrix& M, const Vector& piv )
{
   if( M.getRows() != piv.getSize() ) {
      throw std::runtime_error( "-!> Failed to undo order in Matrix according to Vector. The matrix has "
                                + std::to_string( M.getRows() ) + " rows while the vector has "
                                + std::to_string( piv.getSize() ) + "." );
   }

   if constexpr( tnlObjectsOnHost< Matrix, Vector >() ) {
      reorderMatrixAccordingTo_host< 1 >( M, piv );
   }
   else if constexpr( tnlObjectOnCuda< Matrix >() ) {
      if constexpr( tnlObjectOnCuda< Vector >() ) {
         using Index = typename Vector::IndexType;
         TNL::Containers::Vector< Index, TNL::Devices::Host, Index > piv_host;
         piv_host = piv;
         reorderMatrixAccordingTo_device< 1 >( M, piv_host );
      }
      else {
         reorderMatrixAccordingTo_device< 1 >( M, piv );
      }
   }
   else {
      throw Exceptions::NotImplementedError( "Undoing ordering in matrix according to vector failed! The matrix is allocated "
                                             "on the Host and the vector and on the Device." );
   }
}

template< typename Index, typename MatrixView, typename VectorView, typename Real = typename MatrixView::RealType >
std::pair< Real, Index >
pivotRowOfMatrix_host( const Index& j, MatrixView& M, const Index& num_rows, const Index& num_cols, VectorView& piv )
{
   // Find row below the j-th row with max. value in the j-th column
   Real max = TNL::abs( M( j, j ) );  // or M(i,j)
   Index pivRow = j;

   for( Index i = j + 1; i < num_rows; ++i ) {
      Real zij = TNL::abs( M( i, j ) );
      if( zij > max ) {
         max = zij;
         pivRow = i;
      }
   }

   if( pivRow != j ) {  // exchange rows j, pivRow
      swapRows_host( M, j, pivRow, num_cols );
      piv( j ) = pivRow + 1;
   }

   return std::make_pair( max, pivRow );
}

template< const int THREADS_PER_BLOCK = DEFAULT_THREADS_PER_BLOCK,
          typename Index,
          typename MatrixView,
          typename VectorView,
          typename Real = typename MatrixView::RealType >
std::pair< Real, Index >
pivotRowOfMatrix_device( const Index& j, MatrixView& M, const Index& num_rows, const Index& num_cols, VectorView& piv )
{
#ifdef __CUDACC__
   auto fetchAbsElement = [ = ] __cuda_callable__( Index row )
   {
      return abs( M( row, j ) );
   };
   // FIXME: Remove typing to int once https://gitlab.com/tnl-project/tnl/-/issues/130 is solved
   std::pair< Real, Index > max_elem =
      TNL::Algorithms::reduceWithArgument< TNL::Devices::Cuda >( (int) j, (int) num_rows, fetchAbsElement, TNL::MaxWithArg{} );

   if( max_elem.second != j ) {  // Only need to swap rows if the pivoting row is different from j
      swapRows_device< THREADS_PER_BLOCK >( M, j, max_elem.second, num_cols );
      piv( j ) = max_elem.second + 1;
   }

   return max_elem;
#endif
}

template< typename Index, typename MatrixView, typename VectorView, typename Real >
std::pair< Real, Index >
BaseDecomposer::pivotRowOfMatrix( const Index& j, MatrixView& M, const Index& num_rows, const Index& num_cols, VectorView& piv )
{
   if constexpr( tnlObjectsOnHost< MatrixView, VectorView >() ) {
      return pivotRowOfMatrix_host( j, M, num_rows, num_cols, piv );
   }
   else if constexpr( tnlObjectOnCuda< MatrixView >() && tnlObjectOnHost< VectorView >() ) {
      return pivotRowOfMatrix_device( j, M, num_rows, num_cols, piv );
   }
   else {
      throw Exceptions::NotImplementedError( "pivotRowOfMatrix() supports two cases: all TNL objects on the Host, or the "
                                             "matrix on the Device and the pivoting vector on the Host." );
   }
}

template< typename Index, typename MatrixView, typename VectorView, typename Real = typename MatrixView::RealType >
std::pair< Real, Index >
pivotRowOfMatrices_host( const Index& j,
                         MatrixView& A,
                         MatrixView& M,
                         const Index& num_rows,
                         const Index& num_cols,
                         VectorView& piv )
{
   std::pair< Real, Index > max_elem = pivotRowOfMatrix_host( j, M, num_rows, num_cols, piv );

   if( max_elem.second != j ) {
      swapRows_host( A, j, max_elem.second, num_cols );
   }

   return max_elem;
}

template< const int THREADS_PER_BLOCK = DEFAULT_THREADS_PER_BLOCK,
          typename Index,
          typename MatrixView,
          typename VectorView,
          typename Real = typename MatrixView::RealType >
std::pair< Real, Index >
pivotRowOfMatrices_device( const Index& j,
                           MatrixView& A,
                           MatrixView& M,
                           const Index& num_rows,
                           const Index& num_cols,
                           VectorView& piv )
{
#ifdef __CUDACC__
   auto fetchAbsElement = [ = ] __cuda_callable__( Index row )
   {
      return abs( M( row, j ) );
   };
   // FIXME: Remove typing to int once https://gitlab.com/tnl-project/tnl/-/issues/130 is solved
   std::pair< Real, Index > max_elem =
      TNL::Algorithms::reduceWithArgument< TNL::Devices::Cuda >( (int) j, (int) num_rows, fetchAbsElement, TNL::MaxWithArg{} );

   if( max_elem.second != j ) {
      swapRows_device< THREADS_PER_BLOCK >( A, M, j, max_elem.second, num_cols );
      piv( j ) = max_elem.second + 1;
   }

   return max_elem;
#endif
}

template< typename Index, typename MatrixView, typename VectorView, typename Real >
std::pair< Real, Index >
BaseDecomposer::pivotRowOfMatrices( const Index& j,
                                    MatrixView& A,
                                    MatrixView& M,
                                    const Index& num_rows,
                                    const Index& num_cols,
                                    VectorView& piv )
{
   if constexpr( tnlObjectsOnHost< MatrixView, VectorView >() ) {
      return pivotRowOfMatrices_host( j, A, M, num_rows, num_cols, piv );
   }
   else if constexpr( tnlObjectOnCuda< MatrixView >() && tnlObjectOnHost< VectorView >() ) {
      return pivotRowOfMatrices_device( j, A, M, num_rows, num_cols, piv );
   }
   else {
      throw Exceptions::NotImplementedError( "pivotRowOfMatrices() supports two cases: all TNL objects on the Host, or the "
                                             "matrix on the Device and the pivoting vector on the Host." );
   }
}

template< typename Matrix >
Matrix
BaseDecomposer::packMatrices( const Matrix& L, const Matrix& U )
{
   using Index = typename Matrix::IndexType;
   using Device = typename Matrix::DeviceType;

   Matrix M;
   M.setLike( L );
   M.addMatrix( L );
   M.addMatrix( U );

   // Subtract 1s from the main diagonal to remove the undesired unit diagonal.
   auto matrixView = M.getView();
   auto initMatrix = [ = ] __cuda_callable__( Index i ) mutable
   {
      matrixView( i, i ) -= 1;
   };
   TNL::Algorithms::parallelFor< Device >( (Index) 0, M.getColumns(), initMatrix );

   return M;
}

template< typename Matrix >
std::tuple< Matrix, Matrix >
getUnpackedMatrix_host( const Matrix& M, const Decomposition::Containers::MatrixFormat format )
{
   using Index = typename Matrix::IndexType;
   Matrix L;
   Matrix U;
   L = M;
   U = M;

   if( format == Decomposition::Containers::MatrixFormat::UnitDiagIn_U ) {
      for( Index i = 0; i < U.getRows(); ++i )
         U( i, i ) = 1;
   }
   else {
      for( Index i = 0; i < U.getRows(); ++i )
         L( i, i ) = 1;
   }

   // Set the upper triangle of L and the lower triangle of U to 0
   for( Index i = 0; i < L.getRows(); ++i ) {
      for( Index j = i + 1; j < U.getColumns(); ++j ) {
         L( i, j ) = 0;
         U( j, i ) = 0;
      }
   }

   return std::make_tuple( L, U );
}

#ifdef __CUDACC__
template< typename Matrix, typename Index >
__global__
void
UnpackMatrixKernel( Matrix L,
                    Matrix U,
                    const Index numRows,
                    const Index numCols,
                    const Decomposition::Containers::MatrixFormat format )
{
   const Index row = blockIdx.y * blockDim.y + threadIdx.y;
   const Index col = blockIdx.x * blockDim.x + threadIdx.x;

   if( row >= numRows || col >= numCols )
      return;

   if( format == Decomposition::Containers::MatrixFormat::UnitDiagIn_U )
      U( row, row ) = 1;
   else
      L( row, row ) = 1;

   __syncthreads();
   // Set the upper triangle of L and the lower triangle of U to 0
   if( row > col ) {
      U( row, col ) = 0;
   }
   else if( row < col ) {
      L( row, col ) = 0;
   }
   else
      return;
}
#endif

template< typename Matrix >
std::tuple< Matrix, Matrix >
getUnpackedMatrix_device( const Matrix& M, const Decomposition::Containers::MatrixFormat format )
{
#ifdef __CUDACC__
   using Index = typename Matrix::IndexType;

   Matrix L;
   Matrix U;
   L = M;
   U = M;

   // Offset the number of columns to the nearest higher multiple of blockSize
   const Index numCols = ( M.getColumns() + DEFAULT_BLOCK_SIZE - 1 ) / DEFAULT_BLOCK_SIZE * DEFAULT_BLOCK_SIZE;

   const Index blocks = numCols / DEFAULT_BLOCK_SIZE;
   dim3 threads_perBlock( DEFAULT_BLOCK_SIZE, DEFAULT_BLOCK_SIZE );
   dim3 block_perGrid( blocks, blocks );

   // Set the upper triangle of L and the lower triangle of U to 0
   // clang-format off
   UnpackMatrixKernel<<< block_perGrid, threads_perBlock >>>(
      L.getView(), U.getView(), L.getRows(), L.getColumns(), format );
   // clang-format on

   return std::make_tuple( L, U );
#endif
}

template< typename Matrix >
std::tuple< Matrix, Matrix >
BaseDecomposer::getUnpackedMatrices( const Matrix& M, const MatrixFormat format )
{
   if constexpr( tnlObjectOnHost< Matrix >() ) {
      return getUnpackedMatrix_host( M, format );
   }

   if constexpr( tnlObjectOnCuda< Matrix >() ) {
      return getUnpackedMatrix_device( M, format );
   }
}

template< typename Matrix >
void
BaseDecomposer::printMatrix( const Matrix& M )
{
   using Index = typename Matrix::IndexType;
   using Real = typename Matrix::RealType;
   using HostMatrix = TNL::Matrices::DenseMatrix< Real, TNL::Devices::Host, Index >;

   HostMatrix Mhost;
   Mhost = M;

   for( Index i = 0; i < Mhost.getRows(); ++i ) {
      for( Index j = 0; j < Mhost.getRows(); ++j ) {
         std::cout << Mhost( i, j ) << " ";
      }
      std::cout << '\n';
   }
}

template< const int start, typename Vector >
void
BaseDecomposer::setDefaultPivotingValues( Vector& piv )
{
   using Index = typename Vector::IndexType;

   piv.getView().forAllElements(
      [] __cuda_callable__( Index i, Index & value )
      {
         value = i + start;
      } );
}

}  // namespace Decomposition::LU::Decomposers
