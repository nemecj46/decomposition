#pragma once

#include <TNL/String.h>

#include <Decomposition/Containers/MatrixFormat.h>

namespace Decomposition::LU::Decomposers {

class BaseDecomposer
{
protected:
   using MatrixFormat = Decomposition::Containers::MatrixFormat;

public:
   static constexpr MatrixFormat
   getMatrixDiagonalFormat()
   {
      return MatrixFormat::UnitDiagIn_U;
   }

   static TNL::String
   getDecomposerName();

   template< typename MatrixView, typename Index >
   static void
   swapRows( MatrixView& M, const Index& row1, const Index& row2, const Index& num_cols );

   template< typename Matrix, typename Vector >
   static void
   orderMatrixAccordingTo( Matrix& M, const Vector& piv );

   template< typename Matrix, typename Vector >
   static void
   undoOrderInMatrixAccordingTo( Matrix& M, const Vector& piv );

   template< typename Index, typename MatrixView, typename VectorView, typename Real = typename MatrixView::RealType >
   static std::pair< Real, Index >
   pivotRowOfMatrix( const Index& j, MatrixView& M, const Index& num_rows, const Index& num_cols, VectorView& piv );

   template< typename Index, typename MatrixView, typename VectorView, typename Real = typename MatrixView::RealType >
   static std::pair< Real, Index >
   pivotRowOfMatrices( const Index& j,
                       MatrixView& A,
                       MatrixView& M,
                       const Index& num_rows,
                       const Index& num_cols,
                       VectorView& piv );

   template< typename Matrix >
   static Matrix
   packMatrices( const Matrix& L, const Matrix& U );

   template< typename Matrix >
   static std::tuple< Matrix, Matrix >
   getUnpackedMatrices( const Matrix& M, MatrixFormat format );

   template< typename Matrix >
   static void
   printMatrix( const Matrix& M );

   template< const int start = 1, typename Vector >
   static void
   setDefaultPivotingValues( Vector& piv );
};

}  // namespace Decomposition::LU::Decomposers

#include <Decomposition/LU/Decomposers/BaseDecomposer.hpp>
