#pragma once

#include <TNL/String.h>

#include <Decomposition/LU/Decomposers/BaseDecomposer.h>

namespace Decomposition::LU::Decomposers {

class CuSolverDnXgetrfWrapper : public BaseDecomposer
{
public:
   static constexpr MatrixFormat
   getMatrixDiagonalFormat()
   {
      return MatrixFormat::UnitDiagIn_L;
   }

   template< typename Device = TNL::Devices::Cuda >
   static TNL::String
   getDecomposerName( const bool& partialPivoting = false );

   template< typename Matrix >
   static void
   decompose( Matrix& LU );

   template< typename Matrix, typename Vector >
   static void
   decompose( Matrix& LU, Vector& piv );
};

}  // namespace Decomposition::LU::Decomposers

#include <Decomposition/LU/Decomposers/CuSolverDnXgetrfWrapper.hpp>
