#pragma once

#include <Decomposition/LU/Decomposers/CroutMethod.h>

#include <Decomposition/Exceptions/MatrixNotSquare.h>
#include <Decomposition/Exceptions/MatrixNotStronglyRegular.h>
#include <Decomposition/Exceptions/MatrixSingular.h>
#include <Decomposition/Exceptions/NotImplementedError.h>

namespace Decomposition::LU::Decomposers {

template< const int BLOCK_SIZE >
template< typename Device >
TNL::String
CroutMethod< BLOCK_SIZE >::getDecomposerName( const bool& partialPivoting )
{
   TNL::String decomposerName = "";

   // TODO: Remove partialPivoting once PCM without partial pivoting is implemented
   if( partialPivoting && isCuda< Device >() ) {
      decomposerName = "P";
   }

   decomposerName.append( "CM" );

   // TODO: Remove partialPivoting once PCM without partial pivoting is implemented
   if( partialPivoting && isCuda< Device >() ) {
      decomposerName.append( "_" + TNL::convertToString( BLOCK_SIZE ) );
   }

   decomposerName.append( partialPivoting ? " PP" : "" );

   return decomposerName;
}

// WARNING: 1s found on the main diagonal of U (Crout decomposition) are not stored in matrix LU.
// Matrix LU has elements of L on its main diagonal (where L comes from LU decomposition of matrix A).
template< const int BLOCK_SIZE >
template< typename Matrix >
void
CroutMethod< BLOCK_SIZE >::decompose( Matrix& LU )
{
   // TODO: Remove this check once #54 is implemented
   if constexpr( tnlObjectOnHost< Matrix >() ) {
      using Real = typename Matrix::RealType;
      using Index = typename Matrix::IndexType;

      const Index num_rows = LU.getRows();
      const Index num_cols = LU.getColumns();

      if( num_rows != num_cols )
         throw Exceptions::MatrixNotSquare( num_rows, num_cols );

      Index i;
      Index j;
      Index k;
      Real sum = 0;

      auto LU_view = LU.getView();

      for( j = 0; j < num_rows; ++j ) {
         for( i = j; i < num_rows; ++i ) {
            sum = 0;
            for( k = 0; k < j; ++k )
               sum = sum + LU_view( i, k ) * LU_view( k, j );

            LU_view( i, j ) = LU_view( i, j ) - sum;
         }

         if( LU_view( j, j ) == 0 ) {
            if( j != num_rows - 1 )
               throw Exceptions::MatrixNotStronglyRegular( "LU", j, j );
         }

         for( i = j + 1; i < num_rows; ++i ) {
            sum = 0;
            for( k = 0; k < j; ++k )
               sum = sum + LU_view( j, k ) * LU_view( k, i );

            LU_view( j, i ) = ( LU_view( j, i ) - sum ) / LU_view( j, j );
         }
      }
   }
   else {
      throw Exceptions::NotImplementedError( getDecomposerName< typename Matrix::DeviceType >()
                                             + " is implemented only for the CPU." );
   }
}

#ifdef __CUDACC__
template< typename MatrixView, typename Index >
__global__
void
ComputeColumn_kernel( MatrixView LU, const Index col, const Index num_rows )
{
   using Real = typename MatrixView::RealType;
   // Offset threads to start from the diagonal element (incl. it)
   const Index row = blockIdx.x * blockDim.x + threadIdx.x + col;

   if( row >= num_rows )
      return;

   Real sum = 0;
   for( Index k = 0; k < col; ++k ) {
      sum += LU( row, k ) * LU( k, col );
   }

   LU( row, col ) = LU( row, col ) - sum;
}
#endif

#ifdef __CUDACC__
template< typename MatrixView, typename Index >
__global__
void
ComputeRow_kernel( MatrixView LU, Index row, const Index num_cols )
{
   using Real = typename MatrixView::RealType;
   // Offset threads to start from element to the right of the diagonal element
   const Index col = blockIdx.x * blockDim.x + threadIdx.x + row + 1;

   if( col >= num_cols )
      return;

   Real sum = 0;
   for( Index k = 0; k < row; ++k ) {
      sum += LU( row, k ) * LU( k, col );
   }

   LU( row, col ) = ( LU( row, col ) - sum ) / LU( row, row );
}
#endif

// WARNING: 1s found on the main diagonal of U (Crout decomposition) are not stored in matrix LU.
// Matrix LU has elements of L on its main diagonal (where L comes from Crout LU decomposition of matrix A).
template< const int BLOCK_SIZE >
template< typename Matrix, typename Vector >
void
CroutMethod< BLOCK_SIZE >::decompose( Matrix& LU, Vector& piv )
{
   if constexpr( ! tnlObjectOnHost< Vector >() ) {
      throw Exceptions::NotImplementedError(
         getDecomposerName< TNL::Devices::Cuda >( ! piv.empty() )
         + " is implemented only for the CPU/GPU. However, the pivoting vector must be stored on the Host in both cases." );
   }
   else {
      if( piv.empty() ) {
         decompose( LU );
      }

      using Real = typename Matrix::RealType;
      using Index = typename Matrix::IndexType;

      const Index& num_rows = LU.getRows();
      const Index& num_cols = LU.getColumns();

      if( num_rows != num_cols ) {
         throw Exceptions::MatrixNotSquare( num_rows, num_cols );
      }

      const Real piv_tol = 1.0e-5;

      auto LU_view = LU.getView();
      auto piv_view = piv.getView();

      BaseDecomposer::setDefaultPivotingValues( piv );

      if constexpr( tnlObjectsOnHost< Matrix, Vector >() ) {
         Index i;
         Index j;
         Index k;
         Real sum;

         for( j = 0; j < num_cols; ++j ) {
            for( i = j; i < num_rows; ++i ) {
               sum = 0;
               for( k = 0; k < j; ++k )
                  sum = sum + LU_view( i, k ) * LU_view( k, j );

               LU_view( i, j ) = LU_view( i, j ) - sum;
            }

            if( TNL::abs( LU_view( j, j ) ) <= piv_tol ) {
               pivotRowOfMatrix_host( j, LU_view, num_rows, num_cols, piv_view );

               if( LU_view( j, j ) == 0 ) {
                  // Last element does not require pivoting as no elements are computed using it
                  //  - i.e. division by zero cannot occur for the last element
                  if( j != num_rows - 1 )
                     throw Exceptions::MatrixSingular( "LU", j, j );
               }
            }

            for( i = j + 1; i < num_rows; ++i ) {
               sum = 0;
               for( k = 0; k < j; ++k )
                  sum = sum + LU_view( j, k ) * LU_view( k, i );

               LU_view( j, i ) = ( LU_view( j, i ) - sum ) / LU_view( j, j );
            }
         }
      }

      if constexpr( tnlObjectOnCuda< Matrix >() && tnlObjectOnHost< Vector >() ) {
#ifdef __CUDACC__
         constexpr int threads_perBlock = BLOCK_SIZE * BLOCK_SIZE;
         // Round to nearest higher multiple of threads_perBlock - only if threads_perBlock is multiple of 2
         const Index num_cols_rounded = ( num_cols + threads_perBlock - 1 ) & -threads_perBlock;
         const Index block_perGrid = num_cols_rounded / threads_perBlock;

         for( Index diag = 0; diag < num_rows; ++diag ) {
            // clang-format off
            ComputeColumn_kernel<<< block_perGrid, threads_perBlock >>>( LU_view, diag, num_rows );
            // clang-format on

            if( TNL::abs( LU_view.getElement( diag, diag ) ) <= piv_tol ) {
               std::pair< Real, Index > max_elem = pivotRowOfMatrix_device( diag, LU_view, num_rows, num_cols, piv_view );

               if( max_elem.first == 0 ) {
                  // Last element does not require pivoting as no elements are computed using it
                  //  - i.e. division by zero cannot occur for the last element
                  if( diag != num_rows - 1 ) {
                     throw Exceptions::MatrixSingular( "LU", diag, diag );
                  }
               }
            }

            // clang-format off
            ComputeRow_kernel<<< block_perGrid, threads_perBlock >>>( LU_view, diag, num_cols );
            // clang-format on
         }
#endif
      }
   }
}

}  // namespace Decomposition::LU::Decomposers
