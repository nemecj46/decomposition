#pragma once

#include <iostream>

#include <TNL/Algorithms/reduce.h>
#include <TNL/Containers/Array.h>

#include <Decomposition/LU/Decomposers/IterativeCroutMethod.h>

#include <Decomposition/Exceptions/MatricesDimensionsMismatch.h>
#include <Decomposition/Exceptions/MatrixVectorRowsMismatch.h>
#include <Decomposition/Exceptions/MatrixNotSquare.h>
#include <Decomposition/Exceptions/MatrixNotStronglyRegular.h>
#include <Decomposition/Exceptions/MatrixSingular.h>
#include <Decomposition/Exceptions/NotANumber.h>
#include <Decomposition/Exceptions/NotImplementedError.h>

#include <Decomposition/utils/utils.h>

namespace Decomposition::LU::Decomposers {

template< const int BLOCK_SIZE >
template< typename Device >
TNL::String
IterativeCroutMethod< BLOCK_SIZE >::getDecomposerName( const bool& partialPivoting )
{
   TNL::String decomposerName = "ICM";

   if( isCuda< Device >() ) {
      decomposerName.append( "_" + TNL::convertToString( BLOCK_SIZE ) );
   }

   decomposerName.append( partialPivoting ? " PP" : "" );

   return decomposerName;
}

template< const int BLOCK_SIZE >
template< typename Matrix >
void
IterativeCroutMethod< BLOCK_SIZE >::decompose( Matrix& LU )
{
   Matrix A;
   A = LU;
   decompose( A, LU );
}

template< const int BLOCK_SIZE >
template< typename Matrix, typename Vector >
void
IterativeCroutMethod< BLOCK_SIZE >::decompose( Matrix& LU, Vector& piv )
{
   Matrix A;
   A = LU;
   decompose( A, LU, piv );
}

template< typename Matrix >
inline bool
matrixDifferenceWithinTolerance( const Matrix& LU, const Matrix& LUnext, const typename Matrix::RealType& process_tol = 0.001 )
{
   const auto& maxAbsDiffLU = max( abs( LUnext.getValues() - LU.getValues() ) );

#if DEBUG_DECOMP == true
   std::cout << "Max difference in elements of LU matrices: " << maxAbsDiffLU << std::endl;
#endif

   return maxAbsDiffLU <= process_tol;
}

#ifdef __CUDACC__
template< typename BoolArrayView >
inline void
checkDecomposition( BoolArrayView& failed )
{
   if( failed.getElement( 0 ) )
      throw Exceptions::MatrixNotStronglyRegular();
}
#endif

#ifdef __CUDACC__
void
synchronizeStreams( cudaStream_t* streams, const int numStreams )
{
   for( int i = 0; i < numStreams; i++ ) {
      cudaStreamSynchronize( streams[ i ] );
   }
}
#endif

#ifdef __CUDACC__
template< const int BLOCK_SIZE,
          typename ConstMatrixView,
          typename MatrixView,
          typename BoolArrayView,
          typename Index,
          typename Real >
__global__
void
Compute_kernel( const ConstMatrixView A,
                MatrixView LU,
                MatrixView LUnext,
                const Index sec_start_col,
                const Index sec_end_col,
                const Index sec_start_row,
                const Index sec_end_row,
                const Real process_tol,
                BoolArrayView processed,
                BoolArrayView failed )
{
   Index row = blockIdx.y * blockDim.y + threadIdx.y + sec_start_row;
   Index col = blockIdx.x * blockDim.x + threadIdx.x + sec_start_col;

   Index tx = threadIdx.x;
   Index ty = threadIdx.y;

   Real sum = 0;

   Index max_col = sec_end_col - 1;
   Index max_row = sec_end_row - 1;
   Index row_adj = min( row, max_row );
   Index col_adj = min( col, max_col );

   Index min_row_col = min( row_adj, col_adj ) - BLOCK_SIZE;

   __shared__ Real LUsLower[ BLOCK_SIZE ][ BLOCK_SIZE ];
   __shared__ Real LUsUpper[ BLOCK_SIZE ][ BLOCK_SIZE ];

   Index i, k;

   for( i = 0; i <= min_row_col; i += BLOCK_SIZE ) {
      LUsLower[ ty ][ tx ] = LU( row_adj, i + tx );
      LUsUpper[ ty ][ tx ] = LU( i + ty, col_adj );

      __syncthreads();

      #pragma unroll( BLOCK_SIZE )
      for( k = 0; k < BLOCK_SIZE; ++k )
         sum += LUsLower[ ty ][ k ] * LUsUpper[ k ][ tx ];
      __syncthreads();
   }

   LUsLower[ ty ][ tx ] = LU( row_adj, min( i + tx, max_col ) );
   LUsUpper[ ty ][ tx ] = LU( min( i + ty, max_row ), col_adj );

   __syncthreads();

   Index t_to_use = row >= col ? tx : ty;

   for( k = 0; k < t_to_use; ++k )
      sum += LUsLower[ ty ][ k ] * LUsUpper[ k ][ tx ];

   if( row >= sec_end_row || col >= sec_end_col )
      return;

   if( row >= col )
      LUnext( row, col ) = A( row, col ) - sum;
   else {
      if( LU( row, row ) == 0 ) {
   #if DEBUG_DECOMP == true
         printf( "LU( %d, %d ) = 0. Cannot divide by 0.\n", (int) row, (int) row );
   #endif
         failed( 0 ) = true;
      }
      LUnext( row, col ) = ( A( row, col ) - sum ) / LU( row, row );
   }

   // Check convergence
   if( abs( LUnext( row, col ) - LU( row, col ) ) > process_tol )
      processed( 0 ) = false;

   // Assign matrix for this iteration
   LU( row, col ) = LUnext( row, col );
}
#endif

template< const int BLOCK_SIZE >
template< typename Matrix >
void
IterativeCroutMethod< BLOCK_SIZE >::decompose( Matrix& A, Matrix& LU )
{
   using Real = typename Matrix::RealType;
   using Index = typename Matrix::IndexType;

   const Index& num_rows = LU.getRows();
   const Index& num_cols = LU.getColumns();

   if( num_rows != num_cols ) {
      throw Exceptions::MatrixNotSquare( num_rows, num_cols );
   }

   if( A.getRows() != num_rows || A.getColumns() != num_cols ) {
      throw Exceptions::MatricesDimensionsMismatch( A, LU, "A", "LU" );
   }

   Matrix LUnext;
   LUnext.setLike( LU );

   auto A_view = A.getConstView();
   auto LU_view = LU.getView();
   auto LUnext_view = LUnext.getView();

   const Real process_tol = 0.0;

   if constexpr( tnlObjectOnHost< Matrix >() ) {
      bool processed = true;

      Index i;
      Index j;
      Index k;
      Real sum = 0;

      do {
         for( i = 0; i < num_rows; ++i ) {
            for( j = 0; j < num_cols; ++j ) {
               if( i >= j ) {
                  sum = 0;
                  for( k = 0; k < j; ++k )
                     sum = sum + LU_view( i, k ) * LU_view( k, j );

                  LUnext_view( i, j ) = A_view( i, j ) - sum;
               }
               else {
                  if( LU_view( i, i ) == 0 )
                     throw Exceptions::MatrixNotStronglyRegular( "LU", i, i );
                  sum = 0;
                  for( k = 0; k < i; ++k )
                     sum = sum + LU_view( i, k ) * LU_view( k, j );

                  LUnext_view( i, j ) = ( A_view( i, j ) - sum ) / LU_view( i, i );
               }
            }
         }
         processed = matrixDifferenceWithinTolerance( LU, LUnext, process_tol );

         LU = LUnext;
      } while( ! processed );
   }

   if constexpr( tnlObjectOnCuda< Matrix >() ) {
#ifdef __CUDACC__
      Index sec_size = min( max( num_cols / 10, (Index) 256 ), (Index) 1024 );
      sec_size = ( sec_size + BLOCK_SIZE - 1 ) / BLOCK_SIZE * BLOCK_SIZE;

      Index blocks = sec_size / BLOCK_SIZE;
      dim3 threads_perBlock( BLOCK_SIZE, BLOCK_SIZE );
      dim3 block_perGrid( blocks, blocks );

   #if DECOMP_METRICS_ENABLED == true
      const Index nonDiagSecs_perRow = TNL::ceil( (double) num_cols / (double) sec_size ) - 1;
      TNL::Matrices::DenseMatrix< Index, TNL::Devices::Host, Index > iterations_perSection( nonDiagSecs_perRow + 1,
                                                                                            nonDiagSecs_perRow + 1 );
      iterations_perSection.setValue( 0 );
      TNL::Containers::Array< Index, TNL::Devices::Host, Index > pivots_perSection( nonDiagSecs_perRow + 1 );
      pivots_perSection.setValue( 0 );
      Index dSec_id = 0;
   #endif

      // Allocate and initialize an array of stream handles
      int nstreams = 2;
      auto* streams = (cudaStream_t*) malloc( nstreams * sizeof( cudaStream_t ) );
      for( int i = 0; i < nstreams; ++i )
         cudaStreamCreate( &( streams[ i ] ) );

      // Flag to indicate that a matrix failed to decompose on the GPU
      TNL::Containers::Array< bool, TNL::Devices::Cuda > failed{ 1, false };
      // Flag to indicate that a section has been processed
      TNL::Containers::Array< bool, TNL::Devices::Cuda > processed{ 1, false };

      auto processed_view = processed.getView();
      auto failed_view = failed.getView();

      Index dSec_start;
      Index dSec_end;
      Index sec_start;
      Index sec_end;

      for( dSec_start = 0, dSec_end = min( num_cols, sec_size ); dSec_start < dSec_end;
           dSec_start += sec_size, dSec_end = min( num_cols, dSec_end + sec_size ) )
      {
         // Converge the diagonal section first - Default stream
         do {
            processed.setElement( 0, true );

            // clang-format off
            Compute_kernel< BLOCK_SIZE ><<< block_perGrid, threads_perBlock >>>(
               A_view, LU_view, LUnext_view, dSec_start, dSec_end, dSec_start, dSec_end, process_tol, processed_view, failed_view );
            // clang-format on

   #if DECOMP_METRICS_ENABLED == true
            iterations_perSection.addElement( dSec_id, dSec_id, 1 );
   #endif
         } while( ! processed.getElement( 0 ) );

         checkDecomposition( failed );

   #if DECOMP_METRICS_ENABLED == true
         Index sec_id = 0;
   #endif

         for( sec_start = dSec_end, sec_end = min( num_cols, dSec_end + sec_size ); sec_start < sec_end;
              sec_start += sec_size, sec_end = min( num_cols, sec_end + sec_size ) )
         {
            do {
               processed.setElement( 0, true );

               // Run Stream 1: iterate columns - rows should stay the same
               // clang-format off
               Compute_kernel< BLOCK_SIZE ><<< block_perGrid, threads_perBlock, 0, streams[ 0 ] >>>(
                  A_view, LU_view, LUnext_view, sec_start, sec_end, dSec_start, dSec_end, process_tol, processed_view, failed_view );
               // clang-format on

               // Run Stream 2: iterate rows - columns should stay the same
               // clang-format off
               Compute_kernel< BLOCK_SIZE ><<< block_perGrid, threads_perBlock, 0, streams[ 1 ] >>>(
                  A_view, LU_view, LUnext_view, dSec_start, dSec_end, sec_start, sec_end, process_tol, processed_view, failed_view );
               // clang-format on

   #if DECOMP_METRICS_ENABLED == true
               iterations_perSection.addElement( dSec_id + sec_id + 1, dSec_id, 1 );
               iterations_perSection.addElement( dSec_id, dSec_id + sec_id + 1, 1 );
   #endif

               synchronizeStreams( streams, nstreams );
            } while( ! processed.getElement( 0 ) );
            checkDecomposition( failed );
   #if DECOMP_METRICS_ENABLED == true
            sec_id++;
   #endif
         }
   #if DECOMP_METRICS_ENABLED == true
         dSec_id++;
   #endif
      }

      // Release resources
      for( int i = 0; i < nstreams; ++i )
         cudaStreamDestroy( streams[ i ] );

      free( streams );

   #if DECOMP_METRICS_ENABLED == true
      std::cout << "========== Iterative Metrics ==========" << std::endl;
      std::cout << "Iterations per section = \n" << iterations_perSection << std::endl;

      Index total_iters = 0;
      auto f = [ =, &total_iters ]( Index rowIdx, Index columnIdx, Index columnIdx_, Index& value )
      {
         total_iters += value;
      };
      iterations_perSection.forAllElements( f );

      std::cout << "Total iterations = " << total_iters << std::endl;
      std::cout << "==================================================" << std::endl;
   #endif
#endif
   }
}

///////////////////////////////////////////////////
// PIVOTING
///////////////////////////////////////////////////

#ifdef __CUDACC__
template< const int BLOCK_SIZE,
          typename ConstMatrixView,
          typename MatrixView,
          typename BoolArrayView,
          typename Index,
          typename Real >
__global__
void
DSecCompute_kernel( const ConstMatrixView A,
                    MatrixView LU,
                    MatrixView LUnext,
                    const Index sec_start_col,
                    const Index sec_end_col,
                    const Index sec_start_row,
                    const Index sec_end_row,
                    const Real process_tol,
                    BoolArrayView processed,
                    const Index bad_rowcol )
{
   // Start of block
   Index row = blockIdx.y * blockDim.y + sec_start_row;
   Index col = blockIdx.x * blockDim.x + sec_start_col;

   // Terminate threads that are part of a block beginning after the bad element
   // -> They compute values that are not saved -> waste of resources
   if( row > bad_rowcol && col > bad_rowcol ) {
      return;
   }

   Index ty = threadIdx.y;
   Index tx = threadIdx.x;

   // 1 thread per element
   row += ty;
   col += tx;

   Index max_col = sec_end_col - 1;
   Index max_row = sec_end_row - 1;
   Index row_adj = min( row, max_row );
   Index col_adj = min( col, max_col );

   Index min_row_col = min( row_adj, col_adj ) - BLOCK_SIZE;

   __shared__ Real LUsLower[ BLOCK_SIZE ][ BLOCK_SIZE ];
   __shared__ Real LUsUpper[ BLOCK_SIZE ][ BLOCK_SIZE ];

   Index i, k;
   Real sum = 0;

   for( i = 0; i <= min_row_col; i += BLOCK_SIZE ) {
      LUsLower[ ty ][ tx ] = LU( row_adj, i + tx );
      LUsUpper[ ty ][ tx ] = LU( i + ty, col_adj );

      __syncthreads();

      #pragma unroll( BLOCK_SIZE )
      for( k = 0; k < BLOCK_SIZE; ++k )
         sum += LUsLower[ ty ][ k ] * LUsUpper[ k ][ tx ];
      __syncthreads();
   }

   LUsLower[ ty ][ tx ] = LU( row_adj, min( i + tx, max_col ) );
   LUsUpper[ ty ][ tx ] = LU( min( i + ty, max_row ), col_adj );

   __syncthreads();

   // Terminate threads that reach out of bounds as they have served their purpose of reading data
   if( row >= sec_end_row || col >= sec_end_col )
      return;

   // If this thread would write past the bad element
   //    -> useless to write
   //    -> bad quality element
   //    -> save resources && avoid possible divergence if bad quality elements lead to it
   if( row >= bad_rowcol && col > bad_rowcol )
      return;

   // Read LU( row, col ) from shared memory instead of global memory
   Real LU_rowCol;
   // Index to stop at depending on the element location
   Index t_to_use;

   if( row >= col ) {
      t_to_use = tx;
      LU_rowCol = LUsLower[ ty ][ tx ];
   }
   else {
      t_to_use = ty;
      LU_rowCol = LUsUpper[ ty ][ tx ];
   }

   for( k = 0; k < t_to_use; ++k )
      sum += LUsLower[ ty ][ k ] * LUsUpper[ k ][ tx ];

   sum = A( row, col ) - sum;

   if( row < col ) {
      // Check if we are not dividing by zero
      Real divisor = LU( row, row );
      if( ! abs( divisor ) == 0 ) {
         sum /= divisor;
      }
      else {  // Element used for division is 0 - SHOULD NEVER HAPPEN
         printf( "-!> DIVISION BY ZERO\n" );
      }
   }

   // Check convergence
   if( abs( LU_rowCol - sum ) > process_tol )
      processed( 0 ) = false;

   LUnext( row, col ) = sum;
}
#endif

#ifdef __CUDACC__
template< const int BLOCK_SIZE,
          typename ConstMatrixView,
          typename MatrixView,
          typename BoolArrayView,
          typename Index,
          typename Real >
__global__
void
LSecCompute_kernel( const ConstMatrixView A,
                    MatrixView LU,
                    MatrixView LUnext,
                    const Index sec_start_col,
                    const Index sec_end_col,
                    const Index sec_start_row,
                    const Index sec_end_row,
                    const Real process_tol,
                    BoolArrayView processed,
                    const Index sectionId )
{
   Index ty = threadIdx.y;
   Index tx = threadIdx.x;

   Index row = blockIdx.y * blockDim.y + ty + sec_start_row;
   Index col = blockIdx.x * blockDim.x + tx + sec_start_col;

   Index max_col = sec_end_col - 1;
   Index max_row = sec_end_row - 1;
   Index row_adj = min( row, max_row );
   Index col_adj = min( col, max_col );

   // In a lower section row > col always
   Index min_row_col = col_adj - BLOCK_SIZE;

   __shared__ Real LUsLower[ BLOCK_SIZE ][ BLOCK_SIZE ];
   __shared__ Real LUsUpper[ BLOCK_SIZE ][ BLOCK_SIZE ];

   Index i, k;
   Real sum = 0;

   for( i = 0; i <= min_row_col; i += BLOCK_SIZE ) {
      LUsLower[ ty ][ tx ] = LU( row_adj, i + tx );
      LUsUpper[ ty ][ tx ] = LU( i + ty, col_adj );

      __syncthreads();

      #pragma unroll( BLOCK_SIZE )
      for( k = 0; k < BLOCK_SIZE; ++k )
         sum += LUsLower[ ty ][ k ] * LUsUpper[ k ][ tx ];
      __syncthreads();
   }

   LUsLower[ ty ][ tx ] = LU( row_adj, min( i + tx, max_col ) );
   LUsUpper[ ty ][ tx ] = LU( min( i + ty, max_row ), col_adj );

   __syncthreads();

   // Terminate threads that reach out of bounds as they have served their purpose of reading data
   if( row >= sec_end_row || col >= sec_end_col )
      return;

   // row > col => use tx
   for( k = 0; k < tx; ++k )
      sum += LUsLower[ ty ][ k ] * LUsUpper[ k ][ tx ];

   // "row > col" is always true for sections below the diagonal section
   sum = A( row, col ) - sum;

   // Check convergence
   // Read LU( row, col ) from shared memory instead of global memory
   // - row >= col => read from LUsLower
   if( abs( LUsLower[ ty ][ tx ] - sum ) > process_tol )
      processed( sectionId ) = false;

   LUnext( row, col ) = sum;
}
#endif

#ifdef __CUDACC__
// Decomposer assumes that NO bad elements are present in the diagonal section in the same row range
template< const int BLOCK_SIZE,
          typename ConstMatrixView,
          typename MatrixView,
          typename BoolArrayView,
          typename Index,
          typename Real >
__global__
void
RSecCompute_kernel( const ConstMatrixView A,
                    MatrixView LU,
                    MatrixView LUnext,
                    const Index sec_start_col,
                    const Index sec_end_col,
                    const Index sec_start_row,
                    const Index sec_end_row,
                    const Real process_tol,
                    BoolArrayView processed,
                    const Index sectionId )
{
   Index row = blockIdx.y * blockDim.y + threadIdx.y + sec_start_row;
   Index col = blockIdx.x * blockDim.x + threadIdx.x + sec_start_col;

   Index tx = threadIdx.x;
   Index ty = threadIdx.y;

   Real sum = 0;

   Index max_col = sec_end_col - 1;
   Index max_row = sec_end_row - 1;
   Index row_adj = min( row, max_row );
   Index col_adj = min( col, max_col );

   // In a right section row < col always
   Index min_row_col = row_adj - BLOCK_SIZE;

   __shared__ Real LUsLower[ BLOCK_SIZE ][ BLOCK_SIZE ];
   __shared__ Real LUsUpper[ BLOCK_SIZE ][ BLOCK_SIZE ];

   Index i, k;

   for( i = 0; i <= min_row_col; i += BLOCK_SIZE ) {
      LUsLower[ ty ][ tx ] = LU( row_adj, i + tx );
      LUsUpper[ ty ][ tx ] = LU( i + ty, col_adj );

      __syncthreads();

      #pragma unroll( BLOCK_SIZE )
      for( k = 0; k < BLOCK_SIZE; ++k )
         sum += LUsLower[ ty ][ k ] * LUsUpper[ k ][ tx ];
      __syncthreads();
   }

   LUsLower[ ty ][ tx ] = LU( row_adj, min( i + tx, max_col ) );
   LUsUpper[ ty ][ tx ] = LU( min( i + ty, max_row ), col_adj );

   __syncthreads();

   // Terminate threads that reach out of bounds as they have served their purpose of reading data
   if( row >= sec_end_row || col >= sec_end_col )
      return;

   // row < col => use ty
   for( k = 0; k < ty; ++k )
      sum += LUsLower[ ty ][ k ] * LUsUpper[ k ][ tx ];

   // "row < col" is always true for sections to the right of the diagonal section
   sum = ( A( row, col ) - sum ) / LU( row, row );

   // Check convergence
   // Read LU( row, col ) from shared memory instead of global memory
   // - row < col => read from LUsUpper
   if( abs( LUsUpper[ ty ][ tx ] - sum ) > process_tol )
      processed( sectionId ) = false;

   LUnext( row, col ) = sum;
}
#endif

#ifdef __CUDACC__
template< typename MatrixView, typename Index >
__global__
void
DSecAssign_kernel( MatrixView LU,
                   MatrixView LUnext,
                   const Index sec_start_col,
                   const Index sec_end_col,
                   const Index sec_start_row,
                   const Index sec_end_row,
                   const Index badElementIndex )
{
   Index row = blockIdx.y * blockDim.y + threadIdx.y + sec_start_row;
   Index col = blockIdx.x * blockDim.x + threadIdx.x + sec_start_col;

   if( row >= sec_end_row || col >= sec_end_col )
      return;

   if( row >= badElementIndex && col > badElementIndex )
      return;

   LU( row, col ) = LUnext( row, col );
}
#endif

#ifdef __CUDACC__
template< typename MatrixView, typename Index >
__global__
void
NonDiagSecAssign_kernel( MatrixView LU,
                         MatrixView LUnext,
                         const Index sec_start_col,
                         const Index sec_end_col,
                         const Index sec_start_row,
                         const Index sec_end_row )
{
   Index row = blockIdx.y * blockDim.y + threadIdx.y + sec_start_row;
   Index col = blockIdx.x * blockDim.x + threadIdx.x + sec_start_col;

   if( row >= sec_end_row || col >= sec_end_col )
      return;

   LU( row, col ) = LUnext( row, col );
}
#endif

#ifdef __CUDACC__
template< const int BLOCK_SIZE, typename MatrixView, typename VectorView, typename Index, typename Real >
void
pivotBadElement( MatrixView& A,
                 MatrixView& LU,
                 VectorView& piv,
                 const Index& j,
                 const Index& num_rows,
                 const Index& num_cols,
                 Index& badEl_searchStart,
                 const Real& piv_tol_lower,
                 const Real& piv_tol_upper )
{
   // The last element of the matrix does not require pivoting as no elements are computed using it, i.e., division by zero
   // cannot occur for the last element
   if( j >= num_cols - 1 ) {
      return;
   }

   std::pair< Real, Index > max_elem =
      pivotRowOfMatrices_device< BLOCK_SIZE * BLOCK_SIZE >( j, A, LU, num_rows, num_cols, piv );

   if( max_elem.first == 0 ) {
      // The current bad element in the main diagonal is zero even after pivoting -> Decomposition failed
      throw Exceptions::MatrixSingular( "LU", j, j );
   }
   else if( isnan( max_elem.first ) ) {
      // TODO: Test this
      throw Exceptions::NotANumber( j, j );
   }
   else if( max_elem.first <= piv_tol_lower || piv_tol_upper < max_elem.first ) {
      // The value at the index of the bad element is not within the allowed range, however, it is not zero/nan, so the
      // computation can continue -> Search for the next bad element after this one
      badEl_searchStart = j + 1;
   }
}
#endif

template< const int BLOCK_SIZE >
template< typename Matrix, typename Vector >
void
IterativeCroutMethod< BLOCK_SIZE >::decompose( Matrix& A, Matrix& LU, Vector& piv )
{
   // Decompose without pivoting if piv is empty
   if( piv.empty() ) {
      return decompose( A, LU );
   }

   if constexpr( ! ( tnlObjectOnCuda< Matrix >() && tnlObjectOnHost< Vector >() ) ) {
      throw Exceptions::NotImplementedError(
         getDecomposerName< TNL::Devices::Cuda >( ! piv.empty() )
         + " is implemented only for the GPU. The matrix must be stored on the Device and the pivoting vector on the Host." );
   }
   else {
#ifdef __CUDACC__
      using Real = typename Matrix::RealType;
      using Index = typename Matrix::IndexType;

      const Index num_rows = LU.getRows();
      const Index num_cols = LU.getColumns();

      if( num_rows != num_cols ) {
         throw Exceptions::MatrixNotSquare( num_rows, num_cols );
      }

      if( A.getRows() != num_rows || A.getColumns() != num_cols ) {
         throw Exceptions::MatricesDimensionsMismatch( A, LU, "A", "LU" );
      }

      if( piv.getSize() != num_rows ) {
         throw Exceptions::MatrixVectorRowsMismatch( A, piv, "A", "piv" );
      }

      Matrix LUnext;
      LUnext.setLike( LU );

      const Real process_tol = 0.0;
      const Real piv_tol_lower = 1.0e-5;
      const Real piv_tol_upper = 1.0e+5;

      Index sec_size = min( max( num_cols / 10, (Index) 256 ), (Index) 1024 );
      sec_size = ( sec_size + BLOCK_SIZE - 1 ) / BLOCK_SIZE * BLOCK_SIZE;

      Index blocks = sec_size / BLOCK_SIZE;
      dim3 threads_perBlock( BLOCK_SIZE, BLOCK_SIZE );
      dim3 block_perGrid( blocks, blocks );

      // Flag to indicate that a section has been processed
      TNL::Containers::Array< bool, TNL::Devices::Cuda > processed{ 1, false };

      BaseDecomposer::setDefaultPivotingValues( piv );

      // ceil( num_cols / sec_size ) gives us the number of sections in row (incl. diag section -> '- 1' to get num.
      // non-diag sections)
      const Index nonDiagSecs_perRow = TNL::ceil( (double) num_cols / (double) sec_size ) - 1;

   #if DECOMP_METRICS_ENABLED == true
      TNL::Matrices::DenseMatrix< Index, TNL::Devices::Host, Index > iterations_perSection( nonDiagSecs_perRow + 1,
                                                                                            nonDiagSecs_perRow + 1 );
      iterations_perSection.setValue( 0 );
      TNL::Containers::Array< Index, TNL::Devices::Host, Index > pivots_perSection( nonDiagSecs_perRow + 1 );
      pivots_perSection.setValue( 0 );
      Index dSec_id = 0;
   #endif

      // Allocate and initialize an array of stream handles
      auto* streams = (cudaStream_t*) malloc( nonDiagSecs_perRow * sizeof( cudaStream_t ) );
      for( Index i = 0; i < nonDiagSecs_perRow; ++i ) {
         cudaStreamCreate( &( streams[ i ] ) );
      }

      // Allocate `processed` flags for each non-diagonal section
      TNL::Containers::Array< bool, TNL::Devices::Cuda, Index > nonDiagSec_processed{ nonDiagSecs_perRow, false };
      TNL::Containers::Array< bool, TNL::Devices::Host, Index > nonDiagSec_processed_host{ nonDiagSecs_perRow, false };

      // Use views in the computation
      auto A_view = A.getView();
      auto LU_view = LU.getView();
      auto LUnext_view = LUnext.getView();
      auto piv_view = piv.getView();
      auto processed_view = processed.getView();
      auto nonDiagSec_processed_view = nonDiagSec_processed.getView();

      auto get_badEl_colIdxs = [ = ] __cuda_callable__( Index col ) -> Index
      {
         return ( abs( LU_view( col, col ) ) <= piv_tol_lower || piv_tol_upper < abs( LU_view( col, col ) ) ) ? col : num_cols;
      };

      // Diagonal section start and end
      Index dSec_start, dSec_end;
      // Non-diagonal section start, end and id
      Index sec_start, sec_end, sec_id;
      Index badEl_idx;

      // Loop through the diagonal sections
      for( dSec_start = 0, dSec_end = min( num_cols, sec_size ); dSec_start < dSec_end;
           dSec_start += sec_size, dSec_end = min( num_cols, dSec_end + sec_size ) )
      {
         const Index lastElemIn_dSec_idx = dSec_end - 1;
         // Set the starting point of the search for the first bad element
         Index badEl_searchStart = dSec_start;

         do {  // Process the diagonal section and the sections below it
            // Get next badEl_idx
            badEl_idx = TNL::Algorithms::reduce< TNL::Devices::Cuda >(
               badEl_searchStart, dSec_end, get_badEl_colIdxs, TNL::Min{}, num_cols );

            do {  // Process the diagonal section
               // Reset the processed flag
               processed_view.setElement( 0, true );

               // Compute the values up to and including the bad element
               // clang-format off
               DSecCompute_kernel< BLOCK_SIZE ><<< block_perGrid, threads_perBlock >>>(
                  A_view, LU_view, LUnext_view, dSec_start, dSec_end, dSec_start, dSec_end,
                  process_tol, processed_view, badEl_idx );

               // Assign the values computed for the next iteration
               DSecAssign_kernel<<< block_perGrid, threads_perBlock >>>(
                  LU_view, LUnext_view, dSec_start, dSec_end, dSec_start, dSec_end, badEl_idx );
               // clang-format on

               // Check if a bad element is present in a different column
               badEl_idx = TNL::Algorithms::reduce< TNL::Devices::Cuda >(
                  badEl_searchStart, dSec_end, get_badEl_colIdxs, TNL::Min{}, num_cols );

   #if DECOMP_METRICS_ENABLED == true
               iterations_perSection.addElement( dSec_id, dSec_id, 1 );
   #endif
            } while( ! processed_view.getElement( 0 ) );

            // The Diagonal section contains processed values excluding the values to the bottom-right of the bad element
            // Compute the lower sections up to and including the column with the bad element

            // Limit the number of threads used based on the size of the sections
            Index badEl_idx_cutoff = min( badEl_idx + 1, dSec_end );
            Index lSec_width = badEl_idx_cutoff - dSec_start;
            lSec_width = ( lSec_width + BLOCK_SIZE - 1 ) & -BLOCK_SIZE;
            Index lSec_gridWidth = TNL::max( lSec_width / BLOCK_SIZE, (Index) 1 );
            dim3 lSec_blockPerGrid( lSec_gridWidth, blocks );

            // Default to false so that all kernels are run in the first iteration
            nonDiagSec_processed_view.setValue( false );

            do {  // Process the lower sections in parallel - only compute sections that are not yet processed
               nonDiagSec_processed_host = nonDiagSec_processed;
               nonDiagSec_processed_view.setValue( true );

               for( sec_start = dSec_end, sec_end = min( num_cols, dSec_end + sec_size ), sec_id = 0; sec_start < sec_end;
                    sec_start += sec_size, sec_end = min( num_cols, sec_end + sec_size ), ++sec_id )
               {
                  if( ! nonDiagSec_processed_host( sec_id ) ) {
                     // Iterate rows; columns should stay the same
                     // clang-format off
                     LSecCompute_kernel< BLOCK_SIZE >
                        <<< lSec_blockPerGrid, threads_perBlock, 0, streams[ sec_id ] >>>(
                           A_view, LU_view, LUnext_view, dSec_start, badEl_idx_cutoff, sec_start,
                           sec_end, process_tol, nonDiagSec_processed_view, sec_id );

                     NonDiagSecAssign_kernel
                        <<< lSec_blockPerGrid, threads_perBlock, 0, streams[ sec_id ] >>>(
                           LU_view, LUnext_view, dSec_start, badEl_idx_cutoff, sec_start, sec_end );
                     // clang-format on
   #if DECOMP_METRICS_ENABLED == true
                     iterations_perSection.addElement( dSec_id + sec_id + 1, dSec_id, 1 );
   #endif
                  }
               }
               synchronizeStreams( streams, nonDiagSecs_perRow );
            } while( ! TNL::Algorithms::reduce( nonDiagSec_processed_view, TNL::LogicalAnd{} ) );

            // Pivot the bad element
            pivotBadElement< BLOCK_SIZE >(
               A_view, LU_view, piv_view, badEl_idx, num_rows, num_cols, badEl_searchStart, piv_tol_lower, piv_tol_upper );
   #if DECOMP_METRICS_ENABLED == true
            if( badEl_idx < num_cols )
               pivots_perSection( dSec_id ) += 1;
   #endif
         } while( badEl_idx < lastElemIn_dSec_idx );

         nonDiagSec_processed_view.setValue( false );

         // Process sections to the right of the diagonal section in parallel - only compute sections that are not yet processed
         // At this point there are no bad elements in the diagonal section
         do {
            nonDiagSec_processed_host = nonDiagSec_processed;
            nonDiagSec_processed_view.setValue( true );

            for( sec_start = dSec_end, sec_end = min( num_cols, dSec_end + sec_size ), sec_id = 0; sec_start < sec_end;
                 sec_start += sec_size, sec_end = min( num_cols, sec_end + sec_size ), ++sec_id )
            {
               if( ! nonDiagSec_processed_host( sec_id ) ) {
                  // clang-format off
                  RSecCompute_kernel< BLOCK_SIZE >
                     <<< block_perGrid, threads_perBlock, 0, streams[ sec_id ] >>>(
                        A_view, LU_view, LUnext_view, sec_start, sec_end, dSec_start, dSec_end,
                        process_tol, nonDiagSec_processed_view, sec_id );

                  NonDiagSecAssign_kernel
                     <<< block_perGrid, threads_perBlock, 0, streams[ sec_id ] >>>(
                        LU_view, LUnext_view, sec_start, sec_end, dSec_start, dSec_end );
                  // clang-format on
   #if DECOMP_METRICS_ENABLED == true
                  iterations_perSection.addElement( dSec_id, dSec_id + sec_id + 1, 1 );
   #endif
               }
            }
            synchronizeStreams( streams, nonDiagSecs_perRow );
         } while( ! TNL::Algorithms::reduce( nonDiagSec_processed_view, TNL::LogicalAnd{} ) );
   #if DECOMP_METRICS_ENABLED == true
         dSec_id++;
   #endif
      }

      // Release resources
      for( Index i = 0; i < nonDiagSecs_perRow; ++i ) {
         cudaStreamDestroy( streams[ i ] );
      }

      free( streams );

   #if DECOMP_METRICS_ENABLED == true
      std::cout << "========== Iterative & Pivoting Metrics ==========" << std::endl;
      std::cout << "Iterations per section = \n" << iterations_perSection << std::endl;

      Index total_iters = 0;
      auto f = [ =, &total_iters ]( Index rowIdx, Index columnIdx, Index columnIdx_, Index& value )
      {
         total_iters += value;
      };
      iterations_perSection.forAllElements( f );

      std::cout << "Total iterations = " << total_iters << std::endl;
      std::cout << "Pivots per section = " << pivots_perSection << std::endl;

      Index total_pivots = 0;
      pivots_perSection.forAllElements(
         [ &total_pivots ]( Index i, Index& value )
         {
            total_pivots += value;
         } );

      std::cout << "Total pivots = " << total_pivots << std::endl;
      std::cout << "==================================================" << std::endl;
   #endif
#endif
   }
}

}  // namespace Decomposition::LU::Decomposers
