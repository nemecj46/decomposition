#pragma once

#include <Decomposition/LU/Decomposers/CuSolverDnXgetrfWrapper.h>

#include <Decomposition/Exceptions/MatrixNotSquare.h>
#include <Decomposition/Exceptions/NotImplementedError.h>

#ifdef __CUDACC__
   #include <cusolverDn.h>
   #include <Decomposition/utils/CuLibsUtils.h>
#endif

#include <Decomposition/utils/utils.h>

namespace Decomposition::LU::Decomposers {

template< typename Device >
TNL::String
CuSolverDnXgetrfWrapper::getDecomposerName( const bool& partialPivoting )
{
   return partialPivoting ? "CuSolverDnXgetrfWrapper PP" : "CuSolverDnXgetrfWrapper";
}

// Taken from: https://github.com/NVIDIA/CUDALibrarySamples
template< typename Matrix >
void
CuSolverDnXgetrfWrapper::decompose( Matrix& LU )
{
   if constexpr( ! tnlObjectOnCuda< Matrix >() ) {
      throw Exceptions::NotImplementedError( getDecomposerName< typename Matrix::DeviceType >( false )
                                             + " is implemented only for the GPU." );
   }
   else {
#ifdef __CUDACC__
      using Real = typename Matrix::RealType;

      const int64_t num_rows = LU.getRows();
      const int64_t num_cols = LU.getColumns();

      if( num_rows != num_cols ) {
         throw Exceptions::MatrixNotSquare( num_rows, num_cols );
      }

      int info = 0;
      int* d_info = nullptr; /* error info */

      size_t d_lwork = 0;     /* size of workspace */
      void* d_work = nullptr; /* device workspace for getrf */
      size_t h_lwork = 0;     /* size of workspace */
      void* h_work = nullptr; /* host workspace for getrf */

      cusolverDnHandle_t cusolverH = NULL;
      cudaStream_t stream = NULL;

      CUSOLVER_CHECK( cusolverDnCreate( &cusolverH ) );

      CUDA_CHECK( cudaStreamCreateWithFlags( &stream, cudaStreamNonBlocking ) );
      CUSOLVER_CHECK( cusolverDnSetStream( cusolverH, stream ) );

      cusolverDnParams_t params;
      CUSOLVER_CHECK( cusolverDnCreateParams( &params ) );

      // Using new algo: CUSOLVER_ALG_0 (CUSOLVER_ALG_1 is legacy)
      CUSOLVER_CHECK( cusolverDnSetAdvOptions( params, CUSOLVERDN_GETRF, CUSOLVER_ALG_0 ) );

      CUDA_CHECK( cudaMalloc( reinterpret_cast< void** >( &d_info ), sizeof( int ) ) );

      CUSOLVER_CHECK( cusolverDnXgetrf_bufferSize( cusolverH,
                                                   params,
                                                   num_rows,
                                                   num_cols,
                                                   traits< Real >::cuda_data_type,
                                                   LU.getValues().getData(),
                                                   num_rows,
                                                   traits< Real >::cuda_data_type,
                                                   &d_lwork,
                                                   &h_lwork ) );

      CUDA_CHECK( cudaMalloc( reinterpret_cast< void** >( &d_work ), sizeof( Real ) * d_lwork ) );

      CUSOLVER_CHECK( cusolverDnXgetrf( cusolverH,
                                        params,
                                        num_rows,
                                        num_cols,
                                        traits< Real >::cuda_data_type,
                                        LU.getValues().getData(),
                                        num_rows,
                                        nullptr,  // Row Order vector not used
                                        traits< Real >::cuda_data_type,
                                        d_work,
                                        d_lwork,
                                        h_work,
                                        h_lwork,
                                        d_info ) );

      CUDA_CHECK( cudaMemcpyAsync( &info, d_info, sizeof( int ), cudaMemcpyDeviceToHost, stream ) );

      CUDA_CHECK( cudaStreamSynchronize( stream ) );

      if( info < 0 ) {
         std::cout << -info << "-th parameter is wrong! Exiting." << std::endl;
         exit( 1 );
      }

      // Free resources
      CUDA_CHECK( cudaFree( d_info ) );
      CUDA_CHECK( cudaFree( d_work ) );

      CUSOLVER_CHECK( cusolverDnDestroyParams( params ) );
      CUSOLVER_CHECK( cusolverDnDestroy( cusolverH ) );
      CUDA_CHECK( cudaStreamDestroy( stream ) );
#endif
   }
}

// Taken from: https://github.com/NVIDIA/CUDALibrarySamples
template< typename Matrix, typename Vector >
void
CuSolverDnXgetrfWrapper::decompose( Matrix& LU, Vector& piv )
{
   if( piv.empty() ) {
      return decompose( LU );
   }

   if constexpr( ! tnlObjectsOnCuda< Matrix, Vector >() ) {
      throw Exceptions::NotImplementedError( getDecomposerName< typename Matrix::DeviceType >( ! piv.empty() )
                                             + " is implemented only for the GPU." );
   }
   else {
#ifdef __CUDACC__

      using Real = typename Matrix::RealType;

      const int64_t num_rows = LU.getRows();
      const int64_t num_cols = LU.getColumns();

      if( num_rows != num_cols ) {
         throw Exceptions::MatrixNotSquare( num_rows, num_cols );
      }

      int info = 0;
      int* d_info = nullptr; /* error info */

      size_t d_lwork = 0;     /* size of workspace */
      void* d_work = nullptr; /* device workspace for getrf */
      size_t h_lwork = 0;     /* size of workspace */
      void* h_work = nullptr; /* host workspace for getrf */

      cusolverDnHandle_t cusolverH = NULL;
      cudaStream_t stream = NULL;

      CUSOLVER_CHECK( cusolverDnCreate( &cusolverH ) );

      CUDA_CHECK( cudaStreamCreateWithFlags( &stream, cudaStreamNonBlocking ) );
      CUSOLVER_CHECK( cusolverDnSetStream( cusolverH, stream ) );

      cusolverDnParams_t params;
      CUSOLVER_CHECK( cusolverDnCreateParams( &params ) );

      // Using new algo: CUSOLVER_ALG_0 (CUSOLVER_ALG_1 is legacy)
      CUSOLVER_CHECK( cusolverDnSetAdvOptions( params, CUSOLVERDN_GETRF, CUSOLVER_ALG_0 ) );

      CUDA_CHECK( cudaMalloc( reinterpret_cast< void** >( &d_info ), sizeof( int ) ) );

      CUSOLVER_CHECK( cusolverDnXgetrf_bufferSize( cusolverH,
                                                   params,
                                                   num_rows,
                                                   num_cols,
                                                   traits< Real >::cuda_data_type,
                                                   LU.getValues().getData(),
                                                   num_rows,
                                                   traits< Real >::cuda_data_type,
                                                   &d_lwork,
                                                   &h_lwork ) );

      CUDA_CHECK( cudaMalloc( reinterpret_cast< void** >( &d_work ), sizeof( Real ) * d_lwork ) );

      // If size of piv is 0 => piv.getData() retuns nullptr
      CUSOLVER_CHECK( cusolverDnXgetrf( cusolverH,
                                        params,
                                        num_rows,
                                        num_cols,
                                        traits< Real >::cuda_data_type,
                                        LU.getValues().getData(),
                                        num_rows,
                                        piv.getData(),
                                        traits< Real >::cuda_data_type,
                                        d_work,
                                        d_lwork,
                                        h_work,
                                        h_lwork,
                                        d_info ) );

      CUDA_CHECK( cudaMemcpyAsync( &info, d_info, sizeof( int ), cudaMemcpyDeviceToHost, stream ) );

      CUDA_CHECK( cudaStreamSynchronize( stream ) );

      if( info < 0 ) {
         std::cout << -info << "-th parameter is wrong! Exiting." << std::endl;
         exit( 1 );
      }

      // Free resources
      CUDA_CHECK( cudaFree( d_info ) );
      CUDA_CHECK( cudaFree( d_work ) );

      CUSOLVER_CHECK( cusolverDnDestroyParams( params ) );
      CUSOLVER_CHECK( cusolverDnDestroy( cusolverH ) );
      CUDA_CHECK( cudaStreamDestroy( stream ) );
#endif
   }
}

}  // namespace Decomposition::LU::Decomposers
