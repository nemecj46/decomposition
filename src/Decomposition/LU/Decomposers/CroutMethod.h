#pragma once

#include <TNL/String.h>

#include <Decomposition/LU/Decomposers/BaseDecomposer.h>

namespace Decomposition::LU::Decomposers {

template< const int BLOCK_SIZE = DEFAULT_BLOCK_SIZE >
class CroutMethod : public BaseDecomposer
{
public:
   static constexpr MatrixFormat
   getMatrixDiagonalFormat()
   {
      return MatrixFormat::UnitDiagIn_U;
   }

   template< typename Device >
   static TNL::String
   getDecomposerName( const bool& partialPivoting = false );

   template< typename Matrix >
   static void
   decompose( Matrix& LU );

   template< typename Matrix, typename Vector >
   static void
   decompose( Matrix& LU, Vector& piv );
};

}  // namespace Decomposition::LU::Decomposers

#include <Decomposition/LU/Decomposers/CroutMethod.hpp>
