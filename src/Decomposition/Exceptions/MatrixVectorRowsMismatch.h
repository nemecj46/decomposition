#pragma once

#include <Decomposition/Exceptions/DecompositionException.h>

namespace Decomposition::Exceptions {

class MatrixVectorRowsMismatch : public DecompositionException
{
public:
   template< typename Matrix, typename Vector >
   MatrixVectorRowsMismatch( const Matrix& A, const Vector& v, const TNL::String& Aname = "A", const TNL::String& vName = "v" )
   : DecompositionException( getDebugMessage( A, v, Aname, vName ) )
   {}

private:
   template< typename Matrix, typename Vector >
   TNL::String
   getDebugMessage( const Matrix& A, const Vector& v, const TNL::String& Aname, const TNL::String& vName )
   {
      std::stringstream s;
      s << "Matrix and Vector do not have the same number of rows! " << Aname << "( " << A.getRows() << " ) != " << vName
        << "( " << v.getSize() << " )";
      return s.str();
   }
};

}  // namespace Decomposition::Exceptions
