#pragma once

#include <Decomposition/Exceptions/DecompositionException.h>

#include <TNL/String.h>

#include <Decomposition/utils/utils.h>

namespace Decomposition {
namespace Exceptions {

class SolverException : public DecompositionException
{
public:
   SolverException( const TNL::String& msg ) : DecompositionException( "Solving Failed! " + msg ) {}
};

}  // namespace Exceptions
}  // namespace Decomposition
