#pragma once

#include <Decomposition/Exceptions/DecompositionException.h>

#include <TNL/String.h>

namespace Decomposition::Exceptions {

class NotImplementedError : public DecompositionException
{
public:
   NotImplementedError( const TNL::String& msg = "Something is not implemented." ) : DecompositionException( msg ) {}
};

}  // namespace Decomposition::Exceptions
