#pragma once

#include <TNL/String.h>

#include <Decomposition/Exceptions/DecompositionException.h>

namespace Decomposition::Exceptions {

class MatrixNotStronglyRegular : public DecompositionException
{
public:
   MatrixNotStronglyRegular() : DecompositionException( getMessage() ) {}

   template< typename Index >
   MatrixNotStronglyRegular( const TNL::String& mtx, const Index& row, const Index& col )
   : DecompositionException( getDebugMessage( mtx, row, col ) )
   {}

private:
   template< typename Index >
   TNL::String
   getDebugMessage( const TNL::String& mtx, const Index& row, const Index& col )
   {
      return getMessage() + " " + mtx + "( " + TNL::convertToString( row ) + ", " + TNL::convertToString( col )
           + " ) = 0."
             " Cannot divide by 0.";
   }

   static TNL::String
   getMessage()
   {
      return "Input Matrix 'A' is not strongly regular - division by 0 during decomposition.";
   }
};

}  // namespace Decomposition::Exceptions
