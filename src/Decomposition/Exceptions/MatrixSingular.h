#pragma once

#include <TNL/String.h>

#include <Decomposition/Exceptions/DecompositionException.h>

namespace Decomposition::Exceptions {

class MatrixSingular : public DecompositionException
{
public:
   MatrixSingular() : DecompositionException( getMessage() ) {}

   template< typename Index >
   MatrixSingular( const TNL::String& mtx, const Index& row, const Index& col )
   : DecompositionException( getDebugMessage( mtx, row, col ) )
   {}

private:
   template< typename Index >
   TNL::String
   getDebugMessage( const TNL::String& mtx, const Index& row, const Index& col )
   {
      return getMessage() + " " + mtx + "( " + TNL::convertToString( row ) + ", " + TNL::convertToString( col )
           + " ) = 0."
             " Cannot divide by 0.";
   }

   static TNL::String
   getMessage()
   {
      return "Input Matrix 'A' is singular - division by 0 during decomposition with partial pivoting.";
   }
};

}  // namespace Decomposition::Exceptions
