#pragma once

#include <Decomposition/Exceptions/DecompositionException.h>

namespace Decomposition::Exceptions {

class MatricesDimensionsMismatch : public DecompositionException
{
public:
   template< typename Matrix >
   MatricesDimensionsMismatch( const Matrix& A,
                               const Matrix& B,
                               const TNL::String& Aname = "A",
                               const TNL::String& Bname = "B" )
   : DecompositionException( getDebugMessage( A, B, Aname, Bname ) )
   {}

private:
   template< typename Matrix >
   TNL::String
   getDebugMessage( const Matrix& A, const Matrix& B, const TNL::String& Aname, const TNL::String& Bname )
   {
      std::stringstream s;
      s << "Dimensions of matrices do not match! " << Aname << "( " << A.getRows() << ", " << A.getColumns()
        << " ) != " << Bname << "( " << B.getRows() << ", " << B.getColumns() << " )";
      return s.str();
   }
};

}  // namespace Decomposition::Exceptions
