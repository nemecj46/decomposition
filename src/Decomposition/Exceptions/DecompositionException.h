#pragma once

#include <stdexcept>

#include <TNL/String.h>

#include <Decomposition/utils/utils.h>

namespace Decomposition::Exceptions {

class DecompositionException : public std::runtime_error
{
public:
   DecompositionException( const TNL::String& msg ) : std::runtime_error( "-!> " + msg ) {}
};

}  // namespace Decomposition::Exceptions
