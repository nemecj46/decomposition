#pragma once

#include <Decomposition/Exceptions/DecompositionException.h>

#include <TNL/String.h>

namespace Decomposition::Exceptions {

class NotANumber : public DecompositionException
{
public:
   template< typename Index >
   NotANumber( const Index& row, const Index& col ) : DecompositionException( getDebugMessage( row, col ) )
   {}

private:
   template< typename Index >
   TNL::String
   getDebugMessage( const Index& row, const Index& col )
   {
      return getMessage() + " LU( " + TNL::convertToString( row ) + ", " + TNL::convertToString( col )
           + " ) = NaN."
             " Cannot divide by NaN.";
   }

   static TNL::String
   getMessage()
   {
      return "Decomposition with partial pivoting failed - division by NaN during decomposition.";
   }
};

}  // namespace Decomposition::Exceptions
