#pragma once

#include <Decomposition/Exceptions/DecompositionException.h>

#include <TNL/String.h>

namespace Decomposition::Exceptions {

class MatrixNotSquare : public DecompositionException
{
public:
   template< typename Index >
   MatrixNotSquare( const Index& rows, const Index& cols, const TNL::String& mtxName = "A" )
   : DecompositionException( getMessage( rows, cols, mtxName ) )
   {}

private:
   template< typename Index >
   TNL::String
   getMessage( const Index& rows, const Index& cols, const TNL::String& mtxName )
   {
      return "Input matrix '" + mtxName
           + "' is not a square matrix."
             " Rows: "
           + TNL::convertToString( rows ) + ", Columns: " + TNL::convertToString( cols );
   }
};

}  // namespace Decomposition::Exceptions
