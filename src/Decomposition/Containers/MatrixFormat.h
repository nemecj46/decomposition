#pragma once

namespace Decomposition::Containers {

enum MatrixFormat : std::uint8_t
{
   UnitDiagIn_U = 0,  // (Default) Format: In LU, the main diagonal consists of elemens from L's main diagonal - the unit
                      // diagonal of U is omitted.
   UnitDiagIn_L = 1   //           Format: In LU, the main diagonal consists of elemens from U's main diagonal - the unit
                      //           diagonal of L is omitted.
};

}  // namespace Decomposition::Containers
