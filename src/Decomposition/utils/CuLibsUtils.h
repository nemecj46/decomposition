/*
 * Copyright (c) 2019, NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <stdexcept>

#include <cublas_api.h>
#include <cusolverDn.h>
#include <library_types.h>

// CUDA API error checking
#define CUDA_CHECK( err )                                                \
   do {                                                                  \
      cudaError_t err_ = ( err );                                        \
      if( err_ != cudaSuccess ) {                                        \
         printf( "CUDA error %d at %s:%d\n", err_, __FILE__, __LINE__ ); \
         throw std::runtime_error( "CUDA error" );                       \
      }                                                                  \
   } while( 0 )

// cusolver API error checking
#define CUSOLVER_CHECK( err )                                                \
   do {                                                                      \
      cusolverStatus_t err_ = ( err );                                       \
      if( err_ != CUSOLVER_STATUS_SUCCESS ) {                                \
         printf( "cusolver error %d at %s:%d\n", err_, __FILE__, __LINE__ ); \
         throw std::runtime_error( "cusolver error" );                       \
      }                                                                      \
   } while( 0 )

// cublas API error checking
#define CUBLAS_CHECK( err )                                                \
   do {                                                                    \
      cublasStatus_t err_ = ( err );                                       \
      if( err_ != CUBLAS_STATUS_SUCCESS ) {                                \
         printf( "cublas error %d at %s:%d\n", err_, __FILE__, __LINE__ ); \
         throw std::runtime_error( "cublas error" );                       \
      }                                                                    \
   } while( 0 )

// type traits
template< typename T >
struct traits;

template<>
struct traits< float >
{
   static constexpr cudaDataType cuda_data_type = CUDA_R_32F;
};

template<>
struct traits< double >
{
   static constexpr cudaDataType cuda_data_type = CUDA_R_64F;
};
