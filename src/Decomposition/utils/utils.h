#pragma once

#include <chrono>

#include <TNL/String.h>
#include <TNL/Devices/Cuda.h>
#include <TNL/Devices/Host.h>

using namespace std::chrono;

namespace Decomposition {

#define DEFAULT_BLOCK_SIZE 32
#define DEFAULT_THREADS_PER_BLOCK ( DEFAULT_BLOCK_SIZE * DEFAULT_BLOCK_SIZE )

#define DEBUG_DECOMP false
#define DECOMP_METRICS_ENABLED false

template< typename Device >
constexpr bool
isCuda()
{
   return std::is_same_v< Device, TNL::Devices::Cuda >;
}

template< typename Device >
constexpr bool
isHost()
{
   return std::is_same_v< Device, TNL::Devices::Host >;
}

template< typename TNLObject, typename Device >
constexpr bool
tnlObjectOnDevice()
{
   return std::is_same_v< typename TNLObject::DeviceType, Device >;
}

template< typename TNLObject >
constexpr bool
tnlObjectOnHost()
{
   return tnlObjectOnDevice< TNLObject, TNL::Devices::Host >();
}

template< typename TNLObject >
constexpr bool
tnlObjectOnCuda()
{
   return tnlObjectOnDevice< TNLObject, TNL::Devices::Cuda >();
}

template< typename... TNLObjects >
constexpr bool
tnlObjectsOnHost()
{
   return std::bool_constant< ( ... && tnlObjectOnHost< TNLObjects >() ) >::value;
}

template< typename... TNLObjects >
constexpr bool
tnlObjectsOnCuda()
{
   return std::bool_constant< ( ... && tnlObjectOnCuda< TNLObjects >() ) >::value;
}

template< typename... TNLObjects >
constexpr bool
tnlObjectsOnSameDevice()
{
   return tnlObjectsOnHost< TNLObjects... >() || tnlObjectsOnCuda< TNLObjects... >();
}

template< typename StartType >
void
printTime( StartType start, const TNL::String& functionName )
{
   auto stop = high_resolution_clock::now();
   auto duration = duration_cast< microseconds >( stop - start );

   std::cout << "Time taken by " << functionName << ":\t\t " << duration.count() << " microseconds" << std::endl;
}

}  // namespace Decomposition
