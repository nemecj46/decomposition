#pragma once

#include <TNL/Matrices/MatrixReader.h>
#include <TNL/String.h>

#include <Decomposition/LU/Decomposers/CroutMethod.h>
#include <Decomposition/LU/Decomposers/CuSolverDnXgetrfWrapper.h>
#include <Decomposition/LU/Decomposers/IterativeCroutMethod.h>

#include "Benchmarks/utils/Benchmark.h"
#include "Benchmarks/Results/Decomposers/DecomposerBenchmarkResult.h"
#include "Benchmarks/Results/Decomposers/DecomposerBenchmarkResultLoaderHost.h"

using namespace Decomposition::LU::Decomposers;

namespace Decomposition::Benchmark {

template< typename Index, typename Device >
using VectorType = TNL::Containers::Vector< Index, Device, Index >;

template< typename Real, typename Index >
using CudaMatrixRowMajorType =
   TNL::Matrices::DenseMatrix< Real, TNL::Devices::Cuda, Index, TNL::Algorithms::Segments::RowMajorOrder >;

template< typename Real, typename Index >
using CudaMatrixColumnMajorType =
   TNL::Matrices::DenseMatrix< Real, TNL::Devices::Cuda, Index, TNL::Algorithms::Segments::ColumnMajorOrder >;

template< typename Real, typename Index >
using HostMatrixType = TNL::Matrices::DenseMatrix< Real, TNL::Devices::Host, Index, TNL::Algorithms::Segments::RowMajorOrder >;

using BenchmarkType = Decomposition::Benchmark::Benchmark;

template< typename Matrix, typename Vector, typename Index64 = typename Matrix::IndexType >
using BenchmarkResultType = DecomposerBenchmarkResult< Matrix, Vector, Index64 >;
using DecomposerBenchmarkResultLoaderHost = Results::Decomposers::DecomposerBenchmarkResultLoaderHost;

std::tuple< bool, TNL::String, bool, bool, bool >
getParameters( const TNL::Config::ParameterContainer& parameters )
{
   return std::make_tuple( parameters.getParameter< bool >( "verbose-MReader" ),
                           parameters.getParameter< TNL::String >( "threads-per-block" ),
                           parameters.getParameter< bool >( "max-diff" ),
                           parameters.getParameter< bool >( "partial-pivoting" ),
                           parameters.getParameter< bool >( "save-computed-matrix-CM" ) );
}

template< typename Decomposer,
          typename BenchmarkMatrix,
          typename VecDevice = typename BenchmarkMatrix::DeviceType,
          typename Index = typename BenchmarkMatrix::IndexType,
          typename HostMatrix >
void
benchmarkDecomposer( BenchmarkType& benchmark,
                     const HostMatrix& A_host,
                     const HostMatrix& LU_host,
                     const bool& maxDiff,
                     const bool& partialPivoting )
{
   using Device = typename BenchmarkMatrix::DeviceType;
   using Vector = VectorType< Index, VecDevice >;

   BenchmarkMatrix LU;
   try {
      LU = A_host;
   }
   catch( const std::exception& e ) {
      benchmark.addErrorMessage( "Unable to copy the matrix on " + getPerformer< BenchmarkMatrix >() + ": "
                                 + TNL::String( e.what() ) );
      return;
   }

   Vector piv( 0 );

   // Only fill up and set proper size if partial pivoting
   if( partialPivoting ) {
      piv.setSize( LU.getRows() );
   }

   BenchmarkResultType< BenchmarkMatrix, Vector, Index > benchmarkResults(
      A_host, LU_host, LU, piv, Decomposer::getMatrixDiagonalFormat(), maxDiff );

   benchmark.setMetadataElement( { "format", Decomposer::template getDecomposerName< Device >( partialPivoting ) } );

   auto resetBenchmarkMatrixVector = [ & ]()
   {
      LU = A_host;
      if( ! piv.empty() ) {
         piv.setValue( 0 );
      }
   };

   auto decompose = [ & ]()
   {
      Decomposer::decompose( LU, piv );
   };

   benchmark.time< Device >( resetBenchmarkMatrixVector, getPerformer< BenchmarkMatrix >(), decompose, benchmarkResults );
}

template< typename Real = double, typename Index = int >
void
benchmarkDecomposers( Decomposition::Benchmark::BenchmarkType& benchmark,
                      const TNL::Config::ParameterContainer& parameters,
                      const TNL::String& inputFileName,
                      std::ostream& output,
                      const int& verbose )
{
   using HostMatrix = HostMatrixType< Real, Index >;
   using Device = TNL::Devices::Host;
   using Vector = VectorType< Index, Device >;

   // TODO: Replace the next 3 lines once c++20 is used (see https://gitlab.com/tnl-project/decomposition/-/issues/44)
   bool verboseMR;
   bool maxDiff;
   bool partialPivoting;
   bool saveComputedMatrix;
   TNL::String threads_perBlock;
   std::tie( verboseMR, threads_perBlock, maxDiff, partialPivoting, saveComputedMatrix ) = getParameters( parameters );

   HostMatrix A_host;
   HostMatrix LU_host;

   TNL::Matrices::MatrixReader< HostMatrix >::readMtx( inputFileName, A_host, verboseMR );
   LU_host = A_host;

   // Get input matrix filename from the path
   TNL::String matrixName = inputFileName.substr( inputFileName.find_last_of( "/\\" ) + 1 );

   benchmark.setMetadataColumns( {
      { "matrix name",                                                matrixName },
      {   "precision",                                    TNL::getType< Real >() },
      {        "rows",                 TNL::convertToString( LU_host.getRows() ) },
      {     "columns",              TNL::convertToString( LU_host.getColumns() ) },
      {    "nonzeros", TNL::convertToString( LU_host.getNonzeroElementsCount() ) },
   } );
   benchmark.setMetadataWidths( {
      { "matrix name", 32 },
      {      "format", 32 },
   } );

   using Decomposer = CroutMethod<>;

   benchmark.setMetadataElement( { "format", Decomposer::template getDecomposerName< Device >( partialPivoting ) } );

   // 'piv' must be declared before - in case it is saved or benchmark results are loaded into it.
   Vector piv( 0 );

   // If partial pivoting is required, set the proper size
   if( partialPivoting ) {
      piv.setSize( LU_host.getRows() );
   }

   BenchmarkResultType< HostMatrix, Vector > hostBenchmarkResults(
      A_host, LU_host, LU_host, piv, Decomposer::getMatrixDiagonalFormat(), maxDiff );

   // Check if benchmark result exist for the input matrix
   bool benchmarkResultFound =
      DecomposerBenchmarkResultLoaderHost ::loadResult< Decomposer >( hostBenchmarkResults,
                                                                      matrixName,
                                                                      parameters.getParameter< TNL::String >( "machine" ),
                                                                      partialPivoting,
                                                                      parameters.getParameter< int >( "loops" ) );

   // If saved result not found then perform the benchmark normally
   if( ! benchmarkResultFound ) {
      // Perform benchmark on the host
      auto resetHostMatrix = [ & ]()
      {
         LU_host = A_host;
         if( ! piv.empty() ) {
            piv.setValue( 0 );
         }
      };
      auto decomposeDenseHost = [ & ]()
      {
         Decomposer::decompose( LU_host, piv );
      };
      benchmark.time< Device >( resetHostMatrix, getPerformer< HostMatrix >(), decomposeDenseHost, hostBenchmarkResults );

      // Save the resulting LU matrix
      if( saveComputedMatrix ) {
         TNL::String denseHostMatrixLU_filename = inputFileName + "_result_" + TNL::String( TNL::getType< Real >() );
         LU_host.save( denseHostMatrixLU_filename );
         if( partialPivoting ) {  // Save the pivoting vector too
            piv.save( TNL::String( inputFileName + "_result_pivVector_" + TNL::String( TNL::getType< Real >() ) ) );
         }
      }
   }
   else {
      // Load the saved CPU benchmark result
      LU_host.load( inputFileName + "_result_" + TNL::String( TNL::getType< Real >() ) );
      if( partialPivoting ) {  // Load the pivoting vector too
         piv.load( inputFileName + "_result_pivVector_" + TNL::String( TNL::getType< Real >() ) );
      }
      benchmark.logMockResult( getPerformer< HostMatrix >(), hostBenchmarkResults );
   }

#ifdef __CUDACC__
   // Run Decomposition benchmarks for Parallel Crout Method on the GPU
   if( parameters.getParameter< bool >( "with-parallel-crout-gpu-benchmark" ) ) {
      if( threads_perBlock == "all" || threads_perBlock == "8" ) {
         benchmarkDecomposer< CroutMethod< 8 >, CudaMatrixRowMajorType< Real, Index >, TNL::Devices::Host >(
            benchmark, A_host, LU_host, maxDiff, partialPivoting );
      }
      if( threads_perBlock == "all" || threads_perBlock == "16" ) {
         benchmarkDecomposer< CroutMethod< 16 >, CudaMatrixRowMajorType< Real, Index >, TNL::Devices::Host >(
            benchmark, A_host, LU_host, maxDiff, partialPivoting );
      }
      if( threads_perBlock == "all" || threads_perBlock == "32" ) {
         benchmarkDecomposer< CroutMethod< 32 >, CudaMatrixRowMajorType< Real, Index >, TNL::Devices::Host >(
            benchmark, A_host, LU_host, maxDiff, partialPivoting );
      }
   }
#endif

#ifdef __CUDACC__
   // Run Decomposition benchmarks for Iterative Crout Method on the GPU
   if( parameters.getParameter< bool >( "with-iterative-crout-gpu-benchmark" ) ) {
      if( threads_perBlock == "all" || threads_perBlock == "8" ) {
         benchmarkDecomposer< IterativeCroutMethod< 8 >, CudaMatrixRowMajorType< Real, Index >, TNL::Devices::Host >(
            benchmark, A_host, LU_host, maxDiff, partialPivoting );
      }
      if( threads_perBlock == "all" || threads_perBlock == "16" ) {
         benchmarkDecomposer< IterativeCroutMethod< 16 >, CudaMatrixRowMajorType< Real, Index >, TNL::Devices::Host >(
            benchmark, A_host, LU_host, maxDiff, partialPivoting );
      }
      if( threads_perBlock == "all" || threads_perBlock == "32" ) {
         benchmarkDecomposer< IterativeCroutMethod< 32 >, CudaMatrixRowMajorType< Real, Index >, TNL::Devices::Host >(
            benchmark, A_host, LU_host, maxDiff, partialPivoting );
      }
   }

   // Run Decomposition benchmarks for CuSolver's DnXgetrf (Doolittle Method) on the GPU
   if( parameters.getParameter< bool >( "with-cusolver-gpu-benchmark" ) ) {
      benchmarkDecomposer< CuSolverDnXgetrfWrapper, CudaMatrixColumnMajorType< Real, Index >, TNL::Devices::Cuda, int64_t >(
         benchmark, A_host, LU_host, maxDiff, partialPivoting );
   }
#endif
}

}  // namespace Decomposition::Benchmark
