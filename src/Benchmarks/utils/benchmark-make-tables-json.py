#!/usr/bin/python3

from cmath import log
import os
import argparse
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from TNL.BenchmarkLogs import *
from python_libs.constants import *  # Import constant variables
import re  # Regex


####
# Create multiindex for columns
def get_multiindex(input_df, formats):
    level1 = ['Matrix name', 'rows', 'columns', 'nonzeros per row']
    level2 = ['', '', '', '']
    level3 = ['', '', '', '']
    df_data = [[' ', ' ', ' ', ' ']]
    for format in formats:
        data_cols = ['time']
        # No need to compare the BASELINE format to itself -> max. difference only between other formats and baseline
        if not format == BENCHMARK.TYPE.BASELINE:
            data_cols.append('Base MaxAbsDiff')
            data_cols.append('Input MaxAbsDiff')
            data_cols.append('speed-up compared to')
        else:
            data_cols.append('Input MaxAbsDiff')
        for data in data_cols:
            level1.append(format)
            level2.append(data)
            # Write baseline format name only under speedup column, not under anything else
            if data != 'speed-up compared to':
                level3.append('')
            elif "GPU" in format and "- pert" in format:
                level3.append(format.replace(" - pert", ""))
            else:
                level3.append(BENCHMARK.TYPE.BASELINE)
            df_data[0].append(' ')
        if format == 'Best':
            level1.append(format)
            level2.append('format')
            level3.append('')
            df_data[0].append(' ')

            level1.append(format)
            level2.append('performer')
            level3.append('')
            df_data[0].append(' ')

    multiColumns = pd.MultiIndex.from_arrays([level1, level2, level3])
    return multiColumns, df_data

####
# Convert input table to better structured one
def convert_data_frame(input_df, multicolumns, df_data, begin_idx=0, end_idx=-1):
    frames = []
    in_idx = 0
    out_idx = 0

    if end_idx == -1:
        end_idx = len(input_df.index)

    best_count = 0

    while in_idx < len(input_df.index) and out_idx < end_idx:
        # Get matrix name from line number in_idx
        matrixName = input_df.iloc[in_idx]['matrix name']

        # Get all records from the dataframe for that matrix
        df_matrix = input_df.loc[input_df['matrix name'] == matrixName]

        # Print out information id of row where this matrix name first appears out of how many lines
        if out_idx >= begin_idx:
            print(f'{out_idx} : {in_idx} / {len(input_df.index)} : {matrixName}')
        else:
            print(
                f'{out_idx} : {in_idx} / {len(input_df.index)} : {matrixName} - SKIP')

        # Create Frame for Data - columns for output.html
        aux_df = pd.DataFrame(df_data, columns=multicolumns, index=[out_idx])

        fastest_format = {
            "format": input_df.iloc[in_idx]['format'],
            "time": input_df.iloc[in_idx]['time'],
            "Base MaxAbsDiff": input_df.iloc[in_idx][BENCHMARK.TYPE.METRICS.BASE_MAX_ABS_DIFF],
            "Input MaxAbsDiff": input_df.iloc[in_idx][BENCHMARK.TYPE.METRICS.SOLUTION_MAX_ABS_DIFF],
            "performer": input_df.iloc[in_idx]['performer'],
        }

        for index, row in df_matrix.iterrows():
            # Save the data from that run
            aux_df.iloc[0]['Matrix name'] = row['matrix name']
            aux_df.iloc[0]['rows'] = row['rows']
            aux_df.iloc[0]['columns'] = row['columns']
            aux_df.iloc[0]['nonzeros per row'] = float(
                row['nonzeros'])/float(row['rows'])

            current_format = row['format']
            current_device = row['performer']

            # Measured data
            time = pd.to_numeric(row['time'], errors='coerce')
            diff_max = pd.to_numeric(row[BENCHMARK.TYPE.METRICS.BASE_MAX_ABS_DIFF], errors='coerce')
            A_new_diff_max = pd.to_numeric(
                row[BENCHMARK.TYPE.METRICS.SOLUTION_MAX_ABS_DIFF], errors='coerce')

            aux_df.iloc[0][(current_format, 'time', '')] = time
            aux_df.iloc[0][(current_format, 'Base MaxAbsDiff', '')] = diff_max
            aux_df.iloc[0][(current_format, 'Input MaxAbsDiff', '')
                           ] = A_new_diff_max

            if time < fastest_format['time']:
                fastest_format['format'] = current_format
                fastest_format['time'] = time
                fastest_format['Base MaxAbsDiff'] = diff_max
                fastest_format['Input MaxAbsDiff'] = A_new_diff_max
                fastest_format['performer'] = current_device

        # Fastest format fill data
        aux_df.iloc[0][(BENCHMARK.BEST, 'time', '')] = fastest_format['time']
        aux_df.iloc[0][(BENCHMARK.BEST, 'Base MaxAbsDiff', '')
                       ] = fastest_format['Base MaxAbsDiff']
        aux_df.iloc[0][(BENCHMARK.BEST, 'Input MaxAbsDiff', '')
                       ] = fastest_format['Input MaxAbsDiff']
        aux_df.iloc[0][(BENCHMARK.BEST, 'format', '')] = fastest_format['format']
        aux_df.iloc[0][(BENCHMARK.BEST, 'performer', '')
                       ] = fastest_format['performer']
        best_count += 1

        # Add the final line with data to the output
        if out_idx >= begin_idx:
            frames.append(aux_df)

        # Incremenet line id
        out_idx = out_idx + 1

        # Increment unique matrix line id (if a matrix takes up 3 lines - bcs 3 formats -> increment this id by 3)
        in_idx = in_idx + len(df_matrix.index)
    result = pd.concat(frames)
    return result

####
# Compute speed-up of particular formats compared to Cusparse on GPU and CSR on CPU
def compute_baseline_speedup(df, formats):
    # Best format does not have a Device type, so here we add an empty string to DEVICES.ALL
    for format in formats:
        if not format in BENCHMARK.TYPE.BASELINE:
            try:
                format_times_list = df[(format, 'time')]
            except:
                print("-!> EXCEPTION!!!")
                continue
            print('--> Adding speed-up for ', format)
            if "- pert" in format:
                baseline_times_list = df[(
                    format.replace(" - pert", ""), 'time')]
            else:
                baseline_times_list = df[(BENCHMARK.TYPE.BASELINE, 'time')]

            baseline_speedup_list = []

            for (format_time, baseline_time) in zip(format_times_list, baseline_times_list):
                try:
                    baseline_speedup_list.append(baseline_time / format_time)
                except:
                    baseline_speedup_list.append(float('nan'))
            if "- pert" in format:
                df[(format, 'speed-up compared to',
                    format.replace(" - pert", ""))] = baseline_speedup_list
            else:
                df[(format, 'speed-up compared to', BENCHMARK.TYPE.BASELINE)
                   ] = baseline_speedup_list


def compute_speedup(df, formats):
    compute_baseline_speedup(df, formats)

###
# Draw several profiles into one figure
def draw_profiles(formats, profiles, matrices_list, xlabel, ylabel, filename, bar='none', legend_loc='upper right'):
    print(
            f"--> Writing comparison of speed-up of all formats compared to {bar}...")
    fig, axs = plt.subplots(1, 1, figsize=(9, 5))
    latexNames = []
    size = 1
    for format in formats:
        t = np.arange(profiles[format].size)

        axs.plot(t, profiles[format], '-o', ms=1, lw=1)
        size = len(profiles[format])
        latexNames.append(format)
    if bar != 'none':
        bar_data = np.full(size, 1)
        axs.plot(t, bar_data, '-', ms=1, lw=1.5)
        if bar != '':
            latexNames.append(bar)

    axs.set_xticks(t, matrices_list, rotation=45, ha='right')

    axs.legend(latexNames, loc=legend_loc)
    axs.set_xlabel(xlabel)
    axs.set_ylabel(ylabel)
    plt.rcParams.update({
        "text.usetex": True,
        "font.family": "sans-serif",
        "font.sans-serif": ["Helvetica"]})

    plt.tight_layout()
    plt.savefig(filename + '.pdf')

    plt.gca().grid(which='major', axis='x', linestyle='--', alpha=0.3)

    axs.set_yscale('log')
    plt.savefig(filename + '-log.pdf')
    plt.savefig(filename + '-log.png', dpi=200)
    plt.close(fig)

####
# Comparison of speed-up w.r.t. Baseline format
def baseline_speedup_comparison(df, formats, head_size=10):
    speedup_folder_name = "Speed-up-profile"
    if not os.path.exists(speedup_folder_name):
        os.mkdir(speedup_folder_name)

    matrices_list = list(df['Matrix name'].values.tolist())
    base_format = BENCHMARK.TYPE.BASELINE

    # Copy value, not reference using slice notation
    formats_list = formats[:]
    formats_list.remove(base_format)
    formats_list.remove(BENCHMARK.BEST)

    # Speedup comparison of all formats
    profiles = {}

    for format in formats_list:
        df['tmp'] = df[(format, 'time')]
        filtered_df = df.dropna(subset=[('tmp', '', '', '')])

        profiles[format] = filtered_df[(
            format, 'speed-up compared to', base_format)].copy()
    draw_profiles(formats_list, profiles, matrices_list, "Matrices - ascending matrix dimensions", f"Speedup compared to {base_format}", f"{speedup_folder_name}/all-speedup.pdf", base_format, "best")

    # Speedup comparison of invidual formats
    profiles = {}
    for format in formats_list:
        print(
            f"--> Writing comparison of speed-up of {format} compared to {base_format}...")

        fig, axs = plt.subplots(1, 1, figsize=(9, 5))
        size = len(
            filtered_df[(format, 'speed-up compared to', base_format)].index)
        t = np.arange(size)
        bar = np.full(size, 1)

        axs.set_xticks(t, matrices_list, rotation=45, ha='right')

        axs.plot(
            t, filtered_df[(format, 'speed-up compared to', base_format)], '-o', ms=1, lw=1)
        axs.plot(t, bar, '-', ms=1, lw=1)
        axs.legend([format, base_format], loc='upper right')
        axs.set_ylabel('Speedup')
        axs.set_xlabel("Matrices - ascending matrix dimensions")

        plt.rcParams.update({
            "text.usetex": True,
            "font.family": "sans-serif",
            "font.sans-serif": ["Helvetica"]})

        plt.tight_layout()
        plt.savefig(
            f"{speedup_folder_name}/{base_format}-vs-{format}-speed-up.pdf")
        plt.close(fig)

        fig, axs = plt.subplots(1, 1, figsize=(6, 4))
        axs.plot(
            t, filtered_df[(format, 'speed-up compared to', base_format)], '-o', ms=1, lw=1)
        axs.plot(t, bar, '-', ms=1, lw=1)
        # This is the only line that is different from the code block above that does the same thing
        # TODO: Can this block be removed and only the set_yscale( 'log' ) line along with the following one would stay -> save the figure with different name
        axs.legend([format, base_format], loc='lower left')
        axs.set_ylabel('Speedup')
        axs.set_xlabel("Matrices - ascending matrix dimensions")

        axs.set_xticks(t, matrices_list, rotation=45, ha='right')

        axs.set_yscale('log')

        plt.tight_layout()
        plt.savefig(
            f"{speedup_folder_name}/{base_format}-vs-{format}-speed-up-log.pdf")
        plt.close(fig)

        copy_df = df.copy()
        for f in formats_list:
            if not f == format:
                copy_df.drop(labels=f, axis='columns',
                                level=0, inplace=True)
        copy_df.to_html(
            f"{speedup_folder_name}/{base_format}-vs-{format}-speed-up.html")

####
# Make data analysis
def processDf(df, formats, input_file_dir, input_file_name, head_size=10):
    print("--> Writing to CSV file...")
    df.to_csv('output.csv')

    print("--> Writing to HTML file...")
    # Set the output format to scientific with two decimal places
    pd.set_option('display.float_format', '{:.2E}'.format)
    df.to_html('output.html')
    pd.reset_option('display.float_format')

    # Generate tables and figures
    baseline_speedup_comparison(df, formats, head_size)

    best = df[('Best', 'format')].tolist()
    best_formats = list(set(best))
    sum = 0
    for format in formats:
        if not BENCHMARK.BEST in format:
            cases = best.count(format)
            print(f'--> {format} is best in {cases} cases.')
            sum += cases
    print(f'--> Total number of matrices: {sum}.')
    print(f'--> Best formats {best_formats}.')

##################################################
# Main
##################################################


# Parse arguments
parser = argparse.ArgumentParser(
    description='Parse input file to load data from.')
# Optional arguments
parser.add_argument('--input_file', type=str,
                    help='Optional input file path')
args = parser.parse_args()

print(f"-> Script running from '{os.getcwd()}'.")

# If the script is called from within its location in the repository
if os.path.exists(f"{os.getcwd()}/benchmark-make-tables-json.py"):
    print(f"-> Changing dir to '{BENCHMARK.TYPE.DIRECTORY}'...")
    # Change directory to whichever benchmark is being processed
    os.chdir(BENCHMARK.TYPE.DIRECTORY)

# Decide which input file to use
# - Default
# - From input argument
input_file_path = ""

if args.input_file == None:
    input_file_path = BENCHMARK.TYPE.DEFAULT_FILE
elif os.path.exists(args.input_file):
    input_file_path = args.input_file
else:
    print(f"File {args.input_file} not found! Exiting...")
    exit(1)

# Create different variables for input file
input_file_name = os.path.basename(input_file_path)
input_file_dir = input_file_path.replace(input_file_name, "")
input_file_name = input_file_name.split(".")[0]

# Determine the type of benchmark based on the input prefix
if DECOMPOSERS.FILE_PREFIX in input_file_name:
    BENCHMARK.TYPE = DECOMPOSERS
elif SOLVERS.FILE_PREFIX in input_file_name:
    BENCHMARK.TYPE = SOLVERS
elif BENCHMARK.TYPE != None:
    pass
else:
    print(f"Cannot determine the benchmark type from the input file name '{input_file_name}'. \
            It does not contain either '{DECOMPOSERS.FILE_PREFIX}' or '{SOLVERS.FILE_PREFIX}'. \
            Set BENCHMARK.TYPE to either DECOMPOSERS or SOLVERS in constants.py. Exiting...")
    exit(1)

# Create a separate log directory for this specific log file
log_dir = f"{input_file_dir}{input_file_name}"
if not os.path.exists(log_dir):
    print(f"-> Creating log directory: {log_dir}")
    os.mkdir(log_dir)

input_df = get_benchmark_dataframe(input_file_path)

print(f"-> Changing directory to: {log_dir}")
os.chdir(log_dir)

# Sort by matrix columns (ascending) -> Easier to see trend
input_df.sort_values(by=['rows', 'matrix name'], inplace=True, ascending=True)

# Use the log's filename for the html file.
input_df.to_html(f"{input_file_name}.html")

# Create output.html - Matrices as rows, formats as columns -> table in HTML file
# - Will contain inividual rows as matrices
# - Groups of columns are going to be formats + Best
#  - Each group will have for each matrix: time, Base MaxAbsDiff speed-up (vs Dense Crout on CPU)
# list of all formats in the benchmark results
formats = sorted(list(set(input_df['format'].values.tolist())))
formats.append(BENCHMARK.BEST)

# get_multiindex will create the multiple levels of columns for output.html
multicolumns, df_data = get_multiindex(input_df, formats)

print("-> Converting data...")
result = convert_data_frame(input_df, multicolumns, df_data, 0, 2000)

compute_speedup(result, formats)

# Replace empty values in results with NaN
result.replace(to_replace=' ', value=np.nan, inplace=True)

head_size = 25
if not os.path.exists('general'):
    os.mkdir('general')
os.chdir('general')

processDf(result, formats, input_file_dir, input_file_name, head_size)
