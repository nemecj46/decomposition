#pragma once

namespace Decomposition::Benchmark::Messages {

TNL::String
getRunningBenchmarkMessage( const TNL::String& procedureName )
{
   return "Running benchmark for '" + procedureName + "'...";
}

template< typename Precision >
TNL::String
getResultsStatusMessage( const bool& resultsFound, const TNL::String& machine, const TNL::String& procedureName )
{
   const TNL::String precision = TNL::getType< Precision >();
   TNL::String prefix = "->";
   TNL::String resultsFoundString = "found";
   TNL::String nextAction = "Loading results...";
   if( ! resultsFound ) {
      prefix = "-!>";
      resultsFoundString = "NOT FOUND";
      nextAction = getRunningBenchmarkMessage( procedureName );
   }
   return prefix + " Benchmark results for '" + procedureName + "' on machine '" + machine + "' with precision '" + precision
        + "' " + resultsFoundString + ". " + nextAction;
}

}  // namespace Decomposition::Benchmark::Messages
