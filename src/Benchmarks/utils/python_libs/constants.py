#!/usr/bin/python3
## Decomposers:
class DECOMPOSERS():
    DENSE_CROUT = 'CM'
    DENSE_CROUT_PIVOTING = 'CM PP'
    BASELINE = DENSE_CROUT_PIVOTING
    FILE_PREFIX = 'decomposers-benchmark-'
    DIRECTORY = '../Decomposers/scripts'
    DEFAULT_FILE = f"log-files/{FILE_PREFIX}double.log"
    class METRICS():
        BASE_MAX_ABS_DIFF = 'Base MaxAbsDiff'
        SOLUTION_MAX_ABS_DIFF = 'Input MaxAbsDiff'

## Solvers:
class SOLVERS():
    SEQUENTIAL_SOLVER = 'SS'
    SEQUENTIAL_SOLVER_PIVOT = 'SS PP'
    BASELINE = SEQUENTIAL_SOLVER_PIVOT
    FILE_PREFIX = 'solvers-benchmark-'
    DIRECTORY = '../Solvers/scripts'
    DEFAULT_FILE = f"log-files/{FILE_PREFIX}double.log"
    class METRICS():
        BASE_MAX_ABS_DIFF = 'Base MaxAbsDiff'
        SOLUTION_MAX_ABS_DIFF = 'Solution MaxAbsDiff'

## Holder for either benchmark type
class BENCHMARK():
    BEST = 'Best'
    TYPE = None
