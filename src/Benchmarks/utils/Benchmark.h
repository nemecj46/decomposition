#pragma once

#include <TNL/Benchmarks/Benchmarks.h>
#include <TNL/Benchmarks/JsonLogging.h>
#include <TNL/String.h>

#include <Decomposition/utils/utils.h>

namespace Decomposition::Benchmark {

template< typename TNLObject >
TNL::String
getPerformer()
{
   if( tnlObjectOnHost< TNLObject >() ) {
      return "CPU";
   }
   else {
      return "GPU";
   }
}

using Logger = TNL::Benchmarks::JsonLogging;
using TNLBenchmarkType = TNL::Benchmarks::Benchmark< Logger >;

class Benchmark : public TNLBenchmarkType
{
public:
   Benchmark( std::ostream& output, int loops = 10, int verbose = 1 ) : TNLBenchmarkType( output, loops, verbose ) {}

   template< typename BenchmarkResult >
   void
   logMockResult( const TNL::String& performer, BenchmarkResult& result )
   {
      if( this->baseTime == 0.0 )
         this->baseTime = result.time;
      logger.logResult( performer, result.getTableHeader(), result.getRowElements(), result.getColumnWidthHints() );
   }
};

}  // namespace Decomposition::Benchmark
