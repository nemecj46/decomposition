#pragma once

#include <exception>
#include <chrono>
#include <filesystem>

#include <TNL/Devices/Host.h>
#include <TNL/Devices/Cuda.h>
#include <TNL/Config/parseCommandLine.h>

#include "BenchmarkSolvers.h"

using namespace TNL;

template< typename Real >
void
runSolversBenchmarks( Decomposition::Benchmark::BenchmarkType& benchmark,
                      const Config::ParameterContainer& parameters,
                      const String& inputFileName,
                      std::ostream& output,
                      const int& verbose )
{
   try {
      Decomposition::Benchmark::benchmarkSolvers< Real >( benchmark, parameters, inputFileName, output, verbose );
   }
   catch( const std::exception& ex ) {
      std::cerr << ex.what() << '\n';
   }
}

String
getDateTime()
{
   using Clock = std::chrono::system_clock;
   const std::time_t timeNow = Clock::to_time_t( Clock::now() );
   return convertToString( std::put_time( std::localtime( &timeNow ), "%F_%T" ) );
}

void
setupConfig( Config::ConfigDescription& config )
{
   config.addDelimiter( "Benchmark settings:" );
   config.addRequiredEntry< String >( "input-file", "Input file name." );
   config.addEntry< bool >( "with-iterative-solver-cpu-benchmark",
                            "All linear systems are also benchmarked on the CPU using the Iterative approach.",
                            false );
   config.addEntry< bool >( "with-iterative-solver-gpu-benchmark",
                            "All linear systems are also benchmarked on the GPU using the Iterative approach.",
                            true );
   config.addEntry< bool >( "with-cublas-trsm-wrapper-gpu-benchmark",
                            "All linear systems are also benchmarked on the GPU using CuBLAS's wrapped trsm.",
                            true );
   config.addEntry< bool >( "with-cusolver-getrs-wrapper-gpu-benchmark",
                            "All linear systems are also benchmarked on the GPU using CuSolvers's wrapped getrs.",
                            true );
   config.addEntry< String >( "log-file", "Log file name.", "./log-files/solvers-benchmark::" + getDateTime() + ".log" );
   config.addEntry< String >( "output-mode", "Mode for opening the log file.", "append" );
   config.addEntryEnum( "append" );
   config.addEntryEnum( "overwrite" );
   config.addEntry< String >( "precision", "Precision of the arithmetics.", "double" );
   config.addEntryEnum( "float" );
   config.addEntryEnum( "double" );
   config.addEntryEnum( "all" );
   config.addEntry< int >( "loops", "Number of iterations for every computation.", 10 );
   config.addEntry< int >( "verbose", "Verbose mode.", 1 );
   config.addEntry< bool >( "verbose-MReader", "Verbose mode for Matrix Reader.", false );
   config.addEntry< int >( "num-right-hand-sides", "Number of right hand sides of AX=B.", 10 );
   config.addEntry< String >(
      "threads-per-block",
      "Number of threads in a thread block (dimension X; thread blocks are 2D; dimension Y is hard-coded to 8).",
      "all" );
   config.addEntryEnum( "8" );
   config.addEntryEnum( "16" );
   config.addEntryEnum( "32" );
   config.addEntryEnum( "64" );
   config.addEntryEnum( "128" );
   config.addEntryEnum( "all" );
   config.addEntry< String >( "machine", "What machine the benchmark is being run on.", "rci-amd-epyc-7543-32c" );
   config.addEntry< bool >( "max-diff", "Compute the max. abs. difference between A*X and B.", true );
   config.addEntry< bool >(
      "save-computed-vector", "Save the computed vector into a <mtx_name>.vec_result_<precision> file.", false );
   config.addEntry< bool >( "partial-pivoting", "Solve with partial pivoting", true );

   config.addDelimiter( "Device settings:" );
   Devices::Host::configSetup( config );
   Devices::Cuda::configSetup( config );
}

int
main( int argc, char* argv[] )
{
   Config::ParameterContainer parameters;
   Config::ConfigDescription conf_desc;

   setupConfig( conf_desc );

   if( ! parseCommandLine( argc, argv, conf_desc, parameters ) )
      return EXIT_FAILURE;

   if( ! Devices::Host::setup( parameters ) || ! Devices::Cuda::setup( parameters ) )
      return EXIT_FAILURE;

   const String& inputFileName = parameters.getParameter< String >( "input-file" );
   const String& logFileName = parameters.getParameter< String >( "log-file" );
   const String& outputMode = parameters.getParameter< String >( "output-mode" );
   const String& precision = parameters.getParameter< String >( "precision" );
   const int& loops = parameters.getParameter< int >( "loops" );
   const int& verbose = parameters.getParameter< int >( "verbose" );

   if( inputFileName.empty() ) {
      std::cerr << "ERROR: Input file name is required." << '\n';
      return EXIT_FAILURE;
   }

   if( std::filesystem::exists( logFileName.getString() ) ) {
      std::cout << "Log file " << logFileName << " exists and ";
      if( outputMode == "append" )
         std::cout << "new logs will be appended." << '\n';
      else
         std::cout << "will be overwritten." << '\n';
   }

   auto mode = std::ios::out;
   if( outputMode == "append" )
      mode |= std::ios::app;
   std::ofstream logFile( logFileName, mode );

   Decomposition::Benchmark::BenchmarkType benchmark( logFile, loops, verbose );

   std::map< std::string, std::string > metadata = Benchmarks::getHardwareMetadata();
   Benchmarks::writeMapAsJson( metadata, logFileName, ".metadata.json" );

   if( precision == "all" || precision == "float" ) {
      runSolversBenchmarks< float >( benchmark, parameters, inputFileName, logFile, verbose );
   }
   if( precision == "all" || precision == "double" ) {
      runSolversBenchmarks< double >( benchmark, parameters, inputFileName, logFile, verbose );
   }
   return EXIT_SUCCESS;
}
