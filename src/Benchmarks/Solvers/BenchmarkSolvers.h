#pragma once

#include <TNL/Matrices/MatrixReader.h>
#include <TNL/String.h>

#include <Decomposition/Containers/MatrixFormat.h>
#include <Decomposition/LU/Decomposers/CroutMethod.h>
#include <Decomposition/LU/Decomposers/GaussianEliminationMethod.h>

#include <Decomposition/LU/Solvers/CuBLAStrsmWrapper.h>
#include <Decomposition/LU/Solvers/CuSolverDnXgetrsWrapper.h>
#include <Decomposition/LU/Solvers/IterativeSolver.h>
#include <Decomposition/LU/Solvers/SequentialSolver.h>

#include "Benchmarks/utils/Benchmark.h"
#include "Benchmarks/Results/Solvers/SolverBenchmarkResult.h"
#include "Benchmarks/Results/Solvers/SolverBenchmarkResultLoaderHost.h"

using namespace Decomposition::LU::Solvers;

namespace Decomposition::Benchmark {

using MatrixFormat = Decomposition::Containers::MatrixFormat;

template< typename Index, typename Device >
using VectorType = TNL::Containers::Vector< Index, Device, Index >;

template< typename Real, typename Index >
using CudaMatrixRowMajorType =
   TNL::Matrices::DenseMatrix< Real, TNL::Devices::Cuda, Index, TNL::Algorithms::Segments::RowMajorOrder >;

template< typename Real, typename Index >
using CudaMatrixColumnMajorType =
   TNL::Matrices::DenseMatrix< Real, TNL::Devices::Cuda, Index, TNL::Algorithms::Segments::ColumnMajorOrder >;

template< typename Real, typename Index >
using HostMatrixType = TNL::Matrices::DenseMatrix< Real, TNL::Devices::Host, Index, TNL::Algorithms::Segments::RowMajorOrder >;

using BenchmarkType = Decomposition::Benchmark::Benchmark;
using CroutMethod = Decomposition::LU::Decomposers::CroutMethod<>;

template< typename Matrix >
using BenchmarkResultType = SolverBenchmarkResult< Matrix >;
using SolverBenchmarkResultLoaderHost = Results::Solvers::SolverBenchmarkResultLoaderHost;

std::tuple< bool, TNL::String, bool, bool, bool, int >
getParameters( const TNL::Config::ParameterContainer& parameters )
{
   return std::make_tuple( parameters.getParameter< bool >( "verbose-MReader" ),
                           parameters.getParameter< TNL::String >( "threads-per-block" ),
                           parameters.getParameter< bool >( "max-diff" ),
                           parameters.getParameter< bool >( "partial-pivoting" ),
                           parameters.getParameter< bool >( "save-computed-vector" ),
                           parameters.getParameter< int >( "num-right-hand-sides" ) );
}

template< typename Solver,
          typename BenchmarkMatrix,
          typename VecDevice = typename BenchmarkMatrix::DeviceType,
          typename Index = typename BenchmarkMatrix::IndexType,
          typename HostMatrix,
          typename HostRealVector,
          typename HostVector >
void
benchmarkSolver( BenchmarkType& benchmark,
                 const HostMatrix& A_res_host,
                 const HostMatrix& LU_host,
                 const HostRealVector& X_host,
                 const HostRealVector& B_host,
                 const HostVector& piv_host,
                 const bool& maxDiff )
{
   using Device = typename BenchmarkMatrix::DeviceType;
   using Vector = VectorType< Index, VecDevice >;

   BenchmarkMatrix LU;
   try {
      LU = LU_host;
   }
   catch( const std::exception& e ) {
      benchmark.addErrorMessage( "Unable to copy the matrix on " + getPerformer< BenchmarkMatrix >() + ": "
                                 + TNL::String( e.what() ) );
      return;
   }

   // X will hold values of B on input
   BenchmarkMatrix X;
   BenchmarkMatrix B;
   Vector piv;
   try {
      B = B_host;
      X = B;
      piv = piv_host;
   }
   catch( const std::exception& e ) {
      benchmark.addErrorMessage( "Unable to copy the matrices on " + getPerformer< BenchmarkMatrix >() + ": "
                                 + TNL::String( e.what() ) );
      return;
   }

   BenchmarkResultType< BenchmarkMatrix > benchmarkResults( A_res_host, X_host, X, B_host, maxDiff );

   benchmark.setMetadataElement(
      { "format", Solver::template getSolverName< typename BenchmarkMatrix::DeviceType >( ! piv.empty() ) } );

   auto resetMatrix = [ & ]()
   {
      X = B;
   };

   auto solve = [ & ]()
   {
      Solver::solve( LU, X, piv );
   };

   benchmark.time< Device >( resetMatrix, getPerformer< BenchmarkMatrix >(), solve, benchmarkResults );
}

template< typename Real = double, typename Index = int >
void
benchmarkSolvers( Decomposition::Benchmark::BenchmarkType& benchmark,
                  const TNL::Config::ParameterContainer& parameters,
                  const TNL::String& inputFilePath,
                  std::ostream& output,
                  const int& verbose )
{
   using HostMatrix = HostMatrixType< Real, Index >;
   using Host = TNL::Devices::Host;
   using HostVector = VectorType< Index, Host >;

   // TODO: Replace the next 3 lines once c++20 is used (see https://gitlab.com/tnl-project/decomposition/-/issues/44)
   bool verboseMR;
   bool maxDiff;
   bool partialPivoting;
   bool saveComputedVector;
   TNL::String threads;
   int nrhs;
   std::tie( verboseMR, threads, maxDiff, partialPivoting, saveComputedVector, nrhs ) = getParameters( parameters );

   // Allocate structures required to solve Ax=B ( (LU)X=B )

   // - Matrices
   HostMatrix A_host;
   HostMatrix LU_host;

   TNL::Matrices::MatrixReader< HostMatrix >::readMtx( inputFilePath, A_host, verboseMR );
   LU_host = A_host;
   HostMatrix B( A_host.getRows(), nrhs );
   B.setValue( 1.0 );
   HostMatrix X_host;
   X_host.setLike( B );

   // - Vectors
   HostVector piv( 0 );
   // If partial pivoting is required, set the proper size and fill up piv
   if( partialPivoting ) {
      piv.setSize( LU_host.getRows() );
   }

   // Decompose matrix A_host stored in LU -> result in LU -> stored L*U into to A_res to use for benchmark result verification
   CroutMethod::decompose( LU_host, piv );
   HostMatrix A_res;
   HostMatrix L;
   HostMatrix U;
   std::tie( L, U ) = BaseDecomposer::getUnpackedMatrices( LU_host, Containers::MatrixFormat::UnitDiagIn_U );
   A_res.getMatrixProduct( L, U );

   if( partialPivoting ) {
      BaseDecomposer::undoOrderInMatrixAccordingTo( A_res, piv );
   }

   // Set up benchmark metadata
   // Get input matrix filename from the path
   TNL::String LU_matrixName = inputFilePath.substr( inputFilePath.find_last_of( "/\\" ) + 1 );

   benchmark.setMetadataColumns( {
      { "matrix name",                                             LU_matrixName },
      {   "precision",                                    TNL::getType< Real >() },
      {        "rows",                 TNL::convertToString( LU_host.getRows() ) },
      {     "columns",              TNL::convertToString( LU_host.getColumns() ) },
      {    "nonzeros", TNL::convertToString( LU_host.getNonzeroElementsCount() ) },
      {        "nrhs",                              TNL::convertToString( nrhs ) },
   } );
   benchmark.setMetadataWidths( {
      { "matrix name", 32 },
      {        "nrhs",  6 },
      {      "format", 32 },
   } );

   using Solver = SequentialSolver<>;

   benchmark.setMetadataElement( { "format", Solver::template getSolverName< Host >( ! piv.empty() ) } );

   BenchmarkResultType< HostMatrix > hostBenchmarkResults( A_res, X_host, X_host, B, maxDiff );

   // Check if benchmark result exist for the input matrix
   bool benchmarkResultFound =
      SolverBenchmarkResultLoaderHost ::loadResult< Solver >( hostBenchmarkResults,
                                                              LU_matrixName,
                                                              parameters.getParameter< TNL::String >( "machine" ),
                                                              partialPivoting,
                                                              parameters.getParameter< int >( "loops" ) );

   // If saved result not found then perform the benchmark normally
   if( ! benchmarkResultFound ) {
      // X holds the values of B on input
      X_host = B;

      auto resetHostMatrix = [ & ]()
      {
         X_host = B;
      };

      auto SolveHost = [ & ]()
      {
         Solver::solve( LU_host, X_host, piv );
      };

      benchmark.time< Host >( resetHostMatrix, getPerformer< HostMatrix >(), SolveHost, hostBenchmarkResults );

      // Save the resulting X vector
      if( saveComputedVector ) {
         TNL::String X_filename = inputFilePath + "_X_result_" + TNL::String( TNL::getType< Real >() );
         X_host.save( X_filename );
      }
   }
   else {
      // Load the saved CPU benchmark result
      X_host.load( inputFilePath + "_X_result_" + TNL::String( TNL::getType< Real >() ) );
      benchmark.logMockResult( getPerformer< HostMatrix >(), hostBenchmarkResults );
   }

   // Run Solver benchmark for Iterative Solver on the CPU
   if( parameters.getParameter< bool >( "with-iterative-solver-cpu-benchmark" ) ) {
      benchmarkSolver< IterativeSolver<>, HostMatrix >( benchmark, A_res, LU_host, X_host, B, piv, maxDiff );
   }

#ifdef __CUDACC__
   // Run Solver benchmark for Iterative Solver on the GPU using different no. threads in a thread block
   if( parameters.getParameter< bool >( "with-iterative-solver-gpu-benchmark" ) ) {
      if( threads == "all" || threads == "8" ) {
         benchmarkSolver< IterativeSolver< 8 >, CudaMatrixRowMajorType< Real, Index >, TNL::Devices::Host >(
            benchmark, A_res, LU_host, X_host, B, piv, maxDiff );
      }
      if( threads == "all" || threads == "16" ) {
         benchmarkSolver< IterativeSolver< 16 >, CudaMatrixRowMajorType< Real, Index >, TNL::Devices::Host >(
            benchmark, A_res, LU_host, X_host, B, piv, maxDiff );
      }
      if( threads == "all" || threads == "32" ) {
         benchmarkSolver< IterativeSolver< 32 >, CudaMatrixRowMajorType< Real, Index >, TNL::Devices::Host >(
            benchmark, A_res, LU_host, X_host, B, piv, maxDiff );
      }
      if( threads == "all" || threads == "64" ) {
         benchmarkSolver< IterativeSolver< 64 >, CudaMatrixRowMajorType< Real, Index >, TNL::Devices::Host >(
            benchmark, A_res, LU_host, X_host, B, piv, maxDiff );
      }
      if( threads == "all" || threads == "128" ) {
         benchmarkSolver< IterativeSolver< 128 >, CudaMatrixRowMajorType< Real, Index >, TNL::Devices::Host >(
            benchmark, A_res, LU_host, X_host, B, piv, maxDiff );
      }
   }

   // Run Solver benchmark for cuBLAS's wrapped trsm on the GPU
   if( parameters.getParameter< bool >( "with-cublas-trsm-wrapper-gpu-benchmark" ) ) {
      benchmarkSolver< CuBLAStrsmWrapper<>, CudaMatrixColumnMajorType< Real, Index >, TNL::Devices::Host >(
         benchmark, A_res, LU_host, X_host, B, piv, maxDiff );
   }

   // Run Solver benchmark for CuSolver's wrapped getrs on the GPU
   if( parameters.getParameter< bool >( "with-cusolver-getrs-wrapper-gpu-benchmark" ) ) {
      // Decompose using GaussianEliminationMethod -> unit diagonal in L.
      LU_host = A_host;
      LU::Decomposers::GaussianEliminationMethod::decompose( LU_host, piv );
      std::tie( L, U ) = BaseDecomposer::getUnpackedMatrices( LU_host, Containers::MatrixFormat::UnitDiagIn_L );
      A_res.getMatrixProduct( L, U );
      BaseDecomposer::undoOrderInMatrixAccordingTo( A_res, piv );

      benchmarkSolver< CuSolverDnXgetrsWrapper<>, CudaMatrixColumnMajorType< Real, Index >, TNL::Devices::Cuda, int64_t >(
         benchmark, A_res, LU_host, X_host, B, piv, maxDiff );
   }
#endif
}

}  // namespace Decomposition::Benchmark
