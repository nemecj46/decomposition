#pragma once

#include <TNL/Devices/Host.h>
#include <TNL/String.h>

#include <Decomposition/LU/Decomposers/CroutMethod.h>

#include "Benchmarks/Results/MachineResults.h"

using namespace Decomposition::LU::Decomposers;

namespace Decomposition::Benchmark::Results::Decomposers::CMPP {

template< typename Precision >
class RCI_AMD_EPYC_7543_32C : public MachineResults< Precision >
{
   using Tuple = typename MachineResults< Precision >::TupleType;
   using Map = typename MachineResults< Precision >::MapType;

private:
   Map
   getBenchmarkResults_double()
   {
      return Map( {
  // 50 matrices - time, stddev, speedup, loops
         {                          "685_bus.mtx",    std::make_tuple( 0.08236229000000,  0.00004685883000, 0.0, 10 ) },
         {                            "b2_ss.mtx",    std::make_tuple( 0.34123760000000,  0.00128823200000, 0.0, 10 ) },
         {                          "bayer06.mtx",   std::make_tuple( 13.57390000000000,  0.83344890000000, 0.0, 10 ) },
         {                          "bayer09.mtx",    std::make_tuple( 9.67235200000000,  0.02518062000000, 0.0, 10 ) },
         {                         "bcsstk01.mtx",    std::make_tuple( 0.00001915930000,  0.00000089033560, 0.0, 10 ) },
         {                         "bcsstk02.mtx",    std::make_tuple( 0.00005360370000,  0.00000238883600, 0.0, 10 ) },
         {                         "bcsstk03.mtx",    std::make_tuple( 0.00026618370000,  0.00001067842000, 0.0, 10 ) },
         {                             "bp_0.mtx",    std::make_tuple( 0.14570230000000,  0.00049671280000, 0.0, 10 ) },
         {                          "bp_1400.mtx",    std::make_tuple( 0.14586960000000,  0.00104802000000, 0.0, 10 ) },
         {                       "circuit204.mtx",    std::make_tuple( 0.29212720000000,  0.00121772300000, 0.0, 10 ) },
         {                     "crout_python.mtx",    std::make_tuple( 0.00000032100000,  0.00000008849984, 0.0, 10 ) },
         {                           "garon1.mtx",   std::make_tuple( 10.78546000000000,  0.03078135000000, 0.0, 10 ) },
         {                          "lns_131.mtx",    std::make_tuple( 0.00043183170000,  0.00000509652300, 0.0, 10 ) },
         {                         "orani678.mtx",    std::make_tuple( 4.43345800000000,  0.02316788000000, 0.0, 10 ) },
         {                       "poc-20_2_2.mtx",    std::make_tuple( 0.54661000000000,  0.00119783500000, 0.0, 10 ) },
         {                       "poc-24_2_2.mtx",    std::make_tuple( 1.60715300000000,  0.01274010000000, 0.0, 10 ) },
         {                       "poc-28_2_2.mtx",    std::make_tuple( 4.09215200000000,  0.16077590000000, 0.0, 10 ) },
         {                       "poc-32_2_2.mtx",   std::make_tuple( 10.08338000000000,  0.25247210000000, 0.0, 10 ) },
         {              "poc-8_4_2-938719427.mtx",    std::make_tuple( 0.00261339600000,  0.00000384341100, 0.0, 10 ) },
         {              "poc-8_4_2-938719741.mtx",    std::make_tuple( 0.00597466000000,  0.00003738367000, 0.0, 10 ) },
         {              "poc-8_4_2-938719829.mtx",    std::make_tuple( 0.00618117100000,  0.00000641672900, 0.0, 10 ) },
         {              "spaceShuttleEntry_3.mtx",    std::make_tuple( 1.66036300000000,  0.01112471000000, 0.0, 10 ) },
         {                         "west0067.mtx",    std::make_tuple( 0.00005546070000,  0.00000125178500, 0.0, 10 ) },
         {                         "west0156.mtx",    std::make_tuple( 0.00077002680000,  0.00001349986000, 0.0, 10 ) },
         {                         "west0381.mtx",    std::make_tuple( 0.01333423000000,  0.00008098665000, 0.0, 10 ) },
         { "Cejka10135_<-1000-1000>_pivoting.mtx", std::make_tuple( 1282.02800000000000,  6.81275200000000, 0.0, 10 ) },
         {  "Cejka1157_<-1000-1000>_pivoting.mtx",    std::make_tuple( 0.41138660000000,  0.00313159100000, 0.0, 10 ) },
         {  "Cejka2599_<-1000-1000>_pivoting.mtx",    std::make_tuple( 4.99711000000000,  0.01836415000000, 0.0, 10 ) },
         {  "Cejka3839_<-1000-1000>_pivoting.mtx",   std::make_tuple( 20.78894000000000,  0.03031316000000, 0.0, 10 ) },
         {  "Cejka4156_<-1000-1000>_pivoting.mtx",   std::make_tuple( 22.81988000000000,  0.01068492000000, 0.0, 10 ) },
         {  "Cejka5069_<-1000-1000>_pivoting.mtx",   std::make_tuple( 51.96920000000000,  0.03548842000000, 0.0, 10 ) },
         {   "Cejka558_<-1000-1000>_pivoting.mtx",    std::make_tuple( 0.04314733000000,  0.00005293191000, 0.0, 10 ) },
         {  "Cejka6075_<-1000-1000>_pivoting.mtx",   std::make_tuple( 90.55185000000000,  0.04298461000000, 0.0, 10 ) },
         {  "Cejka6192_<-1000-1000>_pivoting.mtx",   std::make_tuple( 84.74306000000000,  0.28836500000000, 0.0, 10 ) },
         {  "Cejka6574_<-1000-1000>_pivoting.mtx",  std::make_tuple( 102.02150000000000,  0.08058077000000, 0.0, 10 ) },
         {   "Cejka665_<-1000-1000>_pivoting.mtx",    std::make_tuple( 0.07578419000000,  0.00008438158000, 0.0, 10 ) },
         {  "Cejka7510_<-1000-1000>_pivoting.mtx",  std::make_tuple( 157.15500000000000,  0.09862475000000, 0.0, 10 ) },
         {  "Cejka7972_<-1000-1000>_pivoting.mtx",  std::make_tuple( 171.49200000000000,  0.15706270000000, 0.0, 10 ) },
         {  "Cejka8385_<-1000-1000>_pivoting.mtx",  std::make_tuple( 250.66010000000000,  0.57739090000000, 0.0, 10 ) },
         {  "Cejka9234_<-1000-1000>_pivoting.mtx",  std::make_tuple( 308.50800000000000,  1.17104300000000, 0.0, 10 ) },
         {                             "c-22.mtx",   std::make_tuple( 17.09761000000000,  0.09044045000000, 0.0, 10 ) },
         {                         "exdata_1.mtx",   std::make_tuple( 88.14956000000000,  0.16518390000000, 0.0, 10 ) },
         {                               "fp.mtx",  std::make_tuple( 149.01080000000000,  0.18857810000000, 0.0, 10 ) },
         {                "freeFlyingRobot_9.mtx",   std::make_tuple( 39.49096000000000,  0.13339990000000, 0.0, 10 ) },
         {                           "heart1.mtx",   std::make_tuple( 16.40102000000000,  0.04490537000000, 0.0, 10 ) },
         {                         "msc10848.mtx",  std::make_tuple( 653.46090000000000, 67.10716000000000, 0.0, 10 ) },
         {                             "nd3k.mtx",  std::make_tuple( 261.35780000000000,  0.23125230000000, 0.0, 10 ) },
         {                           "sinc15.mtx",  std::make_tuple( 536.82410000000000,  0.36130090000000, 0.0, 10 ) },
         {                     "TSC_OPF_1047.mtx",  std::make_tuple( 182.46340000000000,  0.32396310000000, 0.0, 10 ) },
         {                 "TSOPF_FS_b162_c1.mtx",  std::make_tuple( 484.36690000000000,  0.49148480000000, 0.0, 10 ) }
      } );
   }

   Map
   getBenchmarkResults_float()
   {
      return Map( {
  // 50 matrices - time, stddev, speedup, loops
         {                          "685_bus.mtx",   std::make_tuple( 0.08374395000000,  0.00011089010000, 0.0, 10 ) },
         {                            "b2_ss.mtx",   std::make_tuple( 0.34461470000000,  0.00079443040000, 0.0, 10 ) },
         {                          "bayer06.mtx",   std::make_tuple( 7.84439400000000,  0.02514559000000, 0.0, 10 ) },
         {                          "bayer09.mtx",   std::make_tuple( 8.05405800000000,  0.00805244700000, 0.0, 10 ) },
         {                         "bcsstk01.mtx",   std::make_tuple( 0.00002044930000,  0.00000112799900, 0.0, 10 ) },
         {                         "bcsstk02.mtx",   std::make_tuple( 0.00005495870000,  0.00000109287000, 0.0, 10 ) },
         {                         "bcsstk03.mtx",   std::make_tuple( 0.00027125060000,  0.00000571908500, 0.0, 10 ) },
         {                             "bp_0.mtx",   std::make_tuple( 0.14538660000000,  0.00025482490000, 0.0, 10 ) },
         {                          "bp_1400.mtx",   std::make_tuple( 0.14523930000000,  0.00067325460000, 0.0, 10 ) },
         {                       "circuit204.mtx",   std::make_tuple( 0.29886260000000,  0.00191508700000, 0.0, 10 ) },
         {                     "crout_python.mtx",   std::make_tuple( 0.00000032200000,  0.00000011621820, 0.0, 10 ) },
         {                           "garon1.mtx",   std::make_tuple( 9.76665100000000,  0.01448299000000, 0.0, 10 ) },
         {                          "lns_131.mtx",   std::make_tuple( 0.00045800860000,  0.00000613641500, 0.0, 10 ) },
         {                         "orani678.mtx",   std::make_tuple( 4.40335800000000,  0.00396940000000, 0.0, 10 ) },
         {                       "poc-20_2_2.mtx",   std::make_tuple( 0.55459440000000,  0.00302756000000, 0.0, 10 ) },
         {                       "poc-24_2_2.mtx",   std::make_tuple( 1.60992400000000,  0.00458423800000, 0.0, 10 ) },
         {                       "poc-28_2_2.mtx",   std::make_tuple( 3.97008500000000,  0.01035489000000, 0.0, 10 ) },
         {                       "poc-32_2_2.mtx",   std::make_tuple( 8.77516100000000,  0.00993859700000, 0.0, 10 ) },
         {              "poc-8_4_2-938719427.mtx",   std::make_tuple( 0.00276639600000,  0.00002157564000, 0.0, 10 ) },
         {              "poc-8_4_2-938719741.mtx",   std::make_tuple( 0.00624661300000,  0.00003583734000, 0.0, 10 ) },
         {              "poc-8_4_2-938719829.mtx",   std::make_tuple( 0.00602637300000,  0.00002421190000, 0.0, 10 ) },
         {              "spaceShuttleEntry_3.mtx",   std::make_tuple( 1.67271800000000,  0.00866396900000, 0.0, 10 ) },
         {                         "west0067.mtx",   std::make_tuple( 0.00005784190000,  0.00000169957900, 0.0, 10 ) },
         {                         "west0156.mtx",   std::make_tuple( 0.00088018120000,  0.00001537948000, 0.0, 10 ) },
         {                         "west0381.mtx",   std::make_tuple( 0.01304311000000,  0.00000872728800, 0.0, 10 ) },
         { "Cejka10135_<-1000-1000>_pivoting.mtx", std::make_tuple( 374.23700000000000,  0.29929790000000, 0.0, 10 ) },
         {  "Cejka1157_<-1000-1000>_pivoting.mtx",   std::make_tuple( 0.41490050000000,  0.00093912610000, 0.0, 10 ) },
         {  "Cejka2599_<-1000-1000>_pivoting.mtx",   std::make_tuple( 4.86804700000000,  0.01827486000000, 0.0, 10 ) },
         {  "Cejka3839_<-1000-1000>_pivoting.mtx",  std::make_tuple( 17.24717000000000,  0.02046039000000, 0.0, 10 ) },
         {  "Cejka4156_<-1000-1000>_pivoting.mtx",  std::make_tuple( 21.48282000000000,  0.02904239000000, 0.0, 10 ) },
         {  "Cejka5069_<-1000-1000>_pivoting.mtx",  std::make_tuple( 46.19847000000000,  0.12927850000000, 0.0, 10 ) },
         {   "Cejka558_<-1000-1000>_pivoting.mtx",   std::make_tuple( 0.04411936000000,  0.00009102364000, 0.0, 10 ) },
         {  "Cejka6075_<-1000-1000>_pivoting.mtx",  std::make_tuple( 79.25742000000000,  0.56390220000000, 0.0, 10 ) },
         {  "Cejka6192_<-1000-1000>_pivoting.mtx",  std::make_tuple( 69.83460000000000,  0.03391934000000, 0.0, 10 ) },
         {  "Cejka6574_<-1000-1000>_pivoting.mtx",  std::make_tuple( 90.25901000000000,  0.03777288000000, 0.0, 10 ) },
         {   "Cejka665_<-1000-1000>_pivoting.mtx",   std::make_tuple( 0.07644317000000,  0.00026265500000, 0.0, 10 ) },
         {  "Cejka7510_<-1000-1000>_pivoting.mtx", std::make_tuple( 144.84850000000000,  0.47360020000000, 0.0, 10 ) },
         {  "Cejka7972_<-1000-1000>_pivoting.mtx", std::make_tuple( 157.82740000000000,  0.39768850000000, 0.0, 10 ) },
         {  "Cejka8385_<-1000-1000>_pivoting.mtx", std::make_tuple( 276.41940000000000,  1.22457700000000, 0.0, 10 ) },
         {  "Cejka9234_<-1000-1000>_pivoting.mtx", std::make_tuple( 268.39930000000000,  0.75487880000000, 0.0, 10 ) },
         {                             "c-22.mtx",  std::make_tuple( 15.26797000000000,  0.01350377000000, 0.0, 10 ) },
         {                         "exdata_1.mtx",  std::make_tuple( 77.03774000000000,  0.42554280000000, 0.0, 10 ) },
         {                               "fp.mtx", std::make_tuple( 159.89400000000000,  1.91711600000000, 0.0, 10 ) },
         {                "freeFlyingRobot_9.mtx",  std::make_tuple( 36.14866000000000,  0.14875910000000, 0.0, 10 ) },
         {                           "heart1.mtx",  std::make_tuple( 12.60662000000000,  0.04926468000000, 0.0, 10 ) },
         {                         "msc10848.mtx", std::make_tuple( 481.67220000000000, 25.84531000000000, 0.0, 10 ) },
         {                             "nd3k.mtx", std::make_tuple( 225.91540000000000,  1.14731800000000, 0.0, 10 ) },
         {                           "sinc15.mtx", std::make_tuple( 517.04490000000000,  1.93635400000000, 0.0, 10 ) },
         {                     "TSC_OPF_1047.mtx", std::make_tuple( 173.64100000000000,  0.55186900000000, 0.0, 10 ) },
         {                 "TSOPF_FS_b162_c1.mtx", std::make_tuple( 443.71320000000000,  2.11747700000000, 0.0, 10 ) }
      } );
   }

public:
   RCI_AMD_EPYC_7543_32C()
   : MachineResults< Precision >( CroutMethod<>::template getDecomposerName< TNL::Devices::Host >( true ),
                                  getBenchmarkResults_double(),
                                  getBenchmarkResults_float() )
   {}
};

}  // namespace Decomposition::Benchmark::Results::Decomposers::CMPP
