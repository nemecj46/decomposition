#pragma once

#include <TNL/Devices/Host.h>
#include <TNL/String.h>

#include <Decomposition/LU/Decomposers/CroutMethod.h>

#include "Benchmarks/Results/MachineResults.h"

using namespace Decomposition::LU::Decomposers;

namespace Decomposition::Benchmark::Results::Decomposers::CM {

template< typename Precision >
class GP7 : public MachineResults< Precision >
{
   using Tuple = typename MachineResults< Precision >::TupleType;
   using Map = typename MachineResults< Precision >::MapType;

private:
   Map
   getBenchmarkResults_double()
   {
      return Map( {
  // 53 matrices
         {               "Chem97ZtZ.mtx",    std::make_tuple( 9.84787200000000,   0.01058599000000, 0.0, 10 ) },
         {                "crystk01.mtx",  std::make_tuple( 149.66930000000000,   0.03915784000000, 0.0, 10 ) },
         {                "s1rmq4m1.mtx",  std::make_tuple( 232.30980000000000,   0.02637972000000, 0.0, 10 ) },
         {                "s2rmq4m1.mtx",  std::make_tuple( 235.32530000000000,   1.30550900000000, 0.0, 10 ) },
         {                "problem1.mtx",    std::make_tuple( 0.01800236000000,   0.00019382450000, 0.0, 10 ) },
         {                     "ex5.mtx",    std::make_tuple( 0.00000376370000,   0.00000030240340, 0.0, 10 ) },
         {                   "add20.mtx",    std::make_tuple( 7.01276200000000,   0.01014627000000, 0.0, 10 ) },
         {                   "add32.mtx",  std::make_tuple( 158.78230000000000,   0.04505096000000, 0.0, 10 ) },
         {                "1138_bus.mtx",    std::make_tuple( 0.39157790000000,   0.00545256700000, 0.0, 10 ) },
         {                 "494_bus.mtx",    std::make_tuple( 0.03003513000000,   0.00006599262000, 0.0, 10 ) },
         {                 "662_bus.mtx",    std::make_tuple( 0.07396621000000,   0.00013275340000, 0.0, 10 ) },
         {                 "685_bus.mtx",    std::make_tuple( 0.08262030000000,   0.00021581300000, 0.0, 10 ) },
         {                  "arc130.mtx",    std::make_tuple( 0.00042674000000,   0.00000117328000, 0.0, 10 ) },
         {                "bcsstk01.mtx",    std::make_tuple( 0.00001949460000,   0.00000027587360, 0.0, 10 ) },
         {                "bcsstk02.mtx",    std::make_tuple( 0.00005056430000,   0.00000028761360, 0.0, 10 ) },
         {                "bcsstk03.mtx",    std::make_tuple( 0.00026611790000,   0.00000033414780, 0.0, 10 ) },
         {                "bcsstk04.mtx",    std::make_tuple( 0.00045325430000,   0.00001117569000, 0.0, 10 ) },
         {                "bcsstk05.mtx",    std::make_tuple( 0.00073718810000,   0.00001227191000, 0.0, 10 ) },
         {                "bcsstk06.mtx",    std::make_tuple( 0.01827620000000,   0.00004961223000, 0.0, 10 ) },
         {                "bcsstk07.mtx",    std::make_tuple( 0.01830773000000,   0.00019042630000, 0.0, 10 ) },
         {                "bcsstk08.mtx",    std::make_tuple( 0.32662190000000,   0.00032492610000, 0.0, 10 ) },
         {                "bcsstk09.mtx",    std::make_tuple( 0.34954310000000,   0.00045936110000, 0.0, 10 ) },
         {                "bcsstk10.mtx",    std::make_tuple( 0.33829120000000,   0.00026553870000, 0.0, 10 ) },
         {                "bcsstk11.mtx",    std::make_tuple( 0.85934890000000,   0.00094328200000, 0.0, 10 ) },
         {                "bcsstk12.mtx",    std::make_tuple( 0.85727860000000,   0.00055746950000, 0.0, 10 ) },
         {                "bcsstk13.mtx",    std::make_tuple( 3.27535600000000,   0.00846980700000, 0.0, 10 ) },
         {                "bcsstk14.mtx",    std::make_tuple( 2.18869900000000,   0.00766884500000, 0.0, 10 ) },
         {                "bcsstk15.mtx",   std::make_tuple( 64.21702000000000,   0.01673679000000, 0.0, 10 ) },
         {                "bcsstk16.mtx",  std::make_tuple( 149.26600000000000,   0.02244733000000, 0.0, 10 ) },
         {                "bcsstk19.mtx",    std::make_tuple( 0.14237780000000,   0.00017837380000, 0.0, 10 ) },
         {                "bcsstk20.mtx",    std::make_tuple( 0.02877795000000,   0.00025125650000, 0.0, 10 ) },
         {                "bcsstk21.mtx",   std::make_tuple( 43.59313000000000,   0.00876105200000, 0.0, 10 ) },
         {                "bcsstk22.mtx",    std::make_tuple( 0.00051687590000,   0.00001802620000, 0.0, 10 ) },
         {                "bcsstk23.mtx",   std::make_tuple( 24.28490000000000,   0.01497462000000, 0.0, 10 ) },
         {                "bcsstk24.mtx",   std::make_tuple( 42.97807000000000,   0.01083967000000, 0.0, 10 ) },
         {                "bcsstk26.mtx",    std::make_tuple( 2.45525200000000,   0.01286426000000, 0.0, 10 ) },
         {                "bcsstk27.mtx",    std::make_tuple( 0.49068230000000,   0.00035742380000, 0.0, 10 ) },
         {                "bcsstk28.mtx",   std::make_tuple( 98.17538000000000,   0.01378376000000, 0.0, 10 ) },
         {                "bcsstm02.mtx",    std::make_tuple( 0.00005124170000,   0.00000042114050, 0.0, 10 ) },
         {                "bcsstm26.mtx",    std::make_tuple( 2.42468100000000,   0.00368008100000, 0.0, 10 ) },
         {                "bcsstm27.mtx",    std::make_tuple( 0.49206420000000,   0.00451990600000, 0.0, 10 ) },
         {          "LeGresley_2508.mtx",    std::make_tuple( 8.56767900000000,   0.00996399800000, 0.0, 10 ) },
         {                  "pivtol.mtx",    std::make_tuple( 0.00019424030000,   0.00000038234050, 0.0, 10 ) },
         {                     "Na5.mtx",  std::make_tuple( 285.55150000000000,   0.03399113000000, 0.0, 10 ) },
         {                "raefsky1.mtx",   std::make_tuple( 27.93310000000000,   0.01266908000000, 0.0, 10 ) },
         {                "raefsky2.mtx",   std::make_tuple( 27.79956000000000,   0.01473048000000, 0.0, 10 ) },
         {                  "swang1.mtx",   std::make_tuple( 25.89782000000000,   0.01026277000000, 0.0, 10 ) },
         {                  "swang2.mtx",   std::make_tuple( 25.48082000000000,   0.01025894000000, 0.0, 10 ) },
         {                   "wang1.mtx",   std::make_tuple( 17.67169000000000,   0.01291121000000, 0.0, 10 ) },
         {                   "wang2.mtx",   std::make_tuple( 17.49066000000000,   0.01436624000000, 0.0, 10 ) },
         {  "Cejka1641_<-1000-1000>.mtx",    std::make_tuple( 1.45146400000000,   0.00852605600000, 0.0, 10 ) },
         {  "Cejka2842_<-1000-1000>.mtx",   std::make_tuple( 15.71129000000000,   0.00977178800000, 0.0, 10 ) },
         {  "Cejka3352_<-1000-1000>.mtx",   std::make_tuple( 31.14461000000000,   0.28269140000000, 0.0, 10 ) },
 // 3 matricesBig
         {  "Cejka5943_<-1000-1000>.mtx",  std::make_tuple( 309.09300000000000,   0.09710069000000, 0.0, 10 ) },
         {  "Cejka7580_<-1000-1000>.mtx",  std::make_tuple( 776.21220000000000,   0.07393911000000, 0.0, 10 ) },
         { "Cejka10793_<-1000-1000>.mtx", std::make_tuple( 2646.12300000000000, 121.47600000000000, 0.0, 10 ) },
 // 7 matrices
         {                 "sts4098.mtx",   std::make_tuple( 77.27321000000000,   0.01817006000000, 0.0, 10 ) },
         {                "s3rmt3m3.mtx",  std::make_tuple( 210.81630000000000,   0.03941748000000, 0.0, 10 ) },
         {                    "poli.mtx",   std::make_tuple( 69.37545000000000,   0.03742810000000, 0.0, 10 ) },
         {                 "bundle1.mtx", std::make_tuple( 2430.84900000000000,   0.12987040000000, 0.0, 10 ) },
         {                      "fp.mtx",  std::make_tuple( 735.04950000000000,   9.96240500000000, 0.0, 10 ) },
         {               "rail_5177.mtx",  std::make_tuple( 189.34750000000000,   0.10873220000000, 0.0, 10 ) },
         {                    "c-31.mtx",  std::make_tuple( 206.38930000000000,   0.05868068000000, 0.0, 10 ) }
      } );
   }

   Map
   getBenchmarkResults_float()
   {
      return Map( {
  // 53 matrices
         {               "Chem97ZtZ.mtx",    std::make_tuple( 8.10661700000000,  0.00874798200000, 0.0, 10 ) },
         {                "crystk01.mtx",  std::make_tuple( 113.63770000000000,  0.01835664000000, 0.0, 10 ) },
         {                "s1rmq4m1.mtx",  std::make_tuple( 175.83920000000000,  0.38362100000000, 0.0, 10 ) },
         {                "s2rmq4m1.mtx",  std::make_tuple( 174.28210000000000,  0.01906222000000, 0.0, 10 ) },
         {                "problem1.mtx",    std::make_tuple( 0.01724502000000,  0.00004358987000, 0.0, 10 ) },
         {                     "ex5.mtx",    std::make_tuple( 0.00000378510000,  0.00000033536560, 0.0, 10 ) },
         {                   "add20.mtx",   std::make_tuple( 23.27032000000000,  0.01330377000000, 0.0, 10 ) },
         {                   "add32.mtx",  std::make_tuple( 117.59090000000000,  0.03385111000000, 0.0, 10 ) },
         {                "1138_bus.mtx",    std::make_tuple( 0.38285590000000,  0.00020378460000, 0.0, 10 ) },
         {                 "494_bus.mtx",    std::make_tuple( 0.02950421000000,  0.00007024868000, 0.0, 10 ) },
         {                 "662_bus.mtx",    std::make_tuple( 0.07323357000000,  0.00011605890000, 0.0, 10 ) },
         {                 "685_bus.mtx",    std::make_tuple( 0.08268839000000,  0.00212922400000, 0.0, 10 ) },
         {                  "arc130.mtx",    std::make_tuple( 0.00246320400000,  0.00001738259000, 0.0, 10 ) },
         {                "bcsstk01.mtx",    std::make_tuple( 0.00001978870000,  0.00000029842100, 0.0, 10 ) },
         {                "bcsstk02.mtx",    std::make_tuple( 0.00005088750000,  0.00000026339550, 0.0, 10 ) },
         {                "bcsstk03.mtx",    std::make_tuple( 0.00026462370000,  0.00001134623000, 0.0, 10 ) },
         {                "bcsstk04.mtx",    std::make_tuple( 0.00044672050000,  0.00001499840000, 0.0, 10 ) },
         {                "bcsstk05.mtx",    std::make_tuple( 0.00071313250000,  0.00001319245000, 0.0, 10 ) },
         {                "bcsstk06.mtx",    std::make_tuple( 0.01794072000000,  0.00017514800000, 0.0, 10 ) },
         {                "bcsstk07.mtx",    std::make_tuple( 0.01793808000000,  0.00021798480000, 0.0, 10 ) },
         {                "bcsstk08.mtx",    std::make_tuple( 0.32138730000000,  0.00033818860000, 0.0, 10 ) },
         {                "bcsstk09.mtx",    std::make_tuple( 0.33008260000000,  0.00055091400000, 0.0, 10 ) },
         {                "bcsstk10.mtx",    std::make_tuple( 0.33246190000000,  0.00027615170000, 0.0, 10 ) },
         {                "bcsstk11.mtx",    std::make_tuple( 0.84167900000000,  0.00042896360000, 0.0, 10 ) },
         {                "bcsstk12.mtx",    std::make_tuple( 0.84446910000000,  0.00041100560000, 0.0, 10 ) },
         {                "bcsstk13.mtx",    std::make_tuple( 2.78049500000000,  0.00639404900000, 0.0, 10 ) },
         {                "bcsstk14.mtx",    std::make_tuple( 1.96692000000000,  0.00058052720000, 0.0, 10 ) },
         {                "bcsstk15.mtx",   std::make_tuple( 48.22583000000000,  0.01191169000000, 0.0, 10 ) },
         {                "bcsstk16.mtx",  std::make_tuple( 110.83390000000000,  0.01428441000000, 0.0, 10 ) },
         {                "bcsstk19.mtx",    std::make_tuple( 0.13952460000000,  0.00013713670000, 0.0, 10 ) },
         {                "bcsstk20.mtx",    std::make_tuple( 0.02815115000000,  0.00004864862000, 0.0, 10 ) },
         {                "bcsstk21.mtx",   std::make_tuple( 33.39700000000000,  0.01579019000000, 0.0, 10 ) },
         {                "bcsstk22.mtx",    std::make_tuple( 0.00050503750000,  0.00000109863000, 0.0, 10 ) },
         {                "bcsstk23.mtx",   std::make_tuple( 19.79385000000000,  0.01288287000000, 0.0, 10 ) },
         {                "bcsstk24.mtx",   std::make_tuple( 32.04466000000000,  0.01093683000000, 0.0, 10 ) },
         {                "bcsstk26.mtx",    std::make_tuple( 2.23052300000000,  0.00248330100000, 0.0, 10 ) },
         {                "bcsstk27.mtx",    std::make_tuple( 0.47685990000000,  0.00025504000000, 0.0, 10 ) },
         {                "bcsstk28.mtx",   std::make_tuple( 75.95165000000000,  0.02042619000000, 0.0, 10 ) },
         {                "bcsstm02.mtx",    std::make_tuple( 0.00005227700000,  0.00000447024000, 0.0, 10 ) },
         {                "bcsstm26.mtx",    std::make_tuple( 2.22975300000000,  0.00399506900000, 0.0, 10 ) },
         {                "bcsstm27.mtx",    std::make_tuple( 0.47762750000000,  0.00029435190000, 0.0, 10 ) },
         {          "LeGresley_2508.mtx",    std::make_tuple( 7.29191500000000,  0.00815303000000, 0.0, 10 ) },
         {                  "pivtol.mtx",    std::make_tuple( 0.00019358870000,  0.00000025668570, 0.0, 10 ) },
         {                     "Na5.mtx",  std::make_tuple( 214.56980000000000,  1.34113700000000, 0.0, 10 ) },
         {                "raefsky1.mtx",   std::make_tuple( 22.07132000000000,  0.00836788800000, 0.0, 10 ) },
         {                "raefsky2.mtx",   std::make_tuple( 22.06241000000000,  0.01670033000000, 0.0, 10 ) },
         {                  "swang1.mtx",   std::make_tuple( 19.97301000000000,  0.00948969100000, 0.0, 10 ) },
         {                  "swang2.mtx",   std::make_tuple( 20.13289000000000,  0.01514214000000, 0.0, 10 ) },
         {                   "wang1.mtx",   std::make_tuple( 14.11449000000000,  0.00751547000000, 0.0, 10 ) },
         {                   "wang2.mtx",   std::make_tuple( 13.96659000000000,  0.00936593700000, 0.0, 10 ) },
         {  "Cejka1641_<-1000-1000>.mtx",    std::make_tuple( 1.35133100000000,  0.00058988510000, 0.0, 10 ) },
         {  "Cejka2842_<-1000-1000>.mtx",   std::make_tuple( 12.95092000000000,  0.01104541000000, 0.0, 10 ) },
         {  "Cejka3352_<-1000-1000>.mtx",   std::make_tuple( 25.47440000000000,  0.01207168000000, 0.0, 10 ) },
 // 3 matricesBig
         {  "Cejka5943_<-1000-1000>.mtx",  std::make_tuple( 241.50460000000000,  0.15968830000000, 0.0, 10 ) },
         {  "Cejka7580_<-1000-1000>.mtx",  std::make_tuple( 581.13190000000000,  1.11538600000000, 0.0, 10 ) },
         { "Cejka10793_<-1000-1000>.mtx", std::make_tuple( 2017.31300000000000,  3.02731800000000, 0.0, 10 ) },
 // 7 matrices
         {                 "sts4098.mtx",   std::make_tuple( 58.00440000000000,  0.01225206000000, 0.0, 10 ) },
         {                "s3rmt3m3.mtx",  std::make_tuple( 163.18000000000000,  0.04167250000000, 0.0, 10 ) },
         {                    "poli.mtx",   std::make_tuple( 51.14877000000000,  0.00899000600000, 0.0, 10 ) },
         {                 "bundle1.mtx", std::make_tuple( 1901.78100000000000, 14.49746000000000, 0.0, 10 ) },
         {                      "fp.mtx",  std::make_tuple( 844.30390000000000,  3.41645600000000, 0.0, 10 ) },
         {               "rail_5177.mtx",  std::make_tuple( 140.53620000000000,  0.03345193000000, 0.0, 10 ) },
         {                    "c-31.mtx",  std::make_tuple( 159.33170000000000,  0.04057524000000, 0.0, 10 ) }
      } );
   }

public:
   GP7()
   : MachineResults< Precision >( CroutMethod<>::template getDecomposerName< TNL::Devices::Host >( false ),
                                  getBenchmarkResults_double(),
                                  getBenchmarkResults_float() )
   {}
};

}  // namespace Decomposition::Benchmark::Results::Decomposers::CM
