#pragma once

#include <TNL/Devices/Host.h>
#include <TNL/String.h>

#include <Decomposition/LU/Decomposers/CroutMethod.h>

#include "Benchmarks/Results/MachineResults.h"

using namespace Decomposition::LU::Decomposers;

namespace Decomposition::Benchmark::Results::Decomposers::CM {

template< typename Precision >
class RCI_AMD_EPYC_7543_32C : public MachineResults< Precision >
{
   using Tuple = typename MachineResults< Precision >::TupleType;
   using Map = typename MachineResults< Precision >::MapType;

private:
   Map
   getBenchmarkResults_double()
   {
      return Map( {
  // 53 matrices
         {               "Chem97ZtZ.mtx",   std::make_tuple( 4.53843200000000, 0.01013921000000, 0.0, 10 ) },
         {                "crystk01.mtx",  std::make_tuple( 55.08981000000000, 0.12050630000000, 0.0, 10 ) },
         {                "s1rmq4m1.mtx",  std::make_tuple( 81.27488000000000, 0.21318340000000, 0.0, 10 ) },
         {                "s2rmq4m1.mtx",  std::make_tuple( 81.37886000000000, 0.71401880000000, 0.0, 10 ) },
         {                "problem1.mtx",   std::make_tuple( 0.01757790000000, 0.00013224500000, 0.0, 10 ) },
         {                     "ex5.mtx",   std::make_tuple( 0.00000347150000, 0.00000064121880, 0.0, 10 ) },
         {                   "add20.mtx",   std::make_tuple( 3.79774100000000, 0.01176585000000, 0.0, 10 ) },
         {                   "add32.mtx",  std::make_tuple( 50.93347000000000, 0.17768200000000, 0.0, 10 ) },
         {                "1138_bus.mtx",   std::make_tuple( 0.39315720000000, 0.00048111330000, 0.0, 10 ) },
         {                 "494_bus.mtx",   std::make_tuple( 0.02991211000000, 0.00007411628000, 0.0, 10 ) },
         {                 "662_bus.mtx",   std::make_tuple( 0.07347247000000, 0.00012443260000, 0.0, 10 ) },
         {                 "685_bus.mtx",   std::make_tuple( 0.08223280000000, 0.00004453445000, 0.0, 10 ) },
         {                  "arc130.mtx",   std::make_tuple( 0.00040527130000, 0.00000246661500, 0.0, 10 ) },
         {                "bcsstk01.mtx",   std::make_tuple( 0.00001766400000, 0.00000070228140, 0.0, 10 ) },
         {                "bcsstk02.mtx",   std::make_tuple( 0.00004911850000, 0.00000205799800, 0.0, 10 ) },
         {                "bcsstk03.mtx",   std::make_tuple( 0.00025297480000, 0.00000295692800, 0.0, 10 ) },
         {                "bcsstk04.mtx",   std::make_tuple( 0.00042696650000, 0.00000357329200, 0.0, 10 ) },
         {                "bcsstk05.mtx",   std::make_tuple( 0.00068672110000, 0.00001103596000, 0.0, 10 ) },
         {                "bcsstk06.mtx",   std::make_tuple( 0.01805150000000, 0.00001657697000, 0.0, 10 ) },
         {                "bcsstk07.mtx",   std::make_tuple( 0.01810544000000, 0.00008844977000, 0.0, 10 ) },
         {                "bcsstk08.mtx",   std::make_tuple( 0.32391490000000, 0.00025364760000, 0.0, 10 ) },
         {                "bcsstk09.mtx",   std::make_tuple( 0.33372050000000, 0.00056055260000, 0.0, 10 ) },
         {                "bcsstk10.mtx",   std::make_tuple( 0.33775300000000, 0.00050642250000, 0.0, 10 ) },
         {                "bcsstk11.mtx",   std::make_tuple( 0.85470110000000, 0.00389520500000, 0.0, 10 ) },
         {                "bcsstk12.mtx",   std::make_tuple( 0.84640070000000, 0.00058491420000, 0.0, 10 ) },
         {                "bcsstk13.mtx",   std::make_tuple( 2.15153100000000, 0.00207370700000, 0.0, 10 ) },
         {                "bcsstk14.mtx",   std::make_tuple( 1.57570700000000, 0.00206149500000, 0.0, 10 ) },
         {                "bcsstk15.mtx",  std::make_tuple( 20.57132000000000, 0.01888074000000, 0.0, 10 ) },
         {                "bcsstk16.mtx",  std::make_tuple( 40.03413000000000, 0.11346070000000, 0.0, 10 ) },
         {                "bcsstk19.mtx",   std::make_tuple( 0.14211550000000, 0.00041076230000, 0.0, 10 ) },
         {                "bcsstk20.mtx",   std::make_tuple( 0.02877177000000, 0.00025677540000, 0.0, 10 ) },
         {                "bcsstk21.mtx",  std::make_tuple( 14.56640000000000, 0.05341328000000, 0.0, 10 ) },
         {                "bcsstk22.mtx",   std::make_tuple( 0.00049192780000, 0.00000361875300, 0.0, 10 ) },
         {                "bcsstk23.mtx",   std::make_tuple( 9.95528400000000, 0.02652938000000, 0.0, 10 ) },
         {                "bcsstk24.mtx",  std::make_tuple( 15.84520000000000, 0.07564371000000, 0.0, 10 ) },
         {                "bcsstk26.mtx",   std::make_tuple( 1.90579900000000, 0.00455519200000, 0.0, 10 ) },
         {                "bcsstk27.mtx",   std::make_tuple( 0.48514720000000, 0.00056561660000, 0.0, 10 ) },
         {                "bcsstk28.mtx",  std::make_tuple( 32.02693000000000, 0.08006216000000, 0.0, 10 ) },
         {                "bcsstm02.mtx",   std::make_tuple( 0.00004841430000, 0.00000067152960, 0.0, 10 ) },
         {                "bcsstm26.mtx",   std::make_tuple( 1.90341200000000, 0.00437368800000, 0.0, 10 ) },
         {                "bcsstm27.mtx",   std::make_tuple( 0.48628900000000, 0.00080829250000, 0.0, 10 ) },
         {          "LeGresley_2508.mtx",   std::make_tuple( 4.35420800000000, 0.01247014000000, 0.0, 10 ) },
         {                  "pivtol.mtx",   std::make_tuple( 0.00018865060000, 0.00000190284100, 0.0, 10 ) },
         {                     "Na5.mtx",  std::make_tuple( 67.21655000000000, 0.02245706000000, 0.0, 10 ) },
         {                "raefsky1.mtx",  std::make_tuple( 11.62083000000000, 0.06588546000000, 0.0, 10 ) },
         {                "raefsky2.mtx",  std::make_tuple( 11.52409000000000, 0.01465514000000, 0.0, 10 ) },
         {                  "swang1.mtx",  std::make_tuple( 11.86346000000000, 0.02109001000000, 0.0, 10 ) },
         {                  "swang2.mtx",  std::make_tuple( 11.61129000000000, 0.01533901000000, 0.0, 10 ) },
         {                   "wang1.mtx",   std::make_tuple( 7.92130400000000, 0.02704062000000, 0.0, 10 ) },
         {                   "wang2.mtx",   std::make_tuple( 7.90658200000000, 0.01698863000000, 0.0, 10 ) },
         {  "Cejka1641_<-1000-1000>.mtx",   std::make_tuple( 1.17906400000000, 0.00206762900000, 0.0, 10 ) },
         {  "Cejka2842_<-1000-1000>.mtx",   std::make_tuple( 6.91100000000000, 0.00875593900000, 0.0, 10 ) },
         {  "Cejka3352_<-1000-1000>.mtx",  std::make_tuple( 11.53909000000000, 0.00744339900000, 0.0, 10 ) },
 // 3 matricesBig
         {  "Cejka5943_<-1000-1000>.mtx", std::make_tuple( 104.61500000000000, 0.61529340000000, 0.0, 10 ) },
         {  "Cejka7580_<-1000-1000>.mtx", std::make_tuple( 159.64210000000000, 0.87028140000000, 0.0, 10 ) },
         { "Cejka10793_<-1000-1000>.mtx", std::make_tuple( 699.96340000000000, 2.69247300000000, 0.0, 10 ) },
 // 7 matrices
         {                 "sts4098.mtx",  std::make_tuple( 26.10821000000000, 0.08993718000000, 0.0, 10 ) },
         {                "s3rmt3m3.mtx",  std::make_tuple( 74.01062000000000, 0.13750560000000, 0.0, 10 ) },
         {                    "poli.mtx",  std::make_tuple( 20.84435000000000, 0.03498289000000, 0.0, 10 ) },
         {                 "bundle1.mtx", std::make_tuple( 679.38460000000000, 2.87114100000000, 0.0, 10 ) },
         {                      "fp.mtx", std::make_tuple( 154.82320000000000, 0.20641000000000, 0.0, 10 ) },
         {               "rail_5177.mtx",  std::make_tuple( 67.90296000000000, 0.53103710000000, 0.0, 10 ) },
         {                    "c-31.mtx",  std::make_tuple( 72.38632000000000, 0.21704670000000, 0.0, 10 ) }
      } );
   }

   Map
   getBenchmarkResults_float()
   {
      return Map( {
  // 53 matrices
         {               "Chem97ZtZ.mtx",   std::make_tuple( 4.39753600000000, 0.00624907600000, 0.0, 10 ) },
         {                "crystk01.mtx",  std::make_tuple( 45.19011000000000, 0.08925713000000, 0.0, 10 ) },
         {                "s1rmq4m1.mtx",  std::make_tuple( 67.85091000000000, 0.10142560000000, 0.0, 10 ) },
         {                "s2rmq4m1.mtx",  std::make_tuple( 69.03897000000000, 0.69268970000000, 0.0, 10 ) },
         {                "problem1.mtx",   std::make_tuple( 0.01678465000000, 0.00003001291000, 0.0, 10 ) },
         {                     "ex5.mtx",   std::make_tuple( 0.00000334430000, 0.00000053968740, 0.0, 10 ) },
         {                   "add20.mtx",   std::make_tuple( 4.63954900000000, 0.00379161600000, 0.0, 10 ) },
         {                   "add32.mtx",  std::make_tuple( 36.69365000000000, 0.17835280000000, 0.0, 10 ) },
         {                "1138_bus.mtx",   std::make_tuple( 0.38405170000000, 0.00053345750000, 0.0, 10 ) },
         {                 "494_bus.mtx",   std::make_tuple( 0.02917469000000, 0.00002652052000, 0.0, 10 ) },
         {                 "662_bus.mtx",   std::make_tuple( 0.07300897000000, 0.00010955240000, 0.0, 10 ) },
         {                 "685_bus.mtx",   std::make_tuple( 0.08150717000000, 0.00026748890000, 0.0, 10 ) },
         {                  "arc130.mtx",   std::make_tuple( 0.00053338910000, 0.00000300243300, 0.0, 10 ) },
         {                "bcsstk01.mtx",   std::make_tuple( 0.00001852860000, 0.00000301686600, 0.0, 10 ) },
         {                "bcsstk02.mtx",   std::make_tuple( 0.00004726610000, 0.00000062640220, 0.0, 10 ) },
         {                "bcsstk03.mtx",   std::make_tuple( 0.00024785690000, 0.00000322865000, 0.0, 10 ) },
         {                "bcsstk04.mtx",   std::make_tuple( 0.00041569480000, 0.00000250063000, 0.0, 10 ) },
         {                "bcsstk05.mtx",   std::make_tuple( 0.00067168600000, 0.00000336129300, 0.0, 10 ) },
         {                "bcsstk06.mtx",   std::make_tuple( 0.01748483000000, 0.00010683300000, 0.0, 10 ) },
         {                "bcsstk07.mtx",   std::make_tuple( 0.01739746000000, 0.00002096752000, 0.0, 10 ) },
         {                "bcsstk08.mtx",   std::make_tuple( 0.32467060000000, 0.00160974500000, 0.0, 10 ) },
         {                "bcsstk09.mtx",   std::make_tuple( 0.33107380000000, 0.00032558360000, 0.0, 10 ) },
         {                "bcsstk10.mtx",   std::make_tuple( 0.33320710000000, 0.00155830000000, 0.0, 10 ) },
         {                "bcsstk11.mtx",   std::make_tuple( 0.84620470000000, 0.00102731200000, 0.0, 10 ) },
         {                "bcsstk12.mtx",   std::make_tuple( 0.84443170000000, 0.00315103200000, 0.0, 10 ) },
         {                "bcsstk13.mtx",   std::make_tuple( 2.14391900000000, 0.00418639400000, 0.0, 10 ) },
         {                "bcsstk14.mtx",   std::make_tuple( 1.56644000000000, 0.00292214200000, 0.0, 10 ) },
         {                "bcsstk15.mtx",  std::make_tuple( 18.30871000000000, 0.03267797000000, 0.0, 10 ) },
         {                "bcsstk16.mtx",  std::make_tuple( 38.36298000000000, 0.06604007000000, 0.0, 10 ) },
         {                "bcsstk19.mtx",   std::make_tuple( 0.14153860000000, 0.00306459000000, 0.0, 10 ) },
         {                "bcsstk20.mtx",   std::make_tuple( 0.02798900000000, 0.00001923055000, 0.0, 10 ) },
         {                "bcsstk21.mtx",  std::make_tuple( 12.97039000000000, 0.00674144700000, 0.0, 10 ) },
         {                "bcsstk22.mtx",   std::make_tuple( 0.00048632450000, 0.00000332486700, 0.0, 10 ) },
         {                "bcsstk23.mtx",   std::make_tuple( 8.31471600000000, 0.00681347300000, 0.0, 10 ) },
         {                "bcsstk24.mtx",  std::make_tuple( 12.55839000000000, 0.01314372000000, 0.0, 10 ) },
         {                "bcsstk26.mtx",   std::make_tuple( 1.88814900000000, 0.00247619500000, 0.0, 10 ) },
         {                "bcsstk27.mtx",   std::make_tuple( 0.47967070000000, 0.00033674470000, 0.0, 10 ) },
         {                "bcsstk28.mtx",  std::make_tuple( 29.59191000000000, 0.14220580000000, 0.0, 10 ) },
         {                "bcsstm02.mtx",   std::make_tuple( 0.00004738640000, 0.00000085747190, 0.0, 10 ) },
         {                "bcsstm26.mtx",   std::make_tuple( 1.88933300000000, 0.00481307000000, 0.0, 10 ) },
         {                "bcsstm27.mtx",   std::make_tuple( 0.47890790000000, 0.00101406200000, 0.0, 10 ) },
         {          "LeGresley_2508.mtx",   std::make_tuple( 4.21933000000000, 0.00375331200000, 0.0, 10 ) },
         {                  "pivtol.mtx",   std::make_tuple( 0.00018803580000, 0.00000139986200, 0.0, 10 ) },
         {                     "Na5.mtx",  std::make_tuple( 62.39946000000000, 0.09371300000000, 0.0, 10 ) },
         {                "raefsky1.mtx",   std::make_tuple( 9.28674800000000, 0.02188906000000, 0.0, 10 ) },
         {                "raefsky2.mtx",   std::make_tuple( 9.30545700000000, 0.01823079000000, 0.0, 10 ) },
         {                  "swang1.mtx",   std::make_tuple( 8.74441800000000, 0.02164098000000, 0.0, 10 ) },
         {                  "swang2.mtx",   std::make_tuple( 8.73619300000000, 0.01496171000000, 0.0, 10 ) },
         {                   "wang1.mtx",   std::make_tuple( 6.62998800000000, 0.01480620000000, 0.0, 10 ) },
         {                   "wang2.mtx",   std::make_tuple( 6.71934900000000, 0.05113936000000, 0.0, 10 ) },
         {  "Cejka1641_<-1000-1000>.mtx",   std::make_tuple( 1.18323200000000, 0.00256818700000, 0.0, 10 ) },
         {  "Cejka2842_<-1000-1000>.mtx",   std::make_tuple( 6.22104200000000, 0.01907162000000, 0.0, 10 ) },
         {  "Cejka3352_<-1000-1000>.mtx",  std::make_tuple( 10.34322000000000, 0.02100850000000, 0.0, 10 ) },
 // 3 matricesBig
         {  "Cejka5943_<-1000-1000>.mtx",  std::make_tuple( 90.11137000000000, 0.82544600000000, 0.0, 10 ) },
         {  "Cejka7580_<-1000-1000>.mtx", std::make_tuple( 151.79810000000000, 0.75931010000000, 0.0, 10 ) },
         { "Cejka10793_<-1000-1000>.mtx", std::make_tuple( 643.34620000000000, 2.58832800000000, 0.0, 10 ) },
 // 7 matrices
         {                 "sts4098.mtx",  std::make_tuple( 21.77260000000000, 0.06801612000000, 0.0, 10 ) },
         {                "s3rmt3m3.mtx",  std::make_tuple( 61.98667000000000, 0.25231180000000, 0.0, 10 ) },
         {                    "poli.mtx",  std::make_tuple( 18.81993000000000, 0.01364839000000, 0.0, 10 ) },
         {                 "bundle1.mtx", std::make_tuple( 617.51200000000000, 4.40690700000000, 0.0, 10 ) },
         {                      "fp.mtx", std::make_tuple( 179.70540000000000, 6.12100800000000, 0.0, 10 ) },
         {               "rail_5177.mtx",  std::make_tuple( 55.11240000000000, 0.13922210000000, 0.0, 10 ) },
         {                    "c-31.mtx",  std::make_tuple( 59.32075000000000, 0.19422710000000, 0.0, 10 ) }
      } );
   }

public:
   RCI_AMD_EPYC_7543_32C()
   : MachineResults< Precision >( CroutMethod<>::template getDecomposerName< TNL::Devices::Host >( false ),
                                  getBenchmarkResults_double(),
                                  getBenchmarkResults_float() )
   {}
};

}  // namespace Decomposition::Benchmark::Results::Decomposers::CM
