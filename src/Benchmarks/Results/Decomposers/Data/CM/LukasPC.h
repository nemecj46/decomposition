#pragma once

#include <TNL/Devices/Host.h>
#include <TNL/String.h>

#include <Decomposition/LU/Decomposers/CroutMethod.h>

#include "Benchmarks/Results/MachineResults.h"

using namespace Decomposition::LU::Decomposers;

namespace Decomposition::Benchmark::Results::Decomposers::CM {

template< typename Precision >
class LukasPC : public MachineResults< Precision >
{
   using Tuple = typename MachineResults< Precision >::TupleType;
   using Map = typename MachineResults< Precision >::MapType;

private:
   Map
   getBenchmarkResults_double()
   {
      return Map( {
  // 53 matrices
         {               "Chem97ZtZ.mtx",    std::make_tuple( 4.40645900000000, 0.05174052000000, 0.0, 10 ) },
         {                "crystk01.mtx",   std::make_tuple( 71.50868000000000, 0.40476950000000, 0.0, 10 ) },
         {                "s1rmq4m1.mtx",  std::make_tuple( 117.73690000000000, 0.65029860000000, 0.0, 10 ) },
         {                "s2rmq4m1.mtx",  std::make_tuple( 115.99040000000000, 0.35942300000000, 0.0, 10 ) },
         {                "problem1.mtx",    std::make_tuple( 0.01374012000000, 0.00003669357000, 0.0, 10 ) },
         {                     "ex5.mtx",    std::make_tuple( 0.00000479310000, 0.00000087899090, 0.0, 10 ) },
         {                   "add20.mtx",    std::make_tuple( 3.45155900000000, 0.02069358000000, 0.0, 10 ) },
         {                   "add32.mtx",   std::make_tuple( 72.05365000000000, 0.34956600000000, 0.0, 10 ) },
         {                "1138_bus.mtx",    std::make_tuple( 0.31156900000000, 0.00149600100000, 0.0, 10 ) },
         {                 "494_bus.mtx",    std::make_tuple( 0.02379936000000, 0.00003932145000, 0.0, 10 ) },
         {                 "662_bus.mtx",    std::make_tuple( 0.05875709000000, 0.00003802575000, 0.0, 10 ) },
         {                 "685_bus.mtx",    std::make_tuple( 0.06445487000000, 0.00031213320000, 0.0, 10 ) },
         {                  "arc130.mtx",    std::make_tuple( 0.00031832730000, 0.00000273452300, 0.0, 10 ) },
         {                "bcsstk01.mtx",    std::make_tuple( 0.00001424610000, 0.00000063898430, 0.0, 10 ) },
         {                "bcsstk02.mtx",    std::make_tuple( 0.00003937210000, 0.00000085832630, 0.0, 10 ) },
         {                "bcsstk03.mtx",    std::make_tuple( 0.00019959380000, 0.00000493805800, 0.0, 10 ) },
         {                "bcsstk04.mtx",    std::make_tuple( 0.00033471030000, 0.00001136040000, 0.0, 10 ) },
         {                "bcsstk05.mtx",    std::make_tuple( 0.00053457910000, 0.00000273481600, 0.0, 10 ) },
         {                "bcsstk06.mtx",    std::make_tuple( 0.01393665000000, 0.00012326200000, 0.0, 10 ) },
         {                "bcsstk07.mtx",    std::make_tuple( 0.01393963000000, 0.00012272420000, 0.0, 10 ) },
         {                "bcsstk08.mtx",    std::make_tuple( 0.25500270000000, 0.00070624000000, 0.0, 10 ) },
         {                "bcsstk09.mtx",    std::make_tuple( 0.26232150000000, 0.00031932870000, 0.0, 10 ) },
         {                "bcsstk10.mtx",    std::make_tuple( 0.26333850000000, 0.00106090600000, 0.0, 10 ) },
         {                "bcsstk11.mtx",    std::make_tuple( 0.66601010000000, 0.00112615900000, 0.0, 10 ) },
         {                "bcsstk12.mtx",    std::make_tuple( 0.66658320000000, 0.00093867920000, 0.0, 10 ) },
         {                "bcsstk13.mtx",    std::make_tuple( 1.69668300000000, 0.00686483200000, 0.0, 10 ) },
         {                "bcsstk14.mtx",    std::make_tuple( 1.25080800000000, 0.00114359600000, 0.0, 10 ) },
         {                "bcsstk15.mtx",   std::make_tuple( 33.80335000000000, 0.29485680000000, 0.0, 10 ) },
         {                "bcsstk16.mtx",   std::make_tuple( 66.08246000000000, 0.27302090000000, 0.0, 10 ) },
         {                "bcsstk19.mtx",    std::make_tuple( 0.11369240000000, 0.00018362630000, 0.0, 10 ) },
         {                "bcsstk20.mtx",    std::make_tuple( 0.02259015000000, 0.00014045300000, 0.0, 10 ) },
         {                "bcsstk21.mtx",   std::make_tuple( 19.26191000000000, 0.06000820000000, 0.0, 10 ) },
         {                "bcsstk22.mtx",    std::make_tuple( 0.00040163460000, 0.00001406090000, 0.0, 10 ) },
         {                "bcsstk23.mtx",   std::make_tuple( 11.13001000000000, 0.02822134000000, 0.0, 10 ) },
         {                "bcsstk24.mtx",   std::make_tuple( 19.64263000000000, 0.03433826000000, 0.0, 10 ) },
         {                "bcsstk26.mtx",    std::make_tuple( 1.57661600000000, 0.00195381100000, 0.0, 10 ) },
         {                "bcsstk27.mtx",    std::make_tuple( 0.38825180000000, 0.00050250280000, 0.0, 10 ) },
         {                "bcsstk28.mtx",   std::make_tuple( 44.62331000000000, 0.18801390000000, 0.0, 10 ) },
         {                "bcsstm02.mtx",    std::make_tuple( 0.00003914420000, 0.00000085730100, 0.0, 10 ) },
         {                "bcsstm26.mtx",    std::make_tuple( 1.55476400000000, 0.00126366000000, 0.0, 10 ) },
         {                "bcsstm27.mtx",    std::make_tuple( 0.39109800000000, 0.00024517170000, 0.0, 10 ) },
         {          "LeGresley_2508.mtx",    std::make_tuple( 5.32319300000000, 0.01530405000000, 0.0, 10 ) },
         {                  "pivtol.mtx",    std::make_tuple( 0.00014922060000, 0.00000119442300, 0.0, 10 ) },
         {                     "Na5.mtx",  std::make_tuple( 132.85950000000000, 0.64886020000000, 0.0, 10 ) },
         {                "raefsky1.mtx",   std::make_tuple( 13.11868000000000, 0.02629897000000, 0.0, 10 ) },
         {                "raefsky2.mtx",   std::make_tuple( 12.93943000000000, 0.05462690000000, 0.0, 10 ) },
         {                  "swang1.mtx",   std::make_tuple( 12.11355000000000, 0.06035367000000, 0.0, 10 ) },
         {                  "swang2.mtx",   std::make_tuple( 12.11011000000000, 0.13546130000000, 0.0, 10 ) },
         {                   "wang1.mtx",    std::make_tuple( 8.38355600000000, 0.04826125000000, 0.0, 10 ) },
         {                   "wang2.mtx",    std::make_tuple( 8.26139800000000, 0.06054105000000, 0.0, 10 ) },
         {  "Cejka1641_<-1000-1000>.mtx",    std::make_tuple( 0.95293430000000, 0.00128947800000, 0.0, 10 ) },
         {  "Cejka2842_<-1000-1000>.mtx",    std::make_tuple( 8.57092100000000, 0.09630752000000, 0.0, 10 ) },
         {  "Cejka3352_<-1000-1000>.mtx",   std::make_tuple( 13.89154000000000, 0.04644258000000, 0.0, 10 ) },
 // 3 matricesBig
         {  "Cejka5943_<-1000-1000>.mtx",  std::make_tuple( 149.77460000000000, 0.55462270000000, 0.0, 10 ) },
         {  "Cejka7580_<-1000-1000>.mtx",  std::make_tuple( 345.24750000000000, 0.48473760000000, 0.0, 10 ) },
         { "Cejka10793_<-1000-1000>.mtx", std::make_tuple( 1254.93800000000000, 8.29924100000000, 0.0, 10 ) },
 // 7 matrices
         {                 "sts4098.mtx",   std::make_tuple( 30.24701000000000, 0.18696490000000, 0.0, 10 ) },
         {                "s3rmt3m3.mtx",  std::make_tuple( 104.17610000000000, 0.36933170000000, 0.0, 10 ) },
         {                    "poli.mtx",   std::make_tuple( 29.93955000000000, 0.14857270000000, 0.0, 10 ) },
         {                 "bundle1.mtx", std::make_tuple( 1186.87900000000000, 2.68226000000000, 0.0, 10 ) },
         {                      "fp.mtx",  std::make_tuple( 348.25390000000000, 1.26430700000000, 0.0, 10 ) },
         {               "rail_5177.mtx",   std::make_tuple( 90.15917000000000, 0.48388940000000, 0.0, 10 ) },
         {                    "c-31.mtx",   std::make_tuple( 98.66057000000000, 0.60625670000000, 0.0, 10 ) }
      } );
   }

   Map
   getBenchmarkResults_float()
   {
      return Map( {
  // 53 matrices
         {               "Chem97ZtZ.mtx",    std::make_tuple( 3.85032800000000, 0.00313183600000, 0.0, 10 ) },
         {                "crystk01.mtx",   std::make_tuple( 58.23891000000000, 0.48754580000000, 0.0, 10 ) },
         {                "s1rmq4m1.mtx",   std::make_tuple( 92.39709000000000, 0.91328680000000, 0.0, 10 ) },
         {                "s2rmq4m1.mtx",   std::make_tuple( 93.94924000000000, 0.32387330000000, 0.0, 10 ) },
         {                "problem1.mtx",    std::make_tuple( 0.01376537000000, 0.00002315844000, 0.0, 10 ) },
         {                     "ex5.mtx",    std::make_tuple( 0.00000310500000, 0.00000052654530, 0.0, 10 ) },
         {                   "add20.mtx",    std::make_tuple( 4.09810200000000, 0.00352730900000, 0.0, 10 ) },
         {                   "add32.mtx",   std::make_tuple( 56.02145000000000, 0.34898170000000, 0.0, 10 ) },
         {                "1138_bus.mtx",    std::make_tuple( 0.30932430000000, 0.00016686320000, 0.0, 10 ) },
         {                 "494_bus.mtx",    std::make_tuple( 0.02365033000000, 0.00006002483000, 0.0, 10 ) },
         {                 "662_bus.mtx",    std::make_tuple( 0.05916743000000, 0.00008713010000, 0.0, 10 ) },
         {                 "685_bus.mtx",    std::make_tuple( 0.06599074000000, 0.00006097446000, 0.0, 10 ) },
         {                  "arc130.mtx",    std::make_tuple( 0.00054082310000, 0.00011056630000, 0.0, 10 ) },
         {                "bcsstk01.mtx",    std::make_tuple( 0.00001590110000, 0.00000080221010, 0.0, 10 ) },
         {                "bcsstk02.mtx",    std::make_tuple( 0.00004122920000, 0.00000093453110, 0.0, 10 ) },
         {                "bcsstk03.mtx",    std::make_tuple( 0.00021038580000, 0.00000145713200, 0.0, 10 ) },
         {                "bcsstk04.mtx",    std::make_tuple( 0.00035505450000, 0.00000156286600, 0.0, 10 ) },
         {                "bcsstk05.mtx",    std::make_tuple( 0.00057166920000, 0.00000317811900, 0.0, 10 ) },
         {                "bcsstk06.mtx",    std::make_tuple( 0.01416893000000, 0.00003999863000, 0.0, 10 ) },
         {                "bcsstk07.mtx",    std::make_tuple( 0.01418697000000, 0.00005592179000, 0.0, 10 ) },
         {                "bcsstk08.mtx",    std::make_tuple( 0.26105450000000, 0.00030669530000, 0.0, 10 ) },
         {                "bcsstk09.mtx",    std::make_tuple( 0.26753780000000, 0.00063070020000, 0.0, 10 ) },
         {                "bcsstk10.mtx",    std::make_tuple( 0.27070990000000, 0.00011688470000, 0.0, 10 ) },
         {                "bcsstk11.mtx",    std::make_tuple( 0.68375270000000, 0.00091456790000, 0.0, 10 ) },
         {                "bcsstk12.mtx",    std::make_tuple( 0.68229390000000, 0.00089298720000, 0.0, 10 ) },
         {                "bcsstk13.mtx",    std::make_tuple( 1.70552500000000, 0.02906818000000, 0.0, 10 ) },
         {                "bcsstk14.mtx",    std::make_tuple( 1.26828700000000, 0.00257702400000, 0.0, 10 ) },
         {                "bcsstk15.mtx",   std::make_tuple( 20.82640000000000, 0.07175512000000, 0.0, 10 ) },
         {                "bcsstk16.mtx",   std::make_tuple( 53.07882000000000, 0.12329340000000, 0.0, 10 ) },
         {                "bcsstk19.mtx",    std::make_tuple( 0.11100700000000, 0.00031808820000, 0.0, 10 ) },
         {                "bcsstk20.mtx",    std::make_tuple( 0.02283948000000, 0.00006535566000, 0.0, 10 ) },
         {                "bcsstk21.mtx",   std::make_tuple( 15.27662000000000, 0.03564153000000, 0.0, 10 ) },
         {                "bcsstk22.mtx",    std::make_tuple( 0.00049414420000, 0.00013081730000, 0.0, 10 ) },
         {                "bcsstk23.mtx",    std::make_tuple( 8.65033900000000, 0.03794239000000, 0.0, 10 ) },
         {                "bcsstk24.mtx",   std::make_tuple( 15.16749000000000, 0.08445149000000, 0.0, 10 ) },
         {                "bcsstk26.mtx",    std::make_tuple( 1.52949800000000, 0.00065130050000, 0.0, 10 ) },
         {                "bcsstk27.mtx",    std::make_tuple( 0.38848570000000, 0.00040317230000, 0.0, 10 ) },
         {                "bcsstk28.mtx",   std::make_tuple( 37.38110000000000, 0.13981160000000, 0.0, 10 ) },
         {                "bcsstm02.mtx",    std::make_tuple( 0.00004134210000, 0.00000102956000, 0.0, 10 ) },
         {                "bcsstm26.mtx",    std::make_tuple( 1.52294200000000, 0.00105674800000, 0.0, 10 ) },
         {                "bcsstm27.mtx",    std::make_tuple( 0.37976400000000, 0.00058162360000, 0.0, 10 ) },
         {          "LeGresley_2508.mtx",    std::make_tuple( 3.51296400000000, 0.00362448500000, 0.0, 10 ) },
         {                  "pivtol.mtx",    std::make_tuple( 0.00016113970000, 0.00001216553000, 0.0, 10 ) },
         {                     "Na5.mtx",  std::make_tuple( 102.74700000000000, 0.83765050000000, 0.0, 10 ) },
         {                "raefsky1.mtx",    std::make_tuple( 9.66651800000000, 0.02178920000000, 0.0, 10 ) },
         {                "raefsky2.mtx",    std::make_tuple( 9.76231200000000, 0.05791140000000, 0.0, 10 ) },
         {                  "swang1.mtx",    std::make_tuple( 9.08400200000000, 0.06171463000000, 0.0, 10 ) },
         {                  "swang2.mtx",    std::make_tuple( 9.11996600000000, 0.13442350000000, 0.0, 10 ) },
         {                   "wang1.mtx",    std::make_tuple( 6.24722300000000, 0.01253182000000, 0.0, 10 ) },
         {                   "wang2.mtx",    std::make_tuple( 6.20018000000000, 0.01071667000000, 0.0, 10 ) },
         {  "Cejka1641_<-1000-1000>.mtx",    std::make_tuple( 0.95645970000000, 0.00110272400000, 0.0, 10 ) },
         {  "Cejka2842_<-1000-1000>.mtx",    std::make_tuple( 7.26364400000000, 0.01682874000000, 0.0, 10 ) },
         {  "Cejka3352_<-1000-1000>.mtx",   std::make_tuple( 10.96742000000000, 0.02280888000000, 0.0, 10 ) },
 // 3 matricesBig
         {  "Cejka5943_<-1000-1000>.mtx",  std::make_tuple( 121.07570000000000, 1.18296800000000, 0.0, 10 ) },
         {  "Cejka7580_<-1000-1000>.mtx",  std::make_tuple( 287.02230000000000, 1.41358000000000, 0.0, 10 ) },
         { "Cejka10793_<-1000-1000>.mtx", std::make_tuple( 1075.10000000000000, 2.44856400000000, 0.0, 10 ) },
 // 7 matrices
         {                 "sts4098.mtx",   std::make_tuple( 25.57303000000000, 0.13320300000000, 0.0, 10 ) },
         {                "s3rmt3m3.mtx",   std::make_tuple( 81.59842000000000, 0.54032620000000, 0.0, 10 ) },
         {                    "poli.mtx",   std::make_tuple( 23.84398000000000, 0.06382767000000, 0.0, 10 ) },
         {                 "bundle1.mtx", std::make_tuple( 1005.59400000000000, 2.68910700000000, 0.0, 10 ) },
         {                      "fp.mtx",  std::make_tuple( 290.85950000000000, 1.74797100000000, 0.0, 10 ) },
         {               "rail_5177.mtx",   std::make_tuple( 71.53534000000000, 0.30206020000000, 0.0, 10 ) },
         {                    "c-31.mtx",   std::make_tuple( 80.76183000000000, 0.21837290000000, 0.0, 10 ) }
      } );
   }

public:
   LukasPC()
   : MachineResults< Precision >( CroutMethod<>::template getDecomposerName< TNL::Devices::Host >( false ),
                                  getBenchmarkResults_double(),
                                  getBenchmarkResults_float() )
   {}
};

}  // namespace Decomposition::Benchmark::Results::Decomposers::CM
