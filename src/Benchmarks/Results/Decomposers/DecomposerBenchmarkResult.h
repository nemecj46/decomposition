#pragma once

#include <TNL/Benchmarks/Benchmarks.h>
#include <TNL/Matrices/DenseMatrix.h>

#include <Decomposition/Containers/MatrixFormat.h>
#include <Decomposition/LU/Decomposers/BaseDecomposer.h>

template< typename Matrix >
double
getMaxAbsDiff( const Matrix& A, const Matrix& B )
{
   return TNL::max( TNL::abs( A.getValues() - B.getValues() ) );
}

namespace Decomposition::Benchmark {

template< typename BenchmarkMatrix, typename BenchmarkVector, typename VectorIndex = typename BenchmarkVector::IndexType >
struct DecomposerBenchmarkResult : TNL::Benchmarks::BenchmarkResult
{
   using MatrixFormat = Decomposition::Containers::MatrixFormat;

   using BenchmarkMatrixType = BenchmarkMatrix;
   using Real = typename BenchmarkMatrix::RealType;
   using Index = typename BenchmarkMatrix::IndexType;
   using Device = typename BenchmarkMatrix::DeviceType;

   using HostMatrix = TNL::Matrices::DenseMatrix< Real, TNL::Devices::Host, Index, TNL::Algorithms::Segments::RowMajorOrder >;
   using HostMatrixDouble =
      TNL::Matrices::DenseMatrix< double, TNL::Devices::Host, Index, TNL::Algorithms::Segments::RowMajorOrder >;
   using HostVector = TNL::Containers::Vector< VectorIndex, TNL::Devices::Host, VectorIndex >;

   const HostMatrix& A_host;
   const HostMatrix& LU_host;
   const BenchmarkMatrix& benchmark_LU;
   const BenchmarkVector& benchmark_piv;
   const MatrixFormat mtxFormat;
   const bool absMaxDiff_A_PLU;

   DecomposerBenchmarkResult( const HostMatrix& A_host,
                              const HostMatrix& LU_host,
                              const BenchmarkMatrix& benchmark_LU,
                              const BenchmarkVector& benchmark_piv,
                              const MatrixFormat mtxFormat,
                              const bool& absMaxDiff_A_PLU = false )
   : A_host( A_host ), LU_host( LU_host ), benchmark_LU( benchmark_LU ), benchmark_piv( benchmark_piv ), mtxFormat( mtxFormat ),
     absMaxDiff_A_PLU( absMaxDiff_A_PLU )
   {}

   [[nodiscard]] HeaderElements
   getTableHeader() const override
   {
      if( absMaxDiff_A_PLU )
         return HeaderElements(
            { "time", "stddev", "stddev/time", "loops", "speedup", "Base MaxAbsDiff", "Input MaxAbsDiff" } );
      else
         return HeaderElements( { "time", "stddev", "stddev/time", "loops", "speedup", "Base MaxAbsDiff" } );
   }

   [[nodiscard]] std::vector< int >
   getColumnWidthHints() const override
   {
      if( absMaxDiff_A_PLU )
         return std::vector< int >( { 14, 14, 14, 6, 14, 20, 24 } );
      else
         return std::vector< int >( { 14, 14, 14, 6, 14, 20 } );
   }

   [[nodiscard]] RowElements
   getRowElements() const override
   {
      RowElements elements;
      elements << std::scientific << time << stddev << stddev / time << loops;
      if( speedup != 0.0 )
         elements << speedup;
      else
         elements << "N/A";

      // Create a copy of the results on the host
      HostMatrixDouble benchmark_LU_host, LU_host_copy;
      benchmark_LU_host = benchmark_LU;
      LU_host_copy = LU_host;

      double maxAbsDiff_LU_host_LU;
      // If U has unit diagonal -> can be compared to Crout Method host result
      if( mtxFormat == MatrixFormat::UnitDiagIn_U ) {
         maxAbsDiff_LU_host_LU = getMaxAbsDiff( LU_host_copy, benchmark_LU_host );
      }
      else {
         maxAbsDiff_LU_host_LU = std::numeric_limits< double >::quiet_NaN();
      }

      elements << maxAbsDiff_LU_host_LU;

      if( absMaxDiff_A_PLU ) {
         using BaseDecomposer = Decomposition::LU::Decomposers::BaseDecomposer;
         // Compute result between A and A_new = L*U
         // - Computation peformed on the host to mitigate inaccuracies that can arise during parallel GPU computations.

         HostVector hostPivCopy;
         hostPivCopy = benchmark_piv;

         HostMatrixDouble L;
         HostMatrixDouble U;
         // benchmark_LU_host is reused to hold data then as A_new = L*U

         std::tie( L, U ) = BaseDecomposer::getUnpackedMatrices( benchmark_LU_host, mtxFormat );
         benchmark_LU_host.getMatrixProduct( L, U );

         if( ! benchmark_piv.empty() ) {
            BaseDecomposer::undoOrderInMatrixAccordingTo( benchmark_LU_host, hostPivCopy );
         }

         // Compare results
         HostMatrixDouble A_host_copy;
         A_host_copy = A_host;
         const double maxAbsDiffA = getMaxAbsDiff( A_host_copy, benchmark_LU_host );

         elements << maxAbsDiffA;
      }

      return elements;
   }
};

}  // namespace Decomposition::Benchmark
