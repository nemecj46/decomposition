#pragma once

#include "Benchmarks/utils/BenchmarkMessages.h"

#include "Benchmarks/Results/Decomposers/Data/CM/LukasPC.h"
#include "Benchmarks/Results/Decomposers/Data/CM/GP7.h"
#include "Benchmarks/Results/Decomposers/Data/CM/RCI_AMD_EPYC_7543_32C.h"
#include "Benchmarks/Results/Decomposers/Data/CMPP/RCI_AMD_EPYC_7543_32C.h"

namespace Decomposition::Benchmark::Results::Decomposers {

class DecomposerBenchmarkResultLoaderHost
{
   using Tuple = MachineResults< double >::TupleType;

public:
   template< typename Decomposer, typename BenchmarkResult >
   static bool
   loadResult( BenchmarkResult& result,
               const TNL::String& matrixName,
               const TNL::String& machine,
               const bool& partialPivoting,
               const int& loops )
   {
      using BenchmarkMatrix = typename BenchmarkResult::BenchmarkMatrixType;
      using Device = typename BenchmarkMatrix::DeviceType;
      const TNL::String decomposerName = Decomposer::template getDecomposerName< Device >( partialPivoting );

      if( loops != 10 ) {
         std::cout << "-> Benchmark results are only available for 10 loops. "
                   << Messages::getRunningBenchmarkMessage( decomposerName ) << '\n';
         return false;
      }

      using Precision = typename BenchmarkMatrix::RealType;

      Tuple resultsTuple;
      try {
         resultsTuple = getBenchmarkResultsForDecomposer< Precision >( decomposerName, machine, matrixName );
      }
      catch( const std::exception& ex ) {
         std::cout << ex.what() << '\n';
         return false;
      }

      const bool resultsFound = validResults( resultsTuple );

      if( resultsFound )
         std::tie( result.time, result.stddev, result.speedup, result.loops ) = resultsTuple;

      std::cout << Messages::getResultsStatusMessage< Precision >( resultsFound, machine, decomposerName ) << std::endl;
      return resultsFound;
   }

private:
   template< typename Precision >
   static Tuple
   getBenchmarkResultsForDecomposer( const TNL::String& decomposerName,
                                     const TNL::String& machine,
                                     const TNL::String& matrixName )
   {
      if( decomposerName == "CM" )
         return getCroutBenchmarkResultsForMachine< Precision >( machine, matrixName );
      else if( decomposerName == "CM PP" )
         return getCroutPivotBenchmarkResultsForMachine< Precision >( machine, matrixName );
      else
         return Results::getInvalidTuple();
   }

   template< typename Precision >
   static Tuple
   getCroutBenchmarkResultsForMachine( const TNL::String& machine, const TNL::String& matrixName )
   {
      // Can't use switch with strings in C++ natively
      if( machine == "lukas-pc" )
         return CM::LukasPC< Precision >().getResult( matrixName );
      else if( machine == "gp7" )
         return CM::GP7< Precision >().getResult( matrixName );
      else if( machine == "rci-amd-epyc-7543-32c" )
         return CM::RCI_AMD_EPYC_7543_32C< Precision >().getResult( matrixName );
      else
         return Results::getInvalidTuple();
   }

   template< typename Precision >
   static Tuple
   getCroutPivotBenchmarkResultsForMachine( const TNL::String& machine, const TNL::String& matrixName )
   {
      // Can't use switch with strings in C++ natively
      if( machine == "rci-amd-epyc-7543-32c" )
         return CMPP::RCI_AMD_EPYC_7543_32C< Precision >().getResult( matrixName );
      else
         return Results::getInvalidTuple();
   }

   static bool
   validResults( const Tuple& results )
   {
      return std::get< 0 >( results ) != -1;
   }
};

}  // namespace Decomposition::Benchmark::Results::Decomposers
