#pragma once

#include <TNL/String.h>

#include <utility>

namespace Decomposition::Benchmark::Results {

// Used here and in BenchmarkResultLoaderHost to represent the failure of loading a benchmark result
std::tuple< double, double, double, int >
getInvalidTuple()
{
   return std::make_tuple( -1, -1, -1, -1 );
}

template< typename Precision >
constexpr bool
validPrecision()
{
   return ( std::is_same_v< Precision, double > || std::is_same_v< Precision, float > );
}

template< typename Precision >
class MachineResults
{
public:
   using TupleType = typename std::tuple< double, double, double, int >;
   using MapType = typename std::map< TNL::String, TupleType >;

   TupleType
   getResult( const TNL::String& matrixName )
   {
      if constexpr( std::is_same_v< Precision, double > )
         return getBenchmarkResultForMatrixFromCollection( matrixName, benchmarkResults_double );
      else if constexpr( std::is_same_v< Precision, float > )
         return getBenchmarkResultForMatrixFromCollection( matrixName, benchmarkResults_float );
      else
         return getInvalidTuple();
   }

protected:
   MachineResults( TNL::String procedureName, MapType benchmarkResults_double, MapType benchmarkResults_float )
   : procedureName( std::move( procedureName ) ), benchmarkResults_double( std::move( benchmarkResults_double ) ),
     benchmarkResults_float( std::move( benchmarkResults_float ) )
   {
      checkPrecision();
   }

   TupleType
   getBenchmarkResultForMatrixFromCollection( const TNL::String& matrixName, const MapType& results )
   {
      TupleType result;
      try {
         result = results.at( matrixName );
      }
      catch( const std::out_of_range& ex ) {
         result = getInvalidTuple();
      }

      return result;
   }

   void
   checkPrecision()
   {
      if constexpr( ! validPrecision< Precision >() )
         throw std::runtime_error( "-!> Loading benchmark results failed! Precision '"
                                   + TNL::String( TNL::getType< Precision >() ) + "' not found! "
                                   + Messages::getRunningBenchmarkMessage( procedureName ) );
   }

   friend TupleType
   getInvalidTuple();

   const TNL::String procedureName;
   const MapType benchmarkResults_double, benchmarkResults_float;
};

}  // namespace Decomposition::Benchmark::Results
