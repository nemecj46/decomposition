#pragma once

#include <TNL/Benchmarks/Benchmarks.h>
#include <TNL/Matrices/DenseMatrix.h>

#include <Decomposition/LU/Decomposers/BaseDecomposer.h>

using BaseDecomposer = Decomposition::LU::Decomposers::BaseDecomposer;

template< typename Matrix >
double
getMaxAbsDiff( const Matrix& A, const Matrix& B )
{
   return TNL::max( TNL::abs( A.getValues() - B.getValues() ) );
}

namespace Decomposition::Benchmark {

template< typename BenchmarkMatrix >
struct SolverBenchmarkResult : TNL::Benchmarks::BenchmarkResult
{
   using BenchmarkMatrixType = BenchmarkMatrix;
   using Real = typename BenchmarkMatrix::RealType;
   using Index = typename BenchmarkMatrix::IndexType;

   using HostMatrix = TNL::Matrices::DenseMatrix< Real, TNL::Devices::Host, Index, TNL::Algorithms::Segments::RowMajorOrder >;
   using HostMatrixDouble =
      TNL::Matrices::DenseMatrix< double, TNL::Devices::Host, Index, TNL::Algorithms::Segments::RowMajorOrder >;

   const HostMatrix& A_res;
   const HostMatrix& X_base;
   const BenchmarkMatrix& X_benchmark;
   const HostMatrix& B;
   const bool& maxAbsDiffSolution;

   SolverBenchmarkResult( const HostMatrix& A_res,
                          const HostMatrix& X_base,
                          const BenchmarkMatrix& X_benchmark,
                          const HostMatrix& B,
                          const bool& maxAbsDiffSolution = false )
   : A_res( A_res ), X_base( X_base ), X_benchmark( X_benchmark ), B( B ), maxAbsDiffSolution( maxAbsDiffSolution )
   {}

   [[nodiscard]] HeaderElements
   getTableHeader() const override
   {
      if( maxAbsDiffSolution )
         return HeaderElements(
            { "time", "stddev", "stddev/time", "loops", "speedup", "Base MaxAbsDiff", "Solution MaxAbsDiff" } );
      else
         return HeaderElements( { "time", "stddev", "stddev/time", "loops", "speedup", "Base MaxAbsDiff" } );
   }

   [[nodiscard]] std::vector< int >
   getColumnWidthHints() const override
   {
      if( maxAbsDiffSolution )
         return std::vector< int >( { 14, 14, 14, 6, 14, 20, 24 } );
      else
         return std::vector< int >( { 14, 14, 14, 6, 14, 20 } );
   }

   [[nodiscard]] RowElements
   getRowElements() const override
   {
      RowElements elements;
      elements << std::scientific << time << stddev << stddev / time << loops;
      if( speedup != 0.0 )
         elements << speedup;
      else
         elements << "N/A";

      // Create a copy of the results on the host
      HostMatrixDouble X_benchmarkHostCopy, X_base_copy;
      X_benchmarkHostCopy = X_benchmark;
      X_base_copy = X_base;

      const Real maxAbsDiffBase = getMaxAbsDiff( X_base_copy, X_benchmarkHostCopy );

      elements << maxAbsDiffBase;

      if( maxAbsDiffSolution ) {
         // Compute difference between B and B_result, where B_result = A_res*X
         // - Done only on the host to mitigate inaccuracies that can arise during parallel GPU computations
         HostMatrixDouble B_result, A_res_copy, B_copy;
         B_result.setLike( B );
         A_res_copy = A_res;
         B_copy = B;

         B_result.getMatrixProduct( A_res_copy, X_benchmarkHostCopy );

         // Compare results
         const double maxAbsDiffResult = getMaxAbsDiff( B_copy, B_result );

         elements << maxAbsDiffResult;
      }

      return elements;
   }
};

}  // namespace Decomposition::Benchmark
