#pragma once

#include <TNL/Devices/Host.h>
#include <TNL/String.h>

#include <Decomposition/LU/Solvers/SequentialSolver.h>

#include "Benchmarks/Results/MachineResults.h"

using namespace Decomposition::LU::Solvers;

namespace Decomposition::Benchmark::Results::Solvers::SS {

template< typename Precision >
class RCI_AMD_EPYC_7543_32C : public MachineResults< Precision >
{
   using Tuple = typename MachineResults< Precision >::TupleType;
   using Map = typename MachineResults< Precision >::MapType;

private:
   Map
   getBenchmarkResults_double()
   {
      return Map( {
         { "fake.mtx", std::make_tuple( 0.0, 0.0, 0.0, 10 ) }
      } );
   }

   Map
   getBenchmarkResults_float()
   {
      return Map( {
         { "fake.mtx", std::make_tuple( 0.0, 0.0, 0.0, 10 ) }
      } );
   }

public:
   RCI_AMD_EPYC_7543_32C()
   : MachineResults< Precision >( SequentialSolver<>::template getSolverName< TNL::Devices::Host >( false ),
                                  getBenchmarkResults_double(),
                                  getBenchmarkResults_float() )
   {}
};

}  // namespace Decomposition::Benchmark::Results::Solvers::SS
