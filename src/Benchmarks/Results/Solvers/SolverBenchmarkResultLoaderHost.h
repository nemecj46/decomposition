#pragma once

#include <TNL/String.h>

#include "Benchmarks/utils/BenchmarkMessages.h"
#include "Benchmarks/Results/Solvers/Data/SS/RCI_AMD_EPYC_7543_32C.h"

namespace Decomposition::Benchmark::Results::Solvers {

class SolverBenchmarkResultLoaderHost
{
   using Tuple = MachineResults< double >::TupleType;

public:
   template< typename Solver, typename BenchmarkResult >
   static bool
   loadResult( BenchmarkResult& result,
               const TNL::String& matrixName,
               const TNL::String& machine,
               const bool& partialPivoting,
               const int& loops )
   {
      using BenchmarkMatrix = typename BenchmarkResult::BenchmarkMatrixType;
      using Device = typename BenchmarkMatrix::DeviceType;
      const TNL::String solverName = Solver::template getSolverName< Device >( partialPivoting );

      if( loops != 10 ) {
         std::cout << "-> Benchmark results are only available for 10 loops. "
                   << Messages::getRunningBenchmarkMessage( solverName ) << '\n';
         return false;
      }

      using Precision = typename BenchmarkMatrix::RealType;

      Tuple resultsTuple;
      try {
         resultsTuple = getBenchmarkResultsForSolver< Precision >( solverName, machine, matrixName );
      }
      catch( const std::exception& ex ) {
         std::cout << ex.what() << '\n';
         return false;
      }

      const bool resultsFound = validResults( resultsTuple );

      if( resultsFound )
         std::tie( result.time, result.stddev, result.speedup, result.loops ) = resultsTuple;

      std::cout << Messages::getResultsStatusMessage< Precision >( resultsFound, machine, solverName ) << std::endl;
      return resultsFound;
   }

private:
   template< typename Precision >
   static Tuple
   getBenchmarkResultsForSolver( const TNL::String& solverName, const TNL::String& machine, const TNL::String& matrixName )
   {
      if( solverName == "SS PP" )
         return getSequentialBenchmarkResultsForMachine< Precision >( machine, matrixName );
      else
         return Results::getInvalidTuple();
   }

   template< typename Precision >
   static Tuple
   getSequentialBenchmarkResultsForMachine( const TNL::String& machine, const TNL::String& matrixName )
   {
      // Can't use switch with strings in C++ natively
      if( machine == "rci-amd-epyc-7543-32c" )
         return SS::RCI_AMD_EPYC_7543_32C< Precision >().getResult( matrixName );
      else
         return Results::getInvalidTuple();
   }

   static bool
   validResults( const Tuple& results )
   {
      return std::get< 0 >( results ) != -1;
   }
};

}  // namespace Decomposition::Benchmark::Results::Solvers
