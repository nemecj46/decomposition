#include <iostream>

#include <Decomposition/LU/Solvers/BaseSolver.h>
#include <Decomposition/Containers/MatrixFormat.h>

#include <Decomposition/utils/utils.h>

#ifdef HAVE_GTEST
   #include "gtest/gtest.h"
   #include "gmock/gmock.h"

using ::testing::Contains;

using namespace Decomposition::LU::Solvers;
using MatrixFormat = Decomposition::Containers::MatrixFormat;

template< typename Solver >
void
test_GetFormat()
{
   EXPECT_THAT( ( std::array{ MatrixFormat::UnitDiagIn_U, MatrixFormat::UnitDiagIn_L } ), Contains( Solver::getFormat() ) );
}

template< typename Matrix >
class BaseSolverTest : public ::testing::Test
{
protected:
   using MatrixType = Matrix;
};

// clang-format off
using MatrixTypes = ::testing::Types
<
   BaseSolver< MatrixFormat::UnitDiagIn_U >,
   BaseSolver< MatrixFormat::UnitDiagIn_L >
>;
// clang-format on

TYPED_TEST_SUITE( BaseSolverTest, MatrixTypes );

TYPED_TEST( BaseSolverTest, GetFormatTest )
{
   using MatrixType = typename TestFixture::MatrixType;

   test_GetFormat< MatrixType >();
}

#endif  // HAVE_GTEST
