#include <TNL/Matrices/DenseMatrix.h>

#include <Decomposition/Exceptions/NotImplementedError.h>
#include <Decomposition/LU/Solvers/CuSolverDnXgetrsWrapper.h>

#include "SolverTests.h"

#ifdef HAVE_GTEST
   #include "gtest/gtest.h"
   #include "gmock/gmock.h"

using ::testing::HasSubstr;
using ::testing::ThrowsMessage;

using namespace Decomposition::LU::Solvers;

template< typename Matrix, typename Solver >
void
test_GetSolverName()
{
   using Device = typename Matrix::DeviceType;
   bool partialPivoting = true;
   EXPECT_EQ( Solver::template getSolverName< Device >( partialPivoting ), "CuSolverDnXgetrsWrapper PP" );

   partialPivoting = false;
   EXPECT_EQ( Solver::template getSolverName< Device >( partialPivoting ), "CuSolverDnXgetrsWrapper" );
}

template< typename Matrix >
class CuSolverDnXgetrsWrapperTest : public ::testing::Test
{
protected:
   using MatrixType = Matrix;
};

template< typename Real, typename Device, typename Index >
using DenseMatrixColumnMajorOrder =
   typename TNL::Matrices::DenseMatrix< Real, Device, Index, TNL::Algorithms::Segments::ColumnMajorOrder >;

// clang-format off
using MatrixSolverTypes = ::testing::Types
<
   std::tuple< DenseMatrixColumnMajorOrder< float,  TNL::Devices::Cuda, short >, CuSolverDnXgetrsWrapper<> >,
   std::tuple< DenseMatrixColumnMajorOrder< double, TNL::Devices::Cuda, short >, CuSolverDnXgetrsWrapper<> >,
   std::tuple< DenseMatrixColumnMajorOrder< float,  TNL::Devices::Cuda, int   >, CuSolverDnXgetrsWrapper<> >,
   std::tuple< DenseMatrixColumnMajorOrder< double, TNL::Devices::Cuda, int   >, CuSolverDnXgetrsWrapper<> >,
   std::tuple< DenseMatrixColumnMajorOrder< float,  TNL::Devices::Cuda, long  >, CuSolverDnXgetrsWrapper<> >,
   std::tuple< DenseMatrixColumnMajorOrder< double, TNL::Devices::Cuda, long  >, CuSolverDnXgetrsWrapper<> >
>;
// clang-format on

TYPED_TEST_SUITE( CuSolverDnXgetrsWrapperTest, MatrixSolverTypes );

TYPED_TEST( CuSolverDnXgetrsWrapperTest, GetSolverNameTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_GetSolverName< MatrixType, SolverType >();
}

TYPED_TEST( CuSolverDnXgetrsWrapperTest, SolveSystem2LinearEquationsTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystem2LinearEquations< MatrixType, SolverType >();
}

TYPED_TEST( CuSolverDnXgetrsWrapperTest, SolveSystem3LinearEquationsTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystem3LinearEquations< MatrixType, SolverType >();
}

TYPED_TEST( CuSolverDnXgetrsWrapperTest, SolveSystem3LinearEquations2Test )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystem3LinearEquations2< MatrixType, SolverType >();
}

TYPED_TEST( CuSolverDnXgetrsWrapperTest, SolveWithPivotingSystem3LinearEquationsPivotingVectorTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveWithPivotingSystem3LinearEquations< MatrixType, SolverType, int64_t >();
}

TYPED_TEST( CuSolverDnXgetrsWrapperTest, SolveSystemLinearEquationsOnHostShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystemLinearEquationsOnHostShouldFail< MatrixType, SolverType, int64_t >( "is implemented only for the GPU." );
}

TYPED_TEST( CuSolverDnXgetrsWrapperTest, SolveSystemLinearEquationsWithRowMajorOrderMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystemLinearEquationsWithRowMajorOrderMatrixShouldFail< MatrixType, SolverType, int64_t >();
}

TYPED_TEST( CuSolverDnXgetrsWrapperTest, SolveSystemLinearEquationsNonSquareMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystemLinearEquationsNonSquareMatrixShouldFail< MatrixType, SolverType >();
}

#endif  // HAVE_GTEST
