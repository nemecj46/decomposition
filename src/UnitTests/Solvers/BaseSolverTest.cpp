// OpenMP is not used by this project. If it is installed on the system and the path is
// not provided, then clang-tidy will fail as some TNL files contain references to OpenMP.
#undef HAVE_OPENMP
#include "BaseSolverTest.h"
