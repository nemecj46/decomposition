#include <iostream>

#include <TNL/Matrices/DenseMatrix.h>
#include <TNL/String.h>

#include <Decomposition/LU/Solvers/IterativeSolver.h>

#include <Decomposition/utils/utils.h>
#include "SolverTests.h"

#ifdef HAVE_GTEST
   #include "gtest/gtest.h"

using ::testing::MatchesRegex;
using namespace Decomposition::LU::Solvers;

template< typename Matrix, typename Solver >
void
test_GetSolverName()
{
   using Device = typename Matrix::DeviceType;
   bool partialPivoting = false;
   if( Decomposition::isHost< Device >() ) {
      EXPECT_EQ( Solver::template getSolverName< Device >( partialPivoting ), "IS" );
      partialPivoting = true;
      EXPECT_EQ( Solver::template getSolverName< Device >( partialPivoting ), "IS PP" );
   }
   else {
      EXPECT_THAT( Solver::template getSolverName< Device >( partialPivoting ), MatchesRegex( "IS_[0-9]+" ) );
      partialPivoting = true;
      EXPECT_THAT( Solver::template getSolverName< Device >( partialPivoting ), MatchesRegex( "IS_[0-9]+ PP" ) );
   }
}

template< typename Matrix >
class IterativeSolverTest : public ::testing::Test
{
protected:
   using MatrixType = Matrix;
};

template< typename Real, typename Device, typename Index >
using DenseMatrixRowMajorOrder =
   typename TNL::Matrices::DenseMatrix< Real, Device, Index, TNL::Algorithms::Segments::ColumnMajorOrder >;

// clang-format off
using MatrixSolverTypes = ::testing::Types
<
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Host, short >, IterativeSolver<     > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Host, short >, IterativeSolver<     > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Host, int   >, IterativeSolver<     > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Host, int   >, IterativeSolver<     > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Host, long  >, IterativeSolver<     > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Host, long  >, IterativeSolver<     > >

   #ifdef __CUDACC__
   ,std::tuple< DenseMatrixRowMajorOrder< float, TNL::Devices::Cuda, short >, IterativeSolver<      > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, short >, IterativeSolver<      > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, int   >, IterativeSolver<      > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, int   >, IterativeSolver<      > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, long  >, IterativeSolver<      > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, long  >, IterativeSolver<      > >,

   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, short >, IterativeSolver<    8 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, short >, IterativeSolver<    8 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, int   >, IterativeSolver<    8 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, int   >, IterativeSolver<    8 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, long  >, IterativeSolver<    8 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, long  >, IterativeSolver<    8 > >,

   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, short >, IterativeSolver<   16 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, short >, IterativeSolver<   16 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, int   >, IterativeSolver<   16 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, int   >, IterativeSolver<   16 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, long  >, IterativeSolver<   16 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, long  >, IterativeSolver<   16 > >,

   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, short >, IterativeSolver<   32 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, short >, IterativeSolver<   32 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, int   >, IterativeSolver<   32 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, int   >, IterativeSolver<   32 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, long  >, IterativeSolver<   32 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, long  >, IterativeSolver<   32 > >,

   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, short >, IterativeSolver<   64 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, short >, IterativeSolver<   64 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, int   >, IterativeSolver<   64 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, int   >, IterativeSolver<   64 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, long  >, IterativeSolver<   64 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, long  >, IterativeSolver<   64 > >,

   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, short >, IterativeSolver<  128 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, short >, IterativeSolver<  128 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, int   >, IterativeSolver<  128 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, int   >, IterativeSolver<  128 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, long  >, IterativeSolver<  128 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, long  >, IterativeSolver<  128 > >,
   #endif
>;
// clang-format on

TYPED_TEST_SUITE( IterativeSolverTest, MatrixSolverTypes );

TYPED_TEST( IterativeSolverTest, GetSolverNameTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_GetSolverName< MatrixType, SolverType >();
}

TYPED_TEST( IterativeSolverTest, SolveSystem2LinearEquationsTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystem2LinearEquations< MatrixType, SolverType >();
}

TYPED_TEST( IterativeSolverTest, SolveSystem3LinearEquationsTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystem3LinearEquations< MatrixType, SolverType >();
}

TYPED_TEST( IterativeSolverTest, SolveSystem3LinearEquations2Test )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystem3LinearEquations2< MatrixType, SolverType >();
}

TYPED_TEST( IterativeSolverTest, SolveWithPivotingSystem3LinearEquationsTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveWithPivotingSystem3LinearEquations< MatrixType, SolverType, typename MatrixType::IndexType, TNL::Devices::Host >();
}

TYPED_TEST( IterativeSolverTest, SolveSystemLinearEquationsWithUnitDiagonalInLShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   using Index = typename MatrixType::IndexType;

   constexpr Index TpB = SolverType::threads_perBlock;
   constexpr MatrixFormat Diag = MatrixFormat::UnitDiagIn_L;

   test_SolveSystemLinearEquationsWithUnitDiagonalInLShouldFail< MatrixType,
                                                                 IterativeSolver< TpB, Diag >,
                                                                 typename MatrixType::IndexType,
                                                                 TNL::Devices::Host >();
}

TYPED_TEST( IterativeSolverTest, SolveSystemLinearEquationsNonSquareMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystemLinearEquationsNonSquareMatrixShouldFail< MatrixType, SolverType >();
}

TYPED_TEST( IterativeSolverTest, SolveSystemLinearEquationsMismatchingDimensionsShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystemLinearEquationsMismatchingDimensionsShouldFail< MatrixType, SolverType >();
}

TYPED_TEST( IterativeSolverTest, SolveSystemLinearEquationsPivotingVectorOnDeviceShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystemLinearEquationsPivotingVectorOnDeviceShouldFail< MatrixType, SolverType >(
      "is implemented only for the CPU/GPU. However, the pivoting vector must be stored on the Host in both cases." );
}

#endif  // HAVE_GTEST
