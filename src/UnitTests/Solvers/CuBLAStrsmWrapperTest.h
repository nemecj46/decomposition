
#include <TNL/Matrices/DenseMatrix.h>

#include <Decomposition/Exceptions/NotImplementedError.h>
#include <Decomposition/LU/Solvers/CuBLAStrsmWrapper.h>

#include "SolverTests.h"

#ifdef HAVE_GTEST
   #include "gtest/gtest.h"
   #include "gmock/gmock.h"

using ::testing::HasSubstr;
using ::testing::ThrowsMessage;

using namespace Decomposition::LU::Solvers;
using MatrixFormat = Decomposition::Containers::MatrixFormat;

template< typename Matrix, typename Solver >
void
test_GetSolverName()
{
   using Device = typename Matrix::DeviceType;
   bool partialPivoting = true;
   if( Solver::getFormat() == MatrixFormat::UnitDiagIn_U ) {
      EXPECT_EQ( Solver::template getSolverName< Device >( partialPivoting ), "CuBLAStrsmWrapper_U PP" );
      partialPivoting = false;
      EXPECT_EQ( Solver::template getSolverName< Device >( partialPivoting ), "CuBLAStrsmWrapper_U" );
   }
   else {
      EXPECT_EQ( Solver::template getSolverName< Device >( partialPivoting ), "CuBLAStrsmWrapper_L PP" );
      partialPivoting = false;
      EXPECT_EQ( Solver::template getSolverName< Device >( partialPivoting ), "CuBLAStrsmWrapper_L" );
   }
}

template< typename Matrix >
class CuBLAStrsmWrapperTest : public ::testing::Test
{
protected:
   using MatrixType = Matrix;
};

template< typename Real, typename Device, typename Index >
using DenseMatrixColumnMajorOrder =
   typename TNL::Matrices::DenseMatrix< Real, Device, Index, TNL::Algorithms::Segments::ColumnMajorOrder >;

// clang-format off
using MatrixSolverTypes = ::testing::Types
<
// Unit diagonal is stored in U
   std::tuple< DenseMatrixColumnMajorOrder< float,  TNL::Devices::Cuda, short >, CuBLAStrsmWrapper< MatrixFormat::UnitDiagIn_U > >,
   std::tuple< DenseMatrixColumnMajorOrder< double, TNL::Devices::Cuda, short >, CuBLAStrsmWrapper< MatrixFormat::UnitDiagIn_U > >,
   std::tuple< DenseMatrixColumnMajorOrder< float,  TNL::Devices::Cuda, int   >, CuBLAStrsmWrapper< MatrixFormat::UnitDiagIn_U > >,
   std::tuple< DenseMatrixColumnMajorOrder< double, TNL::Devices::Cuda, int   >, CuBLAStrsmWrapper< MatrixFormat::UnitDiagIn_U > >,
   std::tuple< DenseMatrixColumnMajorOrder< float,  TNL::Devices::Cuda, long  >, CuBLAStrsmWrapper< MatrixFormat::UnitDiagIn_U > >,
   std::tuple< DenseMatrixColumnMajorOrder< double, TNL::Devices::Cuda, long  >, CuBLAStrsmWrapper< MatrixFormat::UnitDiagIn_U > >,
// Unit diagonal is stored in L
   std::tuple< DenseMatrixColumnMajorOrder< float,  TNL::Devices::Cuda, short >, CuBLAStrsmWrapper< MatrixFormat::UnitDiagIn_L > >,
   std::tuple< DenseMatrixColumnMajorOrder< double, TNL::Devices::Cuda, short >, CuBLAStrsmWrapper< MatrixFormat::UnitDiagIn_L > >,
   std::tuple< DenseMatrixColumnMajorOrder< float,  TNL::Devices::Cuda, int   >, CuBLAStrsmWrapper< MatrixFormat::UnitDiagIn_L > >,
   std::tuple< DenseMatrixColumnMajorOrder< double, TNL::Devices::Cuda, int   >, CuBLAStrsmWrapper< MatrixFormat::UnitDiagIn_L > >,
   std::tuple< DenseMatrixColumnMajorOrder< float,  TNL::Devices::Cuda, long  >, CuBLAStrsmWrapper< MatrixFormat::UnitDiagIn_L > >,
   std::tuple< DenseMatrixColumnMajorOrder< double, TNL::Devices::Cuda, long  >, CuBLAStrsmWrapper< MatrixFormat::UnitDiagIn_L > >
>;
// clang-format on

TYPED_TEST_SUITE( CuBLAStrsmWrapperTest, MatrixSolverTypes );

TYPED_TEST( CuBLAStrsmWrapperTest, GetSolverNameTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_GetSolverName< MatrixType, SolverType >();
}

TYPED_TEST( CuBLAStrsmWrapperTest, SolveSystem2LinearEquationsTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystem2LinearEquations< MatrixType, SolverType >();
}

TYPED_TEST( CuBLAStrsmWrapperTest, SolveSystem3LinearEquationsTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystem3LinearEquations< MatrixType, SolverType >();
}

TYPED_TEST( CuBLAStrsmWrapperTest, SolveSystem3LinearEquations2Test )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystem3LinearEquations2< MatrixType, SolverType >();
}

TYPED_TEST( CuBLAStrsmWrapperTest, SolveWithPivotingSystem3LinearEquationsPivotingVectorTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveWithPivotingSystem3LinearEquations< MatrixType, SolverType, typename MatrixType::IndexType, TNL::Devices::Host >();
}

TYPED_TEST( CuBLAStrsmWrapperTest, SolveSystemLinearEquationsOnHostShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystemLinearEquationsOnHostShouldFail< MatrixType, SolverType >(
      "is implemented only for the GPU. The matrix must be stored on the Device and the pivoting vector on the Host." );
}

TYPED_TEST( CuBLAStrsmWrapperTest, SolveSystemLinearEquationsWithRowMajorOrderMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystemLinearEquationsWithRowMajorOrderMatrixShouldFail< MatrixType,
                                                                     SolverType,
                                                                     typename MatrixType::IndexType,
                                                                     TNL::Devices::Host >();
}

TYPED_TEST( CuBLAStrsmWrapperTest, SolveSystemLinearEquationsNonSquareMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystemLinearEquationsNonSquareMatrixShouldFail< MatrixType, SolverType >();
}

TYPED_TEST( CuBLAStrsmWrapperTest, SolveSystemLinearEquationsMismatchingDimensionsShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystemLinearEquationsMismatchingDimensionsShouldFail< MatrixType, SolverType >();
}

#endif  // HAVE_GTEST
