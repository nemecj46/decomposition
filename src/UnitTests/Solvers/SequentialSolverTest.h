#include <iostream>

#include <TNL/Matrices/DenseMatrix.h>

#include <Decomposition/LU/Solvers/SequentialSolver.h>

#include "SolverTests.h"

#ifdef HAVE_GTEST
   #include "gtest/gtest.h"

using namespace Decomposition::LU::Decomposers;
using namespace Decomposition::LU::Solvers;

template< typename Matrix, typename Solver >
void
test_GetSolverName()
{
   using Device = typename Matrix::DeviceType;
   bool partialPivoting = true;
   EXPECT_EQ( Solver::template getSolverName< Device >( partialPivoting ), "SS PP" );

   partialPivoting = false;
   EXPECT_EQ( Solver::template getSolverName< Device >( partialPivoting ), "SS" );
}

template< typename Matrix >
class SequentialSolverTest : public ::testing::Test
{
protected:
   using MatrixType = Matrix;
};

template< typename Real, typename Device, typename Index >
using DenseMatrixRowMajorOrder =
   typename TNL::Matrices::DenseMatrix< Real, Device, Index, TNL::Algorithms::Segments::ColumnMajorOrder >;

// clang-format off
using MatrixSolverTypes = ::testing::Types
<
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Host, short >, SequentialSolver<> >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Host, short >, SequentialSolver<> >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Host, int   >, SequentialSolver<> >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Host, int   >, SequentialSolver<> >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Host, long  >, SequentialSolver<> >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Host, long  >, SequentialSolver<> >
>;
// clang-format on

TYPED_TEST_SUITE( SequentialSolverTest, MatrixSolverTypes );

TYPED_TEST( SequentialSolverTest, GetSolverNameTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_GetSolverName< MatrixType, SolverType >();
}

TYPED_TEST( SequentialSolverTest, SolveSystem2LinearEquationsTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystem2LinearEquations< MatrixType, SolverType >();
}

TYPED_TEST( SequentialSolverTest, SolveSystem3LinearEquationsTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystem3LinearEquations< MatrixType, SolverType >();
}

TYPED_TEST( SequentialSolverTest, SolveSystem3LinearEquations2Test )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystem3LinearEquations2< MatrixType, SolverType >();
}

TYPED_TEST( SequentialSolverTest, SolveWithPivotingSystem3LinearEquationsPivotingVectorTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveWithPivotingSystem3LinearEquations< MatrixType, SolverType >();
}

TYPED_TEST( SequentialSolverTest, SolveSystemLinearEquationsOnDeviceShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystemLinearEquationsOnDeviceShouldFail< MatrixType, SolverType >();
}

TYPED_TEST( SequentialSolverTest, SolveSystemLinearEquationsWithUnitDiagonalInLShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;

   constexpr MatrixFormat Diag = MatrixFormat::UnitDiagIn_L;

   test_SolveSystemLinearEquationsWithUnitDiagonalInLShouldFail< MatrixType, SequentialSolver< Diag > >();
}

TYPED_TEST( SequentialSolverTest, SolveSystemLinearEquationsNonSquareMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystemLinearEquationsNonSquareMatrixShouldFail< MatrixType, SolverType >();
}

TYPED_TEST( SequentialSolverTest, SolveSystemLinearEquationsMismatchingDimensionsShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using SolverType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_SolveSystemLinearEquationsMismatchingDimensionsShouldFail< MatrixType, SolverType >();
}

#endif  // HAVE_GTEST
