#include <iostream>

#include <Decomposition/Containers/MatrixFormat.h>
#include <Decomposition/Exceptions/NotImplementedError.h>
#include <Decomposition/Exceptions/MatrixNotSquare.h>
#include <Decomposition/LU/Decomposers/BaseDecomposer.h>

#include "UnitTests/utils/LinearEquationSystems/TwoEquationsThreeRightSides.h"
#include "UnitTests/utils/LinearEquationSystems/ThreeEquationsThreeRightSides.h"
#include "UnitTests/utils/LinearEquationSystems/ThreeEquationsThreeRightSides2.h"
#include "UnitTests/utils/LinearEquationSystems/ThreeEquationsThreeRightSidesPivoting.h"

#ifdef HAVE_GTEST
   #include "UnitTests/utils/ResultVerification.h"
   #include "gtest/gtest.h"
   #include "gmock/gmock.h"

using ::testing::HasSubstr;
using ::testing::ThrowsMessage;

using namespace Decomposition::LU::Decomposers;
using namespace Decomposition::LU::Solvers;

using MatrixFormat = Decomposition::Containers::MatrixFormat;

template< typename Matrix, typename Solver >
void
test_SolveSystem2LinearEquations()
{
   // Solve system of linear equations:
   // AX = B
   // A = {  2,  5 },
   //     {  1,  2 }
   // B = { 21, 22, 23 },
   //     {  8,  9, 10 }

   Systems::TwoEquationsThreeRightSides< Matrix > twoEqsThreeRSs;
   const Matrix LU = twoEqsThreeRSs.getLU( Solver::getFormat() );

   Matrix X_1 = twoEqsThreeRSs.getB( 1 );

   Solver::solve( LU, X_1 );

   const Matrix X_1_correct = twoEqsThreeRSs.getX( 1 );
   ResultVerification::expectMatrixEqualToMatrix_X_X_correct( X_1, X_1_correct, "X_1", "X_1_correct" );

   Matrix X_12 = twoEqsThreeRSs.getB( 2 );

   Solver::solve( LU, X_12 );

   const Matrix X_12_correct = twoEqsThreeRSs.getX( 2 );
   ResultVerification::expectMatrixEqualToMatrix_X_X_correct( X_12, X_12_correct, "X_12", "X_12_correct" );

   Matrix X_123 = twoEqsThreeRSs.getB( 3 );

   Solver::solve( LU, X_123 );

   const Matrix X_123_correct = twoEqsThreeRSs.getX( 3 );
   ResultVerification::expectMatrixEqualToMatrix_X_X_correct( X_123, X_123_correct, "X_123", "X_123_correct" );

   // FIXME: Use this too when https://gitlab.com/tnl-project/tnl/-/issues/130 is resolved
   // Verify result using vectorProduct
   /*
   Matrix Ax_computed;
   Ax_computed.setLike( LU );
   Vector b_computed( b.getSize(), 0.0 );

   // Extract matrices L and U from LU
   Matrix L, U;
   std::tie( L, U ) = BaseDecomposer::getUnpackedMatrices( LU );

   Ax_computed.getMatrixProduct( L, U );

   Ax_computed.vectorProduct( x, b_computed );

   EXPECT_DOUBLE_EQ( b_computed.getElement( 0 ), b.getElement( 0 ) );
   EXPECT_DOUBLE_EQ( b_computed.getElement( 1 ), b.getElement( 1 ) );
   */
}

template< typename Matrix, typename Solver >
void
test_SolveSystem3LinearEquations()
{
   // Solve system of linear equations:
   // AX = B
   // A = {  2,  3, -1 },
   //     {  3,  2,  1 },
   //     {  1, -5,  3 }
   // B = {  5,  6,  7 },
   //     { 10, 11, 12 },
   //     {  0,  1,  2 }

   Systems::ThreeEquationsThreeRightSides< Matrix > threeEqsThreeRSs;
   const Matrix LU = threeEqsThreeRSs.getLU( Solver::getFormat() );

   Matrix X_1 = threeEqsThreeRSs.getB( 1 );

   Solver::solve( LU, X_1 );

   const Matrix X_1_correct = threeEqsThreeRSs.getX( 1 );
   ResultVerification::expectMatrixEqualToMatrix_X_X_correct( X_1, X_1_correct, "X_1", "X_1_correct" );

   Matrix X_12 = threeEqsThreeRSs.getB( 2 );

   Solver::solve( LU, X_12 );

   const Matrix X_12_correct = threeEqsThreeRSs.getX( 2 );
   ResultVerification::expectMatrixEqualToMatrix_X_X_correct( X_12, X_12_correct, "X_12", "X_12_correct" );

   Matrix X_123 = threeEqsThreeRSs.getB( 3 );

   Solver::solve( LU, X_123 );

   const Matrix X_123_correct = threeEqsThreeRSs.getX( 3 );
   ResultVerification::expectMatrixEqualToMatrix_X_X_correct( X_123, X_123_correct, "X_123", "X_123_correct" );

   // FIXME: Use this too when https://gitlab.com/tnl-project/tnl/-/issues/130 is resolved
   // Verify result using vectorProduct
   /*
   Matrix Ax_computed;
   Ax_computed.setLike( LU );
   Vector b_computed( b.getSize(), 0.0 );

   // Extract matrices L and U from LU
   Matrix L, U;
   std::tie( L, U ) = BaseDecomposer::getUnpackedMatrices( LU );
   Ax_computed.getMatrixProduct( L, U );

   Ax_computed.vectorProduct( x, b_computed );

   EXPECT_DOUBLE_EQ( b_computed.getElement( 0 ), b.getElement( 0 ) );
   EXPECT_DOUBLE_EQ( b_computed.getElement( 1 ), b.getElement( 1 ) );
   EXPECT_DOUBLE_EQ( b_computed.getElement( 2 ), b.getElement( 2 ) );
   */
}

template< typename Matrix, typename Solver >
void
test_SolveSystem3LinearEquations2()
{
   // Solve system of linear equations:
   // AX = B
   // A = { 1,  1,  1 },
   //     { 2, -1, -1 },
   //     { 1, -1,  1 }
   // B = { 3,  4,  5 },
   //     { 3,  4,  5 },
   //     { 9, 10, 11 }

   Systems::ThreeEquationsThreeRightSides2< Matrix > threeEqsThreeRSs;
   const Matrix LU = threeEqsThreeRSs.getLU( Solver::getFormat() );

   Matrix X_1 = threeEqsThreeRSs.getB( 1 );

   Solver::solve( LU, X_1 );

   const Matrix X_1_correct = threeEqsThreeRSs.getX( 1 );
   ResultVerification::expectMatrixEqualToMatrix_X_X_correct( X_1, X_1_correct, "X_1", "X_1_correct" );

   Matrix X_12 = threeEqsThreeRSs.getB( 2 );

   Solver::solve( LU, X_12 );

   const Matrix X_12_correct = threeEqsThreeRSs.getX( 2 );
   ResultVerification::expectMatrixEqualToMatrix_X_X_correct( X_12, X_12_correct, "X_12", "X_12_correct" );

   Matrix X_123 = threeEqsThreeRSs.getB( 3 );

   Solver::solve( LU, X_123 );

   const Matrix X_123_correct = threeEqsThreeRSs.getX( 3 );
   ResultVerification::expectMatrixEqualToMatrix_X_X_correct( X_123, X_123_correct, "X_123", "X_123_correct" );

   // FIXME: Use this too when https://gitlab.com/tnl-project/tnl/-/issues/130 is resolved
   // Verify result using vectorProduct
   /*
   Matrix Ax_computed;
   Ax_computed.setLike( LU );
   Vector b_computed( b.getSize(), 0.0 );

   // Extract matrices L and U from LU
   Matrix L, U;
   std::tie( L, U ) = BaseDecomposer::getUnpackedMatrices( LU );
   Ax_computed.getMatrixProduct( L, U );

   Ax_computed.vectorProduct( x, b_computed );

   EXPECT_DOUBLE_EQ( b_computed.getElement( 0 ), b.getElement( 0 ) );
   EXPECT_DOUBLE_EQ( b_computed.getElement( 1 ), b.getElement( 1 ) );
   EXPECT_DOUBLE_EQ( b_computed.getElement( 2 ), b.getElement( 2 ) );
   */
}

template< typename Matrix,
          typename Solver,
          typename Index = typename Matrix::IndexType,
          typename VecDevice = typename Matrix::DeviceType >
void
test_SolveWithPivotingSystem3LinearEquations()
{
   using Vector = typename TNL::Containers::Vector< Index, VecDevice, Index >;

   // Solve system of linear equations:
   // AX = B
   // A = {  0, -3,  1 },
   //     {  1,  0,  1 },
   //     {  2,  1,  3 }
   // B = {  7,  8,  9 },
   //     {  6,  7,  8 },
   //     { 15, 16, 17 }

   Systems::ThreeEquationsThreeRightSidesPivoting< Matrix, Vector > threeEqsThreeRSsPiv;

   const Matrix LU = threeEqsThreeRSsPiv.getLU( Solver::getFormat() );

   Vector piv = threeEqsThreeRSsPiv.getPivotingVector( Solver::getFormat() );

   Matrix X_1 = threeEqsThreeRSsPiv.getB( 1 );

   Solver::solve( LU, X_1, piv );

   const Matrix X_1_correct = threeEqsThreeRSsPiv.getX( 1 );
   ResultVerification::expectMatrixEqualToMatrix_X_X_correct( X_1, X_1_correct, "X_1", "X_1_correct" );

   Matrix X_12 = threeEqsThreeRSsPiv.getB( 2 );

   Solver::solve( LU, X_12, piv );

   const Matrix X_12_correct = threeEqsThreeRSsPiv.getX( 2 );
   ResultVerification::expectMatrixEqualToMatrix_X_X_correct( X_12, X_12_correct, "X_12", "X_12_correct" );

   Matrix X_123 = threeEqsThreeRSsPiv.getB( 3 );

   Solver::solve( LU, X_123, piv );

   const Matrix X_123_correct = threeEqsThreeRSsPiv.getX( 3 );
   ResultVerification::expectMatrixEqualToMatrix_X_X_correct( X_123, X_123_correct, "X_123", "X_123_correct" );

   // FIXME: Use this too when https://gitlab.com/tnl-project/tnl/-/issues/130 is resolved
   // Verify result using vectorProduct
   /*
   Matrix Ax_computed;
   Ax_computed.setLike( LU );
   VectorReal b_computed( b.getSize(), 0.0 );

   // Extract matrices L and U from LU
   Matrix L, U;
   std::tie( L, U ) = BaseDecomposer::getUnpackedMatrices( LU );
   Ax_computed.getMatrixProduct( L, U );

   // Undo order in matrix according to Vector
   BaseDecomposer::undoOrderInMatrixAccordingTo( Ax_computed, piv );

   Ax_computed.vectorProduct( x, b_computed );

   EXPECT_DOUBLE_EQ( b_computed.getElement( 0 ), b.getElement( 0 ) );
   EXPECT_DOUBLE_EQ( b_computed.getElement( 1 ), b.getElement( 1 ) );
   EXPECT_DOUBLE_EQ( b_computed.getElement( 2 ), b.getElement( 2 ) );
   */
}

template< typename Matrix,
          typename Solver,
          typename Index = typename Matrix::IndexType,
          typename VecDevice = typename Matrix::DeviceType >
void
test_SolveSystemLinearEquationsOnHostShouldFail( const TNL::String& pivotingExceptionMessage_suffix )
{
   using Real = typename Matrix::RealType;
   using Device = typename Matrix::DeviceType;
   using HostMatrix = TNL::Matrices::DenseMatrix< Real, TNL::Devices::Host, Index >;

   const bool partialPivoting = false;
   // clang-format off
   const HostMatrix LU({
      { 1.0,  0.0, 0.0 },
      { 2.0, -3.0, 0.0 },
      { 1.0, -2.0, 2.0 }
   });
   // clang-format on

   // Used as vector b on input
   HostMatrix X( { { 1.0 }, { 1.0 }, { 1.0 } } );

   EXPECT_THAT(
      [ & ]()
      {
         Solver::solve( LU, X );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >( HasSubstr(
         "-!> " + Solver::template getSolverName< Device >( partialPivoting ) + " is implemented only for the GPU." ) ) );

   using Vector = TNL::Containers::Vector< Index, VecDevice, Index >;
   const Vector piv( LU.getRows() );

   EXPECT_THAT(
      [ & ]()
      {
         Solver::solve( LU, X, piv );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >( HasSubstr(
         "-!> " + Solver::template getSolverName< Device >( ! piv.empty() ) + " " + pivotingExceptionMessage_suffix ) ) );
}

template< typename Matrix, typename Solver >
void
test_SolveSystemLinearEquationsOnDeviceShouldFail()
{
   using Real = typename Matrix::RealType;
   using Index = typename Matrix::IndexType;
   using Device = typename Matrix::DeviceType;
   using CudaMatrix = TNL::Matrices::DenseMatrix< Real, TNL::Devices::Cuda, Index >;

   const bool partialPivoting = false;

   // clang-format off
   const CudaMatrix LU({
      { 1.0,  0.0, 0.0 },
      { 2.0, -3.0, 0.0 },
      { 1.0, -2.0, 2.0 }
   });
   // clang-format on

   // Used as vector b on input
   CudaMatrix X( { { 1 }, { 1 }, { 1 } } );

   EXPECT_THAT(
      [ & ]()
      {
         Solver::solve( LU, X );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >( HasSubstr(
         "-!> " + Solver::template getSolverName< Device >( partialPivoting ) + " is implemented only for the CPU." ) ) );

   using Vector = TNL::Containers::Vector< Index, Device, Index >;
   const Vector piv( LU.getRows() );

   EXPECT_THAT(
      [ & ]()
      {
         Solver::solve( LU, X, piv );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >( HasSubstr(
         "-!> " + Solver::template getSolverName< Device >( ! piv.empty() ) + " is implemented only for the CPU." ) ) );
}

template< typename Matrix, typename Solver, typename Index = typename Matrix::IndexType >
void
test_SolveSystemLinearEquationsPivotingVectorOnDeviceShouldFail( const TNL::String& exceptionMessage_suffix )
{
   using Device = typename Matrix::DeviceType;
   using VecDevice = TNL::Devices::Cuda;
   using CudaVector = TNL::Containers::Vector< Index, VecDevice, Index >;

   // clang-format off
   const Matrix LU({
      { 1.0,  0.0, 0.0 },
      { 2.0, -3.0, 0.0 },
      { 1.0, -2.0, 2.0 }
   });
   // clang-format on

   // Used as vector b on input
   Matrix X( { { 1.0 }, { 1.0 }, { 1.0 } } );

   const CudaVector piv( LU.getRows() );

   EXPECT_THAT(
      [ & ]()
      {
         Solver::solve( LU, X, piv );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >(
         HasSubstr( "-!> " + Solver::template getSolverName< Device >( ! piv.empty() ) + " " + exceptionMessage_suffix ) ) );
}

template< typename Matrix,
          typename Solver,
          typename Index = typename Matrix::IndexType,
          typename VecDevice = typename Matrix::DeviceType >
void
test_SolveSystemLinearEquationsWithRowMajorOrderMatrixShouldFail()
{
   using Real = typename Matrix::RealType;
   using Device = typename Matrix::DeviceType;
   using RowMajorOrderMatrix =
      TNL::Matrices::DenseMatrix< Real, TNL::Devices::Cuda, Index, TNL::Algorithms::Segments::RowMajorOrder >;

   const bool partialPivoting = false;

   Systems::ThreeEquationsThreeRightSides< RowMajorOrderMatrix > threeEqsThreeRSs;
   const RowMajorOrderMatrix LU = threeEqsThreeRSs.getLU( Solver::getFormat() );

   RowMajorOrderMatrix X( { { 3 }, { 3 }, { 9 } } );

   EXPECT_THAT(
      [ & ]()
      {
         Solver::solve( LU, X );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >(
         HasSubstr( "-!> Input matrix uses row-major ordering - " + Solver::template getSolverName< Device >( partialPivoting )
                    + " only supports column-major ordering of matrices in GPU memory." ) ) );

   using Vector = TNL::Containers::Vector< Index, VecDevice, Index >;
   const Vector piv( LU.getRows() );

   EXPECT_THAT(
      [ & ]()
      {
         Solver::solve( LU, X, piv );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >(
         HasSubstr( "-!> Input matrix uses row-major ordering - " + Solver::template getSolverName< Device >( ! piv.empty() )
                    + " only supports column-major ordering of matrices in GPU memory." ) ) );
}

template< typename Matrix,
          typename Solver,
          typename Index = typename Matrix::IndexType,
          typename VecDevice = typename Matrix::DeviceType >
void
test_SolveSystemLinearEquationsWithUnitDiagonalInLShouldFail()
{
   using Device = typename Matrix::DeviceType;

   const bool partialPivoting = false;

   Systems::ThreeEquationsThreeRightSides< Matrix > threeEqsThreeRSs;
   const Matrix LU = threeEqsThreeRSs.getLU( Solver::getFormat() );

   Matrix X( { { 3 }, { 3 }, { 9 } } );

   EXPECT_THAT(
      [ & ]()
      {
         Solver::solve( LU, X );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >( HasSubstr(
         "-!> " + Solver::template getSolverName< Device >( partialPivoting ) + " only supports a unit diagonal in U." ) ) );

   using Vector = TNL::Containers::Vector< Index, VecDevice, Index >;
   const Vector piv( LU.getRows() );

   EXPECT_THAT(
      [ & ]()
      {
         Solver::solve( LU, X, piv );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >( HasSubstr(
         "-!> " + Solver::template getSolverName< Device >( ! piv.empty() ) + " only supports a unit diagonal in U." ) ) );
}

template< typename Matrix, typename Solver >
void
test_SolveSystemLinearEquationsNonSquareMatrixShouldFail()
{
   Systems::ThreeEquationsThreeRightSides< Matrix > threeEqsThreeRSs;
   const Matrix LU( {
      { 1, 1, 1 },
      { 2, 2, 2 },
   } );

   Matrix X( { { 3 }, { 3 } } );

   EXPECT_THAT(
      [ & ]()
      {
         Solver::solve( LU, X );
      },
      ThrowsMessage< Decomposition::Exceptions::MatrixNotSquare >(
         HasSubstr( "-!> Input matrix 'LU' is not a square matrix. Rows: 2, Columns: 3" ) ) );
}

template< typename Matrix, typename Solver >
void
test_SolveSystemLinearEquationsMismatchingDimensionsShouldFail()
{
   Systems::ThreeEquationsThreeRightSides< Matrix > threeEqsThreeRSs;
   const Matrix LU = threeEqsThreeRSs.getLU( Solver::getFormat() );

   Matrix X( { { 3 }, { 3 } } );

   EXPECT_THAT(
      [ & ]()
      {
         Solver::solve( LU, X );
      },
      ThrowsMessage< std::runtime_error >(
         HasSubstr( "-!> Matrix dimensions do not match: LU has 3 rows while X (which holds B's values) has 2 rows." ) ) );
}

#endif  // HAVE_GTEST
