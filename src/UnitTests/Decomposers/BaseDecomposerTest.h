#include <iostream>

#include <TNL/Matrices/DenseMatrix.h>
#include <TNL/String.h>

#include <Decomposition/Containers/MatrixFormat.h>
#include <Decomposition/Exceptions/NotImplementedError.h>
#include <Decomposition/LU/Decomposers/BaseDecomposer.h>

#include "UnitTests/utils/Matrices/FourByFour.h"
#include "UnitTests/utils/Matrices/FourByFourPivoting.h"

#ifdef HAVE_GTEST
   #include "UnitTests/utils/ResultVerification.h"
   #include "gtest/gtest.h"
   #include "gmock/gmock.h"

using ::testing::HasSubstr;
using ::testing::ThrowsMessage;

using namespace Decomposition::LU::Decomposers;

using MatrixFormat = Decomposition::Containers::MatrixFormat;

template< typename Index, typename Device >
using VectorType = TNL::Containers::Vector< Index, Device, Index >;

template< typename Matrix >
void
test_GetDecomposerName()
{
   EXPECT_EQ( BaseDecomposer::getDecomposerName(), "Base Decomposer" );
}

template< typename Matrix >
void
test_SwapRows()
{
   using Index = typename Matrix::IndexType;

   Matrices::FourByFourPivoting< Matrix > fourByFourPivoting;

   Matrix A_orig;
   Matrix A;
   A_orig = fourByFourPivoting.getA();
   A = A_orig;

   Index num_cols = A.getColumns();

   Index row1 = 0;
   Index row2 = 1;

   auto A_view = A.getView();

   BaseDecomposer::swapRows( A_view, row1, row2, num_cols );

   EXPECT_EQ( A.getElement( 0, 0 ), 1.0 );
   EXPECT_EQ( A.getElement( 0, 1 ), 8.0 );
   EXPECT_EQ( A.getElement( 0, 2 ), 4.0 );
   EXPECT_EQ( A.getElement( 0, 3 ), 2.0 );
   EXPECT_EQ( A.getElement( 1, 0 ), 0.0 );
   EXPECT_EQ( A.getElement( 1, 1 ), 7.0 );
   EXPECT_EQ( A.getElement( 1, 2 ), 2.0 );
   EXPECT_EQ( A.getElement( 1, 3 ), 5.0 );
   EXPECT_EQ( A.getElement( 2, 0 ), 2.0 );
   EXPECT_EQ( A.getElement( 2, 1 ), 1.0 );
   EXPECT_EQ( A.getElement( 2, 2 ), 9.0 );
   EXPECT_EQ( A.getElement( 2, 3 ), 3.0 );
   EXPECT_EQ( A.getElement( 3, 0 ), 5.0 );
   EXPECT_EQ( A.getElement( 3, 1 ), 4.0 );
   EXPECT_EQ( A.getElement( 3, 2 ), 7.0 );
   EXPECT_EQ( A.getElement( 3, 3 ), 1.0 );

   row1 = 0;
   row2 = 3;

   BaseDecomposer::swapRows( A_view, row1, row2, num_cols );

   EXPECT_EQ( A.getElement( 0, 0 ), 5.0 );
   EXPECT_EQ( A.getElement( 0, 1 ), 4.0 );
   EXPECT_EQ( A.getElement( 0, 2 ), 7.0 );
   EXPECT_EQ( A.getElement( 0, 3 ), 1.0 );
   EXPECT_EQ( A.getElement( 1, 0 ), 0.0 );
   EXPECT_EQ( A.getElement( 1, 1 ), 7.0 );
   EXPECT_EQ( A.getElement( 1, 2 ), 2.0 );
   EXPECT_EQ( A.getElement( 1, 3 ), 5.0 );
   EXPECT_EQ( A.getElement( 2, 0 ), 2.0 );
   EXPECT_EQ( A.getElement( 2, 1 ), 1.0 );
   EXPECT_EQ( A.getElement( 2, 2 ), 9.0 );
   EXPECT_EQ( A.getElement( 2, 3 ), 3.0 );
   EXPECT_EQ( A.getElement( 3, 0 ), 1.0 );
   EXPECT_EQ( A.getElement( 3, 1 ), 8.0 );
   EXPECT_EQ( A.getElement( 3, 2 ), 4.0 );
   EXPECT_EQ( A.getElement( 3, 3 ), 2.0 );

   row1 = 1;
   row2 = 2;

   BaseDecomposer::swapRows( A_view, row1, row2, num_cols );

   EXPECT_EQ( A.getElement( 0, 0 ), 5.0 );
   EXPECT_EQ( A.getElement( 0, 1 ), 4.0 );
   EXPECT_EQ( A.getElement( 0, 2 ), 7.0 );
   EXPECT_EQ( A.getElement( 0, 3 ), 1.0 );
   EXPECT_EQ( A.getElement( 1, 0 ), 2.0 );
   EXPECT_EQ( A.getElement( 1, 1 ), 1.0 );
   EXPECT_EQ( A.getElement( 1, 2 ), 9.0 );
   EXPECT_EQ( A.getElement( 1, 3 ), 3.0 );
   EXPECT_EQ( A.getElement( 2, 0 ), 0.0 );
   EXPECT_EQ( A.getElement( 2, 1 ), 7.0 );
   EXPECT_EQ( A.getElement( 2, 2 ), 2.0 );
   EXPECT_EQ( A.getElement( 2, 3 ), 5.0 );
   EXPECT_EQ( A.getElement( 3, 0 ), 1.0 );
   EXPECT_EQ( A.getElement( 3, 1 ), 8.0 );
   EXPECT_EQ( A.getElement( 3, 2 ), 4.0 );
   EXPECT_EQ( A.getElement( 3, 3 ), 2.0 );

   // Revert the swaps
   row1 = 1;
   row2 = 2;
   BaseDecomposer::swapRows( A_view, row1, row2, num_cols );

   row1 = 0;
   row2 = 3;
   BaseDecomposer::swapRows( A_view, row1, row2, num_cols );

   row1 = 0;
   row2 = 1;
   BaseDecomposer::swapRows( A_view, row1, row2, num_cols );

   ResultVerification::expectMatrixEqualToMatrix_A_Aorig( A, A_orig );
}

template< typename Matrix, typename Vector >
void
test_Order4x4MatrixAccordingToPivVector()
{
   using Index = typename Matrix::IndexType;
   using Device = typename Matrix::DeviceType;

   Matrices::FourByFourPivoting< Matrix > fourByFourPivoting;

   Matrix A_orig;
   Matrix A;
   A_orig = fourByFourPivoting.getA();
   A = A_orig;

   Vector vec_unchanged_order( { 1, 2, 3, 4 } );

   BaseDecomposer::orderMatrixAccordingTo( A, vec_unchanged_order );

   ResultVerification::expectMatrixEqualToMatrix_A_Aorig( A, A_orig );

   Vector piv( { 4, 4, 3, 4 } );

   BaseDecomposer::orderMatrixAccordingTo( A, piv );

   EXPECT_EQ( A.getElement( 0, 0 ), 5.0 );
   EXPECT_EQ( A.getElement( 0, 1 ), 4.0 );
   EXPECT_EQ( A.getElement( 0, 2 ), 7.0 );
   EXPECT_EQ( A.getElement( 0, 3 ), 1.0 );
   EXPECT_EQ( A.getElement( 1, 0 ), 0.0 );
   EXPECT_EQ( A.getElement( 1, 1 ), 7.0 );
   EXPECT_EQ( A.getElement( 1, 2 ), 2.0 );
   EXPECT_EQ( A.getElement( 1, 3 ), 5.0 );
   EXPECT_EQ( A.getElement( 2, 0 ), 2.0 );
   EXPECT_EQ( A.getElement( 2, 1 ), 1.0 );
   EXPECT_EQ( A.getElement( 2, 2 ), 9.0 );
   EXPECT_EQ( A.getElement( 2, 3 ), 3.0 );
   EXPECT_EQ( A.getElement( 3, 0 ), 1.0 );
   EXPECT_EQ( A.getElement( 3, 1 ), 8.0 );
   EXPECT_EQ( A.getElement( 3, 2 ), 4.0 );
   EXPECT_EQ( A.getElement( 3, 3 ), 2.0 );

   // Test for Solvers: M is not a square matrix and ordering according to default vector does not alter it.
   // The elements of each row are equal to the row's index, e.g. row 1 -> { 1, 1, 1, 1, 1, ..., 1 }
   const Index num_rows = 36;
   const Index num_cols = 3;
   Matrix M( num_rows, num_cols );

   auto M_view = M.getView();
   auto setRows = [ = ] __cuda_callable__( Index rowIdx ) mutable
   {
      auto row = M_view.getRow( rowIdx );
      for( Index i = 0; i < num_cols; ++i ) {
         row.setValue( i, rowIdx );
      }
   };
   TNL::Algorithms::parallelFor< Device >( Index{ 0 }, num_rows, setRows );

   Matrix Mcopy = M;

   Vector pivBig( num_rows );
   BaseDecomposer::setDefaultPivotingValues( pivBig );

   BaseDecomposer::orderMatrixAccordingTo( Mcopy, pivBig );

   EXPECT_EQ( M, Mcopy );
}

template< typename Matrix, typename Vector >
void
test_UndoOrderIn4x4MatrixAccordingToPivVector()
{
   using Index = typename Matrix::IndexType;
   using Device = typename Matrix::DeviceType;

   Matrices::FourByFourPivoting< Matrix > fourByFourPivoting;

   Matrix A_orig;
   Matrix A;
   A_orig = fourByFourPivoting.getA();
   A = A_orig;

   Vector vec_unchanged_order( { 1, 2, 3, 4 } );

   BaseDecomposer::undoOrderInMatrixAccordingTo( A, vec_unchanged_order );

   ResultVerification::expectMatrixEqualToMatrix_A_Aorig( A, A_orig );

   Vector piv( { 4, 4, 3, 4 } );

   BaseDecomposer::undoOrderInMatrixAccordingTo( A, piv );

   EXPECT_EQ( A.getElement( 0, 0 ), 1.0 );
   EXPECT_EQ( A.getElement( 0, 1 ), 8.0 );
   EXPECT_EQ( A.getElement( 0, 2 ), 4.0 );
   EXPECT_EQ( A.getElement( 0, 3 ), 2.0 );
   EXPECT_EQ( A.getElement( 1, 0 ), 5.0 );
   EXPECT_EQ( A.getElement( 1, 1 ), 4.0 );
   EXPECT_EQ( A.getElement( 1, 2 ), 7.0 );
   EXPECT_EQ( A.getElement( 1, 3 ), 1.0 );
   EXPECT_EQ( A.getElement( 2, 0 ), 2.0 );
   EXPECT_EQ( A.getElement( 2, 1 ), 1.0 );
   EXPECT_EQ( A.getElement( 2, 2 ), 9.0 );
   EXPECT_EQ( A.getElement( 2, 3 ), 3.0 );
   EXPECT_EQ( A.getElement( 3, 0 ), 0.0 );
   EXPECT_EQ( A.getElement( 3, 1 ), 7.0 );
   EXPECT_EQ( A.getElement( 3, 2 ), 2.0 );
   EXPECT_EQ( A.getElement( 3, 3 ), 5.0 );

   Vector piv2( { 2, 4, 3, 4 } );

   BaseDecomposer::undoOrderInMatrixAccordingTo( A, piv2 );

   ResultVerification::expectMatrixEqualToMatrix_A_Aorig( A, A_orig );

   // Test for Solvers: M is not a square matrix and undoing ordering according to default vector does not alter it.
   const Index num_rows = 36;
   const Index num_cols = 3;
   Matrix M( num_rows, num_cols );

   auto M_view = M.getView();
   auto setRows = [ = ] __cuda_callable__( Index rowIdx ) mutable
   {
      auto row = M_view.getRow( rowIdx );
      for( Index i = 0; i < num_cols; ++i ) {
         row.setValue( i, rowIdx );
      }
   };
   TNL::Algorithms::parallelFor< Device >( Index{ 0 }, num_rows, setRows );

   Matrix Mcopy = M;

   Vector pivBig( num_rows );
   BaseDecomposer::setDefaultPivotingValues( pivBig );

   BaseDecomposer::undoOrderInMatrixAccordingTo( Mcopy, pivBig );

   EXPECT_EQ( M, Mcopy );
}

template< typename Matrix, typename Vector >
void
test_PivotRowsOf4x4Matrices()
{
   using Real = typename Matrix::RealType;
   using Index = typename Matrix::IndexType;

   Matrix A( {
      { 0, 7, 9, 5 },
      { 1, 8, 4, 2 },
      { 2, 1, 8, 3 },
      { 5, 4, 7, 1 }
   } );

   Matrix LU;
   LU = A;

   Vector piv( A.getRows() );
   BaseDecomposer::setDefaultPivotingValues( piv );

   Index row = 0;

   auto A_view = A.getView();
   auto LU_view = LU.getView();
   auto piv_view = piv.getView();

   // Find max. abs in column 0 below row 0 and swap the two rows (0 <-> 3)
   std::pair< Real, Index > max_elem =
      BaseDecomposer::pivotRowOfMatrices( row, A_view, LU_view, A.getRows(), A.getColumns(), piv_view );
   Index pivRow = max_elem.second;

   EXPECT_EQ( pivRow, 3 );

   EXPECT_EQ( A.getElement( 0, 0 ), 5.0 );
   EXPECT_EQ( A.getElement( 0, 1 ), 4.0 );
   EXPECT_EQ( A.getElement( 0, 2 ), 7.0 );
   EXPECT_EQ( A.getElement( 0, 3 ), 1.0 );
   EXPECT_EQ( A.getElement( 1, 0 ), 1.0 );
   EXPECT_EQ( A.getElement( 1, 1 ), 8.0 );
   EXPECT_EQ( A.getElement( 1, 2 ), 4.0 );
   EXPECT_EQ( A.getElement( 1, 3 ), 2.0 );
   EXPECT_EQ( A.getElement( 2, 0 ), 2.0 );
   EXPECT_EQ( A.getElement( 2, 1 ), 1.0 );
   EXPECT_EQ( A.getElement( 2, 2 ), 8.0 );
   EXPECT_EQ( A.getElement( 2, 3 ), 3.0 );
   EXPECT_EQ( A.getElement( 3, 0 ), 0.0 );
   EXPECT_EQ( A.getElement( 3, 1 ), 7.0 );
   EXPECT_EQ( A.getElement( 3, 2 ), 9.0 );
   EXPECT_EQ( A.getElement( 3, 3 ), 5.0 );

   EXPECT_EQ( LU.getElement( 0, 0 ), 5.0 );
   EXPECT_EQ( LU.getElement( 0, 1 ), 4.0 );
   EXPECT_EQ( LU.getElement( 0, 2 ), 7.0 );
   EXPECT_EQ( LU.getElement( 0, 3 ), 1.0 );
   EXPECT_EQ( LU.getElement( 1, 0 ), 1.0 );
   EXPECT_EQ( LU.getElement( 1, 1 ), 8.0 );
   EXPECT_EQ( LU.getElement( 1, 2 ), 4.0 );
   EXPECT_EQ( LU.getElement( 1, 3 ), 2.0 );
   EXPECT_EQ( LU.getElement( 2, 0 ), 2.0 );
   EXPECT_EQ( LU.getElement( 2, 1 ), 1.0 );
   EXPECT_EQ( LU.getElement( 2, 2 ), 8.0 );
   EXPECT_EQ( LU.getElement( 2, 3 ), 3.0 );
   EXPECT_EQ( LU.getElement( 3, 0 ), 0.0 );
   EXPECT_EQ( LU.getElement( 3, 1 ), 7.0 );
   EXPECT_EQ( LU.getElement( 3, 2 ), 9.0 );
   EXPECT_EQ( LU.getElement( 3, 3 ), 5.0 );

   EXPECT_EQ( piv.getElement( 0 ), 4 );
   EXPECT_EQ( piv.getElement( 1 ), 2 );
   EXPECT_EQ( piv.getElement( 2 ), 3 );
   EXPECT_EQ( piv.getElement( 3 ), 4 );

   row = 1;
   // No larger max. abs in column 1 below row 1 -> no rows swapped
   max_elem = BaseDecomposer::pivotRowOfMatrices( row, A_view, LU_view, A.getRows(), A.getColumns(), piv_view );

   pivRow = max_elem.second;

   EXPECT_EQ( pivRow, 1 );

   EXPECT_EQ( A.getElement( 0, 0 ), 5.0 );
   EXPECT_EQ( A.getElement( 0, 1 ), 4.0 );
   EXPECT_EQ( A.getElement( 0, 2 ), 7.0 );
   EXPECT_EQ( A.getElement( 0, 3 ), 1.0 );
   EXPECT_EQ( A.getElement( 1, 0 ), 1.0 );
   EXPECT_EQ( A.getElement( 1, 1 ), 8.0 );
   EXPECT_EQ( A.getElement( 1, 2 ), 4.0 );
   EXPECT_EQ( A.getElement( 1, 3 ), 2.0 );
   EXPECT_EQ( A.getElement( 2, 0 ), 2.0 );
   EXPECT_EQ( A.getElement( 2, 1 ), 1.0 );
   EXPECT_EQ( A.getElement( 2, 2 ), 8.0 );
   EXPECT_EQ( A.getElement( 2, 3 ), 3.0 );
   EXPECT_EQ( A.getElement( 3, 0 ), 0.0 );
   EXPECT_EQ( A.getElement( 3, 1 ), 7.0 );
   EXPECT_EQ( A.getElement( 3, 2 ), 9.0 );
   EXPECT_EQ( A.getElement( 3, 3 ), 5.0 );

   EXPECT_EQ( LU.getElement( 0, 0 ), 5.0 );
   EXPECT_EQ( LU.getElement( 0, 1 ), 4.0 );
   EXPECT_EQ( LU.getElement( 0, 2 ), 7.0 );
   EXPECT_EQ( LU.getElement( 0, 3 ), 1.0 );
   EXPECT_EQ( LU.getElement( 1, 0 ), 1.0 );
   EXPECT_EQ( LU.getElement( 1, 1 ), 8.0 );
   EXPECT_EQ( LU.getElement( 1, 2 ), 4.0 );
   EXPECT_EQ( LU.getElement( 1, 3 ), 2.0 );
   EXPECT_EQ( LU.getElement( 2, 0 ), 2.0 );
   EXPECT_EQ( LU.getElement( 2, 1 ), 1.0 );
   EXPECT_EQ( LU.getElement( 2, 2 ), 8.0 );
   EXPECT_EQ( LU.getElement( 2, 3 ), 3.0 );
   EXPECT_EQ( LU.getElement( 3, 0 ), 0.0 );
   EXPECT_EQ( LU.getElement( 3, 1 ), 7.0 );
   EXPECT_EQ( LU.getElement( 3, 2 ), 9.0 );
   EXPECT_EQ( LU.getElement( 3, 3 ), 5.0 );

   EXPECT_EQ( piv.getElement( 0 ), 4 );
   EXPECT_EQ( piv.getElement( 1 ), 2 );
   EXPECT_EQ( piv.getElement( 2 ), 3 );
   EXPECT_EQ( piv.getElement( 3 ), 4 );

   row = 2;
   // Find max. abs in column 2 below row 2 and swap the two rows (2 <-> 3)
   max_elem = BaseDecomposer::pivotRowOfMatrix( row, A_view, A.getRows(), A.getColumns(), piv_view );

   pivRow = max_elem.second;

   EXPECT_EQ( pivRow, 3 );

   // Expect that A has changed, but LU has remained the same
   EXPECT_EQ( A.getElement( 0, 0 ), 5.0 );
   EXPECT_EQ( A.getElement( 0, 1 ), 4.0 );
   EXPECT_EQ( A.getElement( 0, 2 ), 7.0 );
   EXPECT_EQ( A.getElement( 0, 3 ), 1.0 );
   EXPECT_EQ( A.getElement( 1, 0 ), 1.0 );
   EXPECT_EQ( A.getElement( 1, 1 ), 8.0 );
   EXPECT_EQ( A.getElement( 1, 2 ), 4.0 );
   EXPECT_EQ( A.getElement( 1, 3 ), 2.0 );
   EXPECT_EQ( A.getElement( 2, 0 ), 0.0 );
   EXPECT_EQ( A.getElement( 2, 1 ), 7.0 );
   EXPECT_EQ( A.getElement( 2, 2 ), 9.0 );
   EXPECT_EQ( A.getElement( 2, 3 ), 5.0 );
   EXPECT_EQ( A.getElement( 3, 0 ), 2.0 );
   EXPECT_EQ( A.getElement( 3, 1 ), 1.0 );
   EXPECT_EQ( A.getElement( 3, 2 ), 8.0 );
   EXPECT_EQ( A.getElement( 3, 3 ), 3.0 );

   EXPECT_EQ( LU.getElement( 0, 0 ), 5.0 );
   EXPECT_EQ( LU.getElement( 0, 1 ), 4.0 );
   EXPECT_EQ( LU.getElement( 0, 2 ), 7.0 );
   EXPECT_EQ( LU.getElement( 0, 3 ), 1.0 );
   EXPECT_EQ( LU.getElement( 1, 0 ), 1.0 );
   EXPECT_EQ( LU.getElement( 1, 1 ), 8.0 );
   EXPECT_EQ( LU.getElement( 1, 2 ), 4.0 );
   EXPECT_EQ( LU.getElement( 1, 3 ), 2.0 );
   EXPECT_EQ( LU.getElement( 2, 0 ), 2.0 );
   EXPECT_EQ( LU.getElement( 2, 1 ), 1.0 );
   EXPECT_EQ( LU.getElement( 2, 2 ), 8.0 );
   EXPECT_EQ( LU.getElement( 2, 3 ), 3.0 );
   EXPECT_EQ( LU.getElement( 3, 0 ), 0.0 );
   EXPECT_EQ( LU.getElement( 3, 1 ), 7.0 );
   EXPECT_EQ( LU.getElement( 3, 2 ), 9.0 );
   EXPECT_EQ( LU.getElement( 3, 3 ), 5.0 );

   EXPECT_EQ( piv.getElement( 0 ), 4 );
   EXPECT_EQ( piv.getElement( 1 ), 2 );
   EXPECT_EQ( piv.getElement( 2 ), 4 );
   EXPECT_EQ( piv.getElement( 3 ), 4 );
}

template< typename Matrix >
void
test_Pack4x4Matrix()
{
   Matrices::FourByFour< Matrix > fourByFour;

   const MatrixFormat unitDiagonalIn_U = MatrixFormat::UnitDiagIn_U;

   const Matrix L = fourByFour.getL( unitDiagonalIn_U );
   const Matrix U = fourByFour.getU( unitDiagonalIn_U );

   Matrix LU = BaseDecomposer::packMatrices( L, U );

   const Matrix L_orig = fourByFour.getL( unitDiagonalIn_U );
   const Matrix U_orig = fourByFour.getU( unitDiagonalIn_U );

   ResultVerification::expectMatrixEqualToMatrix( L, L_orig );
   ResultVerification::expectMatrixEqualToMatrix( U, U_orig );

   EXPECT_EQ( LU.getElement( 0, 0 ), 2.0 );
   EXPECT_EQ( LU.getElement( 0, 1 ), 3.0 );
   EXPECT_EQ( LU.getElement( 0, 2 ), 0.0 );
   EXPECT_EQ( LU.getElement( 0, 3 ), 0.0 );
   EXPECT_EQ( LU.getElement( 1, 0 ), 3.0 );
   EXPECT_EQ( LU.getElement( 1, 1 ), -12.0 );
   EXPECT_EQ( LU.getElement( 1, 2 ), -1.0 );
   EXPECT_EQ( LU.getElement( 1, 3 ), 0.0 );
   EXPECT_EQ( LU.getElement( 2, 0 ), 1.0 );
   EXPECT_EQ( LU.getElement( 2, 1 ), 1.0 );
   EXPECT_EQ( LU.getElement( 2, 2 ), 4.0 );
   EXPECT_EQ( LU.getElement( 2, 3 ), 0.0 );
   EXPECT_EQ( LU.getElement( 3, 0 ), 0.0 );
   EXPECT_EQ( LU.getElement( 3, 1 ), 10.0 );
   EXPECT_EQ( LU.getElement( 3, 2 ), 34.0 );
   EXPECT_EQ( LU.getElement( 3, 3 ), 30.0 );
}

template< typename Matrix >
void
test_GetUnpacked4x4Matrix()
{
   Matrices::FourByFourPivoting< Matrix > fourByFourPivoting;

   const Matrix A = fourByFourPivoting.getA();

   Matrix L;
   Matrix U;
   std::tie( L, U ) = BaseDecomposer::getUnpackedMatrices( A, MatrixFormat::UnitDiagIn_U );

   const Matrix A_orig = fourByFourPivoting.getA();

   ResultVerification::expectMatrixEqualToMatrix_A_Aorig( A, A_orig );

   EXPECT_EQ( L.getElement( 0, 0 ), 0.0 );
   EXPECT_EQ( L.getElement( 0, 1 ), 0.0 );
   EXPECT_EQ( L.getElement( 0, 2 ), 0.0 );
   EXPECT_EQ( L.getElement( 0, 3 ), 0.0 );
   EXPECT_EQ( L.getElement( 1, 0 ), 1.0 );
   EXPECT_EQ( L.getElement( 1, 1 ), 8.0 );
   EXPECT_EQ( L.getElement( 1, 2 ), 0.0 );
   EXPECT_EQ( L.getElement( 1, 3 ), 0.0 );
   EXPECT_EQ( L.getElement( 2, 0 ), 2.0 );
   EXPECT_EQ( L.getElement( 2, 1 ), 1.0 );
   EXPECT_EQ( L.getElement( 2, 2 ), 9.0 );
   EXPECT_EQ( L.getElement( 2, 3 ), 0.0 );
   EXPECT_EQ( L.getElement( 3, 0 ), 5.0 );
   EXPECT_EQ( L.getElement( 3, 1 ), 4.0 );
   EXPECT_EQ( L.getElement( 3, 2 ), 7.0 );
   EXPECT_EQ( L.getElement( 3, 3 ), 1.0 );

   EXPECT_EQ( U.getElement( 0, 0 ), 1.0 );
   EXPECT_EQ( U.getElement( 0, 1 ), 7.0 );
   EXPECT_EQ( U.getElement( 0, 2 ), 2.0 );
   EXPECT_EQ( U.getElement( 0, 3 ), 5.0 );
   EXPECT_EQ( U.getElement( 1, 0 ), 0.0 );
   EXPECT_EQ( U.getElement( 1, 1 ), 1.0 );
   EXPECT_EQ( U.getElement( 1, 2 ), 4.0 );
   EXPECT_EQ( U.getElement( 1, 3 ), 2.0 );
   EXPECT_EQ( U.getElement( 2, 0 ), 0.0 );
   EXPECT_EQ( U.getElement( 2, 1 ), 0.0 );
   EXPECT_EQ( U.getElement( 2, 2 ), 1.0 );
   EXPECT_EQ( U.getElement( 2, 3 ), 3.0 );
   EXPECT_EQ( U.getElement( 3, 0 ), 0.0 );
   EXPECT_EQ( U.getElement( 3, 1 ), 0.0 );
   EXPECT_EQ( U.getElement( 3, 2 ), 0.0 );
   EXPECT_EQ( U.getElement( 3, 3 ), 1.0 );

   L.reset();
   U.reset();

   std::tie( L, U ) = BaseDecomposer::getUnpackedMatrices( A, MatrixFormat::UnitDiagIn_L );

   ResultVerification::expectMatrixEqualToMatrix_A_Aorig( A, A_orig );

   EXPECT_EQ( L.getElement( 0, 0 ), 1.0 );
   EXPECT_EQ( L.getElement( 0, 1 ), 0.0 );
   EXPECT_EQ( L.getElement( 0, 2 ), 0.0 );
   EXPECT_EQ( L.getElement( 0, 3 ), 0.0 );
   EXPECT_EQ( L.getElement( 1, 0 ), 1.0 );
   EXPECT_EQ( L.getElement( 1, 1 ), 1.0 );
   EXPECT_EQ( L.getElement( 1, 2 ), 0.0 );
   EXPECT_EQ( L.getElement( 1, 3 ), 0.0 );
   EXPECT_EQ( L.getElement( 2, 0 ), 2.0 );
   EXPECT_EQ( L.getElement( 2, 1 ), 1.0 );
   EXPECT_EQ( L.getElement( 2, 2 ), 1.0 );
   EXPECT_EQ( L.getElement( 2, 3 ), 0.0 );
   EXPECT_EQ( L.getElement( 3, 0 ), 5.0 );
   EXPECT_EQ( L.getElement( 3, 1 ), 4.0 );
   EXPECT_EQ( L.getElement( 3, 2 ), 7.0 );
   EXPECT_EQ( L.getElement( 3, 3 ), 1.0 );

   EXPECT_EQ( U.getElement( 0, 0 ), 0.0 );
   EXPECT_EQ( U.getElement( 0, 1 ), 7.0 );
   EXPECT_EQ( U.getElement( 0, 2 ), 2.0 );
   EXPECT_EQ( U.getElement( 0, 3 ), 5.0 );
   EXPECT_EQ( U.getElement( 1, 0 ), 0.0 );
   EXPECT_EQ( U.getElement( 1, 1 ), 8.0 );
   EXPECT_EQ( U.getElement( 1, 2 ), 4.0 );
   EXPECT_EQ( U.getElement( 1, 3 ), 2.0 );
   EXPECT_EQ( U.getElement( 2, 0 ), 0.0 );
   EXPECT_EQ( U.getElement( 2, 1 ), 0.0 );
   EXPECT_EQ( U.getElement( 2, 2 ), 9.0 );
   EXPECT_EQ( U.getElement( 2, 3 ), 3.0 );
   EXPECT_EQ( U.getElement( 3, 0 ), 0.0 );
   EXPECT_EQ( U.getElement( 3, 1 ), 0.0 );
   EXPECT_EQ( U.getElement( 3, 2 ), 0.0 );
   EXPECT_EQ( U.getElement( 3, 3 ), 1.0 );
}

template< typename Vector >
void
test_SetDefaultPivotingValues()
{
   Vector vec( 8 );

   BaseDecomposer::setDefaultPivotingValues< 0 >( vec );

   EXPECT_EQ( vec.getElement( 0 ), 0 );
   EXPECT_EQ( vec.getElement( 1 ), 1 );
   EXPECT_EQ( vec.getElement( 2 ), 2 );
   EXPECT_EQ( vec.getElement( 3 ), 3 );
   EXPECT_EQ( vec.getElement( 4 ), 4 );
   EXPECT_EQ( vec.getElement( 5 ), 5 );
   EXPECT_EQ( vec.getElement( 6 ), 6 );
   EXPECT_EQ( vec.getElement( 7 ), 7 );

   BaseDecomposer::setDefaultPivotingValues( vec );

   EXPECT_EQ( vec.getElement( 0 ), 1 );
   EXPECT_EQ( vec.getElement( 1 ), 2 );
   EXPECT_EQ( vec.getElement( 2 ), 3 );
   EXPECT_EQ( vec.getElement( 3 ), 4 );
   EXPECT_EQ( vec.getElement( 4 ), 5 );
   EXPECT_EQ( vec.getElement( 5 ), 6 );
   EXPECT_EQ( vec.getElement( 6 ), 7 );
   EXPECT_EQ( vec.getElement( 7 ), 8 );
}

template< typename Matrix, typename Vector >
void
test_OrderMatrixAccordingToPivVectorMismatchingDimensionsShouldFail()
{
   Matrices::FourByFourPivoting< Matrix > fourByFourPivoting;

   Matrix A = fourByFourPivoting.getA();

   Vector piv( 3 );
   BaseDecomposer::setDefaultPivotingValues( piv );

   EXPECT_THAT(
      [ & ]()
      {
         BaseDecomposer::orderMatrixAccordingTo( A, piv );
      },
      ThrowsMessage< std::runtime_error >(
         HasSubstr( "-!> Failed to order Matrix according to Vector. The matrix has 4 rows while the vector has 3." ) ) );
}

template< typename Matrix, typename Vector >
void
test_UndoOrderInMatrixAccordingToPivVectorMismatchingDimensionsShouldFail()
{
   Matrices::FourByFourPivoting< Matrix > fourByFourPivoting;

   Matrix A = fourByFourPivoting.getA();

   Vector piv( 3 );
   BaseDecomposer::setDefaultPivotingValues( piv );

   EXPECT_THAT(
      [ & ]()
      {
         BaseDecomposer::undoOrderInMatrixAccordingTo( A, piv );
      },
      ThrowsMessage< std::runtime_error >( HasSubstr( "-!> Failed to undo order in Matrix according to Vector. The "
                                                      "matrix has 4 rows while the vector has 3." ) ) );
}

template< typename Matrix, typename Vector >
void
test_OrderMatrixAccordingToPivVectorMatrixOnHostVectorOnDeviceShouldFail()
{
   using Index = typename Matrix::IndexType;
   using CudaVector = VectorType< Index, TNL::Devices::Cuda >;

   // Vector will always be on the other device
   Matrices::FourByFourPivoting< Matrix > fourByFourPivoting;

   Matrix A = fourByFourPivoting.getA();

   CudaVector piv( A.getRows() );
   BaseDecomposer::setDefaultPivotingValues( piv );

   EXPECT_THAT(
      [ & ]()
      {
         BaseDecomposer::orderMatrixAccordingTo( A, piv );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >(
         HasSubstr( "-!> Ordering matrix according to vector failed! The matrix is allocated on the Host and the vector and on "
                    "the Device." ) ) );
}

template< typename Matrix, typename Vector >
void
test_UndoOrderInMatrixAccordingToPivVectorMatrixOnHostVectorOnDeviceShouldFail()
{
   using Index = typename Matrix::IndexType;
   using CudaVector = VectorType< Index, TNL::Devices::Cuda >;

   // Vector will always be on the other device
   Matrices::FourByFourPivoting< Matrix > fourByFourPivoting;

   Matrix A = fourByFourPivoting.getA();

   CudaVector piv( A.getRows() );
   BaseDecomposer::setDefaultPivotingValues( piv );

   EXPECT_THAT(
      [ & ]()
      {
         BaseDecomposer::undoOrderInMatrixAccordingTo( A, piv );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >(
         HasSubstr( "-!> Undoing ordering in matrix according to vector failed! The matrix is allocated on the Host and the "
                    "vector and on the Device." ) ) );
}

template< typename Matrix, typename Vector >
void
test_PivotRowsOf4x4MatrixAndMatricesTNLObjectsAllocatedOnDeviceShouldFail()
{
   using Index = typename Matrix::IndexType;

   Matrices::FourByFourPivoting< Matrix > fourByFourPivoting;

   Matrix A = fourByFourPivoting.getA();

   Vector piv( A.getRows() );
   BaseDecomposer::setDefaultPivotingValues( piv );

   auto A_view = A.getView();
   auto piv_view = piv.getView();

   const Index row = 0;

   EXPECT_THAT(
      [ & ]()
      {
         BaseDecomposer::pivotRowOfMatrix( row, A_view, A.getRows(), A.getColumns(), piv_view );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >(
         HasSubstr( "-!> pivotRowOfMatrix() supports two cases: all TNL objects on the Host, or the matrix on the Device and "
                    "the pivoting vector on the Host." ) ) );

   Matrix LU;
   LU = A;

   auto LU_view = LU.getView();

   EXPECT_THAT(
      [ & ]()
      {
         BaseDecomposer::pivotRowOfMatrices( row, A_view, LU_view, A.getRows(), A.getColumns(), piv_view );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >(
         HasSubstr( "-!> pivotRowOfMatrices() supports two cases: all TNL objects on the Host, or the matrix on the Device and "
                    "the pivoting vector on the Host." ) ) );
}

template< typename Matrix >
class BaseDecomposerTest : public ::testing::Test
{
protected:
   using MatrixType = Matrix;
};

// clang-format off
using MatrixTypes = ::testing::Types
<
   std::tuple< TNL::Matrices::DenseMatrix< float,  TNL::Devices::Host, short >, VectorType< short,   TNL::Devices::Host > >,
   std::tuple< TNL::Matrices::DenseMatrix< double, TNL::Devices::Host, short >, VectorType< short,   TNL::Devices::Host > >,
   std::tuple< TNL::Matrices::DenseMatrix< float,  TNL::Devices::Host, int   >, VectorType< int,     TNL::Devices::Host > >,
   std::tuple< TNL::Matrices::DenseMatrix< double, TNL::Devices::Host, int   >, VectorType< int,     TNL::Devices::Host > >,
   std::tuple< TNL::Matrices::DenseMatrix< float,  TNL::Devices::Host, int   >, VectorType< int64_t, TNL::Devices::Host > >,
   std::tuple< TNL::Matrices::DenseMatrix< double, TNL::Devices::Host, int   >, VectorType< int64_t, TNL::Devices::Host > >,
   std::tuple< TNL::Matrices::DenseMatrix< float,  TNL::Devices::Host, long  >, VectorType< long,    TNL::Devices::Host > >,
   std::tuple< TNL::Matrices::DenseMatrix< double, TNL::Devices::Host, long  >, VectorType< long,    TNL::Devices::Host > >
   #ifdef __CUDACC__
  ,std::tuple< TNL::Matrices::DenseMatrix< float,  TNL::Devices::Cuda, short >, VectorType< short,   TNL::Devices::Cuda > >,
   std::tuple< TNL::Matrices::DenseMatrix< double, TNL::Devices::Cuda, short >, VectorType< short,   TNL::Devices::Cuda > >,
   std::tuple< TNL::Matrices::DenseMatrix< float,  TNL::Devices::Cuda, int   >, VectorType< int,     TNL::Devices::Cuda > >,
   std::tuple< TNL::Matrices::DenseMatrix< double, TNL::Devices::Cuda, int   >, VectorType< int,     TNL::Devices::Cuda > >,
   std::tuple< TNL::Matrices::DenseMatrix< float,  TNL::Devices::Cuda, int   >, VectorType< int64_t, TNL::Devices::Cuda > >,
   std::tuple< TNL::Matrices::DenseMatrix< double, TNL::Devices::Cuda, int   >, VectorType< int64_t, TNL::Devices::Cuda > >,
   std::tuple< TNL::Matrices::DenseMatrix< float,  TNL::Devices::Cuda, long  >, VectorType< long,    TNL::Devices::Cuda > >,
   std::tuple< TNL::Matrices::DenseMatrix< double, TNL::Devices::Cuda, long  >, VectorType< long,    TNL::Devices::Cuda > >,
   std::tuple< TNL::Matrices::DenseMatrix< float,  TNL::Devices::Cuda, short >, VectorType< short,   TNL::Devices::Host > >,
   std::tuple< TNL::Matrices::DenseMatrix< double, TNL::Devices::Cuda, short >, VectorType< short,   TNL::Devices::Host > >,
   std::tuple< TNL::Matrices::DenseMatrix< float,  TNL::Devices::Cuda, int   >, VectorType< int,     TNL::Devices::Host > >,
   std::tuple< TNL::Matrices::DenseMatrix< double, TNL::Devices::Cuda, int   >, VectorType< int,     TNL::Devices::Host > >,
   std::tuple< TNL::Matrices::DenseMatrix< float,  TNL::Devices::Cuda, int   >, VectorType< int64_t, TNL::Devices::Host > >,
   std::tuple< TNL::Matrices::DenseMatrix< double, TNL::Devices::Cuda, int   >, VectorType< int64_t, TNL::Devices::Host > >,
   std::tuple< TNL::Matrices::DenseMatrix< float,  TNL::Devices::Cuda, long  >, VectorType< long,    TNL::Devices::Host > >,
   std::tuple< TNL::Matrices::DenseMatrix< double, TNL::Devices::Cuda, long  >, VectorType< long,    TNL::Devices::Host > >
   #endif
>;
// clang-format on

TYPED_TEST_SUITE( BaseDecomposerTest, MatrixTypes );

TYPED_TEST( BaseDecomposerTest, GetDecomposerNameTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;

   test_GetDecomposerName< MatrixType >();
}

TYPED_TEST( BaseDecomposerTest, SwapRowsTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;

   test_SwapRows< MatrixType >();
}

TYPED_TEST( BaseDecomposerTest, Order4x4MatrixAccordingToPivVectorTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using VectorTypeTest = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_Order4x4MatrixAccordingToPivVector< MatrixType, VectorTypeTest >();
}

TYPED_TEST( BaseDecomposerTest, UndoOrderIn4x4MatrixAccordingToPivVectorTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using VectorTypeTest = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_UndoOrderIn4x4MatrixAccordingToPivVector< MatrixType, VectorTypeTest >();
}

TYPED_TEST( BaseDecomposerTest, PivotRowsOf4x4MatricesTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using VectorTypeTest = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnCuda< VectorTypeTest >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >(
         testing::UnitTest::GetInstance()->current_test_info()->name(),
         "The vector is allocated on the Device while the test assumes it is allocated on the Host." );
   }

   test_PivotRowsOf4x4Matrices< MatrixType, VectorTypeTest >();
}

TYPED_TEST( BaseDecomposerTest, Pack4x4MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;

   test_Pack4x4Matrix< MatrixType >();
}

TYPED_TEST( BaseDecomposerTest, GetUnpacked4x4MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;

   test_GetUnpacked4x4Matrix< MatrixType >();
}

TYPED_TEST( BaseDecomposerTest, OrderMatrixAccordingToPivVectorMismatchingDimensionsShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using VectorTypeTest = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_OrderMatrixAccordingToPivVectorMismatchingDimensionsShouldFail< MatrixType, VectorTypeTest >();
}

TYPED_TEST( BaseDecomposerTest, UndoOrderInMatrixAccordingToPivVectorMismatchingDimensionsShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using VectorTypeTest = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_UndoOrderInMatrixAccordingToPivVectorMismatchingDimensionsShouldFail< MatrixType, VectorTypeTest >();
}

TYPED_TEST( BaseDecomposerTest, OrderMatrixAccordingToPivVectorMatrixOnHostVectorOnDeviceShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using VectorTypeTest = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnCuda< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >(
         testing::UnitTest::GetInstance()->current_test_info()->name(),
         "This is a failure test. The matrix is allocated on the Device while the test assumes it is allocated on the Host." );
   }

   test_OrderMatrixAccordingToPivVectorMatrixOnHostVectorOnDeviceShouldFail< MatrixType, VectorTypeTest >();
}

TYPED_TEST( BaseDecomposerTest, UndoOrderInMatrixAccordingToPivVectorMatrixOnHostVectorOnDeviceShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using VectorTypeTest = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnCuda< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >(
         testing::UnitTest::GetInstance()->current_test_info()->name(),
         "This is a failure test. The matrix is allocated on the Device while the test assumes it is allocated on the Host." );
   }

   test_UndoOrderInMatrixAccordingToPivVectorMatrixOnHostVectorOnDeviceShouldFail< MatrixType, VectorTypeTest >();
}

TYPED_TEST( BaseDecomposerTest, PivotRowsOf4x4MatrixAndMatricesTNLObjectsAllocatedOnDeviceShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using VectorTypeTest = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< VectorTypeTest >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >(
         testing::UnitTest::GetInstance()->current_test_info()->name(),
         "This is a failure test. The vector is allocated on the Host while the tests assumes it is allocated on the Device." );
   }

   test_PivotRowsOf4x4MatrixAndMatricesTNLObjectsAllocatedOnDeviceShouldFail< MatrixType, VectorTypeTest >();
}

#endif  // HAVE_GTEST
