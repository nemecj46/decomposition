#include <iostream>

#include <TNL/Matrices/DenseMatrix.h>

#include <Decomposition/LU/Decomposers/IterativeCroutMethod.h>

#include "DecomposerTests.h"

#ifdef HAVE_GTEST
   #include "gmock/gmock.h"
   #include "gtest/gtest.h"

using ::testing::MatchesRegex;
using namespace Decomposition::LU::Decomposers;

template< typename Matrix, typename Decomposer >
void
test_GetDecomposerName()
{
   using Device = typename Matrix::DeviceType;
   bool partialPivoting = false;

   if( Decomposition::isHost< Device >() ) {
      EXPECT_EQ( Decomposer::template getDecomposerName< Device >( partialPivoting ), "ICM" );
      partialPivoting = true;
      EXPECT_EQ( Decomposer::template getDecomposerName< Device >( partialPivoting ), "ICM PP" );
   }
   else {
      EXPECT_THAT( Decomposer::template getDecomposerName< Device >( partialPivoting ), MatchesRegex( "ICM_[0-9]+" ) );
      partialPivoting = true;
      EXPECT_THAT( Decomposer::template getDecomposerName< Device >( partialPivoting ), MatchesRegex( "ICM_[0-9]+ PP" ) );
   }
}

template< typename Matrix, typename Decomposer >
void
test_Decompose38x38MatrixDifferentInitialEstimateOfDecomposedMatrix()
{
   using Real = typename Matrix::RealType;

   Matrices::cage5< Matrix > cage5;

   Matrix A = cage5.getA();
   Matrix LU;
   LU.setLike( A );
   LU.setValue( 1 );

   Decomposer::decompose( A, LU );

   const Matrix LU_correct = cage5.getLU( Decomposer::getMatrixDiagonalFormat() );
   const Real tolerance = 1e-4;
   const Real zeroToleranceFallback = 0.0;
   ResultVerification::expectMatrixNearMatrix_LU_LUcorrect( LU, LU_correct, tolerance, zeroToleranceFallback );
}

template< typename Matrix, typename Decomposer >
void
test_DecomposeWithPivoting4x4PivotingMatrixDifferentInitialEstimateOfDecomposedMatrix()
{
   using Real = typename Matrix::RealType;
   using Index = typename Matrix::IndexType;
   using Vector = VectorType< Index, TNL::Devices::Host >;

   Matrices::FourByFourPivoting< Matrix > fourByFourPivoting;

   Matrix A = fourByFourPivoting.getA();
   Matrix LU;
   LU.setLike( A );
   LU.setValue( 1 );
   Vector piv( LU.getRows() );

   Decomposer::decompose( A, LU, piv );

   Matrix L;
   Matrix U;
   std::tie( L, U ) = Decomposer::getUnpackedMatrices( LU, Decomposer::getMatrixDiagonalFormat() );

   LU.getMatrixProduct( L, U );
   Decomposer::undoOrderInMatrixAccordingTo( LU, piv );

   const Matrix A_orig = fourByFourPivoting.getA();
   const Real tolerance = 1e-7;
   const Real zeroToleranceFallback = 0.0;
   ResultVerification::expectMatrixNearMatrix_LU_Aorig( LU, A_orig, tolerance, zeroToleranceFallback );
}

template< typename Matrix, typename Decomposer >
void
test_DecomposeMatrixWithPivotingFromInitialEstimateMismatchingDimensionsShouldFail()
{
   using Index = typename Matrix::IndexType;
   using Vector = VectorType< Index, TNL::Devices::Host >;

   // clang-format off
   Matrix A_extraRow( {
      { 4, 24 },
      { 4, 24 },
      { 2, 15 }
   });

   Matrix A_extraCol( {
      { 4, 24, 2 },
      { 2, 15, 2 }
   });

   Matrix LU( {
      { 4, 24 },
      { 2, 15 }
   });
   // clang-format on

   EXPECT_THAT(
      [ & ]()
      {
         Decomposer::decompose( A_extraRow, LU );
      },
      ThrowsMessage< Decomposition::Exceptions::MatricesDimensionsMismatch >(
         HasSubstr( "-!> Dimensions of matrices do not match! A( 3, 2 ) != LU( 2, 2 )" ) ) );

   EXPECT_THAT(
      [ & ]()
      {
         Decomposer::decompose( A_extraCol, LU );
      },
      ThrowsMessage< Decomposition::Exceptions::MatricesDimensionsMismatch >(
         HasSubstr( "-!> Dimensions of matrices do not match! A( 2, 3 ) != LU( 2, 2 )" ) ) );

   Vector piv( LU.getRows() );

   EXPECT_THAT(
      [ & ]()
      {
         Decomposer::decompose( A_extraRow, LU, piv );
      },
      ThrowsMessage< Decomposition::Exceptions::MatricesDimensionsMismatch >(
         HasSubstr( "-!> Dimensions of matrices do not match! A( 3, 2 ) != LU( 2, 2 )" ) ) );

   EXPECT_THAT(
      [ & ]()
      {
         Decomposer::decompose( A_extraCol, LU, piv );
      },
      ThrowsMessage< Decomposition::Exceptions::MatricesDimensionsMismatch >(
         HasSubstr( "-!> Dimensions of matrices do not match! A( 2, 3 ) != LU( 2, 2 )" ) ) );
}

template< typename Matrix, typename Decomposer >
void
test_DecomposeMatrixWithPivotingMatrixVectorRowsMismatchingShouldFail()
{
   using Index = typename Matrix::IndexType;
   using Vector = VectorType< Index, TNL::Devices::Host >;

   // clang-format off
   Matrix LU( {
      { 4, 24 },
      { 2, 15 }
   });
   // clang-format on

   Vector piv( LU.getRows() + 1 );

   EXPECT_THAT(
      [ & ]()
      {
         Decomposer::decompose( LU, piv );
      },
      ThrowsMessage< Decomposition::Exceptions::MatrixVectorRowsMismatch >(
         HasSubstr( "-!> Matrix and Vector do not have the same number of rows! A( 2 ) != piv( 3 )" ) ) );
}

template< typename Matrix, typename Decomposer >
void
test_GetMatrixDiagonalFormat()
{
   EXPECT_EQ( Decomposer::getMatrixDiagonalFormat(), MatrixFormat::UnitDiagIn_U );
}

template< typename Matrix >
class IterativeCroutMethodTest : public ::testing::Test
{
protected:
   using MatrixType = Matrix;
};

template< typename Real, typename Device, typename Index >
using DenseMatrixRowMajorOrder =
   typename TNL::Matrices::DenseMatrix< Real, Device, Index, TNL::Algorithms::Segments::RowMajorOrder >;

// clang-format off
using MatrixTypes = ::testing::Types
<
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Host, short >, IterativeCroutMethod<    > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Host, short >, IterativeCroutMethod<    > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Host, int   >, IterativeCroutMethod<    > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Host, int   >, IterativeCroutMethod<    > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Host, long  >, IterativeCroutMethod<    > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Host, long  >, IterativeCroutMethod<    > >
   #ifdef __CUDACC__
   ,std::tuple< DenseMatrixRowMajorOrder< float, TNL::Devices::Cuda, short >, IterativeCroutMethod<  8 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, short >, IterativeCroutMethod<  8 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, int   >, IterativeCroutMethod<  8 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, int   >, IterativeCroutMethod<  8 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, long  >, IterativeCroutMethod<  8 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, long  >, IterativeCroutMethod<  8 > >,

   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, short >, IterativeCroutMethod< 16 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, short >, IterativeCroutMethod< 16 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, int   >, IterativeCroutMethod< 16 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, int   >, IterativeCroutMethod< 16 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, long  >, IterativeCroutMethod< 16 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, long  >, IterativeCroutMethod< 16 > >,

   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, short >, IterativeCroutMethod< 32 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, short >, IterativeCroutMethod< 32 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, int   >, IterativeCroutMethod< 32 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, int   >, IterativeCroutMethod< 32 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, long  >, IterativeCroutMethod< 32 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, long  >, IterativeCroutMethod< 32 > >
   #endif
>;
// clang-format on

TYPED_TEST_SUITE( IterativeCroutMethodTest, MatrixTypes );

TYPED_TEST( IterativeCroutMethodTest, GetDecomposerNameTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_GetDecomposerName< MatrixType, DecomposerType >();
}

TYPED_TEST( IterativeCroutMethodTest, GetMatrixDiagonalFormatTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_GetMatrixDiagonalFormat< MatrixType, DecomposerType >();
}

TYPED_TEST( IterativeCroutMethodTest, Decompose2x2MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_Decompose2x2Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( IterativeCroutMethodTest, Decompose3x3MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_Decompose3x3Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( IterativeCroutMethodTest, Decompose4x4MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_Decompose4x4Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( IterativeCroutMethodTest, Decompose10x10MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_Decompose10x10Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( IterativeCroutMethodTest, Decompose19x19MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_Decompose19x19Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( IterativeCroutMethodTest, Decompose38x38MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_Decompose38x38Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( IterativeCroutMethodTest, Decompose38x38MatrixDifferentInitialEstimateOfDecomposedMatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_Decompose38x38MatrixDifferentInitialEstimateOfDecomposedMatrix< MatrixType, DecomposerType >();
}

TYPED_TEST( IterativeCroutMethodTest, DecomposeWithPivoting2x2MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }
   else {
      test_DecomposeWithPivoting2x2Matrix< MatrixType, DecomposerType, typename MatrixType::IndexType, TNL::Devices::Host >();
   }
}

TYPED_TEST( IterativeCroutMethodTest, DecomposeWithPivoting3x3MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }
   else {
      test_DecomposeWithPivoting3x3Matrix< MatrixType, DecomposerType, typename MatrixType::IndexType, TNL::Devices::Host >();
   }
}

TYPED_TEST( IterativeCroutMethodTest, DecomposeWithPivoting4x4MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }
   else {
      test_DecomposeWithPivoting4x4Matrix< MatrixType, DecomposerType, typename MatrixType::IndexType, TNL::Devices::Host >();
   }
}

TYPED_TEST( IterativeCroutMethodTest, DecomposeWithPivoting10x10MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }
   else {
      test_DecomposeWithPivoting10x10Matrix< MatrixType, DecomposerType, typename MatrixType::IndexType, TNL::Devices::Host >();
   }
}

TYPED_TEST( IterativeCroutMethodTest, DecomposeWithPivoting19x19MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }
   else {
      test_DecomposeWithPivoting19x19Matrix< MatrixType, DecomposerType, typename MatrixType::IndexType, TNL::Devices::Host >();
   }
}

TYPED_TEST( IterativeCroutMethodTest, DecomposeWithPivoting38x38MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }
   else {
      test_DecomposeWithPivoting38x38Matrix< MatrixType, DecomposerType, typename MatrixType::IndexType, TNL::Devices::Host >();
   }
}

TYPED_TEST( IterativeCroutMethodTest, DecomposeWithPivoting4x4PivotingMatrixDifferentInitialEstimateOfDecomposedMatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }
   else {
      test_DecomposeWithPivoting4x4PivotingMatrixDifferentInitialEstimateOfDecomposedMatrix< MatrixType, DecomposerType >();
   }
}

TYPED_TEST( IterativeCroutMethodTest, DecomposeWithPivoting4x4PivotingMatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }
   else {
      test_DecomposeWithPivoting4x4PivotingMatrix< MatrixType,
                                                   DecomposerType,
                                                   typename MatrixType::IndexType,
                                                   TNL::Devices::Host >( 1e-6 );
   }
}

TYPED_TEST( IterativeCroutMethodTest, DecomposeWithPivoting4x4MatrixWithZeroLastElementTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }
   else {
      test_DecomposeWithPivoting4x4MatrixWithZeroLastElement< MatrixType,
                                                              DecomposerType,
                                                              typename MatrixType::IndexType,
                                                              TNL::Devices::Host >( 1e-6 );
   }
}

TYPED_TEST( IterativeCroutMethodTest, DecomposeNonSquareMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeNonSquareMatrixShouldFail< MatrixType, DecomposerType >();
}

TYPED_TEST( IterativeCroutMethodTest, DecomposeWithPivotingNonSquareMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }
   else {
      test_DecomposeWithPivotingNonSquareMatrixShouldFail< MatrixType,
                                                           DecomposerType,
                                                           typename MatrixType::IndexType,
                                                           TNL::Devices::Host >();
   }
}

TYPED_TEST( IterativeCroutMethodTest, Decompose10x10SingularMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_Decompose10x10SingularMatrixShouldFail< MatrixType, DecomposerType >();
}

TYPED_TEST( IterativeCroutMethodTest, DecomposeWithPivoting10x10SingularMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }
   else {
      test_DecomposeWithPivoting10x10SingularMatrixShouldFail< MatrixType, DecomposerType, TNL::Devices::Host >();
   }
}

TYPED_TEST( IterativeCroutMethodTest, DecomposeWithPivotingMatrixAllocatedOnHostShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }
   else {
      test_DecomposeWithPivotingMatrixAllocatedOnHostShouldFail< MatrixType,
                                                                 DecomposerType,
                                                                 typename MatrixType::IndexType,
                                                                 TNL::Devices::Host >();
   }
}

TYPED_TEST( IterativeCroutMethodTest, DecomposeWithPivotingMatrixVectorAllocatedOnDeviceShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }
   else {
      test_DecomposeWithPivotingMatrixVectorAllocatedOnDeviceShouldFail< MatrixType, DecomposerType >(
         "is implemented only for the GPU. The matrix must be stored on the Device and the pivoting vector on the Host." );
   }
}

TYPED_TEST( IterativeCroutMethodTest, DecomposeMatrixFromInitialEstimateMismatchingDimensionsShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }
   else {
      test_DecomposeMatrixWithPivotingFromInitialEstimateMismatchingDimensionsShouldFail< MatrixType, DecomposerType >();
   }
}

TYPED_TEST( IterativeCroutMethodTest, DecomposeMatrixWithPivotingMatrixVectorRowsMismatchingShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }
   else {
      test_DecomposeMatrixWithPivotingMatrixVectorRowsMismatchingShouldFail< MatrixType, DecomposerType >();
   }
}

#endif  // HAVE_GTEST
