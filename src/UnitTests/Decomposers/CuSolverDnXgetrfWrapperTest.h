#include <iostream>

#include <TNL/Matrices/DenseMatrix.h>

#include <Decomposition/LU/Decomposers/CuSolverDnXgetrfWrapper.h>

#include "DecomposerTests.h"

#ifdef HAVE_GTEST
   #include "gtest/gtest.h"

using namespace Decomposition::LU::Decomposers;

template< typename Matrix, typename Decomposer >
void
test_GetDecomposerName()
{
   bool partialPivoting = true;
   using Device = typename Matrix::DeviceType;
   EXPECT_EQ( Decomposer::template getDecomposerName< Device >( partialPivoting ), "CuSolverDnXgetrfWrapper PP" );

   partialPivoting = false;
   EXPECT_EQ( Decomposer::template getDecomposerName< Device >( partialPivoting ), "CuSolverDnXgetrfWrapper" );
}

template< typename Matrix, typename Decomposer >
void
test_GetMatrixDiagonalFormat()
{
   EXPECT_EQ( Decomposer::getMatrixDiagonalFormat(), MatrixFormat::UnitDiagIn_L );
}

template< typename Matrix >
class CuSolverDnXgetrfWrapperTest : public ::testing::Test
{
protected:
   using MatrixType = Matrix;
};

template< typename Real, typename Device, typename Index >
using DenseMatrixColumnMajorOrder =
   typename TNL::Matrices::DenseMatrix< Real, Device, Index, TNL::Algorithms::Segments::ColumnMajorOrder >;

// clang-format off
using MatrixTypes = ::testing::Types
<
   #ifdef __CUDACC__
   std::tuple< DenseMatrixColumnMajorOrder< float,  TNL::Devices::Cuda, short >, CuSolverDnXgetrfWrapper >,
   std::tuple< DenseMatrixColumnMajorOrder< double, TNL::Devices::Cuda, short >, CuSolverDnXgetrfWrapper >,
   std::tuple< DenseMatrixColumnMajorOrder< float,  TNL::Devices::Cuda, int   >, CuSolverDnXgetrfWrapper >,
   std::tuple< DenseMatrixColumnMajorOrder< double, TNL::Devices::Cuda, int   >, CuSolverDnXgetrfWrapper >,
   std::tuple< DenseMatrixColumnMajorOrder< float,  TNL::Devices::Cuda, long  >, CuSolverDnXgetrfWrapper >,
   std::tuple< DenseMatrixColumnMajorOrder< double, TNL::Devices::Cuda, long  >, CuSolverDnXgetrfWrapper >
   #endif
>;
   // clang-format on

   // BUG: Block where clang-tidy is disabled - for some reason this is only needed here to avoid
   //      clang-tidy analyzing googletest files.
   #ifndef CLANG_TIDY

TYPED_TEST_SUITE( CuSolverDnXgetrfWrapperTest, MatrixTypes );

TYPED_TEST( CuSolverDnXgetrfWrapperTest, GetDecomposerNameTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_GetDecomposerName< MatrixType, DecomposerType >();
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, GetMatrixDiagonalFormatTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_GetMatrixDiagonalFormat< MatrixType, DecomposerType >();
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, Decompose2x2MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_Decompose2x2Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, Decompose3x3MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_Decompose3x3Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, Decompose4x4MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_Decompose4x4Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, Decompose10x10MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_Decompose10x10Matrix< MatrixType, DecomposerType >( 1e-5, 1e-4 );
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, Decompose19x19MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_Decompose19x19Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, Decompose38x38MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_Decompose38x38Matrix< MatrixType, DecomposerType >( 1e-5, 1e-9 );
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, DecomposeWithPivoting2x2MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting2x2Matrix< MatrixType, DecomposerType, int64_t >();
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, DecomposeWithPivoting3x3MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting3x3Matrix< MatrixType, DecomposerType, int64_t >();
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, DecomposeWithPivoting4x4MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting4x4Matrix< MatrixType, DecomposerType, int64_t >();
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, DecomposeWithPivoting10x10MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting10x10Matrix< MatrixType, DecomposerType, int64_t >();
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, DecomposeWithPivoting19x19MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting19x19Matrix< MatrixType, DecomposerType, int64_t >( 1e-5, 1e-4 );
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, DecomposeWithPivoting38x38MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting38x38Matrix< MatrixType, DecomposerType, int64_t >();
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, DecomposeWithPivoting4x4PivotingMatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting4x4PivotingMatrix< MatrixType, DecomposerType, int64_t >();
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, DecomposeNonSquareMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeNonSquareMatrixShouldFail< MatrixType, DecomposerType >();
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, DecomposeWithPivotingNonSquareMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivotingNonSquareMatrixShouldFail< MatrixType, DecomposerType, int64_t >();
}

TYPED_TEST( CuSolverDnXgetrfWrapperTest, DecomposeWithPivotingMatrixAllocatedOnHostShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivotingMatrixAllocatedOnHostShouldFail< MatrixType, DecomposerType, int64_t >();
}
   #endif
#endif  // HAVE_GTEST
