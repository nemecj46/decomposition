#include <iostream>

#include <TNL/Matrices/DenseMatrix.h>

#include <Decomposition/LU/Decomposers/CroutMethod.h>

#include "UnitTests/utils/UnitTestsMessages.h"

#include "DecomposerTests.h"

using namespace UnitTests::Messages;

#ifdef HAVE_GTEST
   #include "gmock/gmock.h"
   #include "gtest/gtest.h"

using ::testing::MatchesRegex;
using namespace Decomposition::LU::Decomposers;

template< typename Matrix, typename Decomposer >
void
test_GetDecomposerName()
{
   using Device = typename Matrix::DeviceType;
   bool partialPivoting = false;

   if( Decomposition::isHost< Device >() ) {
      EXPECT_EQ( Decomposer::template getDecomposerName< Device >( partialPivoting ), "CM" );
      partialPivoting = true;
      EXPECT_EQ( Decomposer::template getDecomposerName< Device >( partialPivoting ), "CM PP" );
   }
   else {
      // TODO: Update this once PCM without partial pivoting is implemented
      EXPECT_THAT( Decomposer::template getDecomposerName< Device >( partialPivoting ), MatchesRegex( "CM" ) );
      partialPivoting = true;
      EXPECT_THAT( Decomposer::template getDecomposerName< Device >( partialPivoting ), MatchesRegex( "PCM_[0-9]+ PP" ) );
   }
}

template< typename Matrix, typename Decomposer >
void
test_GetMatrixDiagonalFormat()
{
   EXPECT_EQ( Decomposer::getMatrixDiagonalFormat(), MatrixFormat::UnitDiagIn_U );
}

template< typename Matrix >
class CroutMethodTest : public ::testing::Test
{
protected:
   using MatrixType = Matrix;
};

template< typename Real, typename Device, typename Index >
using DenseMatrixRowMajorOrder =
   typename TNL::Matrices::DenseMatrix< Real, Device, Index, TNL::Algorithms::Segments::RowMajorOrder >;

// clang-format off
using MatrixTypes = ::testing::Types
<
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Host, short >, CroutMethod<    > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Host, short >, CroutMethod<    > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Host, int   >, CroutMethod<    > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Host, int   >, CroutMethod<    > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Host, long  >, CroutMethod<    > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Host, long  >, CroutMethod<    > >
   #ifdef __CUDACC__
   ,std::tuple< DenseMatrixRowMajorOrder< float, TNL::Devices::Cuda, short >, CroutMethod<  8 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, short >, CroutMethod<  8 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, int   >, CroutMethod<  8 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, int   >, CroutMethod<  8 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, long  >, CroutMethod<  8 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, long  >, CroutMethod<  8 > >,

   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, short >, CroutMethod< 16 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, short >, CroutMethod< 16 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, int   >, CroutMethod< 16 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, int   >, CroutMethod< 16 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, long  >, CroutMethod< 16 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, long  >, CroutMethod< 16 > >,

   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, short >, CroutMethod< 32 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, short >, CroutMethod< 32 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, int   >, CroutMethod< 32 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, int   >, CroutMethod< 32 > >,
   std::tuple< DenseMatrixRowMajorOrder< float,  TNL::Devices::Cuda, long  >, CroutMethod< 32 > >,
   std::tuple< DenseMatrixRowMajorOrder< double, TNL::Devices::Cuda, long  >, CroutMethod< 32 > >
   #endif
>;
// clang-format on

TYPED_TEST_SUITE( CroutMethodTest, MatrixTypes );

TYPED_TEST( CroutMethodTest, GetDecomposerNameTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_GetDecomposerName< MatrixType, DecomposerType >();
}

TYPED_TEST( CroutMethodTest, GetMatrixDiagonalFormatTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_GetMatrixDiagonalFormat< MatrixType, DecomposerType >();
}

TYPED_TEST( CroutMethodTest, Decompose2x2MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if( Decomposition::tnlObjectOnCuda< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }

   test_Decompose2x2Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( CroutMethodTest, Decompose3x3MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if( Decomposition::tnlObjectOnCuda< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }

   test_Decompose3x3Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( CroutMethodTest, Decompose4x4MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if( Decomposition::tnlObjectOnCuda< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }

   test_Decompose4x4Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( CroutMethodTest, Decompose10x10MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if( Decomposition::tnlObjectOnCuda< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }

   test_Decompose10x10Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( CroutMethodTest, Decompose19x19MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if( Decomposition::tnlObjectOnCuda< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }

   test_Decompose19x19Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( CroutMethodTest, Decompose38x38MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if( Decomposition::tnlObjectOnCuda< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }

   test_Decompose38x38Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( CroutMethodTest, DecomposeWithPivoting2x2MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting2x2Matrix< MatrixType, DecomposerType, typename MatrixType::IndexType, TNL::Devices::Host >();
}

TYPED_TEST( CroutMethodTest, DecomposeWithPivoting3x3MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting3x3Matrix< MatrixType, DecomposerType, typename MatrixType::IndexType, TNL::Devices::Host >();
}

TYPED_TEST( CroutMethodTest, DecomposeWithPivoting4x4MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting4x4Matrix< MatrixType, DecomposerType, typename MatrixType::IndexType, TNL::Devices::Host >();
}

TYPED_TEST( CroutMethodTest, DecomposeWithPivoting10x10MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting10x10Matrix< MatrixType, DecomposerType, typename MatrixType::IndexType, TNL::Devices::Host >();
}

TYPED_TEST( CroutMethodTest, DecomposeWithPivoting19x19MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting19x19Matrix< MatrixType, DecomposerType, typename MatrixType::IndexType, TNL::Devices::Host >();
}

TYPED_TEST( CroutMethodTest, DecomposeWithPivoting38x38MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting38x38Matrix< MatrixType, DecomposerType, typename MatrixType::IndexType, TNL::Devices::Host >();
}

TYPED_TEST( CroutMethodTest, DecomposeWithPivoting4x4PivotingMatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting4x4PivotingMatrix< MatrixType, DecomposerType, typename MatrixType::IndexType, TNL::Devices::Host >(
      1e-6 );
}

TYPED_TEST( CroutMethodTest, DecomposeNonSquareMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if( Decomposition::tnlObjectOnCuda< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }

   test_DecomposeNonSquareMatrixShouldFail< MatrixType, DecomposerType >();
}

TYPED_TEST( CroutMethodTest, DecomposeWithPivotingNonSquareMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivotingNonSquareMatrixShouldFail< MatrixType,
                                                        DecomposerType,
                                                        typename MatrixType::IndexType,
                                                        TNL::Devices::Host >();
}

TYPED_TEST( CroutMethodTest, Decompose10x10SingularMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if( Decomposition::tnlObjectOnCuda< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }

   test_Decompose10x10SingularMatrixShouldFail< MatrixType, DecomposerType >();
}

TYPED_TEST( CroutMethodTest, DecomposeWithPivoting10x10SingularMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting10x10SingularMatrixShouldFail< MatrixType, DecomposerType, TNL::Devices::Host >();
}

TYPED_TEST( CroutMethodTest, DecomposeMatrixAllocatedOnDeviceShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if( Decomposition::tnlObjectOnCuda< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }

   test_DecomposeMatrixAllocatedOnDeviceShouldFail< MatrixType, DecomposerType >();
}

TYPED_TEST( CroutMethodTest, DecomposeWithPivotingMatrixVectorAllocatedOnDeviceShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   if constexpr( Decomposition::tnlObjectOnHost< MatrixType >() ) {
      GTEST_SKIP() << getSkipTestMessage< MatrixType >( testing::UnitTest::GetInstance()->current_test_info()->name() );
   }
   else {
      test_DecomposeWithPivotingMatrixVectorAllocatedOnDeviceShouldFail< MatrixType, DecomposerType >(
         "is implemented only for the CPU/GPU. However, the pivoting vector must be stored on the Host in both cases." );
   }
}

#endif  // HAVE_GTEST
