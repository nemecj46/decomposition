#include <iostream>

#include <TNL/Matrices/DenseMatrix.h>

#include <Decomposition/LU/Decomposers/GaussianEliminationMethod.h>

#include "DecomposerTests.h"

#ifdef HAVE_GTEST
   #include "gtest/gtest.h"

using namespace Decomposition::LU::Decomposers;

template< typename Matrix, typename Decomposer >
void
test_GetDecomposerName()
{
   using Device = typename Matrix::DeviceType;
   bool partialPivoting = true;
   EXPECT_EQ( Decomposer::template getDecomposerName< Device >( partialPivoting ), "GEM PP" );

   partialPivoting = false;
   EXPECT_EQ( Decomposer::template getDecomposerName< Device >( partialPivoting ), "GEM" );
}

template< typename Matrix, typename Decomposer >
void
test_GetMatrixDiagonalFormat()
{
   EXPECT_EQ( Decomposer::getMatrixDiagonalFormat(), MatrixFormat::UnitDiagIn_L );
}

template< typename Matrix >
class GaussianEliminationMethodTest : public ::testing::Test
{
protected:
   using MatrixType = Matrix;
};

// clang-format off
using MatrixTypes = ::testing::Types
<
   std::tuple< TNL::Matrices::DenseMatrix< float,  TNL::Devices::Host, short >, GaussianEliminationMethod >,
   std::tuple< TNL::Matrices::DenseMatrix< double, TNL::Devices::Host, short >, GaussianEliminationMethod >,
   std::tuple< TNL::Matrices::DenseMatrix< float,  TNL::Devices::Host, int   >, GaussianEliminationMethod >,
   std::tuple< TNL::Matrices::DenseMatrix< double, TNL::Devices::Host, int   >, GaussianEliminationMethod >,
   std::tuple< TNL::Matrices::DenseMatrix< float,  TNL::Devices::Host, long  >, GaussianEliminationMethod >,
   std::tuple< TNL::Matrices::DenseMatrix< double, TNL::Devices::Host, long  >, GaussianEliminationMethod >
>;
// clang-format on

TYPED_TEST_SUITE( GaussianEliminationMethodTest, MatrixTypes );

TYPED_TEST( GaussianEliminationMethodTest, GetDecomposerNameTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_GetDecomposerName< MatrixType, DecomposerType >();
}

TYPED_TEST( GaussianEliminationMethodTest, GetMatrixDiagonalFormatTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_GetMatrixDiagonalFormat< MatrixType, DecomposerType >();
}

TYPED_TEST( GaussianEliminationMethodTest, DecomposeWithPivoting2x2MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting2x2Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( GaussianEliminationMethodTest, DecomposeWithPivoting3x3MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting3x3Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( GaussianEliminationMethodTest, DecomposeWithPivoting4x4MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting4x4Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( GaussianEliminationMethodTest, DecomposeWithPivoting10x10MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting10x10Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( GaussianEliminationMethodTest, DecomposeWithPivoting19x19MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting19x19Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( GaussianEliminationMethodTest, DecomposeWithPivoting38x38MatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting38x38Matrix< MatrixType, DecomposerType >();
}

TYPED_TEST( GaussianEliminationMethodTest, DecomposeWithPivoting4x4PivotingMatrixTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting4x4PivotingMatrix< MatrixType, DecomposerType >();
}

TYPED_TEST( GaussianEliminationMethodTest, DecomposeWithPivotingNonSquareMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivotingNonSquareMatrixShouldFail< MatrixType, DecomposerType >();
}

TYPED_TEST( GaussianEliminationMethodTest, DecomposeWithPivoting10x10SingularMatrixShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivoting10x10SingularMatrixShouldFail< MatrixType, DecomposerType >();
}

TYPED_TEST( GaussianEliminationMethodTest, DecomposeWithPivotingMatrixAllocatedOnDeviceShouldFailTest )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeWithPivotingMatrixAllocatedOnDeviceShouldFail< MatrixType, DecomposerType >();
}

TYPED_TEST( GaussianEliminationMethodTest, DecomposeMatrixWithEmptyPivotingVectorShouldFail )
{
   using MatrixType = typename std::tuple_element< 0, decltype( TypeParam() ) >::type;
   using DecomposerType = typename std::tuple_element< 1, decltype( TypeParam() ) >::type;

   test_DecomposeMatrixWithEmptyPivotingVectorShouldFail< MatrixType, DecomposerType >();
}

#endif  // HAVE_GTEST
