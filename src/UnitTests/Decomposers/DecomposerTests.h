#include <iostream>
// For the ASSERT_DURATION_LE macro
#include <future>
#include <chrono>

#include <TNL/Devices/Cuda.h>
#include <TNL/Matrices/DenseMatrix.h>

#include <Decomposition/Containers/MatrixFormat.h>
#include <Decomposition/Exceptions/MatrixNotSquare.h>
#include <Decomposition/Exceptions/MatrixNotStronglyRegular.h>
#include <Decomposition/Exceptions/MatrixSingular.h>
#include <Decomposition/Exceptions/DecompositionException.h>

#include "UnitTests/utils/Matrices/TwoByTwo.h"
#include "UnitTests/utils/Matrices/ThreeByThree.h"
#include "UnitTests/utils/Matrices/FourByFour.h"
#include "UnitTests/utils/Matrices/TenByTen.h"
#include "UnitTests/utils/Matrices/LF10.h"
#include "UnitTests/utils/Matrices/cage5.h"
#include "UnitTests/utils/Matrices/FourByFourPivoting.h"

#ifdef HAVE_GTEST
   #include "UnitTests/utils/ResultVerification.h"
   #include "gtest/gtest.h"
   #include "gmock/gmock.h"

using ::testing::HasSubstr;
using ::testing::Throws;
using ::testing::ThrowsMessage;

using MatrixFormat = Decomposition::Containers::MatrixFormat;

   #define ASSERT_DURATION_LE( secs, stmt )                                                               \
      {                                                                                                   \
         std::promise< bool > completed;                                                                  \
         auto stmt_future = completed.get_future();                                                       \
         std::thread(                                                                                     \
            [ & ]( std::promise< bool >& completed )                                                      \
            {                                                                                             \
               stmt;                                                                                      \
               completed.set_value( true );                                                               \
            },                                                                                            \
            std::ref( completed ) )                                                                       \
            .detach();                                                                                    \
         if( stmt_future.wait_for( std::chrono::seconds( secs ) ) == std::future_status::timeout )        \
            GTEST_FATAL_FAILURE_( "-!> Timed out (> " #secs " seconds). Check code for infinite loops" ); \
      }

template< typename Index, typename Device >
using VectorType = TNL::Containers::Vector< Index, Device, Index >;

template< typename Matrix, typename Decomposer >
void
test_Decompose2x2Matrix()
{
   Matrices::TwoByTwo< Matrix > twoByTwo;

   Matrix LU = twoByTwo.getA();

   Decomposer::decompose( LU );

   const Matrix LU_correct = twoByTwo.getLU( Decomposer::getMatrixDiagonalFormat() );
   ResultVerification::expectMatrixEqualToMatrix( LU, LU_correct );
}

template< typename Matrix, typename Decomposer >
void
test_Decompose3x3Matrix()
{
   Matrices::ThreeByThree< Matrix > threeByThree;

   Matrix LU = threeByThree.getA();

   Decomposer::decompose( LU );

   const Matrix LU_correct = threeByThree.getLU( Decomposer::getMatrixDiagonalFormat() );
   ResultVerification::expectMatrixEqualToMatrix( LU, LU_correct );
}

template< typename Matrix, typename Decomposer >
void
test_Decompose4x4Matrix()
{
   Matrices::FourByFour< Matrix > fourByFour;

   Matrix LU = fourByFour.getA();

   Decomposer::decompose( LU );

   const Matrix LU_correct = fourByFour.getLU( Decomposer::getMatrixDiagonalFormat() );
   ResultVerification::expectMatrixEqualToMatrix( LU, LU_correct );
}

template< typename Matrix, typename Decomposer, typename Real = typename Matrix::RealType >
void
test_Decompose10x10Matrix( const Real& tolerance = 1e-5, const Real& zeroToleranceFallback = 0.0 )
{
   Matrices::TenByTen< Matrix > tenByTen;

   Matrix LU = tenByTen.getA();

   Decomposer::decompose( LU );

   const Matrix LU_correct = tenByTen.getLU( Decomposer::getMatrixDiagonalFormat() );
   ResultVerification::expectMatrixNearMatrix_LU_LUcorrect( LU, LU_correct, tolerance, zeroToleranceFallback );
}

template< typename Matrix, typename Decomposer, typename Real = typename Matrix::RealType >
void
test_Decompose19x19Matrix( const Real& tolerance = 1e-5, const Real& zeroToleranceFallback = 0.0 )
{
   Matrices::LF10< Matrix > LF10;

   Matrix LU = LF10.getA();

   Decomposer::decompose( LU );

   const Matrix LU_correct = LF10.getLU( Decomposer::getMatrixDiagonalFormat() );
   ResultVerification::expectMatrixNearMatrix_LU_LUcorrect( LU, LU_correct, tolerance, zeroToleranceFallback );
}

template< typename Matrix, typename Decomposer, typename Real = typename Matrix::RealType >
void
test_Decompose38x38Matrix( const Real& tolerance = 1e-4, const Real& zeroToleranceFallback = 0.0 )
{
   Matrices::cage5< Matrix > cage5;

   Matrix LU = cage5.getA();

   Decomposer::decompose( LU );

   const Matrix LU_correct = cage5.getLU( Decomposer::getMatrixDiagonalFormat() );
   ResultVerification::expectMatrixNearMatrix_LU_LUcorrect( LU, LU_correct, tolerance, zeroToleranceFallback );
}

//////////
// MATRIX DECOMPOSITION WITH PIVOTING
//////////

template< typename Matrix,
          typename Decomposer,
          typename Index = typename Matrix::IndexType,
          typename VecDevice = typename Matrix::DeviceType >
void
test_DecomposeWithPivoting2x2Matrix()
{
   using Vector = VectorType< Index, VecDevice >;

   Matrices::TwoByTwo< Matrix > twoByTwo;

   Matrix LU = twoByTwo.getA();
   Vector piv( LU.getRows() );

   Decomposer::decompose( LU, piv );

   Matrix L;
   Matrix U;
   std::tie( L, U ) = Decomposer::getUnpackedMatrices( LU, Decomposer::getMatrixDiagonalFormat() );

   LU.getMatrixProduct( L, U );
   Decomposer::undoOrderInMatrixAccordingTo( LU, piv );

   const Matrix A_orig = twoByTwo.getA();
   ResultVerification::expectMatrixEqualToMatrix_LU_Aorig( LU, A_orig );
}

template< typename Matrix,
          typename Decomposer,
          typename Index = typename Matrix::IndexType,
          typename VecDevice = typename Matrix::DeviceType >
void
test_DecomposeWithPivoting3x3Matrix()
{
   using Vector = VectorType< Index, VecDevice >;

   Matrices::ThreeByThree< Matrix > threeByThree;

   Matrix LU = threeByThree.getA();
   Vector piv( LU.getRows() );

   Decomposer::decompose( LU, piv );

   Matrix L;
   Matrix U;
   std::tie( L, U ) = Decomposer::getUnpackedMatrices( LU, Decomposer::getMatrixDiagonalFormat() );

   LU.getMatrixProduct( L, U );
   Decomposer::undoOrderInMatrixAccordingTo( LU, piv );

   const Matrix A_orig = threeByThree.getA();
   ResultVerification::expectMatrixEqualToMatrix_LU_Aorig( LU, A_orig );
}

template< typename Matrix,
          typename Decomposer,
          typename Index = typename Matrix::IndexType,
          typename VecDevice = typename Matrix::DeviceType >
void
test_DecomposeWithPivoting4x4Matrix()
{
   using Vector = VectorType< Index, VecDevice >;

   Matrices::FourByFour< Matrix > fourByFour;

   Matrix LU = fourByFour.getA();
   Vector piv( LU.getRows() );

   Decomposer::decompose( LU, piv );

   Matrix L;
   Matrix U;
   std::tie( L, U ) = Decomposer::getUnpackedMatrices( LU, Decomposer::getMatrixDiagonalFormat() );

   LU.getMatrixProduct( L, U );
   Decomposer::undoOrderInMatrixAccordingTo( LU, piv );

   const Matrix A_orig = fourByFour.getA();
   ResultVerification::expectMatrixEqualToMatrix_LU_Aorig( LU, A_orig );
}

template< typename Matrix,
          typename Decomposer,
          typename Index = typename Matrix::IndexType,
          typename VecDevice = typename Matrix::DeviceType,
          typename Real = typename Matrix::RealType >
void
test_DecomposeWithPivoting10x10Matrix( const Real& tolerance = 1e-5, const Real& zeroToleranceFallback = 1e-4 )
{
   using Vector = VectorType< Index, VecDevice >;

   Matrices::TenByTen< Matrix > tenByTen;

   Matrix LU = tenByTen.getA();
   Vector piv( LU.getRows() );

   Decomposer::decompose( LU, piv );

   Matrix L;
   Matrix U;
   std::tie( L, U ) = Decomposer::getUnpackedMatrices( LU, Decomposer::getMatrixDiagonalFormat() );

   LU.getMatrixProduct( L, U );
   Decomposer::undoOrderInMatrixAccordingTo( LU, piv );

   const Matrix A_orig = tenByTen.getA();
   ResultVerification::expectMatrixNearMatrix_LU_Aorig( LU, A_orig, tolerance, zeroToleranceFallback );
}

template< typename Matrix,
          typename Decomposer,
          typename Index = typename Matrix::IndexType,
          typename VecDevice = typename Matrix::DeviceType,
          typename Real = typename Matrix::RealType >
void
test_DecomposeWithPivoting19x19Matrix( const Real& tolerance = 1e-5, const Real& zeroToleranceFallback = 0.0 )
{
   using Vector = VectorType< Index, VecDevice >;

   Matrices::LF10< Matrix > LF10;

   Matrix LU = LF10.getA();
   Vector piv( LU.getRows() );

   Decomposer::decompose( LU, piv );

   Matrix L;
   Matrix U;
   std::tie( L, U ) = Decomposer::getUnpackedMatrices( LU, Decomposer::getMatrixDiagonalFormat() );

   LU.getMatrixProduct( L, U );
   Decomposer::undoOrderInMatrixAccordingTo( LU, piv );

   const Matrix A_orig = LF10.getA();
   ResultVerification::expectMatrixNearMatrix_LU_Aorig( LU, A_orig, tolerance, zeroToleranceFallback );
}

template< typename Matrix,
          typename Decomposer,
          typename Index = typename Matrix::IndexType,
          typename VecDevice = typename Matrix::DeviceType,
          typename Real = typename Matrix::RealType >
void
test_DecomposeWithPivoting38x38Matrix( const Real& tolerance = 1e-4, const Real& zeroToleranceFallback = 1e-8 )
{
   using Vector = VectorType< Index, VecDevice >;

   Matrices::cage5< Matrix > cage5;

   Matrix LU = cage5.getA();
   Vector piv( LU.getRows() );

   Decomposer::decompose( LU, piv );

   Matrix L;
   Matrix U;
   std::tie( L, U ) = Decomposer::getUnpackedMatrices( LU, Decomposer::getMatrixDiagonalFormat() );

   LU.getMatrixProduct( L, U );
   Decomposer::undoOrderInMatrixAccordingTo( LU, piv );

   const Matrix A_orig = cage5.getA();
   ResultVerification::expectMatrixNearMatrix_LU_Aorig( LU, A_orig, tolerance, zeroToleranceFallback );
}

template< typename Matrix,
          typename Decomposer,
          typename Index = typename Matrix::IndexType,
          typename VecDevice = typename Matrix::DeviceType,
          typename Real = typename Matrix::RealType >
void
test_DecomposeWithPivoting4x4PivotingMatrix( const Real& tolerance = 1e-10, const Real& zeroToleranceFallback = 0.0 )
{
   using Vector = VectorType< Index, VecDevice >;

   Matrices::FourByFourPivoting< Matrix > fourByFourPivoting;

   Matrix LU = fourByFourPivoting.getA();
   Vector piv( LU.getRows() );

   Decomposer::decompose( LU, piv );

   Matrix L;
   Matrix U;
   std::tie( L, U ) = Decomposer::getUnpackedMatrices( LU, Decomposer::getMatrixDiagonalFormat() );

   LU.getMatrixProduct( L, U );
   Decomposer::undoOrderInMatrixAccordingTo( LU, piv );

   const Matrix A_orig = fourByFourPivoting.getA();
   ResultVerification::expectMatrixNearMatrix_LU_Aorig( LU, A_orig, tolerance, zeroToleranceFallback );
}

template< typename Matrix,
          typename Decomposer,
          typename Index = typename Matrix::IndexType,
          typename VecDevice = typename Matrix::DeviceType,
          typename Real = typename Matrix::RealType >
void
test_DecomposeWithPivoting4x4MatrixWithZeroLastElement( const Real& tolerance = 1e-10, const Real& zeroToleranceFallback = 0.0 )
{
   using Vector = VectorType< Index, VecDevice >;

   const Matrix A_orig{
      { 1.0, 2.0, 3.0 },
      { 4.0, 5.0, 6.0 },
      { 7.0, 8.0, 9.0 }
   };

   Matrix LU;
   LU = A_orig;

   Vector piv( LU.getRows() );

   ASSERT_DURATION_LE( 3, { Decomposer::decompose( LU, piv ); } );

   Matrix L;
   Matrix U;
   std::tie( L, U ) = Decomposer::getUnpackedMatrices( LU, Decomposer::getMatrixDiagonalFormat() );

   LU.getMatrixProduct( L, U );
   Decomposer::undoOrderInMatrixAccordingTo( LU, piv );

   ResultVerification::expectMatrixNearMatrix_LU_Aorig( LU, A_orig, tolerance, zeroToleranceFallback );
}

template< typename Matrix, typename Decomposer >
void
test_DecomposeNonSquareMatrixShouldFail()
{
   Matrix LU( {
      { 4, 24 },
      { 2, 15 },
      { 2, 15 }
   } );

   EXPECT_THAT(
      [ & ]()
      {
         Decomposer::decompose( LU );
      },
      ThrowsMessage< Decomposition::Exceptions::MatrixNotSquare >(
         HasSubstr( "-!> Input matrix 'A' is not a square matrix. Rows: 3, Columns: 2" ) ) );
}

template< typename Matrix,
          typename Decomposer,
          typename Index = typename Matrix::IndexType,
          typename VecDevice = typename Matrix::DeviceType >
void
test_DecomposeWithPivotingNonSquareMatrixShouldFail()
{
   using Vector = VectorType< Index, VecDevice >;

   // clang-format off
   Matrix LU({
      { 4, 24 },
      { 2, 15 },
      { 2, 15 }
   });
   // clang-format on

   Vector piv( LU.getRows() );

   EXPECT_THAT(
      [ & ]()
      {
         Decomposer::decompose( LU, piv );
      },
      ThrowsMessage< Decomposition::Exceptions::MatrixNotSquare >(
         HasSubstr( "-!> Input matrix 'A' is not a square matrix. Rows: 3, Columns: 2" ) ) );
}

template< typename Matrix, typename Decomposer >
void
test_Decompose10x10SingularMatrixShouldFail()
{
   Matrix LU( {
      { 0,    0,    0,    0,    0,    0,    0,    0,    0,    0 },
      { 0, -215,    0,    0,    0,    0,    0,    0,    0,    0 },
      { 0,  114, -217,    0,    0,    0,    0,    0,   -1,    0 },
      { 0,  -89,  134, -203,    0,    0,    0,    0,    0,    0 },
      { 0,  -77,   77,  -80,  157,    0,    0,    0,    0,    0 },
      { 0,   94, -150,  138, -142, -188,    0,    0,    0,    0 },
      { 0, -170,   57, -109,  173,  170,  -97,    0,    0,    0 },
      { 0,  176, -253,  177, -241, -120,  140, -184,    0,    0 },
      { 0,  117, -230,  180, -254, -160,  116, -191,  235,    0 },
      { 0, -210,   49, -174,   23,   -9, -106,   -6, -132, -164 }
   } );

   EXPECT_THAT(
      [ & ]()
      {
         Decomposer::decompose( LU );
      },
      Throws< Decomposition::Exceptions::MatrixNotStronglyRegular >() );
}

template< typename Matrix, typename Decomposer, typename VecDevice = typename Matrix::DeviceType >
void
test_DecomposeWithPivoting10x10SingularMatrixShouldFail()
{
   using Index = typename Matrix::IndexType;
   using Vector = VectorType< Index, VecDevice >;

   // clang-format off
   Matrix LU({
      {0,    0,    0,    0,    0,    0,    0,    0,    0,    0},
      {0, -215,    0,    0,    0,    0,    0,    0,    0,    0},
      {0,  114, -217,    0,    0,    0,    0,    0,   -1,    0},
      {0,  -89,  134, -203,    0,    0,    0,    0,    0,    0},
      {0,  -77,   77,  -80,  157,    0,    0,    0,    0,    0},
      {0,  94,  -150,  138, -142, -188,    0,    0,    0,    0},
      {0, -170,   57, -109,  173,  170,  -97,    0,    0,    0},
      {0,  176, -253,  177, -241, -120,  140, -184,    0,    0},
      {0,  117, -230,  180, -254, -160,  116, -191,  235,    0},
      {0, -210,   49, -174,   23,   -9, -106,   -6, -132, -164}
   });
   // clang-format on

   Vector piv( LU.getRows() );

   EXPECT_THAT(
      [ & ]()
      {
         Decomposer::decompose( LU, piv );
      },
      ThrowsMessage< Decomposition::Exceptions::MatrixSingular >(
         HasSubstr( "-!> Input Matrix 'A' is singular - division by 0 during decomposition with partial pivoting. LU( 0, 0 ) = "
                    "0. Cannot divide by 0." ) ) );
}

template< typename Matrix, typename Decomposer >
void
test_DecomposeMatrixAllocatedOnDeviceShouldFail()
{
   using Real = typename Matrix::RealType;
   using Index = typename Matrix::IndexType;
   using cudaMatrix = TNL::Matrices::DenseMatrix< Real, TNL::Devices::Cuda, Index >;

   // clang-format off
   Matrix LU({
      { 4, 24 },
      { 2, 15 }
   });
   // clang-format on

   cudaMatrix LU_cuda;
   LU_cuda = LU;

   EXPECT_THAT(
      [ & ]()
      {
         Decomposer::decompose( LU_cuda );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >(
         HasSubstr( "-!> " + Decomposer::template getDecomposerName< typename Matrix::DeviceType >()
                    + " is implemented only for the CPU." ) ) );
}

template< typename Matrix,
          typename Decomposer,
          typename Index = typename Matrix::IndexType,
          typename VecDevice = typename Matrix::DeviceType >
void
test_DecomposeWithPivotingMatrixAllocatedOnHostShouldFail()
{
   using Real = typename Matrix::RealType;
   using Device = typename Matrix::DeviceType;
   using HostMatrix = TNL::Matrices::DenseMatrix< Real, TNL::Devices::Host, Index >;
   using Vector = VectorType< Index, Device >;

   // clang-format off
   Matrix LU( {
      { 4, 24 },
      { 2, 15 }
   });
   // clang-format on

   HostMatrix LU_host;
   LU_host = LU;
   Vector piv( LU_host.getRows() );

   EXPECT_THAT(
      [ & ]()
      {
         Decomposer::decompose( LU_host, piv );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >(
         HasSubstr( "-!> " + Decomposer::template getDecomposerName< typename Matrix::DeviceType >( ! piv.empty() )
                    + " is implemented only for the GPU." ) ) );
}

template< typename Matrix, typename Decomposer, typename VecDevice = typename Matrix::DeviceType >
void
test_DecomposeWithPivotingMatrixAllocatedOnDeviceShouldFail()
{
   using Real = typename Matrix::RealType;
   using Index = typename Matrix::IndexType;
   using CudaMatrix = TNL::Matrices::DenseMatrix< Real, TNL::Devices::Cuda, Index >;
   using CudaPivVector = VectorType< Index, TNL::Devices::Host >;

   // clang-format off
   Matrix LU({
      { 4, 24 },
      { 2, 15 }
   });
   // clang-format on

   CudaMatrix LU_cuda;
   LU_cuda = LU;

   CudaPivVector piv( LU_cuda.getRows() );

   EXPECT_THAT(
      [ & ]()
      {
         Decomposer::decompose( LU_cuda, piv );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >(
         HasSubstr( "-!> " + Decomposer::template getDecomposerName< typename Matrix::DeviceType >()
                    + " is implemented only for the CPU." ) ) );
}

template< typename Matrix, typename Decomposer >
void
test_DecomposeMatricesAllocatedDifferentDevicesShouldFail()
{
   using Real = typename Matrix::RealType;
   using Device = typename Matrix::DeviceType;
   using Index = typename Matrix::IndexType;

   // clang-format off
   Matrix LU({
      { 4, 24 },
      { 2, 15 }
   });
   // clang-format on

   typedef std::conditional_t< Decomposition::isCuda< Device >(), TNL::Devices::Host, TNL::Devices::Cuda > OtherDevice;
   using OtherDeviceMatrix = TNL::Matrices::DenseMatrix< Real, OtherDevice, Index >;

   OtherDeviceMatrix otherDeviceLU;
   otherDeviceLU.setLike( LU );

   EXPECT_THAT(
      [ & ]()
      {
         Decomposer::decompose( LU, otherDeviceLU );
      },
      ThrowsMessage< std::runtime_error >( HasSubstr( "-!> TNL Objects are stored on different devices!" ) ) );
}

template< typename Matrix, typename Decomposer >
void
test_DecomposeWithPivotingMatrixVectorAllocatedOnDeviceShouldFail( const TNL::String& exceptionMessage_suffix )
{
   using Index = typename Matrix::IndexType;
   using Vector = VectorType< Index, TNL::Devices::Cuda >;

   // clang-format off
   Matrix LU( {
      { 4, 24 },
      { 2, 15 }
   });
   // clang-format on

   Vector piv_cuda( LU.getRows() );

   EXPECT_THAT(
      [ & ]()
      {
         Decomposer::decompose( LU, piv_cuda );
      },
      ThrowsMessage< Decomposition::Exceptions::NotImplementedError >(
         HasSubstr( "-!> " + Decomposer::template getDecomposerName< typename Matrix::DeviceType >( ! piv_cuda.empty() ) + " "
                    + exceptionMessage_suffix ) ) );
}

template< typename Matrix, typename Decomposer, typename VecDevice = typename Matrix::DeviceType >
void
test_DecomposeWithPivotingMatrixObjectsAllocatedDifferentDevicesShouldFail()
{
   using Real = typename Matrix::RealType;
   using Device = typename Matrix::DeviceType;
   using Index = typename Matrix::IndexType;
   using Vector = VectorType< Index, Device >;

   // clang-format off
   Matrix LU({
      { 4, 24 },
      { 2, 15 }
   });
   // clang-format on

   typedef std::conditional_t< Decomposition::isCuda< Device >(), TNL::Devices::Host, TNL::Devices::Cuda > OtherDevice;
   using OtherDeviceMatrix = TNL::Matrices::DenseMatrix< Real, OtherDevice, Index >;

   OtherDeviceMatrix otherDeviceLU;
   otherDeviceLU.setLike( LU );

   Vector piv( otherDeviceLU.getRows() );

   EXPECT_THAT(
      [ & ]()
      {
         Decomposer::decompose( otherDeviceLU, piv );
      },
      ThrowsMessage< std::runtime_error >( HasSubstr( "-!> TNL Objects are stored on different devices!" ) ) );
}

template< typename Matrix, typename Decomposer, typename VecDevice = typename Matrix::DeviceType >
void
test_DecomposeMatrixWithEmptyPivotingVectorShouldFail()
{
   using Index = typename Matrix::IndexType;
   using Vector = VectorType< Index, VecDevice >;
   // clang-format off
   Matrix LU({
      { 4, 24 },
      { 2, 15 }
   });
   // clang-format on

   Vector piv( 0 );

   EXPECT_THAT(
      [ & ]()
      {
         Decomposer::decompose( LU, piv );
      },
      ThrowsMessage< std::runtime_error >(
         HasSubstr( "-!> " + Decomposer::template getDecomposerName< typename Matrix::DeviceType >()
                    + " is implemented only with partial pivoting. Pivoting vector is empty!" ) ) );
}

#endif  // HAVE_GTEST
