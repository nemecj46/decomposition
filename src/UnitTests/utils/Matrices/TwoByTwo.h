#pragma once

#include "UnitTests/utils/Matrices/SampleMatrix.h"

namespace Matrices {

template< typename Matrix >
class TwoByTwo : public SampleMatrix< Matrix >
{
public:
   TwoByTwo() : SampleMatrix< Matrix >() {}

   Matrix
   getA() override
   {
      return Matrix{
         { 4.0, 24.0 },
         { 2.0, 15.0 }
      };
   }

protected:
   Matrix
   getL_UnitDiag() override
   {
      return Matrix{
         { 1.0, 0.0 },
         { 0.5, 1.0 }
      };
   }

   Matrix
   getL_NoUnitDiag() override
   {
      return Matrix{
         { 4.0, 0.0 },
         { 2.0, 3.0 }
      };
   }

   Matrix
   getU_UnitDiag() override
   {
      return Matrix{
         { 1.0, 6.0 },
         { 0.0, 1.0 }
      };
   }

   Matrix
   getU_NoUnitDiag() override
   {
      return Matrix{
         { 4.0, 24.0 },
         { 0.0,  3.0 }
      };
   }
};

}  // namespace Matrices
