#pragma once

#include "UnitTests/utils/Matrices/SampleMatrix.h"

namespace Matrices {

template< typename Matrix >
class FourByFour : public SampleMatrix< Matrix >
{
public:
   FourByFour() : SampleMatrix< Matrix >() {}

   Matrix
   getA() override
   {
      return Matrix{
         { 2.0,  6.0,  0.0,  0.0 },
         { 3.0, -3.0, 12.0,  0.0 },
         { 1.0,  4.0,  3.0,  0.0 },
         { 0.0, 10.0, 24.0, 30.0 }
      };
   }

protected:
   Matrix
   getL_UnitDiag() override
   {
      return Matrix{
         { 1.0,                   0.0, 0.0, 0.0 },
         { 1.5,                   1.0, 0.0, 0.0 },
         { 0.5, -0.083333333333333329, 1.0, 0.0 },
         { 0.0,  -0.83333333333333326, 8.5, 1.0 }
      };
   }

   Matrix
   getL_NoUnitDiag() override
   {
      return Matrix{
         { 2.0,   0.0,  0.0,  0.0 },
         { 3.0, -12.0,  0.0,  0.0 },
         { 1.0,   1.0,  4.0,  0.0 },
         { 0.0,  10.0, 34.0, 30.0 }
      };
   }

   Matrix
   getU_UnitDiag() override
   {
      return Matrix{
         { 1.0, 3.0,  0.0, 0.0 },
         { 0.0, 1.0, -1.0, 0.0 },
         { 0.0, 0.0,  1.0, 0.0 },
         { 0.0, 0.0,  0.0, 1.0 }
      };
   }

   Matrix
   getU_NoUnitDiag() override
   {
      return Matrix{
         { 2.0,   6.0,  0.0,  0.0 },
         { 0.0, -12.0, 12.0,  0.0 },
         { 0.0,   0.0,  4.0,  0.0 },
         { 0.0,   0.0,  0.0, 30.0 }
      };
   }
};

}  // namespace Matrices
