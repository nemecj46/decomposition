#pragma once

#include "UnitTests/utils/Matrices/SampleMatrix.h"

namespace Matrices {

template< typename Matrix >
class ThreeByThree : public SampleMatrix< Matrix >
{
public:
   ThreeByThree() : SampleMatrix< Matrix >() {}

   Matrix
   getA() override
   {
      return Matrix{
         { 2.0,  4.0,  6.0 },
         { 1.0,  6.0, 19.0 },
         { 5.0, 13.0, 33.0 }
      };
   }

protected:
   Matrix
   getL_UnitDiag() override
   {
      return Matrix{
         { 1.0,  0.0, 0.0 },
         { 0.5,  1.0, 0.0 },
         { 2.5, 0.75, 1.0 }
      };
   }

   Matrix
   getL_NoUnitDiag() override
   {
      return Matrix{
         { 2.0, 0.0, 0.0 },
         { 1.0, 4.0, 0.0 },
         { 5.0, 3.0, 6.0 }
      };
   }

   Matrix
   getU_UnitDiag() override
   {
      return Matrix{
         { 1.0, 2.0, 3.0 },
         { 0.0, 1.0, 4.0 },
         { 0.0, 0.0, 1.0 }
      };
   }

   Matrix
   getU_NoUnitDiag() override
   {
      return Matrix{
         { 2.0, 4.0,  6.0 },
         { 0.0, 4.0, 16.0 },
         { 0.0, 0.0,  6.0 }
      };
   }
};

}  // namespace Matrices
