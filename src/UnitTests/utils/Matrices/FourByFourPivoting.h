#pragma once

#include <Decomposition/Exceptions/NotImplementedError.h>

#include "UnitTests/utils/Matrices/SampleMatrix.h"

using namespace Decomposition::Exceptions;

namespace Matrices {

template< typename Matrix >
class FourByFourPivoting : public SampleMatrix< Matrix >
{
   using MatrixFormat = Decomposition::Containers::MatrixFormat;

public:
   FourByFourPivoting() : SampleMatrix< Matrix >() {}

   Matrix
   getA() override
   {
      return Matrix{
         { 0.0, 7.0, 2.0, 5.0 },
         { 1.0, 8.0, 4.0, 2.0 },
         { 2.0, 1.0, 9.0, 3.0 },
         { 5.0, 4.0, 7.0, 1.0 }
      };
   }

   template< MatrixFormat Format >
   Matrix
   getL()
   {
      throw NotImplementedError( "Sample matrix 'FourByFourPivoting'"
                                 " provides neither L, U, nor LU due to"
                                 " the many different pivoting mechanisms." );
   }

   template< MatrixFormat Format >
   Matrix
   getU()
   {
      throw NotImplementedError( "Sample matrix 'FourByFourPivoting'"
                                 " provides neither L, U, nor LU due to"
                                 " the many different pivoting mechanisms." );
   }
};

}  // namespace Matrices
