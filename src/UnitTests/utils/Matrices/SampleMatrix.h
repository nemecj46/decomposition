#pragma once

#include <TNL/Algorithms/parallelFor.h>

#include <Decomposition/Containers/MatrixFormat.h>

namespace Matrices {

template< typename Matrix >
class SampleMatrix
{
   using MatrixFormat = Decomposition::Containers::MatrixFormat;

public:
   SampleMatrix() = default;

   virtual Matrix
   getA()
   {
      return Matrix( 0, 0 );
   }

   Matrix
   getL( const MatrixFormat format )
   {
      if( format == MatrixFormat::UnitDiagIn_U ) {
         return this->getL_NoUnitDiag();
      }
      else if( format == MatrixFormat::UnitDiagIn_L ) {
         return this->getL_UnitDiag();
      }
      else {
         return Matrix();
      }
   }

   Matrix
   getU( const MatrixFormat format )
   {
      if( format == MatrixFormat::UnitDiagIn_L ) {
         return this->getU_NoUnitDiag();
      }
      else if( format == MatrixFormat::UnitDiagIn_U ) {
         return this->getU_UnitDiag();
      }
      else {
         return Matrix();
      }
   }

   Matrix
   getLU( const MatrixFormat format )
   {
      using Index = typename Matrix::IndexType;
      using Device = typename Matrix::DeviceType;

      Matrix LU;
      Matrix L = getL( format );
      Matrix U = getU( format );
      LU.setLike( L );
      LU.addMatrix( L );
      LU.addMatrix( U );

      // Subtract 1s from the main diagonal to remove the undesired unit diagonal.
      auto matrixView = LU.getView();
      auto initMatrix = [ = ] __cuda_callable__( Index i ) mutable
      {
         matrixView.setElement( i, i, matrixView.getElement( i, i ) - 1 );
      };
      TNL::Algorithms::parallelFor< Device >( (Index) 0, LU.getColumns(), initMatrix );

      return LU;
   }

protected:
   virtual Matrix
   getL_UnitDiag()
   {
      return Matrix( 0, 0 );
   }

   virtual Matrix
   getL_NoUnitDiag()
   {
      return Matrix( 0, 0 );
   }

   virtual Matrix
   getU_UnitDiag()
   {
      return Matrix( 0, 0 );
   }

   virtual Matrix
   getU_NoUnitDiag()
   {
      return Matrix( 0, 0 );
   }
};

}  // namespace Matrices
