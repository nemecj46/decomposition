#include "UnitTests/utils/UnitTestsMessages.h"

#ifdef HAVE_GTEST
   #include "gtest/gtest.h"

   #pragma once

using namespace UnitTests::Messages;

class ResultVerification
{
private:
   template< typename Matrix >
   static bool
   dimensionsEqual( const Matrix& A, const Matrix& B )
   {
      return A.getRows() == B.getRows() && A.getColumns() == B.getColumns();
   }

   template< typename Matrix >
   static bool
   dimensionsEqual( const Matrix& A, const Matrix& B, const Matrix& C, const Matrix& D )
   {
      return dimensionsEqual( A, B ) && dimensionsEqual( C, D );
   }

public:
   // Duplicate of 'expectMatrixEqualToMatrix_LU_Aorig' and 'expectMatrixEqualToMatrix'.
   // -> GoogleTest doesn't allow the EXPECT_EQ failure message to be entirely replaced which
   //    leads to confusing output in the terminal.
   template< typename Matrix >
   static void
   expectMatrixEqualToMatrix_A_Aorig( const Matrix& A,
                                      const Matrix& A_orig,
                                      const TNL::String& Aname = "A",
                                      const TNL::String& A_origName = "A_orig" )
   {
      if( ! dimensionsEqual( A, A_orig ) )
         throw std::runtime_error( getDimensionMismatchMessage( A, A_orig, Aname, A_origName ) );

      using Index = typename Matrix::IndexType;

      for( Index i = 0; i < A.getRows(); ++i ) {
         for( Index j = 0; j < A.getColumns(); ++j ) {
            EXPECT_EQ( A.getElement( i, j ), A_orig.getElement( i, j ) ) << getExpectedValueMessage( i, j, A, A_orig, Aname );
         }
      }
   }

   // Duplicate of 'expectMatrixEqualToMatrix_A_Aorig' and 'expectMatrixEqualToMatrix'.
   template< typename Matrix >
   static void
   expectMatrixEqualToMatrix_LU_Aorig( const Matrix& LU,
                                       const Matrix& A_orig,
                                       const TNL::String& LUname = "LU",
                                       const TNL::String& A_origName = "A_orig" )
   {
      if( ! dimensionsEqual( LU, A_orig ) )
         throw std::runtime_error( getDimensionMismatchMessage( LU, A_orig, LUname, A_origName ) );

      using Real = typename Matrix::RealType;
      using Index = typename Matrix::IndexType;

      for( Index i = 0; i < LU.getRows(); ++i ) {
         for( Index j = 0; j < LU.getColumns(); ++j ) {
            // Branch according to precision
            if( std::is_same_v< Real, double > ) {
               EXPECT_DOUBLE_EQ( LU.getElement( i, j ), A_orig.getElement( i, j ) )
                  << getExpectedValueMessage( i, j, LU, A_orig, LUname );
            }
            else {
               EXPECT_FLOAT_EQ( LU.getElement( i, j ), A_orig.getElement( i, j ) )
                  << getExpectedValueMessage( i, j, LU, A_orig, LUname );
            }
         }
      }
   }

   // Duplicate bcs googletest does not allow suppressing the original failure message
   template< typename Matrix >
   static void
   expectMatrixEqualToMatrix_X_X_correct( const Matrix& X,
                                          const Matrix& X_correct,
                                          const TNL::String& Xname = "X",
                                          const TNL::String& X_correct_name = "X_correct" )
   {
      if( ! dimensionsEqual( X, X_correct ) )
         throw std::runtime_error( getDimensionMismatchMessage( X, X_correct, Xname, X_correct_name ) );

      using Real = typename Matrix::RealType;
      using Index = typename Matrix::IndexType;

      for( Index i = 0; i < X.getRows(); ++i ) {
         for( Index j = 0; j < X.getColumns(); ++j ) {
            // Branch according to precision
            if( std::is_same_v< Real, double > ) {
               EXPECT_DOUBLE_EQ( X.getElement( i, j ), X_correct.getElement( i, j ) )
                  << getExpectedValueMessage( i, j, X, X_correct, Xname );
            }
            else {
               EXPECT_FLOAT_EQ( X.getElement( i, j ), X_correct.getElement( i, j ) )
                  << getExpectedValueMessage( i, j, X, X_correct, Xname );
            }
         }
      }
   }

   template< typename Matrix >
   static void
   expectMatrixEqualToMatrix( const Matrix& LU,
                              const Matrix& LU_correct,
                              const TNL::String& LUname = "LU",
                              const TNL::String& LU_correctName = "LU_correct" )
   {
      if( ! dimensionsEqual( LU, LU_correct ) )
         throw std::runtime_error( getDimensionMismatchMessage( LU, LU_correct, LUname, LU_correctName ) );

      using Real = typename Matrix::RealType;
      using Index = typename Matrix::IndexType;

      for( Index i = 0; i < LU.getRows(); ++i ) {
         for( Index j = 0; j < LU.getColumns(); ++j ) {
            if( std::is_same_v< Real, double > ) {
               EXPECT_DOUBLE_EQ( LU.getElement( i, j ), LU_correct.getElement( i, j ) )
                  << getExpectedValueMessage( i, j, LU, LU_correct, LUname );
            }
            else {
               EXPECT_FLOAT_EQ( LU.getElement( i, j ), LU_correct.getElement( i, j ) )
                  << getExpectedValueMessage( i, j, LU, LU_correct, LUname );
            }
         }
      }
   }

   template< typename Matrix, typename Real = typename Matrix::RealType >
   static void
   expectMatrixNearMatrix_LU_Aorig( const Matrix& LU,
                                    const Matrix& A_orig,
                                    const Real& percentTolerance = 1e-5,
                                    const Real& zeroToleranceFallback = 0 )
   {
      const TNL::String LUname = "LU";
      const TNL::String A_origName = "A_orig";
      if( ! dimensionsEqual( LU, A_orig ) )
         throw std::runtime_error( getDimensionMismatchMessage( LU, A_orig, LUname, A_origName ) );

      using Index = typename Matrix::IndexType;

      for( Index i = 0; i < LU.getRows(); ++i ) {
         for( Index j = 0; j < LU.getColumns(); ++j ) {
            Real tolerance = TNL::abs( A_orig.getElement( i, j ) * percentTolerance );
            tolerance = tolerance == 0 ? zeroToleranceFallback : tolerance;
            EXPECT_NEAR( LU.getElement( i, j ), A_orig.getElement( i, j ), tolerance )
               << getExpectedValueMessage( i, j, LU, A_orig, LUname );
         }
      }
   }

   template< typename Matrix, typename Real = typename Matrix::RealType >
   static void
   expectMatrixNearMatrix_LU_LUcorrect( const Matrix& LU,
                                        const Matrix& LU_correct,
                                        const Real& percentTolerance = 1e-5,
                                        const Real& zeroToleranceFallback = 0 )
   {
      const TNL::String LUname = "LU";
      const TNL::String LU_correctName = "LU_correct";
      if( ! dimensionsEqual( LU, LU_correct ) )
         throw std::runtime_error( getDimensionMismatchMessage( LU, LU_correct, LUname, LU_correctName ) );

      using Index = typename Matrix::IndexType;

      for( Index i = 0; i < LU.getRows(); ++i ) {
         for( Index j = 0; j < LU.getColumns(); ++j ) {
            Real tolerance = TNL::abs( LU_correct.getElement( i, j ) * percentTolerance );
            tolerance = tolerance == 0 ? zeroToleranceFallback : tolerance;
            EXPECT_NEAR( LU.getElement( i, j ), LU_correct.getElement( i, j ), tolerance )
               << getExpectedValueMessage( i, j, LU, LU_correct, LUname );
         }
      }
   }
};

#endif  // HAVE_GTEST
