#pragma once

#include <TNL/String.h>

namespace UnitTests::Messages {

const TNL::String matrixDimensionsDoNotMatch_message = "Matrix Dimensions do not match.";
const TNL::String cannotCompareEquality_message = "Cannot compare equality.";

template< typename Matrix >
static TNL::String
getDimensionMismatchCoreMessage( const Matrix& A, const Matrix& B, const TNL::String& Aname, const TNL::String& Bname )
{
   std::stringstream s;
   s << Aname << "( " << A.getRows() << ", " << A.getColumns() << " ) != " << Bname << "( " << B.getRows() << ", "
     << B.getColumns() << " )";
   return s.str();
}

template< typename Matrix >
static TNL::String
getDimensionMismatchMessage( const Matrix& A, const Matrix& B, const TNL::String& Aname, const TNL::String& Bname )
{
   return matrixDimensionsDoNotMatch_message + " " + getDimensionMismatchCoreMessage( A, B, Aname, Bname ) + ". "
        + cannotCompareEquality_message;
}

template< typename Matrix >
static TNL::String
getDimensionMismatchMessage( const Matrix& A, const Matrix& B, const Matrix& C, const Matrix& D )
{
   return matrixDimensionsDoNotMatch_message + " " + getDimensionMismatchCoreMessage( A, B, "L", "L_correct" ) + " and/or "
        + getDimensionMismatchCoreMessage( C, D, "U", "U_correct" ) + ". " + cannotCompareEquality_message;
}

template< typename Matrix, typename Index >
static TNL::String
getExpectedValueMessage( const Index& i, const Index& j, const Matrix& M, const Matrix& M_correct, const TNL::String& Mname )
{
   std::stringstream s;
   s << Mname << "( " << i << ", " << j << " ) = " << M.getElement( i, j ) << "\n"
     << "Expected value: " << M_correct.getElement( i, j );
   return s.str();
}

template< typename Matrix >
static TNL::String
getSkipTestMessage( const TNL::String& testName, const TNL::String& message = "" )
{
   std::stringstream s;
   s << "Skipping " << testName << " for " << TNL::getType< typename Matrix::DeviceType >()
     << " - functionality not implemented. " << message;
   return s.str();
}

template< typename Vector >
static TNL::String
getSkipTestForVectorMessage( const TNL::String& testName )
{
   std::stringstream s;
   s << "Skipping " << testName << " for " << TNL::getType< Vector >() << " - functionality not implemented.";
   return s.str();
}

}  // namespace UnitTests::Messages
