#pragma once

#include "UnitTests/utils/LinearEquationSystems/SampleSystem.h"

namespace Systems {

template< typename Matrix >
class TwoEquationsThreeRightSides : public SampleSystem< Matrix >
{
protected:
   Matrix
   getLU_UnitDiagIn_L() override
   {
      return Matrix{
         { 2.0,  5.0 },
         { 0.5, -0.5 }
      };
   }

   Matrix
   getLU_UnitDiagIn_U() override
   {
      return Matrix{
         { 2.0,  2.5 },
         { 1.0, -0.5 }
      };
   }

public:
   TwoEquationsThreeRightSides() : SampleSystem< Matrix >() {}

   // Solve system of linear equations:
   // AX = B
   // A = {  2,  5 },
   //     {  1,  2 }
   // B = { 21, 22, 23 },
   //     {  8,  9, 10 }

   template< typename Vector >
   Vector
   getPivotingVector()
   {
      return Vector();
   }

   template< typename Index >
   Matrix
   getB( const Index numRightSides = 3 )
   {
      switch( numRightSides ) {
         case 1:
            return Matrix{ { 21.0 }, { 8.0 } };
            break;
         case 2:
            return Matrix{
               { 21.0, 22.0 },
               {  8.0,  9.0 }
            };
            break;
         case 3:
            return Matrix{
               { 21.0, 22.0, 23.0 },
               {  8.0,  9.0, 10.0 }
            };
            break;
         default:
            return Matrix();
            break;
      }
   }

   template< typename Index >
   Matrix
   getX( const Index numRightSides = 3 )
   {
      switch( numRightSides ) {
         case 1:
            return Matrix{ { -2.0 }, { 5.0 } };
            break;
         case 2:
            return Matrix{
               { -2.0, 1.0 },
               {  5.0, 4.0 }
            };
            break;
         case 3:
            return Matrix{
               { -2.0, 1.0, 4.0 },
               {  5.0, 4.0, 3.0 }
            };
            break;
         default:
            return Matrix();
            break;
      }
   }
};

}  // namespace Systems
