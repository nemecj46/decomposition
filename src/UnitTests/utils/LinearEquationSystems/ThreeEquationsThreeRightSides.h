#pragma once

#include "UnitTests/utils/LinearEquationSystems/SampleSystem.h"

namespace Systems {

template< typename Matrix >
class ThreeEquationsThreeRightSides : public SampleSystem< Matrix >
{
protected:
   Matrix
   getLU_UnitDiagIn_L() override
   {
      return Matrix{
         { 2.0,  3.0, -1.0 },
         { 1.5, -2.5,  2.5 },
         { 0.5,  2.6, -3.0 }
      };
   }

   Matrix
   getLU_UnitDiagIn_U() override
   {
      return Matrix{
         { 2.0,  1.5, -0.5 },
         { 3.0, -2.5, -1.0 },
         { 1.0, -6.5, -3.0 }
      };
   }

public:
   ThreeEquationsThreeRightSides() : SampleSystem< Matrix >() {}

   // Solve system of linear equations:
   // AX = B
   // A = {  2,  3, -1 },
   //     {  3,  2,  1 },
   //     {  1, -5,  3 }
   // B = {  5,  6,  7 },
   //     { 10, 11, 12 },
   //     {  0,  1,  2 }

   template< typename Index >
   Matrix
   getB( const Index numRightSides = 3 )
   {
      switch( numRightSides ) {
         case 1:
            return Matrix{ { 5.0 }, { 10.0 }, { 0.0 } };
            break;
         case 2:
            return Matrix{
               {  5.0,  6.0 },
               { 10.0, 11.0 },
               {  0.0,  1.0 }
            };
            break;
         case 3:
            return Matrix{
               {  5.0,  6.0,  7.0 },
               { 10.0, 11.0, 12.0 },
               {  0.0,  1.0,  2.0 }
            };
            break;
         default:
            return Matrix();
            break;
      }
   }

   template< typename Index >
   Matrix
   getX( const Index numRightSides = 3 )
   {
      switch( numRightSides ) {
         case 1:
            return Matrix{ { 1.0 }, { 2.0 }, { 3.0 } };
            break;
         case 2:
            return Matrix{
               { 1.0, 1.8 },
               { 2.0, 1.6 },
               { 3.0, 2.4 }
            };
            break;
         case 3:
            return Matrix{
               { 1.0, 1.8, 2.6 },
               { 2.0, 1.6, 1.2 },
               { 3.0, 2.4, 1.8 }
            };
            break;
         default:
            return Matrix();
            break;
      }
   }
};

}  // namespace Systems
