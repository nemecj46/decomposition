#pragma once

#include "UnitTests/utils/LinearEquationSystems/SampleSystem.h"
#include "Decomposition/Containers/MatrixFormat.h"

namespace Systems {

template< typename Matrix >
class ThreeEquationsThreeRightSides2 : public SampleSystem< Matrix >
{
protected:
   Matrix
   getLU_UnitDiagIn_L() override
   {
      return Matrix{
         { 1.0,       1.0,  1.0 },
         { 2.0,      -3.0, -3.0 },
         { 1.0, 2.0 / 3.0,  2.0 }
      };
   }

   Matrix
   getLU_UnitDiagIn_U() override
   {
      return Matrix{
         { 1.0,  1.0, 1.0 },
         { 2.0, -3.0, 1.0 },
         { 1.0, -2.0, 2.0 }
      };
   }

public:
   ThreeEquationsThreeRightSides2() : SampleSystem< Matrix >() {}

   // Solve system of linear equations:
   // AX = B
   // A = { 1,  1,  1 },
   //     { 2, -1, -1 },
   //     { 1, -1,  1 }
   // B = { 3,  4,  5 },
   //     { 3,  4,  5 },
   //     { 9, 10, 11 }

   template< typename Index >
   Matrix
   getB( const Index numRightSides = 3 )
   {
      switch( numRightSides ) {
         case 1:
            return Matrix{ { 3.0 }, { 3.0 }, { 9.0 } };
            break;
         case 2:
            return Matrix{
               { 3.0,  4.0 },
               { 3.0,  4.0 },
               { 9.0, 10.0 }
            };
            break;
         case 3:
            return Matrix{
               { 3.0,  4.0,  5.0 },
               { 3.0,  4.0,  5.0 },
               { 9.0, 10.0, 11.0 }
            };
            break;
         default:
            return Matrix();
            break;
      }
   }

   template< typename Index >
   Matrix
   getX( const Index numRightSides = 3 )
   {
      switch( numRightSides ) {
         case 1:
            return Matrix{ { 2.0 }, { -3.0 }, { 4.0 } };
            break;
         case 2:
            return Matrix{
               {  2.0,  8.0 / 3.0 },
               { -3.0,       -3.0 },
               {  4.0, 13.0 / 3.0 }
            };
            break;
         case 3:
            return Matrix{
               {  2.0,  8.0 / 3.0, 10.0 / 3.0 },
               { -3.0,       -3.0,       -3.0 },
               {  4.0, 13.0 / 3.0, 14.0 / 3.0 }
            };
            break;
         default:
            return Matrix();
            break;
      }
   }
};

}  // namespace Systems
