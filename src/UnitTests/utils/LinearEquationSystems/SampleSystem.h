#pragma once

#include <Decomposition/Containers/MatrixFormat.h>

namespace Systems {

template< typename Matrix >
class SampleSystem
{
protected:
   using MatrixFormat = Decomposition::Containers::MatrixFormat;

   Matrix virtual getLU_UnitDiagIn_L()
   {
      return Matrix( 0, 0 );
   }

   Matrix virtual getLU_UnitDiagIn_U()
   {
      return Matrix( 0, 0 );
   }

public:
   SampleSystem() = default;

   Matrix
   getLU( const MatrixFormat mtxFormat )
   {
      if( mtxFormat == MatrixFormat::UnitDiagIn_L ) {
         return this->getLU_UnitDiagIn_L();
      }
      else if( mtxFormat == MatrixFormat::UnitDiagIn_U ) {
         return this->getLU_UnitDiagIn_U();
      }
      else {
         return Matrix();
      }
   }

   template< typename Vector >
   Vector
   getPivotingVector()
   {
      return Vector();
   }

   virtual Matrix
   getB()
   {
      return Matrix();
   }

   virtual Matrix
   getX()
   {
      return Matrix();
   }
};

}  // namespace Systems
