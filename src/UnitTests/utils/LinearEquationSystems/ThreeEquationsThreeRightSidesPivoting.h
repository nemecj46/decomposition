#pragma once

#include "UnitTests/utils/LinearEquationSystems/SampleSystem.h"

namespace Systems {

template< typename Matrix, typename Vector >
class ThreeEquationsThreeRightSidesPivoting : public SampleSystem< Matrix >
{
protected:
   Matrix
   getLU_UnitDiagIn_L() override
   {
      return Matrix{
         { 1.0,        0.0,       1.0 },
         { 0.0,       -3.0,       1.0 },
         { 2.0, -1.0 / 3.0, 4.0 / 3.0 }
      };
   }

   Matrix
   getLU_UnitDiagIn_U() override
   {
      return Matrix{
         { 2.0,  0.5, 1.5 },
         { 1.0, -0.5, 1.0 },
         { 0.0, -3.0, 4.0 }
      };
   }

public:
   ThreeEquationsThreeRightSidesPivoting() : SampleSystem< Matrix >() {}

   // Solve system of linear equations:
   // AX = B
   // A = {  0, -3,  1 },
   //     {  1,  0,  1 },
   //     {  2,  1,  3 }
   // B = {  7,  8,  9 },
   //     {  6,  7,  8 },
   //     { 15, 16, 17 }

   Vector
   getPivotingVector( const MatrixFormat mtxFormat )
   {
      return mtxFormat == MatrixFormat::UnitDiagIn_U ? Vector( { 3, 2, 3 } ) : Vector( { 2, 2, 3 } );
   }

   template< typename Index >
   Matrix
   getB( const Index numRightSides = 3 )
   {
      switch( numRightSides ) {
         case 1:
            return Matrix{ { 7.0 }, { 6.0 }, { 15.0 } };
            break;
         case 2:
            return Matrix{
               {  7.0,  8.0 },
               {  6.0,  7.0 },
               { 15.0, 16.0 }
            };
            break;
         case 3:
            return Matrix{
               {  7.0,  8.0,  9.0 },
               {  6.0,  7.0,  8.0 },
               { 15.0, 16.0, 17.0 }
            };
            break;
         default:
            return Matrix();
            break;
      }
   }

   template< typename Index >
   Matrix
   getX( const Index numRightSides = 3 )
   {
      switch( numRightSides ) {
         case 1:
            return Matrix{ { 2.0 }, { -1.0 }, { 4.0 } };
            break;
         case 2:
            return Matrix{
               {  2.0,  3.5 },
               { -1.0, -1.5 },
               {  4.0,  3.5 }
            };
            break;
         case 3:
            return Matrix{
               {  2.0,  3.5,  5.0 },
               { -1.0, -1.5, -2.0 },
               {  4.0,  3.5,  3.0 }
            };
            break;
         default:
            return Matrix();
            break;
      }
   }
};

}  // namespace Systems
